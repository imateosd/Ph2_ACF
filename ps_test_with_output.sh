###
# Script to run the test procedures for PSFEH test
#
# Usage: ./ps_test_with_output.sh <bp> <slot> <hybrid_id>
#
# Sandro Di Mattia
# Modified by PSzydlik
# 7/11/23

timestamp=$(date +%s)
TC_ID=$4

### MPA TEST

fpgaconfig -c settings/PS_FEH.xml -i ps_feh_5g_cic2_TS_v1_1.bin

mux_setup -f settings/PS_FEH.xml --mux_configure $1,$2

BUS=$(lsusb -d 0x10c4:0x87a0 -v | grep PSFV2-$TC_ID -B 13 | grep -o -E 'Bus [0-9]{3}' | grep -o -E '[0-9]{3}' )
DEV=$(lsusb -d 0x10c4:0x87a0 -v | grep PSFV2-$TC_ID -B 13 | grep -o -E 'Device [0-9]{3}' | grep -o -E '[0-9]{3}')
TC=$(lsusb -v -s $BUS:$DEV | grep -o -E 'CMSPH2-[A-Z,0-9]{8}' | sed -n '1 p')
FILE=Results/$3\_$TC\_$timestamp\_$BUS\_$DEV/

#feh_hybrid_test -f settings/PS_FEH.xml -m -t -b --findOpens --findShorts --checkResets --reconfigure --powerUp --PSFEH --original --cicInTest --checkAMUX --output $FILE --USBBus $BUS --USBDev  $DEV --hybridId  $3
feh_hybrid_test -f settings/PS_FEH.xml -b --checkResets --reconfigure --powerUp --PSFEH --original --cicInTest --checkAMUX --output $FILE --USBBus $BUS --USBDev  $DEV --hybridId  $3


###  SSA TEST
# fpgaconfig -c settings/PS_FEH_2xSSA2.xml -i ps_feh_2xSSA2_TS_v1_1.bin

# mux_setup -f settings/PS_FEH_2xSSA2.xml --mux_configure $1,$2

# #BUS=$(lsusb | grep Labs | grep -o -E '[0-9]{3}' | sed -n '1 p') 
# #DEV=$(lsusb | grep Labs | grep -o -E '[0-9]{3}' | sed -n '2 p')

# BUS=$(lsusb -d 0x10c4:0x87a0 -v | grep PSFV2-$TC_ID -B 13 | grep -o -E 'Bus [0-9]{3}' | grep -o -E '[0-9]{3}' )
# DEV=$(lsusb -d 0x10c4:0x87a0 -v | grep PSFV2-$TC_ID -B 13 | grep -o -E 'Device [0-9]{3}' | grep -o -E '[0-9]{3}')
# TC=$(lsusb -v -s $BUS:$DEV | grep -o -E 'CMSPH2-[A-Z,0-9]{8}' | sed -n '1 p')
# FILE=Results/$3\_$TC\_$timestamp\_$BUS\_$DEV/

# # feh_hybrid_test -f settings/PS_FEH_2xSSA2.xml --PSFEH --powerUp --reconfigure --skipAlignment all -b --ssaOutTest ALL --USBBus $BUS --output $FILE --USBDev $DEV --hybridId  $3
# # feh_hybrid_test -f settings/PS_FEH_2xSSA2.xml --PSFEH --powerUp --reconfigure -b --ssaOutTest 01 --USBBus $BUS --output $FILE --USBDev $DEV --hybridId  $3
# # echo feh_hybrid_test -f settings/PS_FEH_2xSSA2.xml --PSFEH --powerUp --reconfigure --skipAlignment all -b --ssaOutTest ALL --USBBus $BUS --output $FILE --USBDev $DEV --hybridId  $3

# ## command used for for SSA lateral 
# ##feh_hybrid_test -f settings/PS_FEH_2xSSA2.xml --PSFEH --powerUp --reconfigure --skipAlignment all -b --ssaOutTest ALL --USBBus $BUS --output $FILE --USBDev $DEV --hybridId  $3
