#include "OTModuleStartUp.h"
#include "ConfigureInfo.h"
#include "D19cI2CInterface.h"
#include "HWInterface/D19cFWInterface.h"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

OTModuleStartUp::OTModuleStartUp() : Tool() {}
OTModuleStartUp::OTModuleStartUp(const std::string& pCnfgFile) : OTModuleStartUp()
{
    fMyConfigInfo = new ConfigureInfo();
    fMyConfigInfo->setConfigurationFile(pCnfgFile);
    fMyConfigInfo->setCalibrationName("OTPowerUp");
}
OTModuleStartUp::~OTModuleStartUp()
{
    fMyConfigInfo = nullptr;
    delete fMyConfigInfo;
}

void OTModuleStartUp::ModuleStartUpPS(const OpticalGroup* pOpticalGroup)
{
    auto cBoardId   = pOpticalGroup->getBeBoardId();
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    LOG(INFO) << BOLDYELLOW << "SystemController::ModuleStartUpPS for BeBoard#" << +(*cBoardIter)->getId() << " OpticalGroup#" << +pOpticalGroup->getId() << RESET;

    auto& clpGBT = pOpticalGroup->flpGBT;
    if(clpGBT == nullptr) return;

    // configure PS ROHs
    const uint8_t cSsaClockDrive = 3;
    const uint8_t cCicClockDrive = 3;

    static_cast<D19clpGBTInterface*>(flpGBTInterface)->ConfigurePSROH(clpGBT);
    const std::vector<uint8_t> cGroupsExamples = {0, 1};
    for(auto cHybrid: *pOpticalGroup)
    {
        // first .. send clock to the SSAs on this hybrid
        uint8_t  cSide        = cHybrid->getId() % 2;
        uint16_t cReadoutRate = static_cast<D19clpGBTInterface*>(flpGBTInterface)->GetRxDataRate(clpGBT, cGroupsExamples[cSide]);
        LOG(INFO) << BOLDMAGENTA << "Readout rate on PS-module (Hybrid# " << +cHybrid->getId() << ") is " << +cReadoutRate << " Mbps" << RESET;

        lpGBTClockConfig cClkCnfg;
        cClkCnfg.fClkFreq         = 4;
        cClkCnfg.fClkDriveStr     = cSsaClockDrive;
        cClkCnfg.fClkInvert       = 0;
        cClkCnfg.fClkPreEmphWidth = 0;
        cClkCnfg.fClkPreEmphMode  = 0; // 3;
        cClkCnfg.fClkPreEmphStr   = 0; // 7;

        LOG(INFO) << BOLDBLUE << "Enabling SSA clock [Side == " << +cSide << "]" << RESET;
        static_cast<D19clpGBTInterface*>(flpGBTInterface)->hybridClock(clpGBT, cClkCnfg, cSide);

        // enable clock to CIC
        cClkCnfg.fClkFreq     = (cReadoutRate == 320) ? 4 : 5;
        cClkCnfg.fClkInvert   = 0;
        cClkCnfg.fClkDriveStr = cCicClockDrive;
        LOG(INFO) << BOLDBLUE << "Enabling CIC clock [Side == " << +cSide << "]" << RESET;
        static_cast<D19clpGBTInterface*>(flpGBTInterface)->cicClock(clpGBT, cClkCnfg, cSide);

        // release resets
        static_cast<D19clpGBTInterface*>(flpGBTInterface)->resetSSA(clpGBT, cSide);
        static_cast<D19clpGBTInterface*>(flpGBTInterface)->resetMPA(clpGBT, cSide);
        static_cast<D19clpGBTInterface*>(flpGBTInterface)->resetCIC(clpGBT, cSide);
    } // hybrid

    auto cConnectedFeTypes = (*cBoardIter)->connectedFrontEndTypes();
    bool cSSAfound         = std::find(cConnectedFeTypes.begin(), cConnectedFeTypes.end(), FrontEndType::SSA) != cConnectedFeTypes.end();
    if(!cSSAfound)
    {
        LOG(INFO) << BOLDYELLOW << "No SSA(1) on PS module" << RESET;
        return;
    }
    LOG(INFO) << BOLDYELLOW << "Forcing SSA(1) clock out to be ON" << RESET;
    for(auto cHybrid: *pOpticalGroup)
    {
        for(uint8_t cSSAId = 0; cSSAId < 8; cSSAId++)
        {
            // if(cSkipSSA3 && cSSAId == 3) continue;
            SSA*    cSSA          = new SSA(cHybrid->getBeBoardId(), cHybrid->getFMCId(), cHybrid->getOpticalGroupId(), cHybrid->getId(), cSSAId, 0, 0, "./settings/SSAFiles/SSA.txt");
            uint8_t cSLVSdriveSSA = cSSA->getReg("SLVS_pad_current");
            cSSA->setOptical(cHybrid->isOptical());
            cSSA->setMasterId(cHybrid->getMasterId());
            LOG(INFO) << BOLDMAGENTA << "SSA " << +cSSAId << " current set to " << +cSLVSdriveSSA << "" << RESET;
            auto cRegItem = cSSA->getRegItem("SLVS_pad_current");
            (fBeBoardInterface->getFirmwareInterface())->SingleRegisterWrite(cSSA, cRegItem, false);
        }
    } // configure SSA clock out .. if SSA1 present
}

void OTModuleStartUp::ModuleStartUp2S(const OpticalGroup* pOpticalGroup)
{
    auto cBoardId   = pOpticalGroup->getBeBoardId();
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    LOG(INFO) << BOLDYELLOW << "OTModuleStartUp::ModuleStartUp2S for BeBoard#" << +(*cBoardIter)->getId() << " OpticalGroup#" << +pOpticalGroup->getId() << RESET;

    // configure 2S SEHs
    auto& clpGBT = pOpticalGroup->flpGBT;
    if(clpGBT == nullptr) return;

    uint8_t                    cHybridClockDrive = 7;
    uint8_t                    cPreEmphMode      = 0; // 3
    uint8_t                    cPreEmphStr       = 0; // 7
    const std::vector<uint8_t> cGroupsExamples   = {0, 1};
    static_cast<D19clpGBTInterface*>(flpGBTInterface)->Configure2SSEH(clpGBT);
    for(auto cHybrid: *pOpticalGroup)
    {
        uint8_t cSide = cHybrid->getId() % 2;
        // work out readout rate
        uint16_t cReadoutRate = flpGBTInterface->GetRxDataRate(clpGBT, cGroupsExamples[cSide]);
        LOG(DEBUG) << BOLDMAGENTA << "Readout rate on 2S-module (Hybrid# " << +cHybrid->getId() << ") is " << +cReadoutRate << " Mbps" << RESET;

        // first .. send clock to the CBCs on this hybrid
        lpGBTClockConfig cClkCnfg;
        cClkCnfg.fClkFreq         = 4;
        cClkCnfg.fClkDriveStr     = cHybridClockDrive;
        cClkCnfg.fClkInvert       = (cSide == 0) ? 1 : 0;
        cClkCnfg.fClkPreEmphWidth = 0;
        cClkCnfg.fClkPreEmphMode  = cPreEmphMode;
        cClkCnfg.fClkPreEmphStr   = cPreEmphStr;
        LOG(DEBUG) << BOLDBLUE << "Enabling Hybrid clock [Side == " << +cSide << "]" << RESET;
        static_cast<D19clpGBTInterface*>(flpGBTInterface)->hybridClock(clpGBT, cClkCnfg, cSide);

        // release resets
        static_cast<D19clpGBTInterface*>(flpGBTInterface)->resetCBC(clpGBT, cSide);
        static_cast<D19clpGBTInterface*>(flpGBTInterface)->resetCIC(clpGBT, cSide);
    }
}
void OTModuleStartUp::StartUpCIC()
{
    LOG(INFO) << BOLDYELLOW << "OTModuleStartUp::StartUpCIC" << RESET;
    for(const auto& cBoard: *fDetectorContainer)
    {
        for(const auto& cOpticalGroup: *cBoard) { CicStartUp(cOpticalGroup); }
    }
}
bool OTModuleStartUp::CicStartUp(const OpticalGroup* pOpticalGroup, bool cStartUpSequence)
{
    auto cBoardId    = pOpticalGroup->getBeBoardId();
    auto cBoardIter  = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    bool cWith2SFEH  = (*cBoardIter)->getEventType() == EventType::VR2S;
    auto cSparsified = (*cBoardIter)->getSparsification();

    auto& clpGBT = pOpticalGroup->flpGBT;
    for(auto cHybrid: *pOpticalGroup)
    {
        if(clpGBT == nullptr) continue;
        auto cInterface = static_cast<D19clpGBTInterface*>(flpGBTInterface);
        if(pOpticalGroup->getFrontEndType() == FrontEndType::OuterTracker2S) { cInterface->cbcReset(clpGBT, true, (cHybrid->getId() % 2 == 0)); }
        else
        {
            cInterface->ssaReset(clpGBT, true, (cHybrid->getId() % 2 == 0));
            cInterface->mpaReset(clpGBT, true, (cHybrid->getId() % 2 == 0));
        }
    } // hold resets to ROCs on the module

    bool cSuccess = true;
    LOG(INFO) << BOLDGREEN << "####################################################################################" << RESET;
    LOG(INFO) << BOLDGREEN << "\t...Configuring CIC on Link#" << +pOpticalGroup->getId() << RESET;
    for(auto cHybrid: *pOpticalGroup)
    {
        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        if(cCic == NULL) continue;

        fCicInterface->ConfigureChip(cCic);
        fCicInterface->EnableFEs(cCic, {0, 1, 2, 3, 4, 5, 6, 7}, false); // make sure all FEs are disabled by default
    }                                                                    // configure CICs

    for(auto cHybrid: *pOpticalGroup)
    {
        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        if(cCic == NULL) continue;

        // if there is an lpGBT .
        // its configuration overwrites whatever is in the xml
        if(clpGBT != nullptr)
        {
            auto     cChipRate     = static_cast<D19clpGBTInterface*>(flpGBTInterface)->GetChipRate(clpGBT);
            uint16_t cClkFrequency = (cChipRate == 5) ? 320 : 640;
            // LOG (INFO) << BOLDYELLOW << " Configuring CIC clock rate to " << cClkFrequency << RESET;
            cCic->setClockFrequency(cClkFrequency);
        }

        // CIC start-up
        auto cType       = FrontEndType::CBC3;
        auto cHybridIter = std::find_if(cHybrid->begin(), cHybrid->end(), [&cType](Ph2_HwDescription::Chip* x) { return x->getFrontEndType() == cType; });
        bool cIs2S       = cHybridIter != cHybrid->end();
        // 0 --> CBC , 1 --> MPA
        uint8_t cModeSelect = (cIs2S) ? 0 : 1;
        uint8_t cBx0Delay   = (cIs2S) ? 8 : 22;
        // select CIC mode
        cSuccess = fCicInterface->SelectMode(cCic, cModeSelect);
        if(!cSuccess)
        {
            LOG(INFO) << BOLDRED << "FAILED " << BOLDBLUE << " to configure CIC mode.." << RESET;
            throw std::runtime_error(std::string("FAILED to set CIC mode ... something is wrong... .. STOPPING"));
        }
        LOG(INFO) << BOLDMAGENTA << "CIC configured for " << (cIs2S ? "2S" : "PS") << " readout." << RESET;

        // configure CIC FE enable register
        // first make sure it is set to 0x00
        fCicInterface->EnableFEs(cCic, {0, 1, 2, 3, 4, 5, 6, 7}, false);
        // figure out which Chips are enabled
        std::vector<uint8_t> cChipIds(0);
        if(pOpticalGroup->getFrontEndType() == FrontEndType::HYBRIDPS) fCicInterface->EnableFEs(cCic, {0, 1, 2, 3, 4, 5, 6, 7}, true);
        for(auto cReadoutChip: *cHybrid)
        {
            // only consider MPAs and CBCs
            if(pOpticalGroup->getFrontEndType() == FrontEndType::HYBRIDPS) continue;
            if(cReadoutChip->getFrontEndType() == FrontEndType::SSA || cReadoutChip->getFrontEndType() == FrontEndType::SSA2) continue;
            cChipIds.push_back(cReadoutChip->getId() % 8);
        }
        fCicInterface->EnableFEs(cCic, cChipIds, true);

        // make sure data rate is correctly configured
        // only works for CIC2
        if(cCic->getFrontEndType() == FrontEndType::CIC2)
        {
            uint8_t cFeConfigReg  = fCicInterface->ReadChipReg(cCic, "FE_CONFIG");
            auto    cClkFrequency = cCic->getClockFrequency();
            uint8_t cNewValue     = (cFeConfigReg & 0xFD) | ((uint8_t)(cClkFrequency == 640) << 1);
            cSuccess              = fCicInterface->WriteChipReg(cCic, "FE_CONFIG", cNewValue);
        }

        // 2S-FEHs
        // CIC start-up sequence
        uint8_t cClkTerm = 1;
        uint8_t cRxTerm  = 1;
        if(cWith2SFEH)
        {
            cClkTerm = 0;
            cRxTerm  = 1;
        }
        cSuccess = fCicInterface->ConfigureTermination(cCic, cClkTerm, cRxTerm);
        if(cSuccess)
        {
            if(cStartUpSequence)
            {
                // LOG(INFO) << BOLDYELLOW << "Launching CIC start-up sequence.." << RESET;
                cSuccess = fCicInterface->StartUp(cCic, cCic->getDriveStrength(), cCic->getEdgeSelect());
            }
            else
            {
                // LOG(INFO) << BOLDYELLOW << "Not launching CIC start-up sequence.. but will configure drive strength and FCMD edge from xml.." << RESET;
                if(fCicInterface->ConfigureDriveStrength(cCic, cCic->getDriveStrength()))
                    cSuccess = fCicInterface->ConfigureFCMDEdge(cCic, cCic->getEdgeSelect());
                else
                    cSuccess = false;
            }
        }
        else
            throw std::runtime_error(std::string("FAILED to start-up CIC ... something is wrong... .. STOPPING"));

        if(cSuccess)
            cSuccess = fCicInterface->SetSparsification(cCic, cSparsified);
        else
            throw std::runtime_error(std::string("FAILED to set CIC sparsification... .. STOPPING"));

        if(cSuccess)
            cSuccess = fCicInterface->ConfigureStubOutput(cCic);
        else
            throw std::runtime_error(std::string("FAILED to configure CIC stub output... .. STOPPING"));

        if(cSuccess)
            cSuccess = fCicInterface->ManualBx0Alignment(cCic, cBx0Delay);
        else
            throw std::runtime_error(std::string("FAILED to configure CIC Bx0 delay... .. STOPPING"));

        fCicInterface->setInitialized(cCic, 1);

    } // all hybrids connected to this OG.. start-up CIC

    for(auto cHybrid: *pOpticalGroup)
    {
        if(clpGBT == nullptr) continue;
        auto cInterface = static_cast<D19clpGBTInterface*>(flpGBTInterface);
        if(pOpticalGroup->getFrontEndType() == FrontEndType::OuterTracker2S) { cInterface->cbcReset(clpGBT, false, (cHybrid->getId() % 2 == 0)); }
        else
        {
            cInterface->ssaReset(clpGBT, false, (cHybrid->getId() % 2 == 0));
            cInterface->mpaReset(clpGBT, false, (cHybrid->getId() % 2 == 0));
        }
    } // release resets to ROCs on the module

    LOG(INFO) << BOLDGREEN << "####################################################################################" << RESET;
    return cSuccess;
}
void OTModuleStartUp::Reconfigure(BeBoard* pBoard)
{
    LOG(INFO) << BOLDMAGENTA << "OTModuleStartUp::Reconfigure Reconfiguring OT hardware..on BeBoard#" << +pBoard->getId() << RESET;
    auto cInterface = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    cInterface->setConfigured(pBoard, 1);
    LOG(INFO) << BOLDYELLOW << "Forcing Link+Board reset for reconfigure : " << BOLDRED << " OFF " << RESET;
    pBoard->setReset(0);
    pBoard->setLinkReset(0);
}
// this should leave the module/hybrid in a state where communication with
// all the FE ASICs is possible
void OTModuleStartUp::InitializeOT(BeBoard* pBoard)
{
    LOG(INFO) << BOLDMAGENTA << "OTModuleStartUp::InitializeOT Initializing OT hardware..on BeBoard#" << +pBoard->getId() << RESET;
    LOG(INFO) << BOLDYELLOW << "Forcing Link+Board reset during first start-up : " << BOLDGREEN << " ON " << RESET;
    pBoard->setLinkReset(1);
    pBoard->setReset(1);
    fBeBoardInterface->ConfigureBoard(pBoard);
    LOG(INFO) << BOLDYELLOW << "Forcing Link+Board reset after first start-up : " << BOLDRED << " OFF " << RESET;
    pBoard->setLinkReset(0);
    pBoard->setReset(0);

    for(auto cOpticalGroup: *pBoard)
    {
        if(cOpticalGroup->flpGBT == nullptr) continue;

        LOG(INFO) << BOLDBLUE << "Now going to configuring lpGBT on  Link#" << +cOpticalGroup->getId() << " on Board " << int(pBoard->getId()) << RESET;
        D19clpGBTInterface* clpGBTInterface = static_cast<D19clpGBTInterface*>(flpGBTInterface);
        if(cOpticalGroup->getReset() == 0)
        {
            LOG(INFO) << BOLDYELLOW << "Will not re-configure lpGBT on Link#" << +cOpticalGroup->getId() << RESET;
            continue;
        }

        if(!clpGBTInterface->ConfigureChip(cOpticalGroup->flpGBT))
        {
            LOG(INFO) << BOLDRED << "SOMETHING FUNNY" << RESET;
            continue;
        }
    }

    // module start-up
    // depends on module type
    for(auto cOpticalGroup: *pBoard)
    {
        if(cOpticalGroup->getReset() == 0)
        {
            LOG(INFO) << BOLDYELLOW << "Will not re-configure lpGBT for specific module type.." << RESET;
            continue;
        }

        if(cOpticalGroup->getFrontEndType() == FrontEndType::OuterTracker2S)
        {
            LOG(INFO) << BOLDYELLOW << "Configuring an OuterTracker2S module on Link#" << +cOpticalGroup->getId() << RESET;
            ModuleStartUp2S(cOpticalGroup);
        }
        if(cOpticalGroup->getFrontEndType() == FrontEndType::OuterTrackerPS)
        {
            LOG(INFO) << BOLDYELLOW << "Configuring an OuterTrackerPS module on Link#" << +cOpticalGroup->getId() << RESET;
            ModuleStartUpPS(cOpticalGroup);
        }
    }
}
void OTModuleStartUp::CheckForResync(BeBoard* pBoard, int cRun)
{
    LOG(INFO) << "OTModuleStartUp::CheckForResync... Iteration " << cRun << RESET;
    if(cRun > 10)
    {
        LOG(INFO) << BOLDRED << "The check for resync has gone on for too long... skipping..." << RESET;
        return;
    }
    // check if a resync is needed
    LOG(INFO) << BOLDBLUE << "Checking if a ReSync is needed for Board" << +pBoard->getId() << RESET;
    bool cReSyncNeeded = false;
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
            if(cCic == NULL) continue;
            if(cReSyncNeeded) continue;

            bool cReSyncCIC = fCicInterface->GetResyncRequest(cCic);
            if(cReSyncCIC) LOG(INFO) << BOLDMAGENTA << "\t... CIC" << +cHybrid->getId() << " requires a ReSync" << RESET;

            std::vector<uint8_t> cResyncsCbcs(0);
            for(auto cChip: *cHybrid)
            {
                if(cChip->getFrontEndType() != FrontEndType::CBC3) continue;
                auto cValue = fReadoutChipInterface->ReadChipReg(cChip, "SerialIface&Error");
                if(cValue != 0x00)
                    LOG(INFO) << BOLDMAGENTA << "\t...Link#" << +cOpticalGroup->getId() << " Hybrid#" << +cHybrid->getId() % 2 << " CBC" << +cChip->getId() << " requires a ReSync : Error Register "
                              << std::bitset<8>(cValue) << RESET;

                cResyncsCbcs.push_back(((cValue & 0x1F) != 0x00) ? 1 : 0);
            }
            bool cReSyncCBCs = std::accumulate(cResyncsCbcs.begin(), cResyncsCbcs.end(), 0) > 0;
            cReSyncNeeded    = cReSyncNeeded || cReSyncCBCs || cReSyncCIC;
        }
    }

    if(cReSyncNeeded)
    {
        LOG(INFO) << BOLDMAGENTA << "OTModuleStartUp::CheckForResync... Sending a ReSync" << RESET;
        // send a ReSync to all chips before starting
        fBeBoardInterface->ChipReSync(pBoard);
        // check resync request has been cleared
        CheckForResync(pBoard, cRun + 1);
        // for(auto cOpticalGroup: *pBoard)
        // {
        //     for(auto cHybrid: *cOpticalGroup)
        //     {
        //         auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        //         if(cCic == NULL) continue;

        //         if(fCicInterface->GetResyncRequest(cCic))
        //         {
        //             LOG(INFO) << BOLDRED << "ReSync request ofrom CIC" << +cHybrid->getId() << RESET;
        //             throw std::runtime_error(std::string("FAILED to clear CIC ReSync request"));
        //         }
        //     }
        // }
    }
    else
        LOG(INFO) << BOLDMAGENTA << "No ReSync needed after OT-module configuration step" << RESET;
}
bool OTModuleStartUp::Initialize()
{
    if(!fColdStart) return false;
    LOG(INFO) << BOLDYELLOW << "OTModuleStartUp::Initialize ..." << RESET;
    for(const auto cBoard: *fDetectorContainer) { InitializeOT(cBoard); } // initialize modules + BeBoards
    return true;
}
bool OTModuleStartUp::StartUp()
{
    LOG(INFO) << BOLDYELLOW << "OTModuleStartUp::StartUp ... printing list of active ROCs" << RESET;
    // update I2C interface for D19c
    // for electrical readout only
    for(const auto& cBoard: *fDetectorContainer)
    {
        if(cBoard->getBoardType() != BoardType::D19C) continue;
        if(cBoard->isOptical() != 0) continue;

        fBeBoardInterface->setBoard(cBoard->getId());
        auto cInterface    = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        auto cI2CInterface = static_cast<D19cI2CInterface*>(cInterface->getFEConfigurationInterface());
        cI2CInterface->ConfigureI2CMap(cBoard);
    }

    auto startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    StartUpCIC();
    auto currentTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    auto cDeltaTime_us     = currentTimeUTC_us - startTimeUTC_us;
    LOG(INFO) << BOLDYELLOW << "Time to start-up CIC " << cDeltaTime_us * 1e-6 << " seconds" << RESET;

    currentTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    ConfigureHw();
    currentTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    cDeltaTime_us     = currentTimeUTC_us - startTimeUTC_us;
    LOG(INFO) << BOLDYELLOW << "Time to configure the ROCs " << cDeltaTime_us * 1e-6 << " seconds" << RESET;

    // check for resync
    for(const auto cBoard: *fDetectorContainer)
    {
        CheckForResync(cBoard);
        // fBeBoardInterface->ChipReSync(cBoard);
    }

    // // ADC - lpGBT
    // for(auto cBoard: *fDetectorContainer)
    // {
    //     for(const auto cOpticalGroup: *cBoard)
    //     {
    //         auto& clpGBT = cOpticalGroup->flpGBT;
    //         if(clpGBT == nullptr) continue;

    //         flpGBTInterface->GetADCOffset(clpGBT, false);
    //         flpGBTInterface->GetADCGain(clpGBT, false);
    //         // flpGBTInterface->SetVrefTune(clpGBT, "ADC2", 0.504);
    //     }
    // }

    // trim bias DACs [only SSAs atm]
    // for(auto cBoard: *fDetectorContainer)
    // {
    //     for(const auto& cOpticalGroup: *cBoard)
    //     {
    //         for(const auto& cHybrid: *cOpticalGroup)
    //         {
    //             for(const auto& cChip: *cHybrid)
    //             {
    //                 if(cChip->getFrontEndType() != FrontEndType::SSA2) continue;

    //                 static_cast<SSA2Interface*>(fReadoutChipInterface)->TrimBiases(cChip); //"VoltageRef","Bandgap",0.275);
    //             }
    //         }
    //     }
    // }

    // measure voltages
    // for(auto cBoard: *fDetectorContainer)
    // {
    //     for(const auto& cOpticalGroup: *cBoard)
    //     {
    //         for(const auto& cHybrid: *cOpticalGroup)
    //         {
    //             // for(const auto& cChip: *cHybrid)
    //             // {
    //             //     if(cChip->getFrontEndType() != FrontEndType::SSA2) continue;
    //             //     auto cTh = fReadoutChipInterface->ReadChipReg(cChip, "Threshold");
    //             //     fReadoutChipInterface->WriteChipReg(cChip, "Threshold", 0);
    //             //     auto cTh_0 = static_cast<SSA2Interface*>(fReadoutChipInterface)->MeasureVoltage(cChip, 8);
    //             //     fReadoutChipInterface->WriteChipReg(cChip, "Threshold", 10);
    //             //     auto cTh_10 = static_cast<SSA2Interface*>(fReadoutChipInterface)->MeasureVoltage(cChip, 8);
    //             //     fReadoutChipInterface->WriteChipReg(cChip, "Threshold", cTh);
    //             //     float cThLSB = std::fabs(cTh_10 - cTh_0) / 10.;
    //             //     LOG(INFO) << BOLDYELLOW << " SSA#" << +cChip->getId() << " Th1 " << cTh_0 << " Th10 " << cTh_10 << " ThLSB " << cThLSB * 1e3 << " mV"
    //             //               << " --> " << ((cThLSB * 1e3) / 54.) * (1e-15 / 1.6e-19) << " electrons "
    //             //               << "\t.. AVDD " << cAVDD << "\t.. PVDD " << cPVDD << "\t.. DVDD " << cDVDD << RESET;
    //             // }
    //             LOG(INFO) << BOLDYELLOW << "Hybrid#" << +cHybrid->getId() << RESET;
    //             LOG(INFO) << BOLDYELLOW << "ChipId\tAVDD\tDVDD" << RESET;
    //             std::map<uint8_t, float> cAVDDs;
    //             std::map<uint8_t, float> cDVDDs;
    //             std::map<uint8_t, float> cIOs;
    //             for(const auto& cChip: *cHybrid)
    //             {
    //                 if(cChip->getFrontEndType() != FrontEndType::SSA2) continue;
    //                 cAVDDs[cChip->getId()] = 2 * static_cast<SSA2Interface*>(fReadoutChipInterface)->MeasureVoltage(cChip, 17);
    //                 // auto  cPVDD  = 2 * static_cast<SSA2Interface*>(fReadoutChipInterface)->MeasureVoltage(cChip, 18);
    //                 cDVDDs[cChip->getId()] = 2 * static_cast<SSA2Interface*>(fReadoutChipInterface)->MeasureVoltage(cChip, 19);
    //                 LOG(INFO) << BOLDYELLOW << "SSA#" << +cChip->getId() << "\t" << std::setprecision(3) << cAVDDs[cChip->getId()] << "\t" << cDVDDs[cChip->getId()]
    //                           << std::setprecision(std::cout.precision()) << RESET;
    //             }
    //             LOG(INFO) << BOLDYELLOW << "ChipId\t13\t12\t14" << RESET;
    //             for(const auto& cChip: *cHybrid)
    //             {
    //                 cIOs[cChip->getId()] = 2 * static_cast<MPA2Interface*>(fReadoutChipInterface)->MeasureVoltage(cChip, 14);
    //                 if(cChip->getFrontEndType() != FrontEndType::MPA2) continue;
    //                 cAVDDs[cChip->getId()] = 2 * static_cast<MPA2Interface*>(fReadoutChipInterface)->MeasureVoltage(cChip, 13);
    //                 cDVDDs[cChip->getId()] = 2 * static_cast<MPA2Interface*>(fReadoutChipInterface)->MeasureVoltage(cChip, 12);
    //                 LOG(INFO) << BOLDYELLOW << "MPA#" << +cChip->getId() << "\t" << std::setprecision(3) << cAVDDs[cChip->getId()] << "\t" << cDVDDs[cChip->getId()] << "\t" << cIOs[cChip->getId()]
    //                           << std::setprecision(std::cout.precision()) << RESET;
    //             }
    //             for(const auto& cChip: *cHybrid)
    //             {
    //                 if(cChip->getFrontEndType() != FrontEndType::MPA2) continue;
    //                 auto cBg = 1 * static_cast<MPA2Interface*>(fReadoutChipInterface)->measureBg(cChip);
    //                 LOG(INFO) << BOLDYELLOW << "Bandgap MPA#" << +cChip->getId() << "\t" << std::setprecision(3) << cBg << "\t" << cBg << std::setprecision(std::cout.precision()) << RESET;
    //             }
    //         }
    //     }
    // }
    // configure package delay from xml node
    for(auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getBoardType() != BoardType::D19C) continue;
        for(const auto cOpticalGroup: *cBoard)
        {
            auto        cCnfg    = cOpticalGroup->getStubCnfg();
            std::string cRegName = "fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link0_link9";
            if(cOpticalGroup->getId() > 9) cRegName = "fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link10_link11";

            LOG(INFO) << BOLDYELLOW << "OTModuleStartUp::StartUp setting stub package delay for Link#" << +cOpticalGroup->getId() << " to " << +cCnfg.first << RESET;
            fBeBoardInterface->WriteBoardReg(cBoard, cRegName, cCnfg.first);
        }
    }

    for(auto cBoard: *fDetectorContainer) { fBeBoardInterface->getFirmwareInterface()->setConfigured(cBoard, 1); }
    return true;
}

// State machine control functions
void OTModuleStartUp::Running() { StartUp(); }

void OTModuleStartUp::Stop() {}

void OTModuleStartUp::Pause() {}

void OTModuleStartUp::Resume() {}