#include "CicFEAlignment.h"

// #ifdef __USE_ROOT__
#include "../Utils/CBCChannelGroupHandler.h"
#include "../Utils/ContainerFactory.h"
#include "../Utils/Occupancy.h"
#include "D19cDPInterface.h"
#include "D19cDebugFWInterface.h"
#include "D19cFWInterface.h"
#include "FastCommandInterface.h"
#include "TriggerInterface.h"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

CicFEAlignment::CicFEAlignment() : OTTool() {}

CicFEAlignment::~CicFEAlignment() {}

void CicFEAlignment::Initialise()
{
    LOG(INFO) << BOLDMAGENTA << "CicFEAlignment::Initialise" << RESET;
    fSuccess = false;
    fWithMPA = false;
    // this is needed if you're going to use groups anywhere
    CBCChannelGroupHandler theChannelGroupHandler;
    theChannelGroupHandler.setChannelGroupParameters(16, 2);
    setChannelGroupHandler(theChannelGroupHandler);

    DetectorDataContainer theOccupancyContainer;
    fDetectorDataContainer = &theOccupancyContainer;
    ContainerFactory::copyAndInitStructure<Occupancy>(*fDetectorContainer, *fDetectorDataContainer);
    LOG(INFO) << BOLDMAGENTA << "CicFEAlignment::Initialise" << RESET;
    // prepare common OTTool
    SetName("CicFEAlignment");
    Prepare();

    // initialize containers holding data from this tool
    ContainerFactory::copyAndInitHybrid<AlignmentValues>(*fDetectorContainer, fPhaseAlignmentStatus);
    ContainerFactory::copyAndInitHybrid<AlignmentValues>(*fDetectorContainer, fPhaseAlignmentValues);
    ContainerFactory::copyAndInitHybrid<AlignmentValues>(*fDetectorContainer, fWordAlignmentValues);
    ContainerFactory::copyAndInitHybrid<uint8_t>(*fDetectorContainer, fWordAlignmentStatus);
    ContainerFactory::copyAndInitHybrid<std::vector<uint8_t>>(*fDetectorContainer, fEnabledChipIds);
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                // 48 lines into the CIC in total
                //  6 per ROC
                //  5 stub + 1 L1A
                // clear PA status
                auto& cWrdAlStats = fWordAlignmentStatus.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<uint8_t>();
                cWrdAlStats       = 0;

                auto& cChipIds = fEnabledChipIds.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::vector<uint8_t>>();
                cChipIds.clear();
                if(cOpticalGroup->getFrontEndType() == FrontEndType::HYBRIDPS)
                {
                    for(uint8_t cChipId = 0; cChipId < 8; cChipId++) cChipIds.push_back(cChipId);
                }
                else
                {
                    for(auto cChip: *cHybrid)
                    {
                        if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2) continue;
                        cChipIds.push_back(cChip->getId() % 8);
                    }
                }

                auto& cPhaseAlStats      = fPhaseAlignmentStatus.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<AlignmentValues>();
                auto& cPhaseAlVals       = fPhaseAlignmentValues.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<AlignmentValues>();
                auto& cWordAlignmentVals = fWordAlignmentValues.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<AlignmentValues>();
                for(auto cChipId: cChipIds)
                {
                    // 48 lines into the CIC in total
                    //  6 per ROC
                    //  5 stub + 1 L1A
                    // clear PA status
                    cPhaseAlStats[cChipId].clear();
                    cPhaseAlStats[cChipId].resize(6, 0);
                    // clear PA values
                    cPhaseAlVals[cChipId].clear();
                    cPhaseAlVals[cChipId].resize(6, 0);
                    // clear WA values
                    cWordAlignmentVals[cChipId].clear();
                    cWordAlignmentVals[cChipId].resize(5, 0);
                }
            }
        }
    }

#ifdef __USE_ROOT__
    fDQMHistogrammer.book(fResultFile, *fDetectorContainer, fSettingsMap);
#endif
}

void CicFEAlignment::writeObjects()
{
#ifdef __USE_ROOT__
    this->SaveResults();
    fDQMHistogrammer.process();
    fResultFile->Flush();
#endif
}
// State machine control functions
bool CicFEAlignment::AlignInputs(bool is2SFEHR)
{
    LOG(INFO) << BOLDMAGENTA << "CicFEAlignment::Aligning Inputs " << RESET;
    // align CIC inputs - first phase
    int nAttemptsToPhaseAlign = 1;
    LOG(INFO) << BOLDRED << "Attempt #" << nAttemptsToPhaseAlign << RESET;
    bool cPhaseAligned = this->PhaseAlignment(is2SFEHR);
    nAttemptsToPhaseAlign++;
    while(!cPhaseAligned)
    {
        LOG(INFO) << BOLDRED << "Attempt # " << nAttemptsToPhaseAlign - 1 << " to phase alignment step on CIC input failed.. " << RESET;
        LOG(INFO) << BOLDRED << "Attempt #" << nAttemptsToPhaseAlign << RESET;
        cPhaseAligned = this->PhaseAlignment(is2SFEHR);
        nAttemptsToPhaseAlign++;
        if(nAttemptsToPhaseAlign > 5) break;
    }
    if(!cPhaseAligned)
    {
        std::stringstream cMessage;
        cMessage << BOLDRED << "FAILED phase alignment step on CIC input .. ";
        LOG(ERROR) << cMessage.str() << RESET;
        // exit(FAILED_PHASE_ALIGNMENT);
        // throw std::runtime_error(std::string("Could not phase align lines in CicFEAlignment..."));
    }
    LOG(INFO) << BOLDGREEN << "SUCCESSFUL " << BOLDBLUE << " phase alignment on CIC inputs... " << RESET;
    fSuccess               = cPhaseAligned;
    fPhaseAlignmentSuccess = cPhaseAligned;

    // go back to starting point
    this->Reset();
    // re-enable register tracking
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid) { cChip->setRegisterTracking(1); }
            }
        }
    }
    // then word align
    int nTriesMax = 3;
    int nTries = 0;
    bool cWordAligned = false;
    while (!cWordAligned and nTries<nTriesMax) {
        cWordAligned = this->WordAlignment();
        nTries = nTries + 1; 
    }
    //bool cWordAligned = this->WordAlignment();

    // summary tree [if ROOT is being used]
    CreateSummaryTree();
    if(!cWordAligned)
    {
        std::stringstream cMessage;
        cMessage << BOLDRED << "FAILED word alignment step on CIC input .. ";
        LOG(ERROR) << cMessage.str() << RESET;
        // throw std::runtime_error(cMessage.str());
    }
    else
    {
        LOG(INFO) << BOLDGREEN << "SUCCESSFUL word alignment on CIC inputs... " << RESET;
    }

    // fSuccess              = (cPhaseAligned && cWordAligned);
    fSuccess              = cPhaseAligned; // the word alignment is not enough to fail the test - we just need to know which lines are broken
    fWordAlignmentSuccess = cWordAligned;
    return fSuccess;
}

void CicFEAlignment::CheckAlignment()
{
    fInvalidList.clear();
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<const OuterTrackerHybrid*>(cHybrid)->fCic;
                fCicInterface->EnableFEs(cCic, {0, 1, 2, 3, 4, 5, 6, 7}, false);
                std::vector<uint8_t> cEnabledFEs(0, 0);
                auto                 cChipIds = fEnabledChipIds.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::vector<uint8_t>>();

                size_t cIndx = 0;
                for(auto cChipId: cChipIds)
                {
                    auto cPhaseAlignmentVals = fPhaseAlignmentValues.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<AlignmentValues>()[cChipId];

                    auto cPhaseAlStats = fPhaseAlignmentStatus.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<AlignmentValues>()[cChipId];

                    std::stringstream cOutput;
                    uint32_t          cId       = (cIndx << 24) | (cOpticalGroup->getId() << 16) | (cHybrid->getId() << 8) | cChipId;
                    size_t            cNAligned = 0;
                    for(uint8_t cLineId = 0; cLineId < 6; cLineId++)
                    {
                        if(cPhaseAlStats[cLineId] == 1)
                        {
                            cOutput << BOLDGREEN << +cPhaseAlignmentVals[cLineId] << " ";
                            cNAligned++;
                        }
                        else
                            cOutput << BOLDRED << +cPhaseAlignmentVals[cLineId] << " ";
                    }
                    if(cNAligned == 6)
                    {
                        cEnabledFEs.push_back(cChipId);
                        continue;
                    }
                    LOG(WARNING) << BOLDRED << "Not all lines succeeded to PA -- Optimal tap found on CIC#" << +cHybrid->getId() << " FE" << +cChipId << " : " << cOutput.str() << RESET;
                    // TEST SYSTEM QUICKFIX 06/11/23  - P.Szydlik
                    // Reason: Failure in CIC IN PA shouldn't cause disabling of FE on PS-FEH
                    if(cOpticalGroup->getFrontEndType() != FrontEndType::HYBRIDPS)
                    {
                        if(std::find(fInvalidList.begin(), fInvalidList.end(), cId) == fInvalidList.end()) fInvalidList.push_back(cId);
                    }
                    cIndx++;
                }
                // TEST SYSTEM QUICKFIX 06/11/23  - P.Szydlik
                // Reason: Failure in CIC IN PA shouldn't cause disabling of FE on PS-FEH
                if(cOpticalGroup->getFrontEndType() == FrontEndType::HYBRIDPS) { fCicInterface->EnableFEs(cCic, {0, 1, 2, 3, 4, 5, 6, 7}, true); }
                else
                {
                    fCicInterface->EnableFEs(cCic, cEnabledFEs, true);
                }
            }
        }
    }

    // Below entry is substracting the failed chip from the fDetectorContainer structure, causing them to be skipped while looping through its objects
    auto cSubset = [this](const ChipContainer* cChip) { return std::find(this->fInvalidList.begin(), this->fInvalidList.end(), cChip->getId() % 8) == this->fInvalidList.end(); };
    fDetectorContainer->addReadoutChipQueryFunction(cSubset);

    // update name container so only valid chips are in the detector description
    // this might not be totally kosher...
    if(fNameContainer != nullptr)
    {
        fNameContainer->reset();
        fNameContainer = nullptr;
        delete fNameContainer;
        fNameContainer = new DetectorDataContainer();
        ContainerFactory::copyAndInitStructure<EmptyContainer, std::string, std::string, std::string, std::string, EmptyContainer>(*fDetectorContainer, *fNameContainer);
    }
}
void CicFEAlignment::CreateSummaryTree()
{
#ifdef __USE_ROOT__
    auto        cFrontendAlignmentTree = new TTree("frontendAlignmentTree", "Frontend alignment values");
    std::string cLineName              = "";
    int         cPAValue               = -1;
    int         cWAValue               = -1;
    bool        cLinePAStatus          = false;
    bool        cLineWAStatus          = false;
    cFrontendAlignmentTree->Branch("Line", &cLineName);
    cFrontendAlignmentTree->Branch("PAValue", &cPAValue);
    cFrontendAlignmentTree->Branch("WAValue", &cWAValue);
    cFrontendAlignmentTree->Branch("PAStatus", &cLinePAStatus);
    cFrontendAlignmentTree->Branch("WAStatus", &cLineWAStatus);

    bool cWrdAlignmentStatusFlag   = true;
    bool cPhaseAlignmentStatusFlag = true;

    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                cLineWAStatus           = fWordAlignmentStatus.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<uint8_t>();
                cWrdAlignmentStatusFlag = cWrdAlignmentStatusFlag && cLineWAStatus;
                auto cChipIds           = fEnabledChipIds.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::vector<uint8_t>>();
                for(auto cChipId: cChipIds)
                {
                    LOG(INFO) << BOLDMAGENTA << "ROC#" << +cChipId << RESET;
                    LOG(INFO) << BOLDMAGENTA << "Line\t\t\t\tTap\tBitslip\tPA_Status\tWA_Status " << RESET;
                    auto cPhaseAlignVals = fPhaseAlignmentValues.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<AlignmentValues>()[cChipId];
                    auto cWordAlignVals  = fWordAlignmentValues.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<AlignmentValues>()[cChipId];
                    auto cPhaseAlStats   = fPhaseAlignmentStatus.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<AlignmentValues>()[cChipId];
                    for(size_t cLineId = 0; cLineId < 6; cLineId++)
                    {
                        std::stringstream cLineNameStream;
                        cLineNameStream << "B#" << +cBoard->getId() << "_OG#" << +cOpticalGroup->getId() << "_CIC#" << +cHybrid->getId() << "_FE#" << +cChipId << "_L#" << +cLineId;
                        cLineName = cLineNameStream.str();
                        cPAValue  = cPhaseAlignVals[cLineId];
                        cWAValue  = (cLineId < 5) ? cWordAlignVals[cLineId] : 15;
                        LOG(INFO) << BOLDMAGENTA << cLineNameStream.str() << " ->\t" << +cPAValue << "\t" << std::setw(7) << +cWAValue << "\t" << std::setw(9) << +cPhaseAlStats[cLineId] << "\t"
                                  << std::setw(9) << +cLineWAStatus << RESET;
                        cPhaseAlignmentStatusFlag = cPhaseAlignmentStatusFlag && (cPhaseAlStats[cLineId] == 1);
                        cFrontendAlignmentTree->Fill();
                    }
                }
            }
        }
    }

    fillSummaryTree("FE_WORD_ALIGN", (double)cWrdAlignmentStatusFlag);
    fillSummaryTree("FE_PHASE_ALIGN", (double)cPhaseAlignmentStatusFlag);
    // cFrontendAlignmentTree->Write();
#endif
}
void CicFEAlignment::Running()
{
    Initialise();
    AlignInputs();
    Reset();
    GetSummary();
    Stop();
    // fDetectorContainer->resetReadoutChipQueryFunction();
}
/// @brief Prints and stores the results of the CicFEAlignment tool. The status is stored in the summaryTree in the ROOT file and the values are stored in a separate TTree in the ROOT file.
void CicFEAlignment::GetSummary()
{
#ifdef __USE_ROOT__
    auto        cFrontendAlignmentTree = new TTree("frontendAlignmentTree", "Frontend alignment values");
    std::string cLineName              = "";
    int         cPAValue               = -1;
    int         cWAValue               = -1;
    bool        cLinePAStatus          = false;
    bool        cLineWAStatus          = false;
    cFrontendAlignmentTree->Branch("Line", &cLineName);
    cFrontendAlignmentTree->Branch("PAValue", &cPAValue);
    cFrontendAlignmentTree->Branch("WAValue", &cWAValue);
    cFrontendAlignmentTree->Branch("PAStatus", &cLinePAStatus);
    cFrontendAlignmentTree->Branch("WAStatus", &cLineWAStatus);

    bool cPhaseAlignmentStatusFlag = true;
    bool cWordAlignmentStatusFlag  = true;
#endif

    std::stringstream cLineNameStream;
    cLineNameStream.str(std::string());

    LOG(INFO) << BOLDBLUE << "==================================================================================" << RESET;
    LOG(INFO) << BOLDBLUE << "CicFEAlignment::GetSummary -> Printing the status of the CicFEAlignment tool" << RESET;
    LOG(INFO) << "CicFEAlignment::GetSummary... fSuccess: " << +fSuccess << RESET;

    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                auto cChipIds = fEnabledChipIds.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::vector<uint8_t>>();
                for(auto cChipId: cChipIds)
                {
                    LOG(INFO) << "CicFEAlignment::GetSummary. Printing phase alignment summary for chip " << +cChipId << RESET;
                    auto& cWordAlignmentValues =
                        fWordAlignmentValues.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<AlignmentValues>()[cChipId];
                    auto& cPhaseAlignmentValues =
                        fPhaseAlignmentValues.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<AlignmentValues>()[cChipId];
                    for(size_t cLineId = 0; cLineId < 6; cLineId++)
                    {
                        // Print Phase alignment values
                        cLineNameStream.str(std::string());
                        cLineNameStream << "B#" << +cBoard->getId() << "_OG#" << +cOpticalGroup->getId() << "_FE#" << +cHybrid->getId() << "_Chip#" << +cChipId << "_L#" << +cLineId;
                        LOG(INFO) << cLineNameStream.str() << RESET;
                        LOG(INFO) << BOLDMAGENTA << cLineNameStream.str() << " ->\t" << +cPhaseAlignmentValues.at(cLineId) << "\t   "
                                  << "." << RESET;
#ifdef __USE_ROOT__
                        cLineName = cLineNameStream.str();
                        cPAValue  = cPhaseAlignmentValues.at(cLineId);
                        if(cLineId != 5)
                            cWAValue = cWordAlignmentValues.at(cLineId);
                        else
                            cWAValue = -1;
                        cLinePAStatus = true; // dummy value //cThisBePhaseAlignmentStatus.at(cLineId);
                        cLineWAStatus = true; // dummy value //cThisBeWordAlignmentStatus.at(cLineId);
                        cFrontendAlignmentTree->Fill();

                        // cPhaseAlignmentStatusFlag = cPhaseAlignmentStatusFlag && cThisBePhaseAlignmentStatus.at(cLineId);
                        // cWordAlignmentStatusFlag  = cWordAlignmentStatusFlag && cThisBeWordAlignmentStatus.at(cLineId);
#endif
                    }
                    LOG(INFO) << "CicFEAlignment::GetSummary. Printing word alignment summary for chip " << +cChipId << RESET;
                    // Print word alignment values
                    for(size_t cLineId = 0; cLineId < 5; cLineId++)
                    {
                        cLineNameStream.str(std::string());
                        cLineNameStream << "B#" << +cBoard->getId() << "_OG#" << +cOpticalGroup->getId() << "_FE#" << +cHybrid->getId() << "_Chip#" << +cChipId << "_L#" << +cLineId;
                        LOG(INFO) << cLineNameStream.str() << RESET;
                        LOG(INFO) << BOLDMAGENTA << cLineNameStream.str() << " ->\t"
                                  << "\t" << +cWordAlignmentValues.at(cLineId) << "\t   "
                                  << "." << RESET;
                    }
                }
            }
        }
    }
#ifdef __USE_ROOT__
    fillSummaryTree("FE_PHASE_ALIGN", (double)(fPhaseAlignmentSuccess && cPhaseAlignmentStatusFlag));
    fillSummaryTree("FE_WORD_ALIGN", (double)(fWordAlignmentSuccess && cWordAlignmentStatusFlag));
    fResultFile->cd();
    cFrontendAlignmentTree->Write();
#endif
    LOG(INFO) << BOLDBLUE << "==================================================================================" << RESET;
}
bool CicFEAlignment::SetBx0Delay(uint8_t pDelay, uint8_t pStubPackageDelay)
{
    // configure Bx0 alignment patterns in CIC
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                OuterTrackerHybrid* theHybrid = static_cast<OuterTrackerHybrid*>(cHybrid);
                if(theHybrid->fCic != NULL)
                {
                    bool cConfigured = fCicInterface->ManualBx0Alignment(theHybrid->fCic, pDelay);
                    if(!cConfigured)
                    {
                        LOG(INFO) << BOLDRED << "Failed to manually set Bx0 delay in CIC..." << RESET;
                        exit(0);
                    }
                }
            }
        }
    }
    return true;
}

void CicFEAlignment::InputLineScan()
{
    // only for stubs ... for L1 line difficult to do this for 2S
    for(uint8_t cLineId = 0; cLineId < 5; cLineId++)
    {
        auto cPattern = GenManPatternOutLine(cLineId);
        ScanInputPhase(cLineId, cPattern, 0, 15);
    }
}
// manually inject pattern on one of the CIC input lines from a CBC
uint8_t CicFEAlignment::GenManPatternOutLine(uint8_t pOutLine)
{
    uint8_t              cPattern          = 0x8A;
    uint8_t              cBendCode_phAlign = cPattern & 0x0F;
    uint8_t              cBendPattern2S    = (cBendCode_phAlign << 4) | cBendCode_phAlign;
    uint8_t              cSyncPattern2S    = cPattern;
    std::vector<uint8_t> cStubs;
    if(pOutLine == 0)
    {
        cStubs.push_back(cPattern);
        cStubs.push_back(cPattern + 20), cStubs.push_back(cPattern + 40);
    }
    else if(pOutLine == 1)
    {
        cStubs.push_back(cPattern - 20);
        cStubs.push_back(cPattern), cStubs.push_back(cPattern + 20);
    }
    else if(pOutLine == 2)
    {
        cStubs.push_back(cPattern - 40);
        cStubs.push_back(cPattern - 20), cStubs.push_back(cPattern);
    }
    else
    {
        cStubs.push_back(0xA0);
        cStubs.push_back(0xAA), cStubs.push_back(0xCA);
    }

    std::vector<uint8_t> cExpectedPatterns{cStubs[0], cStubs[1], cStubs[2], cBendPattern2S, cSyncPattern2S};
    // make sure FE chips are sending expected pattern
    for(auto cBoard: *fDetectorContainer)
    {
        // generate alignment pattern on all stub lines
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                // configure Chips to produce phase alignment patterns
                for(auto cChip: *cHybrid)
                {
                    if(cChip->getFrontEndType() == FrontEndType::CBC3)
                    {
                        auto                 cInterface = static_cast<CbcInterface*>(fReadoutChipInterface);
                        std::vector<uint8_t> cBendLUT   = cInterface->readLUT(static_cast<ReadoutChip*>(cChip));
                        auto                 cIterator  = std::find(cBendLUT.begin(), cBendLUT.end(), cBendCode_phAlign);
                        if(cIterator != cBendLUT.end())
                        {
                            int              cPosition    = std::distance(cBendLUT.begin(), cIterator);
                            double           cBend_strips = -7. + 0.5 * cPosition;
                            std::vector<int> cBends(cStubs.size(), static_cast<int>(cBend_strips * 2));
                            cInterface->injectStubs(static_cast<ReadoutChip*>(cChip), cStubs, cBends);
                        }
                    }
                } // chip
            }     // hybrid
        }         // OG
    }             // board

    return cExpectedPatterns[pOutLine];
}
void CicFEAlignment::ScanInputPhase(uint8_t pOutLine, uint8_t pPattern, uint8_t pStartScan, uint8_t pEndScan)
{
    LOG(INFO) << BOLDBLUE << "Scanning input phase on CIC input line#" << +pOutLine << " - expected pattern is " << std::bitset<8>(pPattern) << RESET;
    for(uint8_t cPhase = pStartScan; cPhase < 1 + pEndScan; cPhase++) { CheckCicInput(pOutLine, pPattern, cPhase); }
}
DetectorDataContainer CicFEAlignment::CheckCicInput(uint8_t pOutLine, uint8_t pPattern, uint8_t pPhase)
{
    DetectorDataContainer cErrorRate;
    ContainerFactory::copyAndInitChip<float>(*fDetectorContainer, cErrorRate);

    DetectorDataContainer cStubData, cLineErrors;
    ContainerFactory::copyAndInitChip<std::string>(*fDetectorContainer, cStubData);
    ContainerFactory::copyAndInitChip<uint32_t>(*fDetectorContainer, cLineErrors);
    CheckOutLine(pOutLine, pPattern, pPhase, cStubData, cLineErrors);
    for(auto cBoard: *fDetectorContainer)
    {
        auto& cStubDataThisBrd = cStubData.at(cBoard->getIndex());
        auto& cErrorsThisBrd   = cLineErrors.at(cBoard->getIndex());
        auto& cErrRateThisBrd  = cErrorRate.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cStubDataThisOpticalGroup   = cStubDataThisBrd->at(cOpticalGroup->getIndex());
            auto& cLineErrorsThisOpticalGroup = cErrorsThisBrd->at(cOpticalGroup->getIndex());
            auto& cErrRateThisOpticalGroup    = cErrRateThisBrd->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cStubDataThisHybrid   = cStubDataThisOpticalGroup->at(cHybrid->getIndex());
                auto& cLineErrorsThisHybrid = cLineErrorsThisOpticalGroup->at(cHybrid->getIndex());
                auto& cErrRateThisHybrid    = cErrRateThisOpticalGroup->at(cHybrid->getIndex());
                for(auto cChip: *cHybrid)
                {
                    auto& cStubDataThisChip   = cStubDataThisHybrid->at(cChip->getIndex());
                    auto& cLineErrorsThisChip = cLineErrorsThisHybrid->at(cChip->getIndex());
                    auto& cErrRateThisChip    = cErrRateThisHybrid->at(cChip->getIndex());
                    auto& cData               = cStubDataThisChip->getSummary<std::string>();
                    auto& cErrorCount         = cLineErrorsThisChip->getSummary<uint32_t>();
                    auto& cErrRate            = cErrRateThisChip->getSummary<float>();
                    cErrRate                  = (float)cErrorCount / cData.length();
                    LOG(DEBUG) << BOLDBLUE << "Expected pattern is " << std::bitset<8>(pPattern) << RESET;
                    LOG(DEBUG) << BOLDBLUE << "Error rate on this line is " << cErrRate << " errors/bit" << RESET;
                    LOG(DEBUG) << BOLDBLUE << "For a sampling phase of " << +pPhase << " " << cErrorCount << " bit errors in the scoped  data : " << cData << " out of " << cData.length() << " bits."
                               << RESET;
                } // chip
            }     // hybrid
        }         // OG
    }             // board

#ifdef __USE_ROOT__
    fDQMHistogrammer.fillManualPhaseScan(pPhase, pOutLine, cLineErrors, cStubData);
#endif
    return cErrorRate;
}
SlvsLineStatus CicFEAlignment::CheckPhyPort(const Hybrid* pHybrid, PhyPortCnfg pPhyPortCnfg, uint8_t pPhase, uint8_t pPattern)
{
    SlvsLineStatus cStatus;
    std::bitset<8> cExpectedPattern(pPattern);
    std::string    cPatternToMatch = cExpectedPattern.to_string();
    auto           cBoardId        = pHybrid->getBeBoardId();
    fBeBoardInterface->setBoard(cBoardId);
    auto cInterface = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());

    auto  cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    auto& cCic       = static_cast<const OuterTrackerHybrid*>(pHybrid)->fCic;
    // select slvs debug line in FC7
    fBeBoardInterface->WriteBoardReg((*cBoardIter), "fc7_daq_cnfg.physical_interface_block.slvs_debug.hybrid_select", pHybrid->getId());
    fBeBoardInterface->WriteBoardReg((*cBoardIter), "fc7_daq_cnfg.physical_interface_block.slvs_debug.chip_select", 0);
    // select phyPort in CIC mux
    fCicInterface->SelectMux(cCic, pPhyPortCnfg.first);
    // set phase tap for this phy port input
    fCicInterface->SetPhaseTap(cCic, pPhyPortCnfg.first, pPhyPortCnfg.second, pPhase);
    // interface to retrieve debug data
    D19cDebugFWInterface* cDebugInterface = cInterface->getDebugInterface();

    // read back data from stub debug
    // for now .. I need to do this twice
    // figure out why in theFW
    cDebugInterface->StubDebug(true, 6, false);
    auto cLines        = cDebugInterface->StubDebug(true, 6, false);
    cStatus.second     = cLines[pPhyPortCnfg.second];
    cStatus.first      = 0;
    auto        cFound = cStatus.second.find(cPatternToMatch);
    std::string cPatternReceived;
    if(cFound != std::string::npos)
    {
        cPatternReceived = cStatus.second.substr(cFound, cStatus.second.length() - cFound) + cStatus.second.substr(0, cFound);
        LOG(DEBUG) << BOLDYELLOW << "Shifted str : " << cPatternReceived << " - bit shift is " << cFound << RESET;
    }
    else
        cPatternReceived = cStatus.second;

    for(uint8_t cSize = 0; cSize < cPatternReceived.length(); cSize += 8)
    {
        auto cSubStr = cPatternReceived.substr(cSize, 8);
        for(uint8_t cIndx = 0; cIndx < cSubStr.size(); cIndx++)
        {
            if(cSubStr[cIndx] != cPatternToMatch[cIndx]) cStatus.first++;
        }
    }
    cStatus.second = cPatternReceived;
    return cStatus;
}
void CicFEAlignment::CheckOutLine(uint8_t pOutLine, uint8_t pPattern, uint8_t pPhase, DetectorDataContainer& pLineData, DetectorDataContainer& pErrorCounter)
{
    // retreive data and compare
    std::bitset<8> cExpectedPattern(pPattern);
    std::string    cPatternToMatch = cExpectedPattern.to_string();
    // const unsigned int cNStubLinesFromFE=5;
    for(auto cBoard: *fDetectorContainer)
    {
        auto& cStubDataThisBrd = pLineData.at(cBoard->getIndex());
        auto& cErrorsThisBrd   = pErrorCounter.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cStubDataThisOpticalGroup   = cStubDataThisBrd->at(cOpticalGroup->getIndex());
            auto& cLineErrorsThisOpticalGroup = cErrorsThisBrd->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cStubDataThisHybrid   = cStubDataThisOpticalGroup->at(cHybrid->getIndex());
                auto& cLineErrorsThisHybrid = cLineErrorsThisOpticalGroup->at(cHybrid->getIndex());
                auto& cCic                  = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;

                for(auto cChip: *cHybrid)
                {
                    auto& cStubDataThisChip   = cStubDataThisHybrid->at(cChip->getIndex());
                    auto& cLineErrorsThisChip = cLineErrorsThisHybrid->at(cChip->getIndex());
                    auto& cData               = cStubDataThisChip->getSummary<std::string>();
                    auto& cErrorCount         = cLineErrorsThisChip->getSummary<uint32_t>();

                    auto cPhyPortCnfg   = fCicInterface->GetPhyPortConfig(cCic, cChip->getId(), pOutLine);
                    auto cPhyPortStatus = CheckPhyPort(cHybrid, cPhyPortCnfg, pPhase, pPattern);
                    cData               = cPhyPortStatus.second;
                    cErrorCount         = cPhyPortStatus.first;
                } // chip
            }     // hybrid
        }         // OG
    }             // board
}
void CicFEAlignment::SetStaticPhaseAlignment(bool is2SFEHR)
{
    LOG(INFO) << BOLDBLUE << "Setting CIC phase to static mode.." << RESET;
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                fCicInterface->GetOptimalTaps(cCic);

                auto cChipIds = fEnabledChipIds.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::vector<uint8_t>>();
                for(auto cChipId: cChipIds)
                {
                    auto& cPhaseAlStats = fPhaseAlignmentStatus.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<AlignmentValues>()[cChipId];
                    auto& cPhaseAlignmentVals =
                        fPhaseAlignmentValues.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<AlignmentValues>()[cChipId];
                    auto              cAlignmentStatusThisFE = fCicInterface->GetAlignmentStatus(cCic, cChipId);
                    auto              cPhaseTapsThisFE       = fCicInterface->GetOptimalTaps(cCic, cChipId);
                    std::stringstream cOutput;
                    for(uint8_t cLineId = 0; cLineId < 6; cLineId++)
                    {
                        auto thisPhaseTap = cPhaseTapsThisFE[cLineId];
                        if(cAlignmentStatusThisFE[cLineId]==0 and cOpticalGroup->getFrontEndType() == FrontEndType::HYBRID2S ) {
                            if (is2SFEHR) { 
                                thisPhaseTap = fPhaseAlignmentValuesFallback2SFEHR.at(cChipId).at(cLineId);
                            }
                            else {
                                thisPhaseTap = fPhaseAlignmentValuesFallback2SFEHL.at(cChipId).at(cLineId);
                            }
                            LOG (INFO) << BOLDRED << "Chip: "<<(+cChipId)<<" : line " <<(+cLineId) << " failed automatic PA: Phase tap value being set to : "<<(+thisPhaseTap)<<RESET;
                            fCicInterface->SetFePhaseTap(cCic, cChipId, cLineId, thisPhaseTap);
                            cPhaseAlignmentVals[cLineId] = thisPhaseTap;
                        }
                        else {
                            cPhaseAlignmentVals[cLineId] = cPhaseTapsThisFE[cLineId];
                        }
                        cPhaseAlStats[cLineId] = cAlignmentStatusThisFE[cLineId];
                        // to test failing line
                        // if( cLineId == 3 && cChip->getId() == 7) cPhaseAlStats[cLineId]=0;
                    }
                }
                fCicInterface->SetStaticPhaseAlignment(cCic);
            }
        }
    }
}
bool CicFEAlignment::PhaseAlignment(uint16_t pWait_us, uint32_t pNTriggers, bool is2SFEHR)
{
    bool cDebug   = false;
    bool cAligned = true;
    LOG(INFO) << BOLDBLUE << "Starting CIC automated phase alignment procedure .... " << RESET;

    // set auto aligner in CIC
    for(auto cBoard: *fDetectorContainer)
    {
        // generate alignment pattern on all stub lines
        fBeBoardInterface->setBoard(cBoard->getId());
        for(auto cOpticalGroup: *cBoard)
        {
            // configure CIC to use automatic phase aligner
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic        = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                auto  cEnabledFEs = fCicInterface->ReadChipReg(cCic, "FE_ENABLE");
                LOG(DEBUG) << "At the start of PA : " << std::bitset<8>(cEnabledFEs) << RESET;
                fCicInterface->SetAutomaticPhaseAlignment(cCic, true);
            } // hybrids
        }     // optical groups
    }         // boards

    // play pattern + check aligner lock
    for(auto cBoard: *fDetectorContainer)
    {
        bool cWithCBC = false;
        // generate alignment pattern on all stub lines
        fBeBoardInterface->setBoard(cBoard->getId());
        for(auto cOpticalGroup: *cBoard)
        {
            LOG(INFO) << BOLDYELLOW << "Generating phase alignment patterns on hybrids connected to link#" << +cOpticalGroup->getId() << RESET;
            // if PS hybrid then use FC7 to generate alignment pattern
            if(cOpticalGroup->getFrontEndType() == FrontEndType::HYBRIDPS)
            {
                auto             cInterface   = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
                D19cDPInterface* cDPInterface = cInterface->getDPInterface();
                LOG(INFO) << BOLDYELLOW << "Using FC7 DP to generate alignment pattern for CIC" << RESET;
                if(cDPInterface->IsRunning())
                {
                    LOG(INFO) << BOLDBLUE << " STATUS : Data Player is running and will be stopped on PS-FEH " << RESET;
                    cDPInterface->Stop();
                }
                // std::vector<uint8_t> cWordAlignmentPatterns{0xA1,0xD0,0x68,0x34,0x1A,0x0D};
                // std::map<uint8_t,uint8_t> cPatternMap;
                // for(uint8_t cLineId=0; cLineId < 6; cLineId++) cPatternMap[cLineId] = cWordAlignmentPatterns[cLineId];
                // cDPInterface->ConfigurePatternAllLines(cPatternMap);
                cDPInterface->ConfigurePatternAllLines(0x55);
                cDPInterface->Start();
            }

            // for modules - configure Chips to produce phase alignment patterns
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    if(cChip->getFrontEndType() == FrontEndType::CBC3) cWithCBC = true;
                    if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2) continue;
                    LOG(DEBUG) << BOLDBLUE << "Generating Patterns needed for phase alignment of CIC inputs on FE#" << +cChip->getId() << RESET;
                    fReadoutChipInterface->producePhaseAlignmentPattern(cChip, 1);
                }
            }
        }
        // send N triggers on L1 lines
        if(cWithCBC)
        {
            if(cBoard->getBoardType() == BoardType::D19C)
            {
                LOG(INFO) << BOLDBLUE << "Sending triggers to FEs to align L1 output from CBCs.." << RESET;
                uint16_t                                      cTriggerSrc = fBeBoardInterface->ReadBoardReg(cBoard, "fc7_daq_cnfg.fast_command_block.trigger_source");
                std::vector<std::pair<std::string, uint32_t>> cRegVec;
                // local triggers - make sure that's what we use here
                uint16_t cSrc = 3;
                if(cTriggerSrc != cSrc)
                {
                    LOG(INFO) << BOLDBLUE << "\t.. Changing trigger source is set to " << +cSrc << RESET;
                    cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.trigger_source", cSrc});
                }
                cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.triggers_to_accept", pNTriggers});
                cRegVec.push_back({"fc7_daq_ctrl.fast_command_block.control.load_config", 0x1});
                fBeBoardInterface->WriteBoardMultReg(cBoard, cRegVec);
            }
            // instead can just try and send N L1As
            FastCommandInterface* cFastCommandInterface;
            if(cBoard->getBoardType() == BoardType::D19C)
            {
                cFastCommandInterface = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface())->getFastCommandInterface();
                for(size_t cTrigNum = 0; cTrigNum < pNTriggers; cTrigNum++) { cFastCommandInterface->SendGlobalL1A(); }
            }
            else
            {
#if defined(__EMP__)
                cFastCommandInterface = static_cast<DTCFWInterface*>(fBeBoardInterface->getFirmwareInterface())->getFastCommandInterface();
                for(size_t cTrigNum = 0; cTrigNum < pNTriggers; cTrigNum++) { cFastCommandInterface->SendGlobalL1A(); }
#endif
            }
        } // in the CBC case you need to send triggers to get alignment data on L1 line
        // check alignment
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                // enable automatic phase aligner
                auto& cCic    = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                bool  cLocked = fCicInterface->CheckPhaseAlignerLock(cCic);
                // if locked .. switch to automatic phase aligner mode with best values
                if(cLocked)
                { LOG(INFO) << BOLDBLUE << "Phase aligner on CIC" << +cHybrid->getId() << BOLDGREEN << " LOCKED " << BOLDBLUE << " ... storing values and switching to static phase " << RESET; }
                else
                    LOG(INFO) << BOLDBLUE << "Phase aligner on CIC" << +cHybrid->getId() << BOLDRED << " FAILED to LOCK " << BOLDBLUE << " ... storing values and switching to static phase " << RESET;
                cAligned = cAligned && cLocked;
            } // CICs
        }     // OG
    }
    // TEST SYSTEM QUICKFIX 06/11/23
    // P.Szydlik
    // Reason: This method fills the structure holding status and values of PA
    // this step is needed because otherwise status of all line is presumed failed
    // TODO: REVIEW IT
    this->SetStaticPhaseAlignment(is2SFEHR);
    //if(cAligned) this->SetStaticPhaseAlignment();
    //else {
    //    // automatic phase alignment failed, try preset values
    //    this->SetStaticPhaseAlignmentPredefined();
    //}
    // check
    for(auto cBoard: *fDetectorContainer)
    {
        if(!cDebug) continue;

        fBeBoardInterface->setBoard(cBoard->getId());
        auto cInterface = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());

        D19cDebugFWInterface* cDebugInterface = cInterface->getDebugInterface();
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                for(uint8_t cPhyPort = 0; cPhyPort < 12; cPhyPort++)
                {
                    fCicInterface->SelectMux(cCic, cPhyPort);
                    cDebugInterface->StubDebug(true, 4);
                }
                fCicInterface->ControlMux(cCic, 0);
            }
        }
    }

    // remove FEs with failed PA
    CheckAlignment();
    return cAligned;
}
void CicFEAlignment::ScopeStubLines(uint8_t pLineId)
{
    auto                  cPortConnections = fCicInterface->GetPhyPortConnections();
    auto                  cInterface       = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cDebugFWInterface* cDebugInterface  = cInterface->getDebugInterface();

    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        // fBeBoardInterface->ChipReSync(cBoard);
        for(auto cOpticalGroup: *cBoard)
        {
            std::vector<uint8_t> cPatterns;
            if(cOpticalGroup->getFrontEndType() == FrontEndType::HYBRIDPS)
            {
                auto             cInterface   = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
                D19cDPInterface* cDPInterface = cInterface->getDPInterface();
                cPatterns                     = cDPInterface->GetPatternOnLines();
            }
            else
            {
                // 2S FEH
                cPatterns = fReadoutChipInterface->getWordAlignmentPatterns();
            };
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == NULL) continue;

                // auto cFeMapping = fCicInterface->getMapping(cCic);
                fBeBoardInterface->WriteBoardReg(cBoard, "fc7_daq_cnfg.physical_interface_block.slvs_debug.hybrid_select", cHybrid->getId());
                fBeBoardInterface->WriteBoardReg(cBoard, "fc7_daq_cnfg.physical_interface_block.slvs_debug.chip_select", 0);

                LOG(INFO) << BOLDYELLOW << "Scope stub line#" << +pLineId << " on Hybrid#" << +cHybrid->getId() << RESET;
                for(auto cChip: *cHybrid)
                {
                    // auto cPatterns    = fReadoutChipInterface->getWordAlignmentPatterns();
                    auto cPhyPortCnfg = fCicInterface->SelectPhyPortInput(cCic, cChip->getId(), pLineId);
                    auto cPhyPort     = cPhyPortCnfg.first;
                    auto cBeLine      = cPhyPortCnfg.second % 4 + 1; // SLVS lines 1,2,3,4 --> stub lines
                    cDebugInterface->StubDebug(true, 6, false);
                    auto cCheck = cDebugInterface->CheckData(cBeLine, cPatterns[pLineId]);
                    // auto cValue = fReadoutChipInterface->ReadChipReg(cChip,"SerialIface&Error");

                    LOG(INFO) << BOLDYELLOW << "FE#"
                              << +cChip->getId()
                              // << " SerialIface&Error register " << std::bitset<8>(cValue)
                              << " SLVS_Output#" << +pLineId << " PhyPort#" << +cPhyPort << " BeLine#" << +cBeLine << RESET;
                    LOG(INFO) << BOLDYELLOW << "Expected  :" << std::bitset<8>(cPatterns[pLineId]) << RESET;
                    LOG(INFO) << BOLDYELLOW << "Read-back :" << cCheck.fChecked << RESET;
                    std::string checkedData = cCheck.fChecked;
                    // checkedData.erase(std::remove(checkedData.begin(), checkedData.end(), ' '), checkedData.end());
                    // checkedData.erase(std::remove(checkedData.begin(), checkedData.end(), '\t'), checkedData.end());
                    checkedData.erase(std::remove_if(checkedData.begin(), checkedData.end(), [](char c) { return (c != '0' && c != '1'); }), checkedData.end());
                    LOG(INFO) << "Checked data: " << checkedData << RESET;
                    // LOG(INFO) << "WTF is this: " << std::bitset<8>(cPatterns[pLineId]).to_string() << RESET;

                    if(checkedData.find(std::bitset<8>(cPatterns[pLineId]).to_string()) == std::string::npos)
                    {
                        LOG(ERROR) << "Word alignment error: Expected pattern not found in checked data";
                        // getchar();
                    }

                } // ROCs

                fCicInterface->ControlMux(cCic, 0);
            } // hybrids
        }     // links
    }         // boards
}

std::string CicFEAlignment::ScopeFEStubLine(uint8_t pFEId, uint8_t pLineId)
{
    auto                  cPortConnections = fCicInterface->GetPhyPortConnections();
    auto                  cInterface       = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cDebugFWInterface* cDebugInterface  = cInterface->getDebugInterface();

    std::string output;
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        // fBeBoardInterface->ChipReSync(cBoard);
        for(auto cOpticalGroup: *cBoard)
        {
            std::vector<uint8_t> cPatterns;
            if(cOpticalGroup->getFrontEndType() == FrontEndType::HYBRIDPS)
            {
                auto             cInterface   = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
                D19cDPInterface* cDPInterface = cInterface->getDPInterface();
                cPatterns                     = cDPInterface->GetPatternOnLines();
            }
            else
            {
                cPatterns = fReadoutChipInterface->getWordAlignmentPatterns();
            };
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == NULL) continue;

                // auto cFeMapping = fCicInterface->getMapping(cCic);
                fBeBoardInterface->WriteBoardReg(cBoard, "fc7_daq_cnfg.physical_interface_block.slvs_debug.hybrid_select", cHybrid->getId());
                fBeBoardInterface->WriteBoardReg(cBoard, "fc7_daq_cnfg.physical_interface_block.slvs_debug.chip_select", 0);

                LOG(INFO) << BOLDYELLOW << "Scope stub line#" << +pLineId << " on Hybrid#" << +cHybrid->getId() << RESET;
                for(auto cChip: *cHybrid)
                {
                    // auto cPatterns    = fReadoutChipInterface->getWordAlignmentPatterns();
                    auto cPhyPortCnfg = fCicInterface->SelectPhyPortInput(cCic, cChip->getId(), pLineId);
                    auto cPhyPort     = cPhyPortCnfg.first;
                    auto cBeLine      = cPhyPortCnfg.second % 4 + 1; // SLVS lines 1,2,3,4 --> stub lines
                    cDebugInterface->StubDebug(true, 6, false);
                    auto cCheck = cDebugInterface->CheckData(cBeLine, cPatterns[pLineId]);
                    // auto cValue = fReadoutChipInterface->ReadChipReg(cChip,"SerialIface&Error");

                    LOG(INFO) << BOLDYELLOW << "FE#"
                              << +cChip->getId()
                              // << " SerialIface&Error register " << std::bitset<8>(cValue)
                              << " SLVS_Output#" << +pLineId << " PhyPort#" << +cPhyPort << " BeLine#" << +cBeLine << RESET;
                    LOG(INFO) << BOLDYELLOW << "Expected  :" << std::bitset<8>(cPatterns[pLineId]) << RESET;
                    LOG(INFO) << BOLDYELLOW << "Read-back :" << cCheck.fChecked << RESET;

                    if(+cChip->getId() != pFEId) continue;

                    std::string checkedData = cCheck.fChecked;
                    // checkedData.erase(std::remove(checkedData.begin(), checkedData.end(), ' '), checkedData.end());
                    // checkedData.erase(std::remove(checkedData.begin(), checkedData.end(), '\t'), checkedData.end());
                    checkedData.erase(std::remove_if(checkedData.begin(), checkedData.end(), [](char c) { return (c != '0' && c != '1'); }), checkedData.end());
                    LOG(INFO) << "Checked data: " << checkedData << RESET;
                    output = checkedData;
                    break;
                    // LOG(INFO) << "WTF is this: " << std::bitset<8>(cPatterns[pLineId]).to_string() << RESET;

                } // ROCs

                fCicInterface->ControlMux(cCic, 0);
            } // hybrids
        }     // links
    }         // boards
    return output;
}

bool CicFEAlignment::WordAlignment(uint32_t pWait_us)
{
    bool cAligned = true;

    // check if any of the CBCs need a resync
    for(auto cBoard: *fDetectorContainer) { CheckForResync(cBoard); }

    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->ChipReSync(cBoard);
        for(auto cOpticalGroup: *cBoard)
        {
            // if PS hybrid then use FC7 to generate alignment pattern
            if(cOpticalGroup->getFrontEndType() == FrontEndType::HYBRIDPS)
            {
                auto             cInterface   = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
                D19cDPInterface* cDPInterface = cInterface->getDPInterface();
                LOG(INFO) << BOLDYELLOW << "Using FC7 DP to generate alignment pattern for CIC" << RESET;
                if(cDPInterface->IsRunning())
                {
                    LOG(INFO) << BOLDBLUE << " STATUS : Data Player is running and will be stopped on PS-FEH " << RESET;
                    cDPInterface->Stop();
                }
                // std::vector<uint8_t> cWordAlignmentPatterns{0xA1,0xA1,0xA1,0xA1,0xA1,0xA1};
                // // std::vector<uint8_t> cWordAlignmentPatterns{0xA1,0xD0,0x68,0x34,0x1A,0x0D};
                // std::map<uint8_t,uint8_t> cPatternMap;
                // for(uint8_t cLineId=0; cLineId < 6; cLineId++) cPatternMap[cLineId] = cWordAlignmentPatterns[cLineId];
                // cDPInterface->ConfigurePatternAllLines(cPatternMap);
                // uint8_t pattern = 0x81;
                uint8_t pattern = 0x3E;
                cDPInterface->ConfigurePatternAllLines(pattern);
                LOG(INFO) << BOLDBLUE << "DEBUG L943: Pattern = " << std::hex << unsigned(pattern) << std::dec << RESET;
                cDPInterface->Start();
            }
            // if on module then use FE asics to configure alignment pattern
            // bool cASICFound = false;
            for(auto cHybrid: *cOpticalGroup)
            {
                if(cOpticalGroup->getFrontEndType() == FrontEndType::HYBRIDPS) { continue; }
                for(auto cChip: *cHybrid) { fReadoutChipInterface->produceWordAlignmentPattern(cChip); }
            }
        }
    } // produce WA pattern

    for(int cLineId = 0; cLineId < 5; cLineId++)
    {
        if(cLineId != 0) continue;
        ScopeStubLines(cLineId);
    }

    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            LOG(INFO) << BOLDYELLOW << "Auto WA procedure  Link#" << +cOpticalGroup->getId() << RESET;
            std::vector<uint8_t> cWordAligned(0);
            // run automated alignment procedure
            cAligned = true;
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == NULL) continue;

                auto& cWrdAlStats = fWordAlignmentStatus.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<uint8_t>();
                // configure word alignment pattern on CBCs
                std::vector<uint8_t> cAlignmentPatterns;
                if(cOpticalGroup->getFrontEndType() == FrontEndType::HYBRIDPS)
                {
                    auto             cInterface   = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
                    D19cDPInterface* cDPInterface = cInterface->getDPInterface();
                    cAlignmentPatterns            = cDPInterface->GetPatternOnLines();
                }
                else
                    cAlignmentPatterns = fReadoutChipInterface->getWordAlignmentPatterns();
                for(size_t cLine = 0; cLine < 6; cLine++)
                { LOG(INFO) << "DEBUG L991 " << BOLDBLUE << "cAlignmentPatterns for line " << cLine << " = 0x" << std::hex << +(uint8_t)cAlignmentPatterns[cLine] << std::dec << RESET; };
                bool skipAutomatedAlignment = false;
                // bool testing = true;
                bool cSuccessAlign = false;
                LOG(INFO) << "READING BACK: " << RESET;
                LOG(INFO) << "MASK: 0x" << std::hex << unsigned(fCicInterface->ReadChipReg(cCic, "MASK_BLOCK2")) << std::dec << RESET;

                fCicInterface->WriteChipReg(cCic, "MASK_BLOCK2", 0xFF);
                if(!skipAutomatedAlignment) { cSuccessAlign = fCicInterface->AutomatedWordAlignment(cCic, cAlignmentPatterns, pWait_us * 10000); }
                // if(testing) {
                //    //Scope one stub line for a front end
                //    //check for the pattern
                //    //try a new delay if the pattern is not correc
                //    std::string cRegName   =  "MISC_CTRL";
                //    uint16_t    cToggleOnExtWaDelay = 0x2;
                //    fCicInterface->WriteChipReg(cCic, cRegName, cToggleOnExtWaDelay);
                //    LOG(INFO) << "READING BACK: " << RESET;
                //    LOG(INFO) << "MISC_CTRL: 0x" << std::hex << unsigned(fCicInterface->ReadChipReg(cCic, cRegName)) << std::dec << RESET;
                //    bool didWeSucceed = false;
                //    for(int delayBlock = 0; delayBlock <= 19; delayBlock++) {
                //        std::string delayBlockToChek = "EXT_WA_DELAY";
                //        if(delayBlock < 10 ) {
                //            delayBlockToChek += "0" + std::to_string(delayBlock);
                //        }
                //        else {
                //            delayBlockToChek += std::to_string(delayBlock);
                //        }
                //        LOG(INFO) << "DEBUG L1012 " << BOLDBLUE << "delayBlockToChek = " << delayBlockToChek << RESET;
                //        fCicInterface->WriteChipReg(cCic, delayBlockToChek, 0x0); // this just works for line 00
                //        for(uint16_t cDelayValue = 0; cDelayValue <= 0xF; cDelayValue++) {
                //            LOG(INFO) << "Actual delay: 0x" << std::hex << RESET;
                //            LOG(INFO) << unsigned(fCicInterface->ReadChipReg(cCic, delayBlockToChek)) << std::dec << RESET;
                //            LOG(INFO) << "Searching for pattern"<< RESET;
                //            didWeSucceed = SearchForPatternInStubLine(cAlignmentPatterns[0]);
                //            LOG(INFO) << "Pattern searched" << RESET;
                //            if(!didWeSucceed) {
                //                LOG(INFO) << BOLDBLUE << "going to delay: cDelayValue = 0x" << std::hex << unsigned(cDelayValue) << std::dec << RESET;
                //            } else{
                //                LOG(INFO) << BOLDGREEN <<  "We found the pattern! " << RESET;
                //                //cSuccessAlign = true;
                //                //break;
                //            }
                //            fCicInterface->WriteChipReg(cCic, delayBlockToChek, cDelayValue); // this just works for line 00
                //        }
                //        for(uint16_t cDelayValue = 0; cDelayValue <= 0xF; cDelayValue++) {
                //            cDelayValue = cDelayValue << 4;
                //            LOG(INFO) << "Actual delay: 0x" << std::hex << RESET;
                //            LOG(INFO) << unsigned(fCicInterface->ReadChipReg(cCic, delayBlockToChek)) << std::dec << RESET;
                //            LOG(INFO) << "Searching for pattern"<< RESET;
                //            didWeSucceed = SearchForPatternInStubLine(cAlignmentPatterns[0]);
                //            LOG(INFO) << "Pattern searched" << RESET;
                //            if(!didWeSucceed) {
                //                LOG(INFO) << BOLDBLUE << "going to delay: cDelayValue = 0x" << std::hex << unsigned(cDelayValue) << std::dec << RESET;
                //            } else{
                //                LOG(INFO) << BOLDGREEN <<  "We found the pattern! " << RESET;
                //                //cSuccessAlign = true;
                //                //break;
                //            }
                //            fCicInterface->WriteChipReg(cCic, delayBlockToChek, cDelayValue); // this just works for line 00
                //        }
                //    }
                // auto pPortConfig = fCicInterface->ReadChipReg(cCic, "PHY_PORT_CONFIG");
                // LOG(INFO) << "PhyportConfig is in: "
                // for(uint16_t cDelayValue = 0; cDelayValue <= 0xF; cDelayValue++) {
                //    uint32_t cDelaytmp = cDelayValue << 4;
                //    LOG(INFO) << "DEBUG L1020 " << BOLDBLUE << "cDelaytmp = 0x" << std::hex << unsigned(cDelaytmp) << std::dec << RESET;
                //    LOG(INFO) << "Actual delay: 0x" << std::hex << RESET;
                //    LOG(INFO) << unsigned(fCicInterface->ReadChipReg(cCic, "scPhaseSelectB3i3")) << std::dec << RESET;
                //    LOG(INFO) << "Searching for pattern"<< RESET;
                //    didWeSucceed = SearchForPatternInStubLine();
                //    LOG(INFO) << "Pattern searched" << RESET;
                //    if(!didWeSucceed) {
                //        LOG(INFO) << BOLDBLUE << "going to delay: cDelayValue = 0x" << std::hex << unsigned(cDelayValue) << std::dec << RESET;
                //        fCicInterface->WriteChipReg(cCic, "scPhaseSelectB3i3",cDelaytmp); // this just works for line 00
                //        getchar();
                //    } else{
                //        LOG(INFO) << "We found the pattern! " << RESET;
                //        //break;
                //    }
                //}

                //}

                // LOG(INFO) << "Press a key to continue" << RESET;
                // getchar();

                // // to test failing WA
                // cSuccessAlign = false;
                cWrdAlStats = cSuccessAlign ? 1 : 0;
                cWordAligned.push_back(cSuccessAlign ? 1 : 0);
                cAligned = cAligned && (cSuccessAlign);
                // LOG(INFO) << "DEBUG L999 " << BOLDBLUE << "cAligned = " << cAligned << " cSuccessAlign = "<< cSuccessAlign << RESET;
            }

            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == NULL) continue;

                auto& cWrdAlStats = fWordAlignmentStatus.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<uint8_t>();
                // get result of automated word alignment
                std::vector<std::vector<uint8_t>> cWordAlignmentValues = fCicInterface->GetWordAlignmentValues(cCic);
                // check status
                std::stringstream cMessage;
                cMessage << BOLDBLUE << "Automated word alignment procedure ";
                if(cWrdAlStats == 1)
                    cMessage << BOLDGREEN << " SUCCEEDED!";
                else
                    cMessage << BOLDRED << " FAILED!";
                LOG(INFO) << cMessage.str() << BOLDBLUE << " on Link#" << +cOpticalGroup->getId() << " hybrid#" << +cHybrid->getId() << RESET;
                auto cChipIds = fEnabledChipIds.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::vector<uint8_t>>();
                for(auto cChipId: cChipIds)
                {
                    auto& cWordAlignmentVals = fWordAlignmentValues.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<AlignmentValues>()[cChipId];
                    std::stringstream cOutput;
                    for(size_t cLine = 0; cLine < 5; cLine++)
                    {
                        cWordAlignmentVals[cLine] = (cWrdAlStats == 1) ? cWordAlignmentValues[cChipId][cLine] : 15;
                        cOutput << +cWordAlignmentVals[cLine] << " ";
                    }
                    // LOG(INFO) << BOLDBLUE << "Word alignment values for FE#" << +cChip->getId() << " : " << cOutput.str() << RESET;
                    LOG(INFO) << BOLDBLUE << "Word alignment values for FE#" << cChipId << " : " << cOutput.str() << RESET;
                }
            }
        }
    }
    return cAligned;
}
void CicFEAlignment::Stop()
{
    LOG(INFO) << BOLDYELLOW << "CicFEAlignment::Stop" << RESET;
    // if on module then use FE asics to configure alignment pattern
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == NULL) continue;
                // configure word alignment pattern on CBCs
                std::vector<uint8_t> cAlignmentPatterns;
                if(cOpticalGroup->getFrontEndType() == FrontEndType::HYBRIDPS)
                {
                    auto             cInterface   = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
                    D19cDPInterface* cDPInterface = cInterface->getDPInterface();
                    cDPInterface->Stop();
                    cDPInterface->ConfigurePatternAllLines(0x00);
                    cDPInterface->Start();
                    cDPInterface->Stop();
                }
                if(cOpticalGroup->getFrontEndType() == FrontEndType::OuterTrackerPS)
                {
                    LOG(INFO) << BOLDYELLOW << "Readout mode on PS module.." << RESET;
                    // for(auto cChip: *cHybrid) fReadoutChipInterface->WriteChipReg(cChip, "ReadoutMode", 0x00);
                }
            } // hybrids
        }     // OGs
    }         // boards

    dumpConfigFiles();
    // Destroy();
}

bool CicFEAlignment::SearchForPatternInStubLine(uint8_t pPattern)
{
    std::string patternWeAreSearchingFor = std::bitset<8>(pPattern).to_string();
    patternWeAreSearchingFor += patternWeAreSearchingFor;
    LOG(INFO) << "Starting sw word alignment" << RESET;
    auto cWaPattern = ScopeFEStubLine(0, 0); // line and FEH too will have to become parameters
    LOG(INFO) << "cWaPattern: " << cWaPattern << RESET;
    bool patternFound = cWaPattern.find(patternWeAreSearchingFor) != std::string::npos;
    if(patternFound)
    {
        // Pattern not found
        LOG(INFO) << BOLDGREEN << "Pattern found in cWaPattern" << RESET;
        // Handle the case when the pattern is not found
    }
    else
    {
        // Pattern found
        LOG(INFO) << "Pattern not found in cWaPattern" << RESET;
        getchar();
        // Continue with the rest of the code
    }

    LOG(INFO) << "The pattern we're getting is: " << cWaPattern << RESET;
    return patternFound;
}
//
// bool CicFEAlignment::SearchForPatternInStubLine(std::string pPattern) {
//
//}
//

void CicFEAlignment::Pause() {}

void CicFEAlignment::Resume() {}

// #endif
