/*!
 *
 * \file CicFEAlignment.h
 * \brief CIC FE alignment class, automated alignment procedure for CICs
 * connected to FEs
 * \author Sarah SEIF EL NASR-STOREY
 * \date 28 / 06 / 19
 *
 * \Support : sarah.storey@cern.ch
 *
 */

#ifndef CicFEAlignment_h__
#define CicFEAlignment_h__

#include "OTTool.h"
#include <map>
// #ifdef __USE_ROOT__

#ifndef AlignmentValues
typedef std::map<uint8_t, std::vector<uint8_t>> AlignmentValues;
#endif

#ifndef StubLineData
typedef std::vector<std::string> StubLineData;
#endif

#ifndef PhyPortCnfg
typedef std::pair<uint8_t, uint8_t> PhyPortCnfg;
#endif

#ifndef SlvsLineStatus
typedef std::pair<uint32_t, std::string> SlvsLineStatus;
#endif

#ifdef __USE_ROOT__
#include "DQMUtils/DQMHistogramCicFEAlignment.h"
#include "TH1.h"
#endif

// add break codes here
const uint8_t FAILED_PHASE_ALIGNMENT = 1;
const uint8_t FAILED_WORD_ALIGNMENT  = 2;
const uint8_t FAILED_BX_ALIGNMENT    = 3;

class CicFEAlignment : public OTTool
{
    using RegisterVector      = std::vector<std::pair<std::string, uint8_t>>;
    using TestGroupChannelMap = std::map<int, std::vector<uint8_t>>;

  public:
    CicFEAlignment();
    ~CicFEAlignment();

    void Initialise();
    bool AlignInputs(bool is2SFEHR = false);
    void SetStaticPhaseAlignment(bool is2SFEHR = false);
    // manual scan of CIC input phases
    void                  InputLineScan();
    uint8_t               GenManPatternOutLine(uint8_t pLine);
    void                  ScanInputPhase(uint8_t pOutLine, uint8_t pPattern, uint8_t pStartScan, uint8_t pEndScan);
    DetectorDataContainer CheckCicInput(uint8_t pOutLine, uint8_t pPattern, uint8_t pPhase);
    void                  CheckOutLine(uint8_t pOutLine, uint8_t pPattern, uint8_t pPhase, DetectorDataContainer& pLineData, DetectorDataContainer& pErrorCounter);
    SlvsLineStatus        CheckPhyPort(const Ph2_HwDescription::Hybrid* pHybrid, PhyPortCnfg pPhyPortCnfg, uint8_t pPhase, uint8_t pPattern);
    // automated configuration of CIC input phase and delay
    bool        PhaseAlignment(uint16_t pWait_us = 10, uint32_t pNTriggers = 500, bool is2SFEHR = false);
    bool        WordAlignment(uint32_t pWait_us = 10);
    bool        Bx0Alignment(uint8_t pFe = 0, uint8_t pLine = 4, uint16_t pDelay = 1, uint16_t pWait_ms = 100, int cNrials = 3);
    bool        SetBx0Delay(uint8_t pDelay = 8, uint8_t pStubPackageDelay = 3);
    bool        BackEndAlignment();
    void        Running() override;
    void        Stop() override;
    void        Pause() override;
    void        Resume() override;
    void        writeObjects();
    void        GetSummary();
    bool        SearchForPatternInStubLine(uint8_t pPattern);
    std::string ScopeFEStubLine(uint8_t pFEId, uint8_t pLineId);

    // get alignment results
    uint8_t getPhaseAlignmentValue(Ph2_HwDescription::BeBoard* pBoard, Ph2_HwDescription::OpticalGroup* pGroup, Ph2_HwDescription::Hybrid* pFe, Ph2_HwDescription::ReadoutChip* pChip, uint8_t pLine)
    {
        return fPhaseAlignmentValues.getObject(pBoard->getId())->getObject(pGroup->getId())->getObject(pChip->getId())->getSummary<AlignmentValues>()[pChip->getId()][pLine];
    }
    uint8_t getWordAlignmentValue(Ph2_HwDescription::BeBoard* pBoard, Ph2_HwDescription::OpticalGroup* pGroup, Ph2_HwDescription::Hybrid* pFe, Ph2_HwDescription::ReadoutChip* pChip, uint8_t pLine)
    {
        return fWordAlignmentValues.getObject(pBoard->getId())->getObject(pGroup->getId())->getObject(pChip->getId())->getSummary<AlignmentValues>()[pChip->getId()][pLine];
    }
    bool getStatus() const { return fSuccess; }
    void ScopeStubLines(uint8_t pLineId = 0); // uint8_t pFeId, uint8_t pLineId);
    void CheckAlignment(); 
    std::vector<uint8_t> getListOfInvalidFEs(){return fInvalidList;}
    void CreateSummaryTree();
    
    
  protected:
  private:
    // status
    bool fSuccess;
    // invalid ids
    std::vector<uint8_t> fInvalidList;
    bool                 fPhaseAlignmentSuccess;
    bool                 fWordAlignmentSuccess;
    // Containers
    DetectorDataContainer fEnabledChipIds;
    DetectorDataContainer fPhaseAlignmentStatus;
    DetectorDataContainer fWordAlignmentStatus;
    DetectorDataContainer fPhaseAlignmentValues;
    DetectorDataContainer fWordAlignmentValues;
    DetectorDataContainer fRegMapContainer;
    DetectorDataContainer fBoardRegContainer;

    // mapping of FEs for CIC
    std::vector<uint8_t> fFEMapping{3, 2, 1, 0, 4, 5, 6, 7}; // FE --> FE CIC [2S]

    // hardcoded CIC input phase alignment values, in case of failure of automatic procedure
    //2SFEHL
    std::map<uint8_t, std::vector<uint8_t> > fPhaseAlignmentValuesFallback2SFEHL = {
        {0,  std::vector<uint8_t>{8,8,8,8,9,8}},
        {1,  std::vector<uint8_t>{8,8,8,8,9,8}},
        {2,  std::vector<uint8_t>{8,8,8,8,8,9}},
        {3,  std::vector<uint8_t>{8,9,9,8,8,8}},
        {4,  std::vector<uint8_t>{8,8,8,8,8,7}},
        {5,  std::vector<uint8_t>{7,7,7,7,7,7}},
        {6,  std::vector<uint8_t>{6,6,7,6,7,7}},
        {7,  std::vector<uint8_t>{6,6,6,6,7,5}},
    };
    //2SFEHR
    std::map<uint8_t, std::vector<uint8_t> > fPhaseAlignmentValuesFallback2SFEHR = {
        {0,  std::vector<uint8_t>{5,6,6,6,6,5}},
        {1,  std::vector<uint8_t>{6,6,6,6,7,6}},
        {2,  std::vector<uint8_t>{6,7,7,6,7,7}},
        {3,  std::vector<uint8_t>{7,8,8,7,8,7}},
        {4,  std::vector<uint8_t>{8,8,8,8,9,8}},
        {5,  std::vector<uint8_t>{8,8,8,8,8,8}},
        {6,  std::vector<uint8_t>{8,8,8,8,8,8}},
        {7,  std::vector<uint8_t>{8,9,8,9,9,7}},
    };

    // expected number of bx first stub
    // appears after resync
    // different for CBC and MPA
    uint8_t fStubBxDelay2S = 8;
    uint8_t fStubBxDelayPS = 22;

#ifdef __USE_ROOT__
    DQMHistogramCicFEAlignment fDQMHistogrammer;
#endif
};

#endif
// #endif
