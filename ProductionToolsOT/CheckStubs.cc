#include "CheckStubs.h"
#include "../Utils/ChannelGroupHandler.h"
#include "../Utils/ContainerFactory.h"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

CheckStubs::CheckStubs() : Tool() {}

CheckStubs::~CheckStubs() {}

void CheckStubs::Initialise(void)
{
    fNEvents = findValueInSettings<double>("Nevents", 10);
}

void CheckStubs::ReenableChannels(void)
{
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    CbcInterface*              theCbcInterface = static_cast<CbcInterface*>(fReadoutChipInterface);
                    ChannelGroup<NCHANNELS, 1> cChannelMask;
                    cChannelMask.enableAllChannels();
                    theCbcInterface->maskChannelGroup(cChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask)));
                }
            }
        }
    }
}

void CheckStubs::FillStubsTree(std::string pParameter, std::string pValue)
{
#if defined(__USE_ROOT__)
	fResultFile->cd();
	if(gROOT->FindObject("StubsTree") != nullptr)
	{
		fStubsTree = static_cast<TTree*>(gROOT->FindObject("StubsTree"));
	}
	else
	{
		fStubsTree = new TTree("StubsTree", "Bad stub lines");
		fStubsTree->Branch("Parameter", &fStubsTreeParameter);
		fStubsTree->Branch("Value", &fStubsTreeValue);
	}
	fStubsTreeParameter = pParameter;
	fStubsTreeValue     = pValue;
	fStubsTree->Fill();
#endif
}


bool CheckStubs::TestStubLines()
{
    bool allPass = true;
    FillStubsTree(" "," ");

    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            setSameDacBeBoard(cBoard, "Threshold", 1023);
            for(auto cHybrid: *cOpticalGroup)
            { 
                CbcInterface*              theCbcInterface = static_cast<CbcInterface*>(fReadoutChipInterface);
                ChannelGroup<NCHANNELS, 1> cChannelMask;
                // first disable everything on all chips
                for(auto cChip: *cHybrid)
                {
                    cChannelMask.disableAllChannels();
                    theCbcInterface->maskChannelGroup(cChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask)));
                }
		
                std::vector<uint8_t> channelsToUnmask = {168,171,202,203,252,251};
                std::vector<std::pair<uint8_t, uint8_t>> expectedStubs;
                expectedStubs.push_back(std::make_pair(170,1));
                expectedStubs.push_back(std::make_pair(204,0));
                expectedStubs.push_back(std::make_pair(254,9));
                for(auto cChip: *cHybrid)
		{

                    fReadoutChipInterface->WriteChipReg(cChip, "LayerSwap", 0);
                    fReadoutChipInterface->WriteChipReg(cChip, "ClusterCut", 4);

                    cChannelMask.disableAllChannels();
                    this->UnmaskChannels(channelsToUnmask, cChannelMask);
                    theCbcInterface->maskChannelGroup(cChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask)));  
          
                    auto stubs = ReadStubs(cHybrid->getId(), cChip->getId());
                    
                    bool pass = true;
                    if (stubs.size()==expectedStubs.size())
                    {
                        for (uint8_t istub=0; istub<expectedStubs.size(); istub++)
                        {
                           auto expectedStub = expectedStubs.at(istub);
                           //LOG(INFO) << "Expected stub: "<<(+expectedStub.first) << " : "<<(+expectedStub.second)<<RESET;
                           bool foundStub = false;
                           for (uint8_t jstub=0; jstub<stubs.size(); jstub++) 
                           { 
                               //auto stub = stubs.at(jstub);
                               //LOG(INFO) << "Read stub: "<<(+stub.first) << " : "<<(+stub.second)<<RESET;
                               if (expectedStub == stubs.at(jstub))
                               {
                                   //LOG(INFO) << "Found it!" << RESET;
                                   foundStub = true;
                               }   
                           }
                           if (!foundStub) 
                           {
                               pass = false;
                               break;
                           }
                        }
                    }
                    else 
                    {     
                        pass = false;
                    }
                    if (pass) 
                    {
                        LOG(INFO) << GREEN << "CBC " << (+cChip->getId()) << " passed stub line test" << RESET;
                    }
                    else 
                    {
                        LOG(INFO) << RED << "CBC " << (+cChip->getId()) << " failed stub line test" << RESET;
                    }
                    allPass = allPass && pass;              
                    
                    //remask all channels on chip
                    cChannelMask.disableAllChannels();
                    theCbcInterface->maskChannelGroup(cChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask)));  
		}
		
                
            }
        }
    }

    int status = 1;
    if(allPass)
    {
        LOG(INFO) << BOLDGREEN << "CheckStubs PASSED" << RESET;
    }
    else
    {
        LOG(INFO) << BOLDRED << "CheckStubs FAILED" << RESET;
        status = 0;
    }
    fillSummaryTree("CheckStubsTest_STATUS", status); 
    return allPass;
}

std::vector<std::pair<uint8_t, uint8_t>> CheckStubs::ReadStubs(uint8_t hybridId, uint8_t chipId)
{
    std::vector<std::pair<uint8_t, uint8_t>> stubs;
    const uint16_t NEVENT = 1;

    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        uint16_t cOriginalTriggerSrc         = fBeBoardInterface->ReadBoardReg(cBoard, "fc7_daq_cnfg.fast_command_block.trigger_source");
        uint16_t cOriginalTriggerMult = fBeBoardInterface->ReadBoardReg(cBoard, "fc7_daq_cnfg.fast_command_block.misc.trigger_multiplicity");
        std::vector<std::pair<std::string, uint32_t>> cRegVec;
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.misc.trigger_multiplicity", 10});
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.trigger_source", 3});
        fBeBoardInterface->WriteBoardMultReg(cBoard, cRegVec);
        ReadNEvents(cBoard, NEVENT);
        const std::vector<Event*>& cEvents = this->GetEvents();
        for(auto& cEvent: cEvents)
        {
            std::vector<std::pair<uint8_t, uint8_t>> stubs_thisevent;
            for(const auto cOpticalGroup: *cBoard)
            {
                for(const auto cHybrid: *cOpticalGroup)
                {
                    auto cBxId = cEvent->BxId(cHybrid->getId());
                    for(const auto cChip: *cHybrid)
                    {
                            if (cChip->getId() != chipId) continue;

			    auto              cStubs     = cEvent->StubVector(cChip->getHybridId(), cChip->getId());
			    auto              cThreshold = fReadoutChipInterface->ReadChipReg(cChip, "Threshold");
			    auto              cHits      = cEvent->GetHits(cChip->getHybridId(), cChip->getId());
			    std::stringstream cOut;
			    cOut << " Hits in Channels :";
			    for(auto cHit: cHits) cOut << " " << +cHit << ",";
			    cOut << " Stubs [Seed,BendCode]:";
			    for(auto cStub: cStubs) cOut << " [" << +cStub.getPosition() << "," << +cStub.getBend() << " ],";
			    LOG(INFO) << BOLDYELLOW << "Chip#" << +cChip->getId() << " [ " << cThreshold << " ] " << cStubs.size() << " stubs and " << cHits.size() << " hits "
                                      << " BxId " << cBxId << "\t..." << cOut.str() << RESET;
                            for (auto cStub: cStubs) {
                                stubs_thisevent.push_back(std::make_pair(cStub.getPosition(),cStub.getBend()));
                            }

                    }
                }
            }
            if (stubs_thisevent.size()>stubs.size()) 
            {
                stubs = stubs_thisevent;
            }
        }
        cRegVec.clear();
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.misc.trigger_multiplicity", cOriginalTriggerMult});
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.trigger_source", cOriginalTriggerSrc});
        fBeBoardInterface->WriteBoardMultReg(cBoard, cRegVec);
    }
    return stubs;
}

void CheckStubs::UnmaskChannels(std::vector<uint8_t> pToUnmask, ChannelGroup<NCHANNELS, 1>& pChannelMask)
{
    for(size_t cChnl: pToUnmask)
    {
        LOG(DEBUG) << GREEN << "enabling " << +cChnl << RESET;
        pChannelMask.enableChannel(cChnl);
    }
}

void CheckStubs::ConfigureCalibration() {}

void CheckStubs::Running()
{
    LOG(INFO) << "Starting CheckStubs measurement.";
    Initialise();
    TestStubLines();
    LOG(INFO) << "Done with CheckStubs.";
    ReenableChannels();
}

void CheckStubs::Stop(void)
{
    LOG(INFO) << "Stopping CheckStubs measurement.";
    dumpConfigFiles();
    SaveResults();
    closeFileHandler();
    LOG(INFO) << "CheckStubs stopped.";
}

void CheckStubs::Pause() {}

void CheckStubs::Resume() {}
