/*!
 *
 * \file CheckCbcNeighbors.h
 * \brief CheckCbcNeighbors class
 * \author Lesya Horyn
 * \date 11/07/22
 *
 */

#ifndef CheckCbcNeighbors_h__
#define CheckCbcNeighbors_h__

#include "Tool.h"
#include <map>
// Calibration is not running on the SoC: I need to instantiate the DQM histrgrammer here
using namespace Ph2_HwDescription;

class CheckCbcNeighbors : public Tool
{
  public:
    CheckCbcNeighbors();
    ~CheckCbcNeighbors();

    void Initialise(void);
    void ReenableChannels(void);

    // State machine
    void Running() override;
    void Stop() override;
    void ConfigureCalibration() override;
    void Pause() override;
    void Resume() override;
    void Reset();
    bool TestCbcNeighbors();
    // returns number of stubs
    std::vector<uint8_t> CheckStubs(uint8_t hybridId, std::vector<uint8_t> chipIds, bool shouldFindStubs);
    bool                 TestCbcConnectionsList(std::vector<std::pair<ReadoutChip*, ReadoutChip*>> chipPairs);
    void                 FillCBCLatLinesTree(std::string, std::string);

  private:
    //
    uint32_t fNEvents;
    // channel maps
    DetectorDataContainer fSharedCorrelationLayerOutHigh;
    DetectorDataContainer fSharedSeedLayerOutHigh;
    DetectorDataContainer fSharedCorrelationLayerOutLow;
    DetectorDataContainer fSharedSeedLayerOutLow;
    // test result
    DetectorDataContainer fGoodBumps;
    DetectorDataContainer fBadBumps;

    const std::vector<uint8_t>              fTestBToA_TestedLatChannel = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 0, 2, 4};
    const std::vector<std::vector<uint8_t>> fTestBToA_CBCA{{250}, {250}, {250}, {250}, {250}, {250}, {250}, {250}, {250}, {252}, {252}, {252}, {252}, {250, 252}, {250, 252}, {250, 252}};
    const std::vector<std::vector<uint8_t>>
                            fTestBToA_CBCB{{1}, {3}, {5}, {7}, {9}, {11}, {13}, {15}, {17}, {19}, {19, 21}, {17, 19, 21, 23}, {0, 17, 19, 21, 23, 25}, {0, 19}, {0, 2, 19, 21}, {0, 2, 4, 19}};
    const std::vector<bool> fTestBToA_ReturnStub{true, true, true, true, true, true, true, true, true, true, false, false, false, true, true, false};

    const std::vector<uint8_t>              fTestAToB_TestedLatChannel = {253, 251, 249, 247, 245, 243, 241, 239, 237, 235, 233, 231, 252, 250};
    const std::vector<std::vector<uint8_t>> fTestAToB_CBCA{{253}, {251}, {249}, {247}, {245}, {243}, {241}, {239}, {237}, {235}, {233, 235}, {231, 233, 235, 237}, {235, 252}, {237, 250, 252}};
    const std::vector<std::vector<uint8_t>> fTestAToB_CBCB{{4}, {4}, {4}, {4}, {4}, {4}, {4}, {4}, {2}, {0}, {0}, {0}, {0, 2}, {0, 2}};
    const std::vector<bool>                 fTestAToB_ReturnStub{true, true, true, true, true, true, true, true, true, true, false, false, true, false};

    // negative test with tested lines dropped, to cross-check test logic
    // const std::vector<std::vector<uint8_t>> fTestBToA_CBCA{{250},{250},{250},{250},{250},{250},{250},{250},{250},{252},{252},{252},{250,252},{252},{250,252},{250,252}};
    // const std::vector<std::vector<uint8_t>> fTestBToA_CBCB{{},{},{},{},{},{},{},{},{},{},{19},{17,19,21},{19},{0,17,19,21,23},{0,19,21},{0,2,19}};
    // const std::vector<bool>                 fTestBToA_ReturnStub{false,false,false,false,false,false,false,false,false,false,true,true,false,true,false,true};

    // const std::vector<std::vector<uint8_t>> fTestAToB_CBCA{{},{},{},{},{},{},{},{},{},{},{235},{233,235,237},{235},{237,252}};
    // const std::vector<std::vector<uint8_t>> fTestAToB_CBCB{{4},{4},{4},{4},{4},{4},{4},{4},{2},{0},{0},{0},{0,2},{0,2}};
    // const std::vector<bool>                 fTestAToB_ReturnStub{false,false,false,false,false,false,false,false,false,false,true,true,false,true};

    void   UnmaskChannels(std::vector<uint8_t> pToUnmask, ChannelGroup<NCHANNELS, 1>& pChannelMask);
    size_t fNHighestCorrelationLayer{12};
    size_t fNHighestSeedLayer{2};
    size_t fNLowestCorrelationLayer{13};
    size_t fNLowestSeedLayer{3};
#if defined(__USE_ROOT__)
    TTree* fCBCLatLinesTree = nullptr;
#endif
    std::string fCBCLatLinesTreeParameter = "";
    std::string fCBCLatLinesTreeValue     = "";
};
#endif
