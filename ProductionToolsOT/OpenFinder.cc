#if defined(__TCUSB__) && defined(__USE_ROOT__)
#include "OpenFinder.h"
#include "../Utils/ThresholdAndNoise.h"
#include "CBCChannelGroupHandler.h"
#include "ContainerFactory.h"
#include "D19cTriggerInterface.h"
#include "DataContainer.h"
#include "Occupancy.h"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

OpenFinder::OpenFinder() : OTTool()
{
    fParameters.fAntennaTriggerSource = 7;
    fParameters.antennaDelay          = 50;
    fParameters.potentiometer         = this->findValueInSettings<double>("AntennaPotentiometer");
    fParameters.nTriggers             = this->findValueInSettings<double>("Nevents");
}

void OpenFinder::ReadAntennaVoltage()
{
#if defined(__TCUSB__)
    TC_PSFE            cTC_PSFE(fUsbBus, fUsbDev);
    std::vector<float> cMeasurements(fNreadings, 0.);
    for(size_t cIndex = 0; cIndex < fNreadings; cIndex++)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(fVoltageMeasurementWait_ms));
        cTC_PSFE.adc_get(TC_PSFE::measurement::ANT_PULL, cMeasurements[cIndex]);
    }
    fAtennaVoltageMeasurement = this->getStats(cMeasurements);
#endif
}

OpenFinder::~OpenFinder() {}
void OpenFinder::Reset()
{
    // set everything back to original values .. like I wasn't here
    for(auto cBoard: *fDetectorContainer)
    {
        BeBoard* theBoard = static_cast<BeBoard*>(cBoard);
        LOG(INFO) << BOLDBLUE << "Resetting all registers on back-end board " << +cBoard->getId() << RESET;
        auto&                                         cBeRegMap = fBoardRegContainer.at(cBoard->getIndex())->getSummary<BeBoardRegMap>();
        std::vector<std::pair<std::string, uint32_t>> cVecBeBoardRegs;
        cVecBeBoardRegs.clear();
        for(auto cReg: cBeRegMap) cVecBeBoardRegs.push_back(make_pair(cReg.first, cReg.second));
        fBeBoardInterface->WriteBoardMultReg(theBoard, cVecBeBoardRegs);

        auto& cRegMapThisBoard = fRegMapContainer.at(cBoard->getIndex());

        for(auto cOpticalGroup: *cBoard)
        {
            auto& cRegMapThisOpticalGroup = cRegMapThisBoard->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cRegMapThisHybrid = cRegMapThisOpticalGroup->at(cHybrid->getIndex());
                LOG(INFO) << BOLDBLUE << "Resetting all registers on readout chips connected to FEhybrid#" << (cHybrid->getId()) << " back to their original values..." << RESET;
                for(auto cChip: *cHybrid)
                {
                    auto&                                         cRegMapThisChip = cRegMapThisHybrid->at(cChip->getIndex())->getSummary<ChipRegMap>();
                    std::vector<std::pair<std::string, uint16_t>> cVecRegisters;
                    cVecRegisters.clear();
                    for(auto cReg: cRegMapThisChip) cVecRegisters.push_back(make_pair(cReg.first, cReg.second.fValue));
                    fReadoutChipInterface->WriteChipMultReg(static_cast<ReadoutChip*>(cChip), cVecRegisters);
                }
            }
        }
    }
    resetPointers();
}
void OpenFinder::ConfigureInjection()
{
    fParameters.antennaGroup  = findValueInSettings<double>("AntennaGroup", 0);
    fParameters.antennaDelay  = findValueInSettings<double>("AntennaDelay", 100);
    fParameters.potentiometer = findValueInSettings<double>("AntennaPotentiometer", 0x265);
    fParameters.latencyRange  = findValueInSettings<double>("AntennaLatencyRange", 10);
}
void OpenFinder::ConfigureInjection(Parameters pParameters) { fParameters = pParameters; }
void OpenFinder::Initialise()
{
    Prepare();

    LOG(INFO) << "With SSA set to " << ((fWithSSA) ? 1 : 0) << RESET;
    if(fWithCBC)
    {
        CBCChannelGroupHandler theChannelGroupHandler;
        theChannelGroupHandler.setChannelGroupParameters(16, 2); // 16*2*8
        setChannelGroupHandler(theChannelGroupHandler);
    }
    if(fWithSSA)
    {
        // SSAChannelGroupHandler theChannelGroupHandler;
        // theChannelGroupHandler.setChannelGroupParameters(1, NSSACHANNELS); // 16*2*8
        // setChannelGroupHandler(theChannelGroupHandler, FrontEndType::SSA2);
        for(auto cBoard: *fDetectorContainer)
        {
            auto cFrontEndTypes = cBoard->connectedFrontEndTypes();
            for(auto cFrontEndType: cFrontEndTypes)
            {
                uint16_t cRowsToSkip        = 1;
                uint16_t cColsToSkip        = 2;
                size_t   cNClustersPerGroup = NSSACHANNELS / (cRowsToSkip * cColsToSkip);
                // // retrieve injection config
                // for(auto opticalGroup: *cBoard)
                // {
                //     for(auto hybrid: *opticalGroup)
                //     {
                //         for(auto chip: *hybrid)
                //         {
                //             if(chip->getFrontEndType() != cFrontEndType || cNClustersPerGroup != 0) continue;
                //             auto cDefInjection = chip->getDefInj();
                //             cRowsToSkip        = cDefInjection.first;
                //             cColsToSkip        = cDefInjection.second;
                //             cNClustersPerGroup = NSSACHANNELS / (cRowsToSkip * cColsToSkip);
                //         } // chips
                //     }     // hybrids
                // }         // links
                SSAChannelGroupHandler theChannelGroupHandler;
                theChannelGroupHandler.setChannelGroupParameters(cNClustersPerGroup, 1);
                setChannelGroupHandler(theChannelGroupHandler, cFrontEndType);
                LOG(INFO) << BOLDBLUE << "OpenFinder with SSAs : Inject every " << +cRowsToSkip << " row(s). " << RESET;
            }
        }
    }
    // Read some settings from the map
    auto cSetting       = fSettingsMap.find("Nevents");
    fEventsPerPoint     = (cSetting != std::end(fSettingsMap)) ? boost::any_cast<double>(cSetting->second) * 1 : 100;
    cSetting            = fSettingsMap.find("TestPulseAmplitude");
    fTestPulseAmplitude = (cSetting != std::end(fSettingsMap)) ? boost::any_cast<double>(cSetting->second) : 0;
    // Set fTestPulse based on the test pulse amplitude
    fTestPulse = (fTestPulseAmplitude & 0x1);
    // Import the rest of parameters from the user settings

    // set the antenna switch min and max values
    int cAntennaSwitchMinValue = (fParameters.antennaGroup > 0) ? fParameters.antennaGroup : 1;
    int cAntennaSwitchMaxValue = (fParameters.antennaGroup > 0) ? (fParameters.antennaGroup + 1) : 5;

    // prepare container
    ContainerFactory::copyAndInitStructure<ChannelList>(*fDetectorContainer, fOpens);

    // retreive original settings for all chips and all back-end boards
    ContainerFactory::copyAndInitStructure<ChipRegMap>(*fDetectorContainer, fRegMapContainer);
    ContainerFactory::copyAndInitStructure<BeBoardRegMap>(*fDetectorContainer, fBoardRegContainer);
    ContainerFactory::copyAndInitStructure<ScanSummaries>(*fDetectorContainer, fInTimeOccupancy);
    for(auto cBoard: *fDetectorContainer)
    {
        fBoardRegContainer.at(cBoard->getIndex())->getSummary<BeBoardRegMap>() = static_cast<BeBoard*>(cBoard)->getBeBoardRegMap();
        auto& cRegMapThisBoard                                                 = fRegMapContainer.at(cBoard->getIndex());
        auto& cOpens                                                           = fOpens.at(cBoard->getIndex());
        auto& cOccupancy                                                       = fInTimeOccupancy.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cOpensOpticalGroup      = cOpens->at(cOpticalGroup->getIndex());
            auto& cOccupancyOpticalGroup  = cOccupancy->at(cOpticalGroup->getIndex());
            auto& cRegMapThisOpticalGroup = cRegMapThisBoard->at(cOpticalGroup->getIndex());

            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cOpensHybrid      = cOpensOpticalGroup->at(cHybrid->getIndex());
                auto& cRegMapThisHybrid = cRegMapThisOpticalGroup->at(cHybrid->getIndex());
                auto& cOccupancyHybrid  = cOccupancyOpticalGroup->at(cHybrid->getIndex());
                for(auto cChip: *cHybrid)
                {
                    cOpensHybrid->at(cChip->getIndex())->getSummary<ChannelList>().clear();
                    cRegMapThisHybrid->at(cChip->getIndex())->getSummary<ChipRegMap>() = static_cast<ReadoutChip*>(cChip)->getRegMap();
                    auto& cThisOcc                                                     = cOccupancyHybrid->at(cChip->getIndex())->getSummary<ScanSummaries>();
                    for(int cAntennaPosition = cAntennaSwitchMinValue; cAntennaPosition < cAntennaSwitchMaxValue; cAntennaPosition++)
                    {
                        ScanSummary cSummary;
                        cSummary.first  = 0;
                        cSummary.second = 0;
                        cThisOcc.push_back(cSummary);
                    }
                }
            }
        }
    }

    for(auto cBoard: *fDetectorContainer)
    {
        if(!fWithSSA) continue;
        cBoard->setEventType(EventType::PSAS);
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid) { fReadoutChipInterface->WriteChipReg(cChip, "AnalogueAsync", 1); }
            }
        }
    }

#ifdef __USE_ROOT__
    fDQMHistogramOpenFinder.book(fResultFile, *fDetectorContainer, fSettingsMap);
#endif
}
// Antenna map generator by Sarah (used to be in Tools.cc)
// TO-DO - generalize for other hybrids
// I think this is really the only thing that needs to change between
// 2S and PS
OpenFinder::antennaChannelsMap OpenFinder::returnAntennaMap()
{
    antennaChannelsMap cAntennaMap;
    for(int cAntennaSwitch = 1; cAntennaSwitch < 5; cAntennaSwitch++)
    {
        std::vector<int> cOffsets(2);
        if((cAntennaSwitch - 1) % 2 == 0)
        {
            cOffsets[0] = 2 + (cAntennaSwitch < 2);
            cOffsets[1] = 0 + (cAntennaSwitch < 2);
        }
        else
        {
            cOffsets[0] = 0 + (cAntennaSwitch < 2);
            cOffsets[1] = 2 + (cAntennaSwitch < 2);
        }
        cbcChannelsMap cTmpMap;
        for(int cCbc = 0; cCbc < 8; cCbc++)
        {
            int           cOffset = cOffsets[(cCbc % 2)];
            channelVector cTmpList;
            cTmpList.clear();
            std::string channels = " ";
            for(unsigned int cChannel = (unsigned int)cOffset; cChannel < 254; cChannel += 4) { cTmpList.push_back(cChannel); }
            cTmpMap.emplace(cCbc, cTmpList);
            for(auto cChannel: cTmpList)
            {
                if(cChannel <= 20) channels += ", " + std::to_string(cChannel);
            }
            // LOG(INFO) << "CBC " << +cCbc << RESET;
            // LOG(INFO) << channels << RESET;
            // LOG(INFO) << "End " << cTmpList.end() << RESET;
        }
        cAntennaMap.emplace(cAntennaSwitch, cTmpMap);
    }
    return cAntennaMap;

    // antennaChannelsMap cAntennaMap;
    // for(int cAntennaSwitch = 1; cAntennaSwitch < 5; cAntennaSwitch++)
    // {
    //     std::vector<int> cOffsets(2);
    //     if((cAntennaSwitch - 1) % 2 == 0)
    //     {
    //         cOffsets[0] = 0 + (cAntennaSwitch > 2);
    //         cOffsets[1] = 2 + (cAntennaSwitch > 2);
    //     }
    //     else
    //     {
    //         cOffsets[0] = 2 + (cAntennaSwitch > 2);
    //         cOffsets[1] = 0 + (cAntennaSwitch > 2);
    //     }
    //     cbcChannelsMap cTmpMap;
    //     for(int cCbc = 0; cCbc < 8; cCbc++)
    //     {
    //         int           cOffset = cOffsets[(cCbc % 2)];
    //         channelVector cTmpList;
    //         cTmpList.clear();
    //         for(int cChannel = cOffset; cChannel < 254; cChannel += 4) { cTmpList.push_back(cChannel); }
    //         cTmpMap.emplace(cCbc, cTmpList);
    //     }
    //     cAntennaMap.emplace(cAntennaSwitch, cTmpMap);
    // }
    // return cAntennaMap;
}

bool OpenFinder::FindLatency(BeBoard* pBoard, std::vector<uint16_t> pLatencies)
{
    LOG(INFO) << BOLDBLUE << "Scanning latency to find charge injected by antenna in time ..." << RESET;
    // Preparing the antenna map, the list of opens and the hit counter
    auto  cAntennaMap            = returnAntennaMap();
    int   cAntennaSwitchMinValue = (fParameters.antennaGroup > 0) ? fParameters.antennaGroup : 1;
    auto  cBeBoard               = static_cast<BeBoard*>(pBoard);
    auto& cSummaryThisBoard      = fInTimeOccupancy.at(pBoard->getIndex());
    auto  cSearchAntennaMap      = cAntennaMap.find(fAntennaPosition);
    // scan latency and record optimal latency
    for(auto cLatency: pLatencies)
    {
        setSameDacBeBoard(static_cast<BeBoard*>(cBeBoard), "TriggerLatency", cLatency);
        fBeBoardInterface->ChipReSync(cBeBoard); // NEED THIS! ??
        this->ReadNEvents(cBeBoard, fEventsPerPoint);
        const std::vector<Event*>& cEvents = this->GetEvents();
        for(auto cOpticalGroup: *pBoard)
        {
            auto& cSummaryThisOpticalGroup = cSummaryThisBoard->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto&  cSummaryThisHybrid = cSummaryThisOpticalGroup->at(cHybrid->getIndex());
                float  cHybridOccupancy   = 0;
                size_t cNConnectedChnls   = 0;
                for(auto cChip: *cHybrid)
                {
                    auto& cSummaryThisChip = cSummaryThisHybrid->at(cChip->getIndex());
                    auto& cSummary         = cSummaryThisChip->getSummary<ScanSummaries>()[fAntennaPosition - cAntennaSwitchMinValue];

                    auto cConnectedChannels = cSearchAntennaMap->second.find((int)cChip->getId())->second;
                    cNConnectedChnls += cConnectedChannels.size();
                    uint32_t cOccupancy = 0;
                    for(auto cEvent: cEvents)
                    {
                        auto cHits = cEvent->GetHits(cHybrid->getId(), cChip->getId());
                        for(auto cHit: cHits)
                        {
                            if(std::find(cConnectedChannels.begin(), cConnectedChannels.end(), cHit) != cConnectedChannels.end()) { cOccupancy++; }
                        }
                    }
                    cHybridOccupancy += cOccupancy;
                    float cEventOccupancy = cOccupancy / static_cast<float>(fEventsPerPoint * cConnectedChannels.size());
                    if(cEventOccupancy >= cSummary.second)
                    {
                        cSummary.first  = cLatency;
                        cSummary.second = cEventOccupancy;
                    }
                }
                LOG(INFO) << BOLDBLUE << "L1A latency set to " << +cLatency << "\t..On average " << cHybridOccupancy / (fEventsPerPoint) << " hits/event readout from Hybrid#" << +cHybrid->getId()
                          << "\t.. " << cNConnectedChnls << " channels connected to this AG" << RESET;
            }
        }
    }

    // set optimal latency for each chip
    bool cFailed = false;
    for(auto cOpticalGroup: *pBoard)
    {
        auto& cSummaryThisOpticalGroup = cSummaryThisBoard->at(cOpticalGroup->getIndex());
        for(auto cHybrid: *cOpticalGroup)
        {
            auto& cSummaryThisHybrid = cSummaryThisOpticalGroup->at(cHybrid->getIndex());
            for(auto cChip: *cHybrid)
            {
                auto& cSummaryThisChip = cSummaryThisHybrid->at(cChip->getIndex())->getSummary<ScanSummaries>()[fAntennaPosition - cAntennaSwitchMinValue];
                auto  cReadoutChip     = static_cast<ReadoutChip*>(cChip);
                if(cSummaryThisChip.second == 0)
                {
                    LOG(INFO) << BOLDRED << "FAILED to find optimal latency "
                              << " for chip " << +cChip->getId() << " hit occupancy " << cSummaryThisChip.second << RESET;
                    cFailed = (cFailed || true);
                    continue;
                }
                fReadoutChipInterface->WriteChipReg(cReadoutChip, "TriggerLatency", cSummaryThisChip.first);
                LOG(INFO) << BOLDBLUE << "Optimal latency "
                          << " for chip " << +cChip->getId() << " was " << cSummaryThisChip.first << " hit occupancy " << cSummaryThisChip.second << RESET;
            }
        }
    }
    if(!cFailed) fBeBoardInterface->ChipReSync(pBoard);

    return !cFailed;
}

void OpenFinder::CountOpens(BeBoard* pBoard)
{
    // Preparing the antenna map, the list of opens and the hit counter
    auto cAntennaMap       = returnAntennaMap();
    auto cBeBoard          = static_cast<BeBoard*>(pBoard);
    auto cSearchAntennaMap = cAntennaMap.find(fAntennaPosition);
    // scan latency and record optimal latency
    this->ReadNEvents(cBeBoard, fEventsPerPoint);
    const std::vector<Event*>& cEvents = this->GetEvents();
    // summarize information per channel
    DetectorDataContainer cMeasurement;
    ContainerFactory::copyAndInitStructure<int>(*fDetectorContainer, cMeasurement);
    auto& cSummaryThisBoard = cMeasurement.at(pBoard->getIndex());
    for(auto cEvent: cEvents)
    {
        size_t cMissingHits = 0;
        for(auto cOpticalGroup: *pBoard)
        {
            auto& cSummaryThisOpticalGroup = cSummaryThisBoard->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cSummaryThisHybrid = cSummaryThisOpticalGroup->at(cHybrid->getIndex());
                for(auto cChip: *cHybrid)
                {
                    auto  cConnectedChannels = cSearchAntennaMap->second.find((int)cChip->getId())->second;
                    auto& cSummaryThisChip   = cSummaryThisHybrid->at(cChip->getIndex());
                    auto  cHits              = cEvent->GetHits(cHybrid->getId(), cChip->getId());
                    for(auto cConnectedChannel: cConnectedChannels)
                    {
                        if(std::find(cHits.begin(), cHits.end(), cConnectedChannel) == cHits.end())
                        {
                            cSummaryThisChip->getChannelContainer<int>()->at(cConnectedChannel) += 1;
                            // LOG (INFO) << BOLDYELLOW << " Hit missing from Chnl#" << cConnectedChannel << RESET;
                            cMissingHits++;
                        }
                    } // channels connected
                }     // CBCs
            }         // hybrids
        }             // OGs
    }                 // events

    auto& cOpens = fOpens.at(pBoard->getIndex());
    for(auto cOpticalGroup: *pBoard)
    {
        auto& cOpensThisOpticalGroup   = cOpens->at(cOpticalGroup->getIndex());
        auto& cSummaryThisOpticalGroup = cSummaryThisBoard->at(cOpticalGroup->getIndex());
        for(auto cHybrid: *cOpticalGroup)
        {
            auto& cOpensThisHybrid   = cOpensThisOpticalGroup->at(cHybrid->getIndex());
            auto& cSummaryThisHybrid = cSummaryThisOpticalGroup->at(cHybrid->getIndex());
            for(auto cChip: *cHybrid)
            {
                auto              cConnectedChannels = cSearchAntennaMap->second.find((int)cChip->getId())->second;
                auto&             cSummaryThisChip   = cSummaryThisHybrid->at(cChip->getIndex());
                auto&             cOpensThisChip     = cOpensThisHybrid->at(cChip->getIndex())->getSummary<ChannelList>();
                std::stringstream cChipSummary;
                cChipSummary << "Chip#" << +cChip->getId() << " [ " << cConnectedChannels.size() << " channels in test group]" << RESET;
                std::stringstream cMissingHits;
                std::stringstream cPossibleOpens;
                for(auto cConnectedChannel: cConnectedChannels)
                {
                    if(cSummaryThisChip->getChannelContainer<int>()->at(cConnectedChannel) == 0) continue;

                    if(cSummaryThisChip->getChannelContainer<int>()->at(cConnectedChannel) >= THRESHOLD_OPEN * fEventsPerPoint)
                    {
                        cPossibleOpens << +cConnectedChannel << "[" << cSummaryThisChip->getChannelContainer<int>()->at(cConnectedChannel) << "], ";
                        cOpensThisChip.push_back(cConnectedChannel);
                    }
                    else
                        cMissingHits << +cConnectedChannel << "[" << cSummaryThisChip->getChannelContainer<int>()->at(cConnectedChannel) << "], ";
                } // channels
                if(cOpensThisChip.size() > 0)
                {
                    LOG(INFO) << BOLDRED << cChipSummary.str() << " -- Found " << cOpensThisChip.size() << " Possible Opens " << cPossibleOpens.str() << RESET;
                    if(cMissingHits.str().length() > 0) LOG(WARNING) << BOLDYELLOW << " Missing hits : " << cMissingHits.str() << RESET;
                }
                else if(cMissingHits.str() != "")
                    LOG(WARNING) << BOLDYELLOW << cChipSummary.str() << " -- Found missing hits : " << cMissingHits.str() << RESET;
                else
                    LOG(INFO) << BOLDGREEN << cChipSummary.str() << " -- nothing of interest to report" << RESET;
            } // chips
        }     // hybrids
    }         // OGs

    /*auto& cOpens            = fOpens.at(pBoard->getIndex());
    for(auto cOpticalGroup: *pBoard)
    {
        auto& cOpensThisOpticalGroup   = cOpens->at(cOpticalGroup->getIndex());
        auto& cSummaryThisOpticalGroup = cSummaryThisBoard->at(cOpticalGroup->getIndex());
        for(auto cHybrid: *cOpticalGroup)
        {
            auto& cOpensThisHybrid   = cOpensThisOpticalGroup->at(cHybrid->getIndex());
            auto& cSummaryThisHybrid = cSummaryThisOpticalGroup->at(cHybrid->getIndex());
            for(auto cChip: *cHybrid)
            {
                auto  cConnectedChannels = cSearchAntennaMap->second.find((int)cChip->getId())->second;
                auto& cSummaryThisChip   = cSummaryThisHybrid->at(cChip->getIndex());
                for(auto cEvent: cEvents)
                {
                    auto cHits = cEvent->GetHits(cHybrid->getId(), cChip->getId());
                    for(auto cConnectedChannel: cConnectedChannels)
                    {
                        if(std::find(cHits.begin(), cHits.end(), cConnectedChannel) == cHits.end()) {
                            LOG (INFO) << BOLDRED << "Event#" << +cEvent->GetEventCount() << "\t..FE#" << +cChip->getId() << "\t..Chnl#" << +cConnectedChannel << RESET;
                            cSummaryThisChip->getChannelContainer<Occupancy>()->at(cConnectedChannel).fOccupancy += 1;
                        }
                    }
                }

                auto& cOpensThisChip = cOpensThisHybrid->at(cChip->getIndex())->getSummary<ChannelList>();
                for(auto cConnectedChannel: cConnectedChannels)
                {
                    if(cSummaryThisChip->getChannelContainer<Occupancy>()->at(cConnectedChannel).fOccupancy > THRESHOLD_OPEN * fEventsPerPoint)
                    {
                        cOpensThisChip.push_back(cConnectedChannel);
                        LOG(INFO) << BOLDRED << "Possible open found.."
                                   << " readout chip " << +cChip->getId() << " channel " << +cConnectedChannel << RESET;
                    }
                }
            }
        }
    }*/
}

void OpenFinder::Print()
{
    for(auto cBoard: *fDetectorContainer)
    {
        auto& cOpens = fOpens.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            // create TTree for opens: opensTree
            auto OpensTree = new TTree("Opens", "Open channels in the hybrid");
            // create int for all opens count
            int32_t totalOpens = 0;
            // create variables for TTree branches
            int              nCBC = -1;
            std::vector<int> openChannels;
            // create branches
            OpensTree->Branch("CBC", &nCBC);
            OpensTree->Branch("Channels", &openChannels);

            auto& cOpensThisOpticalGroup = cOpens->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cOpensThisHybrid = cOpensThisOpticalGroup->at(cOpticalGroup->getIndex());
                for(auto cChip: *cHybrid)
                {
                    auto& cOpensThisChip = cOpensThisHybrid->at(cChip->getIndex())->getSummary<ChannelList>();

                    // empty openChannels vector
                    openChannels.clear();

                    if(cOpensThisChip.size() == 0)
                    {
                        LOG(INFO) << BOLDGREEN << "No opens found "
                                  << "on readout chip " << +cChip->getId() << " hybrid " << +cHybrid->getId() << RESET;
                    }
                    else
                    {
                        LOG(INFO) << BOLDRED << "Found " << +cOpensThisChip.size() << " opens in readout chip" << +cChip->getId() << " on FE hybrid " << +cHybrid->getId() << RESET;
                        // add number of opens to total opens counts
                        totalOpens += cOpensThisChip.size();
                    }
                    for(auto cOpenChannel: cOpensThisChip)
                    {
                        LOG(DEBUG) << BOLDRED << "Possible open found.."
                                   << " readout chip " << +cChip->getId() << " channel " << +cOpenChannel << RESET;
                        // store opens on opensChannels vector
                        openChannels.push_back(cOpenChannel);
                    }
                    // store chip opens on OpensTree
                    nCBC = cChip->getId() + 1;
                    OpensTree->Fill();
                }
            }
            fillSummaryTree("nOpens", totalOpens);
            fResultFile->cd();
            OpensTree->Write();
            if(totalOpens == 0) { gDirectory->Delete("Opens;*"); }
        }
    }
}

void OpenFinder::FindOpens2S()
{
    // Set the right threshold - based on the noise measurements
    uint16_t cThreshold;
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    if(cChip->getFrontEndType() == FrontEndType::CBC3)
                    {
                        double cPedeMean   = getSummaryParameter(Form("AvgPedeCBC%d", cChip->getId()));
                        double cPedeStdDev = getSummaryParameter(Form("StDvPedeCBC%d", cChip->getId()));
                        LOG(INFO) << "Mean:" << cPedeMean << RESET;
                        LOG(INFO) << "StdDev:" << cPedeStdDev << RESET;

                        if(cPedeMean != -1.0)
                        {
                            if((cPedeMean + 15 * cPedeStdDev) <= 1023) { cThreshold = (uint16_t)(cPedeMean + 3 * cPedeStdDev); }
                            LOG(INFO) << BOLDBLUE << "Threshold from measurements " << cThreshold << RESET;
                        }
                        // cThreshold = 50;
                        LOG(INFO) << BOLDBLUE << "Threshold to be set  " << cThreshold << RESET;
                        std::string tmpParameter = "thresholdForOpens_" + std::to_string(cChip->getId());
#ifdef __USE_ROOT__
                        fillSummaryTree(tmpParameter, cThreshold);
#endif

                        /*std::string cHistName  = Form("AntennaOccupancy_Even_%d", cChip->getId());

                        if ( gROOT->FindObject(cHistName.c_str()) != nullptr )
                            cHistName  = Form("%s_%s", cHistName.c_str(), "II" );

                        std::string cHistTitle = Form("Occupancy in Even Channels in Chip %d", cChip->getId());

                        TH2F*       fOccupancyHistEven =
                            new TH2F(cHistName.c_str(), cHistTitle.c_str(), (antennaPullupHighEnd - antennaPullupLowEnd), antennaPullupLowEnd -0.5, antennaPullupHighEnd+0.5, 120, -0.5,
                            120+0.5);

                        cHistName  = Form("AntennaOccupancy_Odd_%d", cChip->getId());

                        if ( gROOT->FindObject(cHistName.c_str()) != nullptr )
                            cHistName  = Form("%s_%s", cHistName.c_str(), "II" );

                        cHistTitle = Form("Occupancy in Odd Channels in Chip %d", cChip->getId());

                        TH2F* fOccupancyHistOdd =
                            new TH2F(cHistName.c_str(), cHistTitle.c_str(), (antennaPullupHighEnd - antennaPullupLowEnd), antennaPullupLowEnd-0.5, antennaPullupHighEnd+0.5, 120, -0.5, 120+0.5);

                        fOccupancyHistVect.push_back(fOccupancyHistEven);
                        fOccupancyHistVect.push_back(fOccupancyHistOdd);

                        finalAntennaEven.push_back(0);
                        finalAntennaOdd.push_back(0);*/

                        // fReadoutChipInterface->WriteChipReg(cChip, "AnalogueAsync", 1);
                        fReadoutChipInterface->WriteChipReg(cChip, "Threshold", cThreshold);
                        fReadoutChipInterface->WriteChipReg(cChip, "InjectedCharge", 0);
                        // fReadoutChipInterface->WriteChipReg(cChip, "SAMPLINGMODE", 0);
                    } // Chip type

                } // chip loop
            }     // hybrid loop
        }         // optical group
    }

    // TC_2SFE_V2 Object here
    float      adc_result;
    TC_2SFE_V2 cTC_2SFE(fUsbBus, fUsbDev);
    cTC_2SFE.adc_get(TC_2SFE_V2::measurement::_1V25_HYB, adc_result);
    LOG(INFO) << "Hybrid 1V25: " << +adc_result << RESET;
    // Configure bias for antenna pull-up
    fParameters.potentiometer = this->findValueInSettings<double>("AntennaPotentiometer");

    // Set the user trigger frequency
    uint16_t cTriggerRate = 30; // User trigger frequency
    // Related to the frequency of the antenna trigger (50% duty cycle) by  --->  antenna_trigger_period = 40000/user_trigger_frequency
    // We want at least 520 clock cycles (40MHz clock) to allow for the antenna circuit to charge up again
    // Set to 1200

    // Set the antenna delay and compute the corresponding latency start and stop
    // fParameters.antennaDelay                = 30; // Delay after antenna
    auto                  cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cTriggerInterface* cTriggerInterface = dynamic_cast<D19cTriggerInterface*>(cInterface->getTriggerInterface());
    TriggerConfiguration  cCnfg;
    cCnfg.fTriggerSource = 7;
    cCnfg.fTriggerRate   = cTriggerRate;
    // cCnfg.fNtriggersToAccept = fEventsPerPoint;
    cCnfg.fNtriggersToAccept = 1000;
    cTriggerInterface->ConfigureTriggerFSM(cCnfg);
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        std::vector<std::pair<std::string, uint32_t>> cRegVec;
        cRegVec.clear();
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.antenna_trigger_delay_value", fParameters.antennaDelay});
        cRegVec.push_back({"fc7_daq_ctrl.fast_command_block.control.load_config", 0x1});
        fBeBoardInterface->WriteBoardMultReg(cBoard, cRegVec);
    }
    uint16_t cStart = fParameters.antennaDelay - 1;
    uint16_t cStop  = fParameters.antennaDelay + (fParameters.latencyRange) + 1;
    LOG(INFO) << BOLDBLUE << "Antenna delay set to " << +fParameters.antennaDelay << " .. will scan L1 latency between " << +cStart << " and " << +cStop << RESET;
    // Loop over the antenna groups
    // cAntenna.TurnOnAnalogSwitchChannel(9); // Turning off the antenna
    cTC_2SFE.adc_get(cTC_2SFE.ANT_PULL, adc_result);
    LOG(INFO) << "Antenna pullup: " << +adc_result << RESET;
    cTC_2SFE.antenna_fc7(512, cTC_2SFE.NONE);
    cTC_2SFE.adc_get(cTC_2SFE.ANT_PULL, adc_result);
    LOG(INFO) << "Antenna pullup: " << +adc_result << RESET;
    cTC_2SFE.adc_get(TC_2SFE_V2::measurement::_1V25_HYB, adc_result);
    LOG(INFO) << "Hybrid 1V25: " << +adc_result << RESET;

    // make sure all CBCs are in Sampled mode
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid) { static_cast<CbcInterface*>(fReadoutChipInterface)->selectLogicMode(cChip, "Sampled", true, true); }
            }
        }
    }
    // Latency range based on step 1
    std::vector<DetectorDataContainer*> cContainerVector;
    std::vector<uint16_t>               cListOfLatencies;
    for(int cLatency = cStart; cLatency < cStop; ++cLatency)
    {
        cListOfLatencies.push_back(cLatency);
        cContainerVector.emplace_back(new DetectorDataContainer());
        ContainerFactory::copyAndInitStructure<Occupancy>(*fDetectorContainer, *cContainerVector.back());
    }

    // for(auto cBoard: *fDetectorContainer)
    // {
    //     for(auto cOpticalGroup: *cBoard)
    //     {
    //         for(auto cHybrid: *cOpticalGroup)
    //         {
    //             for(auto cChip: *cHybrid)
    //             {
    //                 static_cast<CbcInterface*>(fReadoutChipInterface)->selectLogicMode(cChip,"Sampled",true,true);
    //             }
    //         }
    //     }
    // }//makre sure we're in sampled mode

    for(int ant_channel = cTC_2SFE._1; ant_channel != cTC_2SFE.ALL; ant_channel++)
    {
        fAntennaPosition = ant_channel;
        LOG(INFO) << BOLDBLUE << "Looking for opens using antenna channel " << +ant_channel << RESET;
        cTC_2SFE.antenna_fc7(fParameters.potentiometer, static_cast<TC_2SFE_V2::ant_channel>(ant_channel));
        float adc_result;
        cTC_2SFE.adc_get(cTC_2SFE.ANT_PULL, adc_result);
        LOG(INFO) << "Antenna pullup: " << +adc_result << RESET;
        for(auto cBoard: *fDetectorContainer)
        {
            auto cBeBoard = static_cast<BeBoard*>(cBoard);
            bool cSuccess = this->FindLatency(cBeBoard, cListOfLatencies);
            if(cSuccess)
                this->CountOpens(cBeBoard);
            else
                exit(FAILED_LATENCY);
        }

        // de-select all channels
        cTC_2SFE.antenna_fc7(511, cTC_2SFE.NONE);
    }
}
void OpenFinder::SelectAntennaPosition(const std::string& cPosition, uint16_t potentiometer)
{
    if(potentiometer != 0) fParameters.potentiometer = potentiometer;
#if defined(__TCUSB__)
    // std::cin.get();
    auto cMapIterator = fAntennaControl.find(cPosition);
    if(cMapIterator != fAntennaControl.end())
    {
        auto&   cChannel = cMapIterator->second;
        TC_PSFE cTC_PSFE(fUsbBus, fUsbDev);
        cTC_PSFE.antenna_fc7(fParameters.potentiometer, cChannel);
        // ReadAntennaVoltage();
        std::vector<float> cMeasurements(fNreadings, 0.);
        for(size_t cIndex = 0; cIndex < fNreadings; cIndex++)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(fVoltageMeasurementWait_ms));
            cTC_PSFE.adc_get(TC_PSFE::measurement::ANT_PULL, cMeasurements[cIndex]);
        }
        fAtennaVoltageMeasurement = this->getStats(cMeasurements);
        if(cPosition != "Disable")
            LOG(INFO) << BOLDBLUE << "Selected antenna channel to "
                      << " inject charge of " << fParameters.potentiometer << " DAC units in [ " << cPosition << ", " << cMapIterator->first << " ] position. This is switch position " << +cChannel
                      << " - antenna Pull-up measurement : " << fAtennaVoltageMeasurement.first << " ± " << fAtennaVoltageMeasurement.second << " mV." << RESET;
    }
    // std::this_thread::sleep_for(std::chrono::milliseconds(100));
    // std::cin.get();
#endif
}
void OpenFinder::BitwiseSearchChip(std::vector<uint8_t> pActiveGroups, std::string pDacName, int pNbits, float targetOccupancy)
{
    std::string dacName                = pDacName;
    size_t      numberOfEvents         = fParameters.nTriggers;
    int         numberOfEventsPerBurst = -1;
    // DetectorDataContainer* outputDataContainer    = fDetectorDataContainer;
    LOG(INFO) << BOLDBLUE << "Number of bits in this DAC is " << +pNbits << RESET;
    bool                   occupanyDirectlyProportionalToDAC;
    DetectorDataContainer* previousStepOccupancyContainer = new DetectorDataContainer();
    ContainerFactory::copyAndInitStructure<Occupancy>(*fDetectorContainer, *previousStepOccupancyContainer);
    DetectorDataContainer* currentStepOccupancyContainer = new DetectorDataContainer();
    ContainerFactory::copyAndInitStructure<Occupancy>(*fDetectorContainer, *currentStepOccupancyContainer);

    DetectorDataContainer* previousDacList = new DetectorDataContainer();
    DetectorDataContainer* currentDacList  = new DetectorDataContainer();

    uint16_t allZeroRegister = 0;
    uint16_t allOneRegister  = (0xFFFF >> (16 - pNbits));
    ContainerFactory::copyAndInitChip<uint16_t>(*fDetectorContainer, *previousDacList, allZeroRegister);
    ContainerFactory::copyAndInitChip<uint16_t>(*fDetectorContainer, *currentDacList, allOneRegister);
    LOG(INFO) << BOLDBLUE << "Setting all bits of register " << dacName << "  to  " << +allZeroRegister << RESET;

    for(auto cBoard: *fDetectorContainer)
    {
        auto boardIndex = cBoard->getIndex();
        setAllGlobalDacBeBoard(boardIndex, dacName, *previousDacList);
        fDetectorDataContainer = previousStepOccupancyContainer;
        LOG(INFO) << BOLDBLUE << "\t\t... measuring occupancy...." << RESET;
        measureBeBoardData(boardIndex, numberOfEvents, numberOfEventsPerBurst);
        LOG(INFO) << BOLDBLUE << "\t\t... " << previousStepOccupancyContainer->at(boardIndex)->getSummary<Occupancy, Occupancy>().fOccupancy << RESET;

        LOG(INFO) << BOLDBLUE << "Setting all bits of register " << dacName << "  to  " << +allOneRegister << RESET;
        setAllGlobalDacBeBoard(boardIndex, dacName, *currentDacList);
        fDetectorDataContainer = currentStepOccupancyContainer;
        LOG(INFO) << BOLDBLUE << "\t\t... measuring occupancy...." << RESET;
        measureBeBoardData(boardIndex, numberOfEvents, numberOfEventsPerBurst);
        LOG(INFO) << BOLDBLUE << "\t\t... " << currentStepOccupancyContainer->at(boardIndex)->getSummary<Occupancy, Occupancy>().fOccupancy << RESET;

        occupanyDirectlyProportionalToDAC = currentStepOccupancyContainer->at(boardIndex)->getSummary<Occupancy, Occupancy>().fOccupancy >
                                            previousStepOccupancyContainer->at(boardIndex)->getSummary<Occupancy, Occupancy>().fOccupancy;

        if(!occupanyDirectlyProportionalToDAC)
        {
            DetectorDataContainer* tmpPointer = previousDacList;
            previousDacList                   = currentDacList;
            currentDacList                    = tmpPointer;
        }

        for(int iBit = pNbits - 1; iBit >= 0; --iBit)
        {
            LOG(INFO) << BOLDBLUE << "Bit number " << +iBit << " of " << dacName << RESET;
            for(auto cOpticalGroup: *(fDetectorContainer->at(boardIndex)))
            {
                for(auto cHybrid: *cOpticalGroup)
                {
                    for(auto cChip: *cHybrid)
                    {
                        if(occupanyDirectlyProportionalToDAC)
                            currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() =
                                previousDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() + (1 << iBit);
                        else
                            currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() =
                                previousDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() & (0xFFFF - (1 << iBit));

                        LOG(DEBUG) << BOLDBLUE << "\t.. current setting is "
                                   << currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() << RESET;
                    }
                }
            }

            setAllGlobalDacBeBoard(boardIndex, dacName, *currentDacList);

            Occupancy noOccupancy;
            ContainerFactory::reinitializeContainer(currentStepOccupancyContainer, noOccupancy);
            fDetectorDataContainer = currentStepOccupancyContainer;
            measureBeBoardData(boardIndex, numberOfEvents, numberOfEventsPerBurst);
            // Determine if it is better or not
            for(auto cOpticalGroup: *(fDetectorContainer->at(boardIndex)))
            {
                for(auto cHybrid: *cOpticalGroup)
                {
                    for(auto cChip: *cHybrid)
                    {
                        std::stringstream cOut;
                        auto&             cCurrentDAC = currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>();

                        auto cChnlHndlr = getChannelGroupHandlerContainer()
                                              ->getObject(cBoard->getId())
                                              ->getObject(cOpticalGroup->getId())
                                              ->getObject(cHybrid->getId())
                                              ->getObject(cChip->getId())
                                              ->getSummary<std::shared_ptr<ChannelGroupHandler>>();

                        std::vector<float> cCurrentOcc;
                        for(int cGrpIndx = 0; cGrpIndx < cChnlHndlr->getNumberOfGroups(); cGrpIndx++)
                        {
                            if(std::find(pActiveGroups.begin(), pActiveGroups.end(), cGrpIndx) == pActiveGroups.end()) continue;

                            auto             cTestGroup = cChnlHndlr->getTestGroup(cGrpIndx);
                            std::bitset<120> cBitset    = std::bitset<120>(std::static_pointer_cast<const ChannelGroup<NSSACHANNELS>>(cTestGroup)->getBitset());
                            // LOG (INFO) << BOLDYELLOW << "Group#" << cGrpIndx << ":" << std::bitset<NSSACHANNELS>(cBitset) << RESET;
                            for(uint32_t cChnl = 0; cChnl < cChip->size(); cChnl++)
                            {
                                if(cBitset[cChnl] == 0) continue;

                                float& cOcc = currentStepOccupancyContainer->at(boardIndex)
                                                  ->at(cOpticalGroup->getIndex())
                                                  ->at(cHybrid->getIndex())
                                                  ->at(cChip->getIndex())
                                                  ->getChannel<Occupancy>(cChnl)
                                                  .fOccupancy;
                                cCurrentOcc.push_back(cOcc);
                            }
                        }
                        float cMeanOcc = std::accumulate(cCurrentOcc.begin(), cCurrentOcc.end(), 0.) / cCurrentOcc.size();
                        cOut << " Occupancy Chip#" << +cChip->getId() << "\t[ global] "
                             << " for a DAC value of " << cCurrentDAC << " number of channels is " << cCurrentOcc.size() << " mean occupancy for those channels is "
                             << std::accumulate(cCurrentOcc.begin(), cCurrentOcc.end(), 0.) / cCurrentOcc.size() << RESET;

                        if(cMeanOcc <= targetOccupancy)
                        {
                            previousDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() =
                                currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>();
                            previousStepOccupancyContainer->at(boardIndex)
                                ->at(cOpticalGroup->getIndex())
                                ->at(cHybrid->getIndex())
                                ->at(cChip->getIndex())
                                ->getSummary<Occupancy, Occupancy>()
                                .fOccupancy = cMeanOcc;
                        }
                        LOG(DEBUG) << BOLDYELLOW << cOut.str() << RESET;
                    }
                }
            }
        }

        setAllGlobalDacBeBoard(boardIndex, dacName, *previousDacList);
    }
    delete previousStepOccupancyContainer;
    delete currentStepOccupancyContainer;
    delete previousDacList;
    delete currentDacList;

    return;
}

void OpenFinder::ScanAntennaCharge(std::vector<uint16_t> pAntennaValues)
{
    size_t numberOfEvents = fParameters.nTriggers;
    LOG(INFO) << "OpenFinder::ScanAntennaCharge .... fParameters.nTriggers -> " << +numberOfEvents << RESET;
    int                  numberOfEventsPerBurst = -1;
    std::vector<uint8_t> cPositions{0, 1};

    std::vector<DetectorDataContainer*> cOccVector;
    fRecycleBin.setDetectorContainer(fDetectorContainer);
    for(auto cContainer: cOccVector) fRecycleBin.free(cContainer);
    cOccVector.clear();
    for(auto i = 0u; i < pAntennaValues.size(); i++) cOccVector.push_back(fRecycleBin.get(&ContainerFactory::copyAndInitStructure<Occupancy>, Occupancy()));
    size_t             cIndx = 0;
    std::vector<float> cAverageOccupancy(0);
    size_t             cIndxMax      = 0;
    float              cMaxOccupancy = 0;

    uint16_t cSelectedAntennaValue = 0;

    for(auto cAntennaValue: pAntennaValues)
    {
        LOG(INFO) << BOLDYELLOW << "Antenna injection value " << cAntennaValue << RESET;
        auto&              cDataContainer = cOccVector[cIndx];
        std::vector<float> cSummaryOcc(0);
        for(auto cPosition: cPositions)
        {
            cSelectedAntennaValue    = cAntennaValue;
            std::string          chn = (cPosition == 0) ? "EvenChannels" : "OddChannels";
            std::vector<uint8_t> cActiveGroups{(uint8_t)(cPosition)};
            // std::vector<uint8_t> cActiveGroups{(uint8_t)(1 - cPosition)};

            // if (chn == "OddChannels")
            //     cSelectedAntennaValue = cAntennaValue + 150;
            SelectAntennaPosition(chn, cSelectedAntennaValue);
            DetectorDataContainer* cOccupancyContainer = new DetectorDataContainer();
            ContainerFactory::copyAndInitStructure<Occupancy>(*fDetectorContainer, *cOccupancyContainer);
            fDetectorDataContainer = cOccupancyContainer;
            for(auto cBoard: *fDetectorContainer)
            {
                auto boardIndex = cBoard->getIndex();

                LOG(INFO) << "numberOfEvents  " << numberOfEvents;

                measureBeBoardData(boardIndex, numberOfEvents, numberOfEventsPerBurst);

                auto ntriggers = fBeBoardInterface->ReadBoardReg(cBoard, "fc7_daq_cnfg.fast_command_block.triggers_to_accept");
                LOG(INFO) << "triggers_to_accept  " << ntriggers;

                for(auto cOpticalGroup: *cBoard)
                {
                    for(auto cHybrid: *cOpticalGroup)
                    {
                        for(auto cChip: *cHybrid)
                        {
                            std::stringstream cOut;
                            auto              cChnlHndlr = getChannelGroupHandlerContainer()
                                                  ->getObject(cBoard->getId())
                                                  ->getObject(cOpticalGroup->getId())
                                                  ->getObject(cHybrid->getId())
                                                  ->getObject(cChip->getId())
                                                  ->getSummary<std::shared_ptr<ChannelGroupHandler>>();

                            std::vector<float> cCurrentOcc;
                            for(int cGrpIndx = 0; cGrpIndx < cChnlHndlr->getNumberOfGroups(); cGrpIndx++)
                            {
                                if(std::find(cActiveGroups.begin(), cActiveGroups.end(), cGrpIndx) == cActiveGroups.end()) continue;

                                // auto             cTestGroup = cChnlHndlr->getTestGroup(cGrpIndx);
                                // std::bitset<120> cBitset    = std::bitset<120>(std::static_pointer_cast<const ChannelGroup<120>>(cTestGroup)->getBitset());
                                for(uint32_t cChnl = 0; cChnl < cChip->size(); cChnl++)
                                {
                                    // if(cBitset[cChnl] == 0){
                                    //     continue;
                                    // }
                                    if(cPosition == 0 && cChnl % 2 != 0) // if even channels selected, skip odd channels
                                        continue;
                                    else if(cPosition != 0 && cChnl % 2 == 0) // if odd channels selected, skip even channels
                                        continue;
                                    float& cOcc = fDetectorDataContainer->getObject(cBoard->getId())
                                                      ->getObject(cOpticalGroup->getId())
                                                      ->getObject(cHybrid->getId())
                                                      ->getObject(cChip->getId())
                                                      ->getChannel<Occupancy>(cChnl)
                                                      .fOccupancy;
                                    cDataContainer->getObject(cBoard->getId())
                                        ->getObject(cOpticalGroup->getId())
                                        ->getObject(cHybrid->getId())
                                        ->getObject(cChip->getId())
                                        ->getChannel<Occupancy>(cChnl)
                                        .fOccupancy = cOcc;
                                    cCurrentOcc.push_back(cOcc);
                                    cSummaryOcc.push_back(cOcc);
                                    LOG(INFO) << "cOcc on cChnl# " << +cChnl << " ->" << cOcc << RESET;
                                }
                            }
                            float cMeanOcc = std::accumulate(cCurrentOcc.begin(), cCurrentOcc.end(), 0.) / cCurrentOcc.size();
                            cOut << "\t..Antenna Position " << +cPosition << " Occupancy Chip#" << +cChip->getId() << "\t[ global] "
                                 << " number of channels is " << cCurrentOcc.size() << " mean occupancy for those channels is " << cMeanOcc << RESET;
                            LOG(INFO) << BOLDYELLOW << cOut.str() << RESET;
                        } // chips
                    }     // hybrids
                }         // optical groups
            }             // boards
            delete cOccupancyContainer;
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
            SelectAntennaPosition("Disable");
        } // antenna positions
#ifdef __USE_ROOT__
        fDQMHistogramOpenFinder.fillSCurvePlots(cSelectedAntennaValue, *cDataContainer);
#endif
        auto cAverageOcc = std::accumulate(cSummaryOcc.begin(), cSummaryOcc.end(), 0.) / cSummaryOcc.size();
        if(cAverageOcc >= cMaxOccupancy)
        {
            cMaxOccupancy = cAverageOcc;
            cIndxMax      = cIndx;
        }
        LOG(INFO) << BOLDBLUE << "Summary Occupancy " << cAverageOcc << RESET;
        cIndx++;
    } // injected charge

    // find injected charge with maximum occupancy
    LOG(INFO) << BOLDYELLOW << "Injected charge at which maximum occupancy is achieved " << pAntennaValues[cIndxMax] << RESET;
    auto& cDataContainer = cOccVector[cIndxMax];
    for(auto cBoard: *fDetectorContainer)
    {
        auto cOpens        = fOpens.getObject(cBoard->getId());
        auto cOccThisBoard = cDataContainer->getObject(cBoard->getId());
        for(auto cOpticalGroup: *cBoard)
        {
            auto cOpensThisOpticalGroup   = cOpens->getObject(cOpticalGroup->getId());
            auto cSummaryThisOpticalGroup = cOccThisBoard->getObject(cOpticalGroup->getId());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto cOpensThisHybrid   = cOpensThisOpticalGroup->getObject(cHybrid->getId());
                auto cSummaryThisHybrid = cSummaryThisOpticalGroup->getObject(cHybrid->getId());
                for(auto cChip: *cHybrid)
                {
                    auto              cSummaryThisChip = cSummaryThisHybrid->getObject(cChip->getId());
                    auto              cOpensThisChip   = cOpensThisHybrid->getObject(cChip->getId())->getSummary<ChannelList>();
                    std::stringstream cChipSummary;
                    cChipSummary << "Chip#" << +cChip->getId() << RESET;
                    std::stringstream cMissingHits;
                    std::stringstream cPossibleOpens;
                    for(uint32_t cChnl = 0; cChnl < cChip->size(); cChnl++)
                    {
                        auto& cOcc = cSummaryThisChip->getChannel<Occupancy>(cChnl).fOccupancy;
                        if(cOcc >= 1) continue;

                        cMissingHits << +cChnl << "[" << cOcc << "], ";
                        if(cOcc <= 1.0 - THRESHOLD_OPEN)
                        {
                            cPossibleOpens << +cChnl << "[" << cOcc << "], ";
                            cOpensThisChip.push_back(cChnl);
                        }
                    } // channels
                    if(cOpensThisChip.size() > 0)
                    {
                        LOG(INFO) << BOLDRED << cChipSummary.str() << " -- Found " << cOpensThisChip.size() << " Possible Opens " << cPossibleOpens.str() << RESET;
                        if(cMissingHits.str().length() > 0) LOG(WARNING) << BOLDYELLOW << " Missing hits : " << cMissingHits.str() << RESET;
                    }
                    else if(cMissingHits.str() != "")
                        LOG(WARNING) << BOLDYELLOW << cChipSummary.str() << " -- Found missing hits : " << cMissingHits.str() << RESET;
                    else
                        LOG(INFO) << BOLDGREEN << cChipSummary.str() << " -- nothing of interest to report" << RESET;
                } // chips
            }     // hybrids
        }         // OGs
    }
    return;
}

void OpenFinder::FindOpensPS()
{
#if defined(__TCUSB__)
    // make sure that async mode is selected
    // that antenna source is 12
    auto                  cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cTriggerInterface* cTriggerInterface = dynamic_cast<D19cTriggerInterface*>(cInterface->getTriggerInterface());
    int                   cUserTriggerRate  = 1200; // user_trigger_frequency
    // // user_trigger_frequency should be such that user_trigger_frequency/2 allows enough time for the antenna circuit to charge up. The circuit needs around 13us, which is equivalent to ~500 clock
    // cycles.

    // // ***********************FOR CORRECT ANTENNA CONTROL TIMING*****************************
    // // delay_after_inject should be smaller than user_trigger_freq/2, so that the shutter is closed before the antenna trigger is allowed to charge up again, but large enough so the shutter is open
    // for 10ns so the antenna can fully discharge
    TestPulseTriggerConfiguration cTPCnfg;
    // cTPCnfg.fDelayAfterFastReset = 50;
    // cTPCnfg.fDelayAfterTP        = 100;  // THIS IS NOT USED ANYWHERE IN THE STATE MACHINE -> src/usr/fast_command/commands_from_ps_counter_fsm.vhd
    cTPCnfg.fDelayBeforeNextTP = 3; // From the antenna injection to the closing of the shutter// This is DELAY_AFTER_INJECT in FW, delay_before_next_test_pulse in settings file
    cTPCnfg.fEnableFastReset   = 1;
    cTPCnfg.fEnableTP          = 1;
    cTPCnfg.fEnableL1A         = 1;
    cTriggerInterface->ConfigureTestPulseFSM(cTPCnfg);
    //
    TriggerConfiguration cCnfg;
    cCnfg.fTriggerSource     = 12;
    cCnfg.fTriggerRate       = cUserTriggerRate;
    cCnfg.fNtriggersToAccept = fEventsPerPoint;
    cTriggerInterface->ConfigureTriggerFSM(cCnfg);

    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        std::vector<std::pair<std::string, uint32_t>> cRegVec;
        cRegVec.clear();
        // cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.trigger_source", 12});
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.cal_pulse", 0x0});
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.antenna", 0x1}); // Change and CHECK!!!
        // cRegVec.push_back({"fc7_daq_ctrl.fast_command_block.control.load_config", 0x1});

        // cnfg_fastblock_i.async_ps_fsm_signals_enable.antenna_trigger_en <= '1';
        // cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.antenna", 0x1});
        // cnfg_fastblock_i.async_ps_fsm_signals_enable.clear_counters_en <= '1';
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.clear_counters", 0x1});
        // cnfg_fastblock_i.async_ps_fsm_signals_enable.open_shutter_en <= '1';
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.open_shutter", 0x1});
        // cnfg_fastblock_i.async_ps_fsm_signals_enable.cal_pulse_en <= '0';
        // cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.cal_pulse", 0x0});
        // cnfg_fastblock_i.async_ps_fsm_signals_enable.close_shutter_en <= '1';
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.close_shutter", 0x1});

        cRegVec.push_back({"fc7_daq_ctrl.fast_command_block.control.load_config", 0x1});

        fBeBoardInterface->WriteBoardMultReg(cBoard, cRegVec);
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    fReadoutChipInterface->WriteChipReg(cChip, "InjectedCharge", 0x0);
                    auto cValue = fReadoutChipInterface->ReadChipReg(cChip, "ENFLAGS");
                    LOG(INFO) << "ENFLAGS is -> " << cValue << RESET;

                    // Removing analog calib enabled
                    fReadoutChipInterface->WriteChipReg(cChip, "ENFLAGS", cValue & 0xEF);

                    cValue = fReadoutChipInterface->ReadChipReg(cChip, "ENFLAGS");
                    LOG(INFO) << "ENFLAGS set to " << cValue << RESET;

                    // // Changing sampling mode to LEVEL detection
                    // fReadoutChipInterface->WriteChipReg(cChip, "ENFLAGS", cValue | 0x20);

                    // cValue = fReadoutChipInterface->ReadChipReg(cChip, "ENFLAGS");
                    // LOG(INFO) << "ENFLAGS set to " << cValue << RESET;
                }
            }
        }
    }

    fParameters.nTriggers = this->findValueInSettings<double>("Nevents");

    LOG(INFO) << BOLDBLUE << "Checking for opens in PS hybrid "
              << " antenna potentiometer will be set to 0x" << std::hex << fParameters.potentiometer << std::dec << " units."
              << "Going to ask for " << +fParameters.nTriggers << " events." << RESET;
    LOG(INFO) << BOLDBLUE << "Hybrid voltages BEFORE selecting antenna position" << RESET;
    SelectAntennaPosition("Disable");

    std::vector<uint16_t> finalAntennaOdd;
    std::vector<uint16_t> finalAntennaEven;

    std::vector<bool> AntennaOdd_set;
    std::vector<bool> AntennaEven_set;
    // bool              antenna_set = false;
    // set thresholds
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    bool cValidChip = cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2;
                    if(!cValidChip) continue;
                    auto cThreshold = fReadoutChipInterface->ReadChipReg(cChip, "Threshold");
#if defined(__USE_ROOT__)

                    double cPedeMean  = getSummaryParameter(Form("AvgPedeSSA%d", cChip->getId()));
                    double cNoiseMean = getSummaryParameter(Form("AvgNoiseSSA%d", cChip->getId()));
                    if(cPedeMean != -1.0)
                    {
                        LOG(INFO) << BOLDYELLOW << "SSA#" << +cChip->getId() << "\t Pedestal :" << cPedeMean << " , Noise " << cNoiseMean << RESET;
                        cThreshold               = (uint16_t)(cPedeMean + 25 * cNoiseMean);
                        std::string tmpParameter = "thresholdForOpens_" + std::to_string(cChip->getId());
                        fillSummaryTree(tmpParameter, cThreshold);
                    }

#endif
                    // cThreshold = 175;
                    finalAntennaEven.push_back(0);
                    finalAntennaOdd.push_back(0);
                    LOG(INFO) << BOLDBLUE << "Setting Threshold to " << cThreshold << " on SSA#" << +cChip->getId() << RESET;
                    fReadoutChipInterface->WriteChipReg(cChip, "Threshold", cThreshold);
                }
            }
        }
    } // board

    LOG(INFO) << BOLDMAGENTA << "Finding the optimal values for the antenna potentiometer..." << RESET;
    std::vector<uint16_t> cAntennaValues(0, 0);
    auto                  cStart = this->findValueInSettings<double>("AntennaPotentiometerLowEnd");
    auto                  cStop  = this->findValueInSettings<double>("AntennaPotentiometerHighEnd");
    for(uint16_t cAntennaValue = cStart; cAntennaValue < cStop; cAntennaValue++) cAntennaValues.push_back(cAntennaValue);
    // std::vector<uint8_t>  cPositions{0,1};
    this->SetTestAllChannels(true);
    ScanAntennaCharge(cAntennaValues);
    //     for(auto cPosition: cPositions)
    //     {
    //         std::string chn = (cPosition == 0) ? "EvenChannels" : "OddChannels";
    //         // uint8_t     cGroup = (cPosition == 0) ? 0 : 1;
    //         for(auto cAntennaValue: cAntennaValues)
    //         {
    //             // SelectAntennaPosition(chn, cAntennaValue);
    //             ScanAntennaCharge({(uint8_t)(1 - cPosition)});
    // //             BitwiseSearchChip({(uint8_t)(1 - cPosition)}, "Threshold", 8, 0.9);
    // //             std::vector<uint16_t> cTh;
    // //             cTh.clear();
    // //             for(auto cBoard: *fDetectorContainer)
    // //             {
    // //                 std::vector<uint16_t> cBoardTh;
    // //                 for(auto cOpticalGroup: *cBoard)
    // //                 {
    // //                     std::vector<uint16_t> cOgTh;
    // //                     for(auto cHybrid: *cOpticalGroup)
    // //                     {
    // //                         std::vector<uint16_t> cHybTh;
    // //                         for(auto cChip: *cHybrid)
    // //                         {
    // //                             auto cThValue = static_cast<ReadoutChip*>(cChip)->getReg("Bias_THDAC");
    // //                             cHybTh.push_back(cThValue);
    // //                             // LOG (INFO) << BOLDYELLOW << "SSA#" << +cChip->getId() << " threshold will be set to " << +(cThValue-10) << RESET;
    // //                         } // chip
    // //                         float cMeanThreshold = std::accumulate(cHybTh.begin(), cHybTh.end(), 0.) / cHybTh.size();
    // //                         cOgTh.push_back((uint16_t)cMeanThreshold);
    // //                     } // hybrid
    // //                     float cMeanThreshold = std::accumulate(cOgTh.begin(), cOgTh.end(), 0.) / cOgTh.size();
    // //                     cBoardTh.push_back((uint16_t)cMeanThreshold);
    // //                 } // OG
    // //                 float cMeanThreshold = std::accumulate(cBoardTh.begin(), cBoardTh.end(), 0.) / cBoardTh.size();
    // //                 cTh.push_back((uint16_t)cMeanThreshold);
    // //             } // board
    // //             float cMeanThreshold = std::accumulate(cTh.begin(), cTh.end(), 0.) / cTh.size();
    // //             LOG(INFO) << BOLDYELLOW << "Average threshold to reach target occupancy is " << cMeanThreshold << RESET;

    // //             // scan threshold around pedestal + extract noise
    // //             std::vector<uint16_t> cDacList;
    // //             cDacList.clear();
    // //             for(int cOffset = -20; cOffset < 20; cOffset++) cDacList.push_back(cMeanThreshold + cOffset);
    // //             std::vector<DetectorDataContainer*> cOccVector;
    // //             fRecycleBin.setDetectorContainer(fDetectorContainer);
    // //             for(auto cContainer: cOccVector) fRecycleBin.free(cContainer);
    // //             cOccVector.clear();
    // //             for(auto i = 0u; i < cDacList.size(); i++) cOccVector.push_back(fRecycleBin.get(&ContainerFactory::copyAndInitStructure<Occupancy>, Occupancy()));
    // //             this->scanDac("Threshold", cDacList, fParameters.nTriggers, cOccVector);

    // // #ifdef __USE_ROOT__
    // //             for(size_t cIndx = 0; cIndx < cDacList.size(); cIndx++) fDQMHistogramOpenFinder.fillSCurvePlots(cDacList[cIndx], *cOccVector[cIndx]);
    // // #endif
    //         }
    //         SelectAntennaPosition("Disable");
    //     }

    return;

    // uint16_t cThresholdForOpens = this->findValueInSettings<double>("ThresholdForOpens");
    // for(auto cBoard: *fDetectorContainer)
    // {
    //     for(auto cOpticalGroup: *cBoard)
    //     {
    //         for(auto cHybrid: *cOpticalGroup)
    //         {
    //             for(auto cChip: *cHybrid)
    //             {
    //                 if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2)
    //                 {
    //                     std::vector<uint8_t> cPositions{0, 1};
    //                     for(auto cPosition_index: cPositions)
    //                     {
    //                         // uint8_t cPosition = 1-cPosition_index;
    //                         uint8_t cPosition = cPosition_index;
    //                         antenna_set       = false;
    //                         for(int i = 0; i < 2 && !antenna_set; i++)
    //                         {
    //                             std::string chn = (cPosition == 0) ? "even" : "odd";

    //                             LOG(INFO) << BOLDMAGENTA << "Finding optimal antenna value for the " << chn << " channels of chip " << +cChip->getId() << RESET;

    //                             antennaPullupLowEnd  = this->findValueInSettings<double>("AntennaPotentiometerLowEnd");
    //                             antennaPullupHighEnd = this->findValueInSettings<double>("AntennaPotentiometerHighEnd");

    //                             // BINARY SEARCH
    //                             uint8_t nTries = 0;
    //                             while(!antenna_set)
    //                             {
    //                                 nTries++;
    //                                 if(antennaPullupHighEnd < antennaPullupLowEnd)
    //                                 {
    //                                     LOG(INFO) << BOLDRED << "Could not find a valid antenna value for " << chn << " channels of chip " << +cChip->getId() << "!!" << RESET;
    //                                     break;
    //                                 }

    //                                 antennaPullup = (antennaPullupHighEnd + antennaPullupLowEnd) / 2;

    //                                 fParameters.potentiometer = antennaPullup;
    //                                 // select antenna position
    //                                 SelectAntennaPosition((cPosition == 0) ? "EvenChannels" : "OddChannels");
    //                                 std::this_thread::sleep_for(std::chrono::milliseconds(1));
    //                                 // check counters
    //                                 BeBoard* cBeBoard = static_cast<BeBoard*>(fDetectorContainer->at(cBoard->getIndex()));
    //                                 this->ReadNEvents(cBeBoard, fParameters.nTriggers);
    //                                 const std::vector<Event*>& cEvents = this->GetEvents();
    //                                 // const std::vector<Event*>& cEvents = this->GetEvents(cBeBoard);
    //                                 for(auto cEvent: cEvents)
    //                                 {
    //                                     // auto cNhits     = cEvent->GetNHits(cHybrid->getId(), cChip->getId());
    //                                     auto cHitVector = cEvent->GetHits(cHybrid->getId(), cChip->getId());

    //                                     for(uint32_t iChannel = 0; iChannel < cChip->size(); ++iChannel)
    //                                     {
    //                                         if(iChannel % 2 == cPosition)
    //                                         {
    //                                             occupancy_avg += cHitVector[iChannel];
    //                                             if(cHitVector[iChannel] >= fParameters.nTriggers)
    //                                             {
    //                                                 high_outliers_channels++;
    //                                                 LOG(DEBUG) << "High outlier " << +iChannel << RESET;
    //                                             }
    //                                         }
    //                                     } // chnl

    //                                     // Find the average ocupancy of the channels
    //                                     occupancy_avg = occupancy_avg / (cChip->size() / 2 * fParameters.nTriggers);
    //                                     LOG(INFO) << "Occupancy on " << chn << " channels of chip " << +cChip->getId() << " for antenna value " << fParameters.potentiometer << ": " << BOLDBLUE
    //                                               << occupancy_avg << RESET;

    //                                     if(occupancy_avg >= 0.90 && occupancy_avg <= 0.99)
    //                                     {
    //                                         antenna_set = true;
    //                                         LOG(INFO) << BOLDGREEN << "Antenna value for " << chn << " channels of chip " << +cChip->getId() << " set to " << antennaPullup << RESET;
    //                                         if(cPosition == 0)
    //                                             finalAntennaEven[cChip->getIndex()] = antennaPullup;
    //                                         else
    //                                             finalAntennaOdd[cChip->getIndex()] = antennaPullup;
    //                                     }
    //                                     else if(occupancy_avg < 0.90)
    //                                     {
    //                                         antennaPullupLowEnd = antennaPullup + 1;
    //                                     }
    //                                     else if(occupancy_avg > 0.99)
    //                                     {
    //                                         antennaPullupHighEnd = antennaPullup - 1;
    //                                     }
    //                                     if(nTries > 50) // Check if we want to keep this
    //                                     {
    //                                         LOG(INFO) << BOLDRED << "Could not find a valid antenna value for " << chn << " channels of chip " << +cChip->getId() << "!!" << RESET;
    //                                         break;
    //                                     }
    //                                 }
    //                             }
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }

//     LOG(INFO) << BOLDBLUE << "Antenna values set, running to open finder." << RESET;

// #if defined(__USE_ROOT__)
//     fResultFile->cd();
//     TString               fOpensTreeParameter = "";
//     std::vector<uint16_t> fOpensTreeValue     = {};
//     TTree*                fOpensTree          = new TTree("opensTree", "Opens in hybrid");
//     fOpensTree->Branch("Chip", &fOpensTreeParameter);
//     fOpensTree->Branch("Value", &fOpensTreeValue);
// #endif

//     bool        cOpensFound = false;
//     std::string Channels    = "";

//     std::vector<uint8_t> cPositions{0, 1};
//     for(auto cBoard: *fDetectorContainer)
//     {
//         for(auto cOpticalGroup: *cBoard)
//         {
//             for(auto cHybrid: *cOpticalGroup)
//             {
//                 for(auto cChip: *cHybrid)
//                 {
//                     for(auto cPosition: cPositions)
//                     {
//                         bool retry     = true;
//                         int  occupancy = 0;
//                         for(int i = 0; i < 2 && retry; i++)
//                         {
//                             retry = false;
//                             // Get Antenna value for chip and channels
//                             if(cPosition == 0)
//                             {
//                                 if(finalAntennaEven[cChip->getIndex()] > 0)
//                                 {
//                                     //   if (cChip->getId() == 6) {
//                                     //     fParameters.potentiometer = 600;
//                                     //     LOG(INFO) << "Potentiometer value: " << 600 << RESET;
//                                     //   }
//                                     //   else {
//                                     fParameters.potentiometer = finalAntennaEven[cChip->getIndex()] + 5;
//                                     LOG(INFO) << "Potentiometer value: " << finalAntennaEven[cChip->getIndex()] << RESET;
//                                     ;
//                                     // }
//                                     Channels = "Even";
//                                 }
//                                 else
//                                 {
//                                     LOG(INFO) << BOLDRED << "Could not set antenna value for the even channels of this chip ( chip " << +cChip->getId() << " ), skipping..." << RESET;
//                                     continue;
//                                 }
//                             }
//                             else
//                             {
//                                 if(finalAntennaOdd[cChip->getIndex()] > 0)
//                                 {
//                                     // if (cChip->getId() == 1) {
//                                     // fParameters.potentiometer = 781;
//                                     // LOG(INFO) << "Potentiometer value: " << 781 << RESET;
//                                     // }
//                                     // else {
//                                     fParameters.potentiometer = finalAntennaOdd[cChip->getIndex()] + 5;
//                                     LOG(INFO) << "Potentiometer value: " << finalAntennaOdd[cChip->getIndex()];
//                                     // }
//                                     Channels = "Odd";
//                                 }
//                                 else
//                                 {
//                                     LOG(INFO) << BOLDRED << "Could not set antenna value for the odd channels of this chip ( chip " << +cChip->getId() << " ), skipping..." << RESET;
//                                     continue;
//                                 }
//                             }
//                             // FillSummaryTree(Form("Antenna_",cChip->GetId(),Channels), finalAntennaEven[cChip->getIndex()] );

//                             // select antenna position
//                             SelectAntennaPosition((cPosition == 0) ? "EvenChannels" : "OddChannels");

//                             // check counters
//                             BeBoard* cBeBoard = static_cast<BeBoard*>(fDetectorContainer->at(cBoard->getIndex()));
//                             // cBeBoard->setEventType(EventType::SSAAS);

//                             this->ReadNEvents(cBeBoard, fParameters.nTriggers);
//                             const std::vector<Event*>& cEvents = this->GetEvents();
//                             // const std::vector<Event*>& cEvents = this->GetEvents(cBeBoard);
//                             cOpensFound = false;
//                             std::vector<uint16_t> opens;
//                             for(auto cEvent: cEvents)
//                             {
//                                 LOG(INFO) << BOLDBLUE << "SSA#" << +cChip->getId() << RESET;
//                                 // auto cNhits     = cEvent->GetNHits(cHybrid->getId(), cChip->getId());
//                                 auto cHitVector = cEvent->GetHits(cHybrid->getId(), cChip->getId());

//                                 std::string tmpParameter = "";

//                                 for(uint32_t iChannel = 0; iChannel < cChip->size(); ++iChannel)
//                                 {
//                                     occupancy += cHitVector[iChannel];
//                                     // LOG(INFO) << "Channels" << +cHitVector[iChannel] << RESET;
//                                     if(iChannel % 2 != cPosition)
//                                     {
//                                         if(cHitVector[iChannel] >= (THRESHOLD_OPEN)*fParameters.nTriggers)
//                                             LOG(DEBUG) << BOLDBLUE << "\t\t... "
//                                                        << " strip " << +iChannel << " detected " << +cHitVector[iChannel] << " hits " << RESET;
//                                         continue;
//                                     }
//                                     else // If channel in injected channels:
//                                     {
//                                         LOG(DEBUG) << BOLDBLUE << "\t... "
//                                                    << " strip " << +iChannel << " detected " << +cHitVector[iChannel] << " hits " << RESET;
//                                         if(cHitVector[iChannel] <= (THRESHOLD_OPEN * 2.5) * fParameters.nTriggers)
//                                         {
//                                             cOpensFound = true;
//                                             LOG(INFO) << BOLDRED << "Chip " << +cChip->getId() << " strip " << +iChannel << " detected " << +cHitVector[iChannel] << " hits when at most "
//                                                       << +fParameters.nTriggers << " were expected." << RESET;
//                                             opens.push_back(iChannel);
//                                         }
//                                         else if(cHitVector[iChannel] > (1.0) * fParameters.nTriggers)
//                                         {
//                                             LOG(INFO) << BOLDBLUE << "Chip " << +cChip->getId() << " strip " << +iChannel << " detected " << +cHitVector[iChannel] << " hits when at most "
//                                                       << +fParameters.nTriggers << " were expected." << RESET;
//                                         }
//                                         else if(cHitVector[iChannel] <= (1.0 - THRESHOLD_OPEN * 2.5) * fParameters.nTriggers)
//                                             LOG(INFO) << BOLDYELLOW << "Chip " << +cChip->getId() << " strip " << +iChannel << " detected " << +cHitVector[iChannel] << " hits when at most "
//                                                       << +fParameters.nTriggers << " were expected." << RESET;
//                                         else
//                                         {
//                                             LOG(INFO) << BOLDGREEN << "Chip " << +cChip->getId() << " strip " << +iChannel << " detected " << +cHitVector[iChannel] << " hits when at most "
//                                                       << +fParameters.nTriggers << " were expected." << RESET;
//                                         }
//                                     }
//                                 } // chnl

//                                 tmpParameter = "";
//                                 tmpParameter = "opens_" + std::to_string(cChip->getId()) + "_" + Channels;
// #if defined(__USE_ROOT__)
//                                 fillSummaryTree(tmpParameter, opens.size());
//                                 if(true)
//                                 {
//                                     fResultFile->cd();
//                                     fOpensTreeParameter.Clear();
//                                     fOpensTreeParameter = "Chip_" + std::to_string(cChip->getId());
//                                     fOpensTreeValue     = opens;
//                                     fOpensTree->Fill();
//                                 }
// #endif
//                             }

//                             if(!cOpensFound)
//                                 LOG(INFO) << BOLDGREEN << "No opens found on the " << Channels << " channels of chip " << +cChip->getId() << RESET;
//                             else
//                                 LOG(INFO) << BOLDRED << +opens.size() << " opens found on the " << Channels << " channels of chip " << +cChip->getId() << RESET;
//                             // disable
//                             fParameters.potentiometer = 512;
//                             SelectAntennaPosition("Disable");

//                             LOG(INFO) << "Avg ccupancy on ALL channels is " << +occupancy / cChip->size() << RESET;
//                         }
//                     }
//                 }
//             }
//         }
//     }
#endif
}

void OpenFinder::FindOpensPS_original()
{
#ifdef __TCUSB__
    float   measurement;
    TC_PSFE cTC_PSFE;
    cTC_PSFE.adc_get(TC_PSFE::measurement::_3V3, measurement);
    LOG(INFO) << "3V3 -> " << +measurement << RESET;

    uint16_t antennaPullupLowEnd  = this->findValueInSettings<double>("AntennaPotentiometerLowEnd");
    uint16_t antennaPullupHighEnd = this->findValueInSettings<double>("AntennaPotentiometerHighEnd");
    fParameters.nTriggers         = this->findValueInSettings<double>("Nevents");

    std::vector<TH2F*> fOccupancyHistVect;
    // uint16_t antennaPullupLowEnd = 540;
    // uint16_t antennaPullupHighEnd = 700;
    uint16_t antennaPullup = (antennaPullupHighEnd - antennaPullupLowEnd) / 2 + antennaPullupLowEnd;
    // fParameters.potentiometer = antennaPullup;

    LOG(DEBUG) << BOLDBLUE << "Checking for opens in PS hybrid "
               << " antenna potentiometer will be set to 0x" << std::hex << fParameters.potentiometer << std::dec << " units."
               << "Going to ask for " << +fParameters.nTriggers << " events." << RESET;
    LOG(INFO) << BOLDBLUE << "Hybrid voltages BEFORE selecting antenna position" << RESET;
    SelectAntennaPosition("Disable");

    std::vector<uint16_t> finalAntennaOdd;
    std::vector<uint16_t> finalAntennaEven;
    // uint16_t              finalAntenna = 0;

    // int    crosstalk_channels     = 0;
    int    high_outliers_channels = 0;
    double occupancy_avg          = 0;
    // double occupancy_crosstk = 0;

    std::vector<bool> AntennaOdd_set;
    std::vector<bool> AntennaEven_set;
    bool              antenna_set = false;
    // make sure that async mode is selected
    // that antenna source is 10
    // and set thresholds
    uint16_t cThreshold = this->findValueInSettings<double>("ThresholdForOpens");
    for(auto cBoard: *fDetectorContainer)
    {
        // ******** FIRMWARE CONFIGURATION ********
        std::vector<std::pair<std::string, uint32_t>> cRegVec;
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.user_trigger_frequency", 1200});
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.test_pulse.delay_before_next_pulse", 3}); // The shutter is closed 3 clock cycles after the trigger
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_delay.after_open_shutter", 50}); // Number of clock cycles after the shutter is open until the antenna trigger is sent
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.antenna", 0x1});
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.clear_counters", 0x1});
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.open_shutter", 0x1});
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.cal_pulse", 0x0});
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.close_shutter", 0x1});
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.antenna_trigger_delay_value", 10});
        cRegVec.push_back({"fc7_daq_ctrl.fast_command_block.control.load_config", 0x1});
        fBeBoardInterface->WriteBoardMultReg(cBoard, cRegVec);

        // make sure async mode is enabled
        setSameDacBeBoard(static_cast<BeBoard*>(cBoard), "AnalogueAsync", 1);
        // first .. set injection amplitude to 0
        setSameDacBeBoard(static_cast<BeBoard*>(cBoard), "InjectedCharge", 0);
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.trigger_source", 12});
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.cal_pulse", 0});
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.antenna", 1});
        for(auto cOpticalGroup: *cBoard)
        {
            // std::vector<std::pair<std::string, uint32_t>> cRegVec;
            // cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.trigger_source", 10});
            // cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.cal_pulse", 0});
            // cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.antenna", 1});
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2)
                    {
                        double cPedeMean   = getSummaryParameter(Form("AvgPedeSSA%d", cChip->getId()));
                        double cPedeStdDev = getSummaryParameter(Form("StDvPedeSSA%d", cChip->getId()));
                        LOG(INFO) << "Mean:" << cPedeMean << RESET;
                        LOG(INFO) << "StdDev:" << cPedeStdDev << RESET;

                        if(cPedeMean != -1.0)
                        {
                            if((cPedeMean + 15 * cPedeStdDev) <= 255) { cThreshold = (uint16_t)(cPedeMean + 3 * cPedeStdDev); }
                            LOG(INFO) << BOLDBLUE << "Threshold from measurements " << cThreshold << RESET;
                        }
                        cThreshold = 50;
                        LOG(INFO) << BOLDBLUE << "Threshold to be set  " << cThreshold << RESET;
                        std::string tmpParameter = "thresholdForOpens_" + std::to_string(cChip->getId());
#ifdef __USE_ROOT__
                        fillSummaryTree(tmpParameter, cThreshold);
#endif

                        std::string cHistName = Form("AntennaOccupancy_Even_%d", cChip->getId());

                        if(gROOT->FindObject(cHistName.c_str()) != nullptr) cHistName = Form("%s_%s", cHistName.c_str(), "II");

                        std::string cHistTitle = Form("Occupancy in Even Channels in Chip %d", cChip->getId());

                        TH2F* fOccupancyHistEven =
                            new TH2F(cHistName.c_str(), cHistTitle.c_str(), (antennaPullupHighEnd - antennaPullupLowEnd), antennaPullupLowEnd - 0.5, antennaPullupHighEnd + 0.5, 120, -0.5, 120 + 0.5);

                        cHistName = Form("AntennaOccupancy_Odd_%d", cChip->getId());

                        if(gROOT->FindObject(cHistName.c_str()) != nullptr) cHistName = Form("%s_%s", cHistName.c_str(), "II");

                        cHistTitle = Form("Occupancy in Odd Channels in Chip %d", cChip->getId());

                        TH2F* fOccupancyHistOdd =
                            new TH2F(cHistName.c_str(), cHistTitle.c_str(), (antennaPullupHighEnd - antennaPullupLowEnd), antennaPullupLowEnd - 0.5, antennaPullupHighEnd + 0.5, 120, -0.5, 120 + 0.5);

                        fOccupancyHistVect.push_back(fOccupancyHistEven);
                        fOccupancyHistVect.push_back(fOccupancyHistOdd);

                        finalAntennaEven.push_back(0);
                        finalAntennaOdd.push_back(0);

                        fReadoutChipInterface->WriteChipReg(cChip, "AnalogueAsync", 1);
                        fReadoutChipInterface->WriteChipReg(cChip, "Threshold", cThreshold);
                        fReadoutChipInterface->WriteChipReg(cChip, "InjectedCharge", 0);
                        // fReadoutChipInterface->WriteChipReg(cChip, "SAMPLINGMODE", 0);
                    } // Chip type
                }     // chip loop
            }         // hybrid loop
        }             // optical group
        fBeBoardInterface->WriteBoardMultReg(cBoard, cRegVec);
        // fBeBoardInterface->WriteBoardMultReg (cBoard, cRegVec);
    }

    // For DEBUG. Sweep antenna value
    // TH1I* occupancyHist = new TH1I("OccupHist", "occupancy histogram for stddev", 60, 0, 60);
    // while(true)
    // {
    // antennaPullup = 750;
    /*  for(antennaPullup = antennaPullupLowEnd; antennaPullup < antennaPullupHighEnd; antennaPullup += 4)
      {
          std::vector<uint8_t> cPositions{0, 1};
          for(auto cPosition: cPositions)
          {
              //   if(cPosition == 1)
              //   continue;
              LOG(INFO) << " Antenna pull up : " << +antennaPullup << RESET;

              std::string chn = (cPosition == 0) ? "even" : "odd";

              //   std::string cHistName  = Form("OccupHist_%d", cChip->getId());
              //   TH1I* occupancyHist = new TH1I( cHistName.c_str(), "occupancy histogram for stddev", 60, 0, 60);

              fParameters.potentiometer = antennaPullup;
              // select antenna position
              SelectAntennaPosition((cPosition == 0) ? "EvenChannels" : "OddChannels");

              for(auto cBoard: *fDetectorContainer)
              {
                  for(auto cOpticalGroup: *cBoard)
                  {
                      for(auto cHybrid: *cOpticalGroup)
                      {
                          for(auto cChip: *cHybrid)
                          {
                              if(cChip->getFrontEndType() == FrontEndType::SSA)
                              {
                                  //   TH2F* fOccupancyHist = fOccupancyHistVect[cChip->getIndex()+cPosition];

                                  //   fOccupancyHist->Clear();

                                  BeBoard* cBeBoard = static_cast<BeBoard*>(fDetectorContainer->at(cBoard->getIndex()));
                                  this->ReadNEvents(cBeBoard, fParameters.nTriggers);
                                  const std::vector<Event*>& cEvents = this->GetEvents();
                                  for(auto cEvent: cEvents)
                                  {
                                    //   auto cNhits     = cEvent->GetNHits(cHybrid->getId(), cChip->getId());
                                      auto cHitVector = cEvent->GetHits(cHybrid->getId(), cChip->getId());

                                      for(uint32_t iChannel = 0; iChannel < cChip->size(); ++iChannel)
                                      {
                                          if(iChannel % 2 == cPosition)
                                          {
                                              occupancy_avg += cHitVector[iChannel];
                                              fOccupancyHistVect[2 * cChip->getIndex() + cPosition]->Fill(antennaPullup, iChannel, cHitVector[iChannel]);
                                              if(cHitVector[iChannel] >= fParameters.nTriggers)
                                              {
                                                  high_outliers_channels++;
                                                  LOG(DEBUG) << "High outlier " << +iChannel << RESET;
                                              }
                                          }
                                      } // chnl
                                  }
                                  //   LOG(INFO) << "Std dev for " << chn << " channels of chip " << +cChip->getId() << ": " << +occupancyHist->GetStdDev() << RESET;
                                  //   fOccupancyHist->Fill(antennaPullup, occupancy_avg, occupancyHist->GetStdDev());
                                  //   fOccupancyHist->Fill(antennaPullup, 2*occupancy_avg/cChip->size());
                              }
                          }
                      }
                  }
              }
          }
      }

     for (int i=0; i+1<int(fOccupancyHistVect.size()); i+=2) {
        auto fOccupancyHistEven = fOccupancyHistVect[i];
        auto fOccupancyHistOdd = fOccupancyHistVect[i+1];
        fOccupancyHistEven->Draw();
        fOccupancyHistOdd->Draw();
        fOccupancyHistEven->Write();
        fOccupancyHistOdd->Write();
     }
    // }
    return;*/

    LOG(INFO) << BOLDMAGENTA << "Finding the optimal values for the antenna potentiometer..." << RESET;

    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2)
                    {
                        std::vector<uint8_t> cPositions{0, 1};
                        for(auto cPosition_index: cPositions)
                        {
                            // uint8_t cPosition = 1-cPosition_index;
                            uint8_t cPosition = cPosition_index;
                            antenna_set       = false;
                            for(int i = 0; i < 2 && !antenna_set; i++)
                            {
                                std::string chn = (cPosition == 0) ? "even" : "odd";

                                LOG(INFO) << BOLDMAGENTA << "Finding optimal antenna value for the " << chn << " channels of chip " << +cChip->getId() << RESET;

                                antennaPullupLowEnd  = this->findValueInSettings<double>("AntennaPotentiometerLowEnd");
                                antennaPullupHighEnd = this->findValueInSettings<double>("AntennaPotentiometerHighEnd");

                                // BINARY SEARCH
                                while(!antenna_set)
                                {
                                    if(antennaPullupHighEnd < antennaPullupLowEnd)
                                    {
                                        LOG(INFO) << BOLDRED << "Could not find a valid antenna value for " << chn << " channels of chip " << +cChip->getId() << "!!" << RESET;
                                        fillSummaryTree("AntennaValue_chip_" + std::to_string(cChip->getId()) + "_" + chn, -1.0);
                                        break;
                                    }

                                    antennaPullup = (antennaPullupHighEnd + antennaPullupLowEnd) / 2;

                                    fParameters.potentiometer = antennaPullup;
                                    // select antenna position
                                    SelectAntennaPosition((cPosition == 0) ? "EvenChannels" : "OddChannels");
                                    std::this_thread::sleep_for(std::chrono::milliseconds(1));
                                    // check counters
                                    BeBoard* cBeBoard = static_cast<BeBoard*>(fDetectorContainer->at(cBoard->getIndex()));
                                    this->ReadNEvents(cBeBoard, fParameters.nTriggers);

                                    // LOG(INFO) << "Reading value on strip 1 " << RESET ;
                                    // uint8_t counter_MSB_strip0 = fReadoutChipInterface->ReadChipReg(cChip, "AC_ReadCounterMSB_S2");
                                    // uint8_t counter_LSB_strip0 = fReadoutChipInterface->ReadChipReg(cChip, "AC_ReadCounterLSB_S2");
                                    // uint16_t cCounterValue_strip0 = ((counter_MSB_strip0 & 0xFF) << 8) | (counter_LSB_strip0 & 0xFF);
                                    // LOG(INFO) << "Value is " << cCounterValue_strip0;

                                    // LOG(INFO) << "Reading value on strip 119 " << RESET ;
                                    // uint8_t counter_MSB_strip119 = fReadoutChipInterface->ReadChipReg(cChip, "AC_ReadCounterMSB_S120");
                                    // uint8_t counter_LSB_strip119 = fReadoutChipInterface->ReadChipReg(cChip, "AC_ReadCounterLSB_S120");
                                    // uint16_t cCounterValue_strip119 = ((counter_MSB_strip119 & 0xFF) << 8) | (counter_LSB_strip119 & 0xFF);
                                    // LOG(INFO) << "Value is " << cCounterValue_strip119;

                                    const std::vector<Event*>& cEvents = this->GetEvents();
                                    // const std::vector<Event*>& cEvents = this->GetEvents(cBeBoard);
                                    for(auto cEvent: cEvents)
                                    {
                                        // auto cNhits     = cEvent->GetNHits(cHybrid->getId(), cChip->getId());
                                        auto cHitVector = cEvent->GetHits(cHybrid->getId(), cChip->getId());

                                        for(uint32_t iChannel = 0; iChannel < cChip->size(); ++iChannel)
                                        {
                                            if(iChannel % 2 == cPosition)
                                            {
                                                occupancy_avg += cHitVector[iChannel];
                                                LOG(INFO) << BOLDMAGENTA << "hits in channel " << +iChannel << "-> " << +cHitVector[iChannel] << RESET;
                                                if(cHitVector[iChannel] >= fParameters.nTriggers)
                                                {
                                                    high_outliers_channels++;
                                                    LOG(DEBUG) << "High outlier " << +iChannel << RESET;
                                                }
                                            }
                                        } // chnl

                                        // Find the average ocupancy of the channels
                                        occupancy_avg = occupancy_avg / (cChip->size() / 2 * (fParameters.nTriggers));
                                        LOG(INFO) << "Occupancy on " << chn << " channels of chip " << +cChip->getId() << " for antenna value " << fParameters.potentiometer << ": " << BOLDBLUE
                                                  << occupancy_avg << RESET;

                                        if(occupancy_avg >= 0.95 && occupancy_avg <= 1.01)
                                        {
                                            antenna_set = true;
                                            LOG(INFO) << BOLDGREEN << "Antenna value for " << chn << " channels of chip " << +cChip->getId() << " set to " << antennaPullup << RESET;
                                            fillSummaryTree("AntennaValue_chip_" + std::to_string(cChip->getId()) + "_" + chn, (double)antennaPullup);
                                            if(cPosition == 0)
                                                finalAntennaEven[cChip->getIndex()] = antennaPullup;
                                            else
                                                finalAntennaOdd[cChip->getIndex()] = antennaPullup;
                                        }
                                        else if(occupancy_avg < 0.95)
                                        {
                                            antennaPullupLowEnd = antennaPullup + 1;
                                        }
                                        else if(occupancy_avg > 1.01)
                                        {
                                            antennaPullupHighEnd = antennaPullup - 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    LOG(INFO) << BOLDBLUE << "Antenna values set, running to open finder." << RESET;

#if defined(__USE_ROOT__)
    fResultFile->cd();
    TString               fOpensTreeParameter = "";
    std::vector<uint16_t> fOpensTreeValue     = {};
    TTree*                fOpensTree          = new TTree("opensTree", "Opens in hybrid");
    fOpensTree->Branch("Chip", &fOpensTreeParameter);
    fOpensTree->Branch("Value", &fOpensTreeValue);
#endif

    bool        cOpensFound = false;
    std::string Channels    = "";

    std::vector<uint8_t> cPositions{0, 1};
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    for(auto cPosition: cPositions)
                    {
                        bool retry     = true;
                        int  occupancy = 0;
                        for(int i = 0; i < 2 && retry; i++)
                        {
                            retry = false;
                            // Get Antenna value for chip and channels
                            if(cPosition == 0)
                            {
                                if(finalAntennaEven[cChip->getIndex()] > 0)
                                {
                                    //   if (cChip->getId() == 6) {
                                    //     fParameters.potentiometer = 600;
                                    //     LOG(INFO) << "Potentiometer value: " << 600 << RESET;
                                    //   }
                                    //   else {
                                    fParameters.potentiometer = finalAntennaEven[cChip->getIndex()] + 5;
                                    LOG(INFO) << "Potentiometer value: " << finalAntennaEven[cChip->getIndex()] << RESET;
                                    ;
                                    // }
                                    Channels = "Even";
                                }
                                else
                                {
                                    LOG(INFO) << BOLDRED << "Could not set antenna value for the even channels of this chip ( chip " << +cChip->getId() << " ), skipping..." << RESET;
                                    continue;
                                }
                            }
                            else
                            {
                                if(finalAntennaOdd[cChip->getIndex()] > 0)
                                {
                                    // if (cChip->getId() == 1) {
                                    // fParameters.potentiometer = 781;
                                    // LOG(INFO) << "Potentiometer value: " << 781 << RESET;
                                    // }
                                    // else {
                                    fParameters.potentiometer = finalAntennaOdd[cChip->getIndex()] + 5;
                                    LOG(INFO) << "Potentiometer value: " << finalAntennaOdd[cChip->getIndex()];
                                    // }
                                    Channels = "Odd";
                                }
                                else
                                {
                                    LOG(INFO) << BOLDRED << "Could not set antenna value for the odd channels of this chip ( chip " << +cChip->getId() << " ), skipping..." << RESET;
                                    continue;
                                }
                            }
                            // FillSummaryTree(Form("Antenna_",cChip->GetId(),Channels), finalAntennaEven[cChip->getIndex()] );

                            // select antenna position
                            SelectAntennaPosition((cPosition == 0) ? "EvenChannels" : "OddChannels");

                            // check counters
                            BeBoard* cBeBoard = static_cast<BeBoard*>(fDetectorContainer->at(cBoard->getIndex()));
                            // cBeBoard->setEventType(EventType::SSAAS);

                            this->ReadNEvents(cBeBoard, fParameters.nTriggers);
                            const std::vector<Event*>& cEvents = this->GetEvents();
                            // const std::vector<Event*>& cEvents = this->GetEvents(cBeBoard);
                            cOpensFound = false;
                            std::vector<uint16_t> opens;
                            for(auto cEvent: cEvents)
                            {
                                LOG(INFO) << BOLDBLUE << "SSA#" << +cChip->getId() << RESET;
                                // auto cNhits     = cEvent->GetNHits(cHybrid->getId(), cChip->getId());
                                auto cHitVector = cEvent->GetHits(cHybrid->getId(), cChip->getId());

                                std::string tmpParameter = "";

                                for(uint32_t iChannel = 0; iChannel < cChip->size(); ++iChannel)
                                {
                                    occupancy += cHitVector[iChannel];
                                    // LOG(INFO) << "Channels" << +cHitVector[iChannel] << RESET;
                                    if(iChannel % 2 != cPosition)
                                    {
                                        if(cHitVector[iChannel] >= (THRESHOLD_OPEN)*fParameters.nTriggers)
                                            LOG(DEBUG) << BOLDBLUE << "\t\t... "
                                                       << " strip " << +iChannel << " detected " << +cHitVector[iChannel] << " hits " << RESET;
                                        continue;
                                    }
                                    else // If channel in injected channels:
                                    {
                                        LOG(DEBUG) << BOLDBLUE << "\t... "
                                                   << " strip " << +iChannel << " detected " << +cHitVector[iChannel] << " hits " << RESET;
                                        if(cHitVector[iChannel] <= (THRESHOLD_OPEN * 2.5) * fParameters.nTriggers)
                                        {
                                            cOpensFound = true;
                                            LOG(INFO) << BOLDRED << "Chip " << +cChip->getId() << " strip " << +iChannel << " detected " << +cHitVector[iChannel] << " hits when at most "
                                                      << +fParameters.nTriggers << " were expected." << RESET;
                                            opens.push_back(iChannel);
                                        }
                                        else if(cHitVector[iChannel] > (1.0) * fParameters.nTriggers)
                                        {
                                            LOG(INFO) << BOLDBLUE << "Chip " << +cChip->getId() << " strip " << +iChannel << " detected " << +cHitVector[iChannel] << " hits when at most "
                                                      << +fParameters.nTriggers << " were expected." << RESET;
                                        }
                                        else if(cHitVector[iChannel] <= (1.0 - THRESHOLD_OPEN) * fParameters.nTriggers)
                                            LOG(INFO) << BOLDYELLOW << "Chip " << +cChip->getId() << " strip " << +iChannel << " detected " << +cHitVector[iChannel] << " hits when at most "
                                                      << +fParameters.nTriggers << " were expected." << RESET;
                                        else
                                        {
                                            LOG(INFO) << BOLDGREEN << "Chip " << +cChip->getId() << " strip " << +iChannel << " detected " << +cHitVector[iChannel] << " hits when at most "
                                                      << +fParameters.nTriggers << " were expected." << RESET;
                                        }
                                    }
                                } // chnl

                                tmpParameter = "";
                                tmpParameter = "opens_" + std::to_string(cChip->getId()) + "_" + Channels;
#ifdef __USE_ROOT__
                                fillSummaryTree(tmpParameter, opens.size());
                                if(true)
                                {
                                    fResultFile->cd();
                                    fOpensTreeParameter.Clear();
                                    fOpensTreeParameter = "Chip_" + std::to_string(cChip->getId());
                                    fOpensTreeValue     = opens;
                                    fOpensTree->Fill();
                                }
#endif
                            }

                            if(!cOpensFound)
                                LOG(INFO) << BOLDGREEN << "No opens found on the " << Channels << " channels of chip " << +cChip->getId() << RESET;
                            else
                                LOG(INFO) << BOLDRED << +opens.size() << " opens found on the " << Channels << " channels of chip " << +cChip->getId() << RESET;
                            // disable. Should I do this?
                            // fParameters.potentiometer = 512;
                            // SelectAntennaPosition("Disable");

                            LOG(INFO) << "Avg ccupancy on ALL channels is " << +occupancy / cChip->size() << RESET;
                        }
                    } // Channel group
                }     // Chip
            }         // Hybrid
        }             // Optical group
    }                 // Board
#endif
}
void OpenFinder::FindOpens() {}
#endif
