#include "SFPMonitoringTool.h"
#include "D19cSFPMonitoringInterface.h"
#include "Parser/FileParser.h"

using namespace Ph2_System;
using namespace Ph2_Parser;
using namespace Ph2_HwInterface;

SFPMonitoringTool::SFPMonitoringTool(std::string puHalConfigFileName, uint16_t pBoardId)
{
    fConfigurationFile = puHalConfigFileName;
    fBoardId           = pBoardId;
    fMyInterface       = nullptr;
    FileParser                                                                  theFileParser;
    const std::map<uint16_t, std::tuple<std::string, std::string, std::string>> theRegManagerInfoList = theFileParser.getRegManagerInfoList(fConfigurationFile);
    const auto&                                                                 theRegManagerInfo     = theRegManagerInfoList.at(fBoardId);
    fMyInterface = new D19cSFPMonitoringInterface(RegManager(std::get<0>(theRegManagerInfo), std::get<1>(theRegManagerInfo), std::get<2>(theRegManagerInfo)));
}
SFPMonitoringTool::~SFPMonitoringTool() {}

std::pair<float, float> SFPMonitoringTool::GetMonitorable(std::string pParameter, std::string pMezzanine, uint8_t pLinkId)
{
    std::vector<float> cMeasurements;
    auto               cSFPId = fMyInterface->getSFPId(pLinkId);
    for(size_t cIndx = 0; cIndx < fNMeasurements; cIndx++) cMeasurements.push_back(fMyInterface->Read(pParameter, pMezzanine, cSFPId));
    auto cStats = calculateStats(cMeasurements);
    return cStats;
}
