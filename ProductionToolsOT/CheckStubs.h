/*!
 *
 * \file CheckStubs.h
 * \brief CheckStubs class
 * \author Stephane Cooperstein
 * \date 12/02/24
 *
 */

#ifndef CheckStubs_h__
#define CheckStubs_h__

#include "Tool.h"
#include <map>
// Calibration is not running on the SoC: I need to instantiate the DQM histrgrammer here
using namespace Ph2_HwDescription;

class CheckStubs : public Tool
{
  public:
    CheckStubs();
    ~CheckStubs();

    void Initialise(void);
    void ReenableChannels(void);

    // State machine
    void Running() override;
    void Stop() override;
    void ConfigureCalibration() override;
    void Pause() override;
    void Resume() override;
    void Reset();
    bool TestStubLines();
    // returns number of stubs
    std::vector<std::pair<uint8_t, uint8_t>> ReadStubs(uint8_t hybridId, uint8_t chipId);
    void FillStubsTree(std::string, std::string);

  private:
    //
    uint32_t fNEvents;

    void   UnmaskChannels(std::vector<uint8_t> pToUnmask, ChannelGroup<NCHANNELS, 1>& pChannelMask);
#if defined(__USE_ROOT__)
    TTree* fStubsTree = nullptr;
#endif
    std::string             fStubsTreeParameter          = "";
    std::string             fStubsTreeValue              = "";

};
#endif
