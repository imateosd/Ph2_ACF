#include "CicInputTest.h"
#include "D19cBackendAlignmentFWInterface.h"
#include "D19cDPInterface.h"
#include "D19cDebugFWInterface.h"
#include "D19cFWInterface.h"
#include "DPInterface.h"
#include "PSTestCardControl.h"
#include "Tool.h"
#include <chrono>

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

CicInputTester::CicInputTester(uint32_t pUsbBus, uint8_t pUsbDev, uint8_t pPattern) : Tool(), fUsbBus(pUsbBus), fUsbDev(pUsbDev), fPattern(pPattern) {}

CicInputTester::~CicInputTester() {}

void CicInputTester::SetInputPatterns(std::vector<uint8_t> pPatterns)
{
    fInputPatterns.clear();
    for(auto cPattern: pPatterns) { fInputPatterns.push_back(cPattern); }
}

void CicInputTester::Initialise()
{
    // assign the front-end type
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getIndex() > 0) continue;
        for(auto cOpticalGroup: *cBoard)
        {
            if(cOpticalGroup->getIndex() > 0) continue;
            fFrontEndType = cOpticalGroup->getFrontEndType();
        }
    }
    if(fFrontEndType == FrontEndType::HYBRIDPS)
    {
        // configure hybrid test card to select CIC
        // PSTestCardControl cHybridController;
        // cHybridController.Initialise(fUsbBus,fUsbDev);
        // cHybridController.SelectCIC(true);
    }

    // initialize containers that hold values found by this tool
    // one value per SLVS line
    ContainerFactory::copyAndInitHybrid<std::map<uint8_t, std::vector<CicInput>>>(*fDetectorContainer, fCicInputLines);
    ContainerFactory::copyAndInitHybrid<std::map<uint8_t, LineConfiguration>>(*fDetectorContainer, fSLVSLineConfigs);
    ContainerFactory::copyAndInitHybrid<std::vector<uint8_t>>(*fDetectorContainer, fEnabledChipIds);
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        // Alignment interface
        auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
        cAlignerInterface->InitializeConfiguration();
        cAlignerInterface->InitializeAlignerObject();
        cAlignerInterface->EnablePrintout(false);
        AlignerObject cAlignerObjct;
        cAlignerObjct.fChip    = 0;
        cAlignerObjct.fOptical = cBoard->isOptical() ? 1 : 0;
        auto& cLineConfigs     = fSLVSLineConfigs.at(cBoard->getIndex());
        auto& cInputLines      = fCicInputLines.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cInputLinesOG  = cInputLines->at(cOpticalGroup->getIndex());
            auto& cLineConfigsOG = cLineConfigs->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == nullptr) continue;

                auto& cChipIds = fEnabledChipIds.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::vector<uint8_t>>();
                cChipIds.clear();
                if(cOpticalGroup->getFrontEndType() == FrontEndType::HYBRIDPS)
                {
                    for(uint8_t cChipId = 0; cChipId < 8; cChipId++) cChipIds.push_back(cChipId);
                }
                else
                {
                    for(auto cChip: *cHybrid)
                    {
                        if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2) continue;
                        cChipIds.push_back(cChip->getId() % 8);
                    }
                }

                auto& cLineConfigsHybrd = cLineConfigsOG->at(cHybrid->getIndex());
                auto& cCnfg             = cLineConfigsHybrd->getSummary<std::map<uint8_t, LineConfiguration>>();
                for(uint8_t cLineIndx = 0; cLineIndx < 7; cLineIndx++)
                {
                    cAlignerObjct.fHybrid = cHybrid->getId();
                    cAlignerObjct.fLine   = cLineIndx;
                    LineConfiguration cLineCnfg;
                    // get sampling phase configured in the BE
                    auto cReply               = cAlignerInterface->RetrieveConfig(cAlignerObjct, cLineCnfg);
                    cCnfg[cLineIndx].fDelay   = cReply.fCnfg.fDelay;
                    cCnfg[cLineIndx].fBitslip = cReply.fCnfg.fBitslip;
                    LOG(INFO) << BOLDYELLOW << "Be-Align configuration on SLVSLIne#" << +cAlignerObjct.fLine << " delay of : " << +cCnfg[cLineIndx].fDelay << " bit-slip of "
                              << +cCnfg[cLineIndx].fBitslip << RESET;
                }
                auto& cInputLinesHybrd = cInputLinesOG->at(cHybrid->getIndex());
                auto& cCicInputMap     = cInputLinesHybrd->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
                cCicInputMap.clear();
                auto cPortConnections = fCicInterface->GetPhyPortConnections();
                for(uint8_t cPhyPort = 0; cPhyPort < 12; cPhyPort++)
                {
                    CicInput              cInput;
                    std::vector<CicInput> cCicInputs;
                    cCicInputs.clear();
                    size_t cIndx = 0; // phy-port input counter
                    for(auto cConnection: cPortConnections[cPhyPort])
                    {
                        cInput.fFe         = cConnection.first;
                        cInput.fOut        = cIndx;
                        cInput.fFeOut      = cConnection.second;
                        cInput.fBeLineIndx = cIndx + 1; // first input from phy-port muxed to first stub line [line1 in the BE]
                        cInput.fData       = "";
                        cCicInputs.push_back(cInput);
                        cIndx++;
                    } // loop over connections to this phy-port
                    cCicInputMap[cPhyPort] = cCicInputs;
                } // loop over phy-ports
            }     // hybrids
        }         // links
    }             // boards

#ifdef __USE_ROOT__
    fDQMHistogram.book(fResultFile, *fDetectorContainer, fSettingsMap);
#endif
}
void CicInputTester::StartInputPattern_PSHybrid()
{
    StopInputPattern_PSHybrid();
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        auto             cInterface   = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        D19cDPInterface* cDPInterface = cInterface->getDPInterface();
        // check if you've got the same pattern on all lines
        if(fInputPatterns.size() == 1)
            cDPInterface->ConfigurePatternAllLines(fInputPatterns[0]);
        else
        {
            size_t cInput = 0;
            for(auto cPattern: fInputPatterns)
            {
                std::vector<uint8_t> cPatterns(1, cPattern);
                cDPInterface->ConfigureLineBRAM(cInput, cPatterns);
                cInput++;
            }
        } // lines from FEs [MPAs]
        cDPInterface->Start();
    } // all boards
}
void CicInputTester::StopInputPattern_PSHybrid()
{
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        auto             cInterface   = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        D19cDPInterface* cDPInterface = cInterface->getDPInterface();
        cDPInterface->Stop();
    }
}
void CicInputTester::StartInputPattern()
{
    if(fFrontEndType == FrontEndType::HYBRIDPS)
        StartInputPattern_PSHybrid();
    else
    {
        // for modules - configure Chips to produce phase alignment patterns
        for(auto cBoard: *fDetectorContainer)
        {
            for(const auto cOpticalGroup: *cBoard)
            {
                for(auto cHybrid: *cOpticalGroup)
                {
                    for(auto cChip: *cHybrid)
                    {
                        if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2) continue;
                        LOG(INFO) << BOLDBLUE << "Generating Patterns needed for phase alignment of CIC inputs on FE#" << +cChip->getId() << RESET;
                        fReadoutChipInterface->producePhaseAlignmentPattern(cChip, 10);
                    } // ROCs
                }     // hybrids
            }         // links
        }             // boards
    }
}
void CicInputTester::StopInputPattern()
{
    if(fFrontEndType == FrontEndType::HYBRIDPS) StopInputPattern_PSHybrid();
}

// check
std::vector<CicInput> CicInputTester::CheckLines(Chip* pCic, uint8_t pPhyPort)
{
    auto startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    if(fVerbose == 1)
        LOG(INFO) << BOLDYELLOW << "At " << startTimeUTC_us << "...For CIC#" << +pCic->getId() << " in OG#" << +pCic->getOpticalGroupId() << " on Hybrid#" << +pCic->getHybridId() << " on BeBoard#"
                  << +pCic->getBeBoardId() << " . Checking sampling delays for stub lines connected to PhyPort#" << +pPhyPort << RESET;

    // get indices of hybrid + optical group
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    uint8_t cBoardIndx        = 0;
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != pCic->getBeBoardId()) continue;
        cBoardIndx = cBoard->getIndex();
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pCic->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pCic->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop
    // cic input lines map
    auto& cCicInputLinesOG = fCicInputLines.at(cBoardIndx)->at(cOpticalGroupIndx);
    auto& cCicInputLines   = cCicInputLinesOG->at(cHybridIndx);
    auto& cCicInputMap     = cCicInputLines->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
    // Debug interface
    fBeBoardInterface->setBoard(pCic->getBeBoardId());
    auto                  cInterface      = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cDebugFWInterface* cDebugInterface = cInterface->getDebugInterface();
    cDebugInterface->SetNIterations(10);
    fCicInterface->SelectMux(pCic, pPhyPort);
    std::vector<CicInput> cFailedCicInputs;

    for(auto& cCicInput: cCicInputMap[pPhyPort])
    {
        int nAttempts = 0;
        while((cCicInput.fNErrors > 0 && nAttempts < 5) || nAttempts == 0)
        {
            // check phase-alignment result
            auto cRslt             = cDebugInterface->CheckData(pCic, cCicInput.fBeLineIndx, cCicInput.fExpected);
            cCicInput.fData        = cRslt.fData;
            cCicInput.fNErrors     = cRslt.fErrors.size();
            cCicInput.fZeroToOne   = cRslt.fZeroToOne;
            cCicInput.fOneToZero   = cRslt.fOneToZero;
            cCicInput.fBitsChecked = cRslt.fBitsChecked;
            std::stringstream cOut;
            cOut << "\t..For a sampling phase (in the BE) of " << +cCicInput.fSamplingPhaseBE << " and a sampling phase (in the CIC) of " << +cCicInput.fSamplingPhaseCIC << " on line#"
                 << +cCicInput.fFeOut << " from FE#" << +cCicInput.fFe << " connected to PhyPort#" << +pPhyPort << " error check on " << cCicInput.fBitsChecked << " bits found "
                 << cCicInput.fZeroToOne << " 0-1 bit flips and " << cCicInput.fOneToZero << " 1-0 bit flips "
                 << " (total = " << cCicInput.fNErrors << " errors) on the SLVSLine#" << +cCicInput.fBeLineIndx << " pattern " << std::bitset<8>(fPattern) << "- Attempt number: "
                 << nAttempts
                 // << "\n" << cCicInput.fData ;
                 << RESET;
            // if( cCicInput.fNErrors != 0 ) LOG (INFO) << BOLDRED << cOut.str() << RESET;
            // if( fVerbose > 0  && cCicInput.fNErrors == 0 ) LOG (INFO) << BOLDGREEN << cOut.str() << RESET;
            if(cCicInput.fNErrors > 0) LOG(INFO) << BOLDRED << cOut.str() << RESET;
            nAttempts++;
        }
        if(cCicInput.fNErrors > cCicInput.fBitsChecked / 8) cFailedCicInputs.push_back(cCicInput);
    }
    fCicInterface->ControlMux(pCic, 0);
    return cFailedCicInputs;
}

void CicInputTester::UpdatePatterns()
{
    for(auto cBoard: *fDetectorContainer)
    {
        auto& cInputLines = fCicInputLines.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cInputLinesOG = cInputLines->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == nullptr) continue;

                auto& cInputLinesHybrd = cInputLinesOG->at(cHybrid->getIndex());
                auto& cCicInputMap     = cInputLinesHybrd->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
                for(auto& cMapItem: cCicInputMap) // map with phy ports
                {
                    for(auto& cCicInput: cMapItem.second) // inputs per phy port
                    {
                        // get sampling phase configured in CIC
                        cCicInput.fExpected = (fInputPatterns.size() == 1) ? fInputPatterns[0] : fInputPatterns[cCicInput.fFeOut];
                    } // inputs
                }     // phy-ports
            }         // hybrids
        }             // links
    }                 // boards
}
void CicInputTester::UpdateInputMap()
{
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        // Alignment interface
        auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
        cAlignerInterface->InitializeConfiguration();
        cAlignerInterface->InitializeAlignerObject();
        cAlignerInterface->EnablePrintout(false);
        AlignerObject cAlignerObjct;
        cAlignerObjct.fChip    = 0;
        cAlignerObjct.fOptical = cBoard->isOptical() ? 1 : 0;
        auto& cInputLines      = fCicInputLines.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cInputLinesOG = cInputLines->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == nullptr) continue;

                auto& cInputLinesHybrd = cInputLinesOG->at(cHybrid->getIndex());
                auto& cCicInputMap     = cInputLinesHybrd->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
                cAlignerObjct.fHybrid  = cCic->getHybridId();
                for(auto& cMapItem: cCicInputMap) // map with phy ports
                {
                    for(auto& cCicInput: cMapItem.second) // inputs per phy port
                    {
                        // get sampling phase configured in CIC
                        cCicInput.fSamplingPhaseCIC = fCicInterface->GetOptimalTap(cCic, cMapItem.first, cCicInput.fOut);
                        cAlignerObjct.fLine         = cCicInput.fBeLineIndx;
                        cCicInput.fExpected         = (fInputPatterns.size() == 1) ? fInputPatterns[0] : fInputPatterns[cCicInput.fFeOut];
                        LineConfiguration cLineCnfg;
                        // get sampling phase configured in the BE
                        auto cReply = cAlignerInterface->RetrieveConfig(cAlignerObjct, cLineCnfg);
                        if(cReply.fSuccess)
                            LOG(DEBUG) << BOLDGREEN << "\t\t... Retreival of alignment values SUCCEEDED on SLVSLine#" << +cCicInput.fBeLineIndx << RESET;
                        else
                            LOG(DEBUG) << BOLDRED << "\t\t... Retreival of alignment values FAILED on SLVSLine#" << +cCicInput.fBeLineIndx << RESET;
                        cCicInput.fSamplingPhaseBE = cReply.fSuccess ? cReply.fCnfg.fDelay : 0xFF;
                        cCicInput.fBitSlipBE       = cReply.fSuccess ? cReply.fCnfg.fBitslip : 0xFF;
                    } // inputs
                }     // phy-ports
            }         // hybrids
        }             // links
    }                 // boards
}
// manual scan 2D phase
void CicInputTester::CicInOffsetCheck(Chip* pCic, uint8_t pPhyPort, uint8_t pSamplingDelay, int pCicInputOffset)
{
    auto cBoardId = pCic->getBeBoardId();
    fBeBoardInterface->setBoard(cBoardId);
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });

    // get indices of hybrid + optical group
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != cBoardId) continue;
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pCic->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pCic->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop
    // cic input lines map
    auto& cCicInputLinesOG = fCicInputLines.at((*cBoardIter)->getIndex())->at(cOpticalGroupIndx);
    auto& cCicInputLines   = cCicInputLinesOG->at(cHybridIndx);
    auto& cCicInputMap     = cCicInputLines->getSummary<std::map<uint8_t, std::vector<CicInput>>>();

    // Alignment interface
    auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
    cAlignerInterface->InitializeConfiguration();
    cAlignerInterface->InitializeAlignerObject();
    cAlignerInterface->EnablePrintout(true);
    AlignerObject cAlignerObjct;
    cAlignerObjct.fChip    = 0;
    cAlignerObjct.fOptical = (*cBoardIter)->isOptical() ? 1 : 0;
    cAlignerObjct.fHybrid  = pCic->getHybridId();
    LineConfiguration cLineCnfg;
    // analogue mux from CIC connected to first 4 stub lines
    // LOG (INFO) << BOLDYELLOW << "Manual scan of sampling delay when looking at lines from PhyPort#" << +pPhyPort <<  " - sampling delay of " << pSamplingDelay << RESET;
    for(auto& cCicInput: cCicInputMap[pPhyPort])
    {
        cCicInput.fSamplingPhaseBE  = pSamplingDelay;
        cCicInput.fBitSlipBE        = 0;
        cCicInput.fSamplingPhaseCIC = fCicInterface->GetOptimalTap(pCic, pPhyPort, cCicInput.fOut) + pCicInputOffset;
    } // loop over stub lines from CIC
    CheckSamplingPhases(pCic, pPhyPort);
}

void CicInputTester::ManualPhaseCheck(Chip* pCic, uint8_t pPhyPort, uint8_t pSamplingDelay, uint8_t pCicInputDelay)
{
    auto cBoardId = pCic->getBeBoardId();
    fBeBoardInterface->setBoard(cBoardId);
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });

    // get indices of hybrid + optical group
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != cBoardId) continue;
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pCic->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pCic->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop
    // cic input lines map
    auto& cCicInputLinesOG = fCicInputLines.at((*cBoardIter)->getIndex())->at(cOpticalGroupIndx);
    auto& cCicInputLines   = cCicInputLinesOG->at(cHybridIndx);
    auto& cCicInputMap     = cCicInputLines->getSummary<std::map<uint8_t, std::vector<CicInput>>>();

    // Alignment interface
    auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
    cAlignerInterface->InitializeConfiguration();
    cAlignerInterface->InitializeAlignerObject();
    cAlignerInterface->EnablePrintout(true);
    AlignerObject cAlignerObjct;
    cAlignerObjct.fChip    = 0;
    cAlignerObjct.fOptical = (*cBoardIter)->isOptical() ? 1 : 0;
    cAlignerObjct.fHybrid  = pCic->getHybridId();
    LineConfiguration cLineCnfg;
    // analogue mux from CIC connected to first 4 stub lines
    // LOG (INFO) << BOLDYELLOW << "Manual scan of sampling delay when looking at lines from PhyPort#" << +pPhyPort <<  " - sampling delay of " << pSamplingDelay << RESET;
    for(auto& cCicInput: cCicInputMap[pPhyPort])
    {
        // fCicInterface->GetOptimalTap(pCic, pPhyPort, cCicInput.fOut)
        cCicInput.fSamplingPhaseBE  = pSamplingDelay;
        cCicInput.fBitSlipBE        = 0;
        cCicInput.fSamplingPhaseCIC = pCicInputDelay;
    } // loop over stub lines from CIC
    CheckSamplingPhases(pCic, pPhyPort);
}
void CicInputTester::Manual2DSCan(int pPhyPort)
{
    uint8_t pStart          = (pPhyPort < 0) ? 0 : pPhyPort;
    uint8_t pEnd            = (pPhyPort < 0) ? 11 : pPhyPort;
    auto    startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    LOG(INFO) << BOLDYELLOW << "CicInputTester::Manual2DSCan @ " << startTimeUTC_us << " started manual scan of sampling delays for "
              << " PhyPort(s) [ " << +pStart << " - " << +pEnd << " ] " << RESET;
    for(uint8_t cSamplingDelay = 0; cSamplingDelay <= 0x1F; cSamplingDelay++)
    {
        for(int cCicPhaseTapOffset = 0; cCicPhaseTapOffset <= 0; cCicPhaseTapOffset++)
        {
            auto currentTime_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
            LOG(INFO) << BOLDYELLOW << "\t\t.. BE-Sampling delay set to " << +cSamplingDelay << " CIC phase tap offset set to " << cCicPhaseTapOffset << " ... so far test has taken "
                      << (currentTime_us - startTimeUTC_us) * 1e-6 / 60. << " minutes to run" << RESET;
            DetectorDataContainer cTestData;
            ContainerFactory::copyAndInitChip<std::map<uint8_t, float>>(*fDetectorContainer, cTestData);
            for(auto cBoard: *fDetectorContainer)
            {
                auto& cErrorsThisBrd = cTestData.at(cBoard->getIndex());
                auto& cCicInputLines = fCicInputLines.at(cBoard->getIndex());
                for(auto cOpticalGroup: *cBoard)
                {
                    auto& cErrorsThisOG        = cErrorsThisBrd->at(cOpticalGroup->getIndex());
                    auto& cCicInputLinesThisOG = cCicInputLines->at(cOpticalGroup->getIndex());
                    for(auto cHybrid: *cOpticalGroup)
                    {
                        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                        if(cCic == nullptr) continue;
                        for(uint8_t cPhyPort = pStart; cPhyPort < pEnd + 1; cPhyPort++) { CicInOffsetCheck(cCic, cPhyPort, cSamplingDelay, cCicPhaseTapOffset); }
                        // now .. convert from phy-port to hybrid
                        auto  cMapping                = fCicInterface->getMapping(cCic);
                        auto& cErrorsThisHybrd        = cErrorsThisOG->at(cHybrid->getIndex());
                        auto& cCicInputLinesThisHybrd = cCicInputLinesThisOG->at(cHybrid->getIndex());
                        auto& cCicInputMap            = cCicInputLinesThisHybrd->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
                        for(auto cChip: *cHybrid)
                        {
                            if(cOpticalGroup->getFrontEndType() != FrontEndType::HYBRIDPS && (cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2))
                                continue;
                            auto& cErrorsThisChip = cErrorsThisHybrd->at(cChip->getIndex());
                            auto& cErrors         = cErrorsThisChip->getSummary<std::map<uint8_t, float>>();
                            for(auto cMapItem: cCicInputMap)
                            {
                                if(pPhyPort >= 0 && cMapItem.first != pPhyPort) continue;
                                for(auto cCicInput: cMapItem.second)
                                {
                                    if(cCicInput.fFe != cMapping[cChip->getId()]) continue;
                                    float             cBER = (float)cCicInput.fNErrors / cCicInput.fBitsChecked;
                                    std::stringstream cOut;
                                    cOut << "\t\t\tROC#" << +cChip->getId() << " CIC_FE#" << +cCicInput.fFe << " PhyPort#" << +cMapItem.first << " PhyPort_In#" << +cCicInput.fOut << " FE_SLVS_Out# "
                                         << +cCicInput.fFeOut << " BeLine#" << +cCicInput.fBeLineIndx << " found " << +cCicInput.fNErrors << " errors when checking " << +cCicInput.fBitsChecked
                                         << " bits on BeLine#" << +cCicInput.fBeLineIndx << " approximate error rate to be " << cBER;
                                    // if(cCicInput.fNErrors == 0) LOG(INFO) << BOLDGREEN << cOut.str() << RESET;
                                    if(cCicInput.fNErrors > cCicInput.fBitsChecked / 2) LOG(INFO) << BOLDRED << cOut.str() << RESET;
                                    cErrors[cCicInput.fFeOut] = cBER;
                                }
                            } // loop over phy ports
                        }     // all ROCs
                    }         // all hybrids
                }             // all OGs
            }                 // all boards
#ifdef __USE_ROOT__
            fDQMHistogram.fillManualPhaseScan(cSamplingDelay, cCicPhaseTapOffset, cTestData);
#endif
        } // phase taps
    }     // sampling delays
}
// auto align BE
void CicInputTester::BePhaseAlign(Chip* pCic, uint8_t pPhyPort)
{
    if(!fPhaseAlign)
    {
        // LOG(INFO) << BOLDRED << "CicInputTester will NOT use uDTC to find optimal sampling delay for stub lines"
        //           << " connected to PhyPort#" << +pPhyPort << " on CIC#" << +pCic->getId() << " in OG#" << +pCic->getOpticalGroupId() << " on Hybrid#" << +pCic->getHybridId() << " on BeBoard#"
        //           << +pCic->getBeBoardId() << RESET;
        return;
    }
    // LOG(INFO) << BOLDYELLOW << "CicInputTester WILL use uDTC to find optimal sampling delay for stub lines"
    //           << " connected to PhyPort#" << +pPhyPort << " on CIC#" << +pCic->getId() << " in OG#" << +pCic->getOpticalGroupId() << " on Hybrid#" << +pCic->getHybridId() << " on BeBoard#"
    //           << +pCic->getBeBoardId() << RESET;
    bool                cAllLinesAreAligned = false;
    size_t              nTries              = 0;
    std::vector<size_t> cLinesToAlign       = {1, 2, 3, 4};
    std::vector<bool>   cAlignedLines       = {false, false, false, false};

    while((nTries == 0) || (!cAllLinesAreAligned && (nTries < 5)))
    {
        LOG(INFO) << "\t"
                  << "BEPhaseAlign - Attempt " << nTries << RESET;
        auto cBoardId = pCic->getBeBoardId();
        fBeBoardInterface->setBoard(cBoardId);
        auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });

        // cic input lines map
        auto& cCicInputMap = fCicInputLines.getObject(cBoardId)->getObject(pCic->getOpticalGroupId())->getObject(pCic->getHybridId())->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
        // Alignment interface
        auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
        cAlignerInterface->InitializeConfiguration();
        cAlignerInterface->InitializeAlignerObject();
        cAlignerInterface->EnablePrintout((fVerbose == 1));
        AlignerObject cAlignerObjct;
        cAlignerObjct.fChip    = 0;
        cAlignerObjct.fOptical = (*cBoardIter)->isOptical() ? 1 : 0;
        cAlignerObjct.fHybrid  = pCic->getHybridId();
        LineConfiguration cLineCnfg;
        cLineCnfg.fPattern       = 0xAA;
        cLineCnfg.fPatternPeriod = 8;
        cLineCnfg.fBitslip       = 0;
        cLineCnfg.fDelay         = 0;
        fCicInterface->SelectMux(pCic, pPhyPort); // analogue mux from CIC connected to first 4 stub lines
        size_t cMaxIters = 10;
        size_t cMaxFails = 4;
        LOG(INFO) << BOLDYELLOW << "Auto-aligning sampling delay when looking at lines from PhyPort#" << +pPhyPort << RESET;
        // for(size_t cLineIndx = 1; cLineIndx < 5; cLineIndx++)
        for(auto& cLineIndx: cLinesToAlign)
        {
            if(cAlignedLines[cLineIndx - 1]) continue;
            cAlignerObjct.fLine         = cLineIndx;
            auto                 cReply = cAlignerInterface->TunePhase(cAlignerObjct, cLineCnfg);
            std::vector<uint8_t> cDelayHists(64, 0);
            std::stringstream    cMsg;
            cMsg << "SLVSLine#" << +cLineIndx << " -- PA will be run (" << cMaxIters << ") times";
            size_t cFails = 0;
            for(size_t cRep = 0; cRep < cMaxIters; cRep++)
            {
                if(cReply.fSuccess)
                {
                    cDelayHists[cReply.fCnfg.fDelay]++;
                    LOG(INFO) << BOLDGREEN << "\t\t Successful PA on Rep#" << cRep << " for tap value " << int(cReply.fCnfg.fDelay) << RESET;
                }
                else
                {
                    cMsg << BOLDRED << ".. Failed PA on Rep#" << cRep << "/" << cMaxIters << "\n";
                    cFails++;
                }
                if((cRep + 1) < cMaxIters) cReply = cAlignerInterface->TunePhase(cAlignerObjct, cLineCnfg);
            }
            auto cSum     = std::accumulate(cDelayHists.begin(), cDelayHists.end(), 0.);
            bool cSuccess = (cSum > 0 && cFails <= cMaxFails);
            auto cMode    = std::max_element(cDelayHists.begin(), cDelayHists.end()) - cDelayHists.begin();
            if(!cSuccess)
            {
                LOG(INFO) << cMsg.str() << RESET;
                LOG(WARNING) << BOLDRED << "\t\t\t...Auto-phase alignment FAILED on SLVSLine#" << +cLineIndx << RESET;
            }
            if(cSum > 0)
            {
                cLineCnfg.fDelay             = cMode;
                cAlignedLines[cLineIndx - 1] = true;
                LOG(INFO) << BOLDGREEN << "\t\t\t... Attempting recovery. Phase tap set to " << cMode << " for line: " << cLineIndx << RESET;
            }
            else
            {
                LOG(WARNING) << BOLDRED << "\t\t\t... Critical failure. Phase tap set to 0x08"
                             << " for line: " << cLineIndx << RESET;
                cLineCnfg.fDelay = 0x08;
            }

            for(auto& cItem: cCicInputMap)
            {
                if(cItem.first != pPhyPort) continue;
                cItem.second[cLineIndx - 1].fAutoAlignBE     = cSuccess;
                cItem.second[cLineIndx - 1].fSamplingPhaseBE = cLineCnfg.fDelay;
                cItem.second[cLineIndx - 1].fBitSlipBE       = 0;
            }

        } // PA all lines - first 4 stub lines from a CIC connected to analogue mux

        // update redo
        cAllLinesAreAligned = cAlignedLines[0];
        for(auto cLine: cAlignedLines) { cAllLinesAreAligned = cAllLinesAreAligned && cLine; }
        nTries++;
    }
    fCicInterface->ControlMux(pCic, 0);
}
void CicInputTester::CheckSamplingPhases(Chip* pCic)
{
    for(uint8_t cPhyPort = 0; cPhyPort < 12; cPhyPort++) { CheckSamplingPhases(pCic, cPhyPort); }
}
void CicInputTester::CheckSamplingPhases(Chip* pCic, uint8_t pPhyPort)
{
    auto cBoardId = pCic->getBeBoardId();
    fBeBoardInterface->setBoard(cBoardId);
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });

    auto& cCicInputMap = fCicInputLines.getObject((*cBoardIter)->getId())->getObject(pCic->getOpticalGroupId())->getObject(pCic->getHybridId())->getSummary<std::map<uint8_t, std::vector<CicInput>>>();

    auto& cLineConfigMap = fSLVSLineConfigs.getObject((*cBoardIter)->getId())->getObject(pCic->getOpticalGroupId())->getObject(pCic->getHybridId())->getSummary<std::map<uint8_t, LineConfiguration>>();

    // Alignment interface
    auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
    cAlignerInterface->InitializeConfiguration();
    cAlignerInterface->InitializeAlignerObject();
    cAlignerInterface->EnablePrintout((fVerbose == 1));
    AlignerObject cAlignerObjct;
    cAlignerObjct.fChip    = 0;
    cAlignerObjct.fOptical = (*cBoardIter)->isOptical() ? 1 : 0;
    cAlignerObjct.fHybrid  = pCic->getHybridId();
    LineConfiguration cLineCnfg;
    // analogue mux from CIC connected to first 4 stub lines
    std::vector<uint8_t> cPhases(0);
    for(auto& cCicInput: cCicInputMap[pPhyPort])
    {
        cPhases.push_back(fCicInterface->GetOptimalTap(pCic, pPhyPort, cCicInput.fOut)); // get sampling phase from CIC
        fCicInterface->SetPhaseTap(pCic, pPhyPort, cCicInput.fOut, cCicInput.fSamplingPhaseCIC);
        cAlignerObjct.fLine = cCicInput.fBeLineIndx;
        cLineCnfg.fDelay    = cCicInput.fSamplingPhaseBE;
        cLineCnfg.fBitslip  = cCicInput.fBitSlipBE;
        cAlignerInterface->ManuallyConfigureLine(cAlignerObjct, cLineCnfg);
        // LOG (INFO) << BOLDYELLOW << "PhyPort#" << +pPhyPort << "\tLine#" << +cAlignerObjct.fLine << "\tDelay" << +cLineCnfg.fDelay << RESET;
        // cLineStatus = cAlignerInterface->WordAlignLine(pChip, pLineId, pAlignmentPattern, pPeriod, pSamplingDelay, (*cBoardIter)->isOptical());

    } // loop over stub lines from CIC
    LOG(INFO) << "THIS IS THE LINE? " << RESET;
    CheckLines(pCic, pPhyPort);

    size_t cIndx = 0;
    // reconfigure back to original settings
    cAlignerInterface->EnablePrintout((fVerbose == 1));
    for(auto& cCicInput: cCicInputMap[pPhyPort])
    {
        cAlignerObjct.fLine = cCicInput.fBeLineIndx;
        cLineCnfg.fDelay    = cLineConfigMap[cAlignerObjct.fLine].fDelay;
        cLineCnfg.fBitslip  = cLineConfigMap[cAlignerObjct.fLine].fBitslip;
        cAlignerInterface->ManuallyConfigureLine(cAlignerObjct, cLineCnfg);
        LOG(INFO) << BOLDYELLOW << "Re-setting phases : CIC PhyPort#" << +pPhyPort << " Input#" << +cCicInput.fOut << " to " << +cPhases[cIndx] << RESET;
        fCicInterface->SetPhaseTap(pCic, pPhyPort, cCicInput.fOut, cPhases[cIndx]);
        cIndx++;
    }
}
void CicInputTester::ResetBe()
{
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        // Alignment interface
        auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
        cAlignerInterface->InitializeConfiguration();
        cAlignerInterface->InitializeAlignerObject();
        cAlignerInterface->EnablePrintout(false);
        AlignerObject cAlignerObjct;
        cAlignerObjct.fChip    = 0;
        cAlignerObjct.fOptical = cBoard->isOptical() ? 1 : 0;
        auto& cLineConfigs     = fSLVSLineConfigs.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cLineConfigsOG = cLineConfigs->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == nullptr) continue;

                auto& cLineConfigsHybrd = cLineConfigsOG->at(cHybrid->getIndex());
                auto& cLineConfigMap    = cLineConfigsHybrd->getSummary<std::map<uint8_t, LineConfiguration>>();
                for(uint8_t cLineIndx = 0; cLineIndx < 7; cLineIndx++)
                {
                    cAlignerObjct.fLine = cLineIndx;
                    LineConfiguration cLineCnfg;
                    cLineCnfg.fDelay   = cLineConfigMap[cAlignerObjct.fLine].fDelay;
                    cLineCnfg.fBitslip = cLineConfigMap[cAlignerObjct.fLine].fBitslip;
                    LOG(INFO) << BOLDYELLOW << "Re-setting Be-Sampling on SLVSLine#" << +cAlignerObjct.fLine << " to " << +cLineCnfg.fDelay << " , Be-bitSlip to " << +cLineCnfg.fBitslip << RESET;
                    cAlignerInterface->ManuallyConfigureLine(cAlignerObjct, cLineCnfg);
                } // SLVS lines
            }     // hybrids
        }         // links
    }             // boards
}
// Production State machine control functions
void CicInputTester::Running()
{
    auto startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    LOG(INFO) << BOLDYELLOW << "CicInputTester::Running" << RESET;
    // replace this with something that loads generic patterns per type
    std::vector<uint8_t> cTestPatterns{0xAA, 0xCC, 0x00, 0xFF};
    // std::vector<uint8_t> cTestPatterns{0xAA, 0x88, 0xCC, 0xDD, 0x00, 0xFF};
    int                                 cPatternId = 0;
    std::map<uint8_t, std::vector<int>> cFailureMap;
    DetectorDataContainer               cBadLines;
    ContainerFactory::copyAndInitHybrid<std::vector<uint32_t>>(*fDetectorContainer, cBadLines);
    for(auto cPattern: cTestPatterns)
    {
#ifdef __USE_ROOT__
        std::stringstream cPattern_str_hex;
        cPattern_str_hex << std::bitset<8>(cPattern);
        fResultFile->cd();
        std::string cParameter          = "Pattern";
        std::string cValue              = "";
        std::string cTitle              = "CICinTree0x" + cPattern_str_hex.str() + "_GoodLines";
        std::string cDesc               = "Lines passing CIC IN test for pattern 0x" + cPattern_str_hex.str();
        auto        cCICinGoodLinesTree = new TTree(cTitle.c_str(), cDesc.c_str());
        cCICinGoodLinesTree->Branch("Parameter", &cParameter);
        cCICinGoodLinesTree->Branch("Value", &cValue);
        cTitle                  = "CICinTree0x" + cPattern_str_hex.str() + "_BadLines";
        cDesc                   = "Lines failing CIC IN test for pattern 0x" + cPattern_str_hex.str();
        auto cCICinBadLinesTree = new TTree(cTitle.c_str(), cDesc.c_str());
        cCICinBadLinesTree->Branch("Parameter", &cParameter);
        cCICinBadLinesTree->Branch("Value", &cValue);
#endif
        std::vector<int> cFailingPorts;
        cFailingPorts.clear();
        std::vector<uint8_t> cAlignmentPattern;
        for(uint8_t cLineId = 0; cLineId < 6; cLineId++)
        {
            cAlignmentPattern.push_back(cPattern); // cWordAlignmentPatterns[cLineId]);
        }
        SetInputPatterns(cAlignmentPattern);
        if(cPatternId == 0) UpdateInputMap();
        UpdatePatterns();
        StartInputPattern();

        // either run BE alignment + check pattern in backend or
        // simply check pattern in the BE

        for(auto cBoard: *fDetectorContainer)
        {
            for(auto cOpticalGroup: *cBoard)
            {
                for(auto cHybrid: *cOpticalGroup)
                {
                    auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                    if(cCic == nullptr) continue;
                    for(uint8_t cPhyPort = 0; cPhyPort < 12; cPhyPort++)
                    {
                        // can replace this with a check for if pattern has enough 1's in an 8 bit period
                        // DEBUG 240124: Using the same pattern as the initial BE alignment for compatibility
                        if(cPattern == 0xAA)
                        {
                            BePhaseAlign(cCic, cPhyPort);
                            if(fVerify) CheckSamplingPhases(cCic, cPhyPort);
                        }
                        else
                            CheckSamplingPhases(cCic, cPhyPort);
                    } // loop over all phy-ports
                }     // all hybrids
            }         // all links
        }             // all boards

        // then fill containers used by DQMhistogrammer
        DetectorDataContainer cPhaseTapData, cSamplingData;
        ContainerFactory::copyAndInitHybrid<std::map<uint8_t, float>>(*fDetectorContainer, cPhaseTapData);
        ContainerFactory::copyAndInitHybrid<std::map<uint8_t, float>>(*fDetectorContainer, cSamplingData);
        DetectorDataContainer cAggrErrors, cAggrTxBits;
        ContainerFactory::copyAndInitHybrid<std::map<uint8_t, float>>(*fDetectorContainer, cAggrErrors);
        ContainerFactory::copyAndInitHybrid<std::map<uint8_t, float>>(*fDetectorContainer, cAggrTxBits);
        for(auto cBoard: *fDetectorContainer)
        {
            for(auto cOpticalGroup: *cBoard)
            {
                for(auto cHybrid: *cOpticalGroup)
                {
                    auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                    if(cCic == nullptr) continue;

                    auto& cTaps   = cPhaseTapData.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::map<uint8_t, float>>();
                    auto& cDelays = cSamplingData.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::map<uint8_t, float>>();
                    auto& cErrors = cAggrErrors.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::map<uint8_t, float>>();
                    auto& cTxBits = cAggrTxBits.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::map<uint8_t, float>>();
                    auto  cCicInputMap =
                        fCicInputLines.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
                    auto& cBadLineList = cBadLines.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::vector<uint32_t>>();
                    for(auto cMapItem: cCicInputMap)
                    {
                        cErrors.clear();
                        cTxBits.clear();
                        std::stringstream cHeader;
                        cHeader << "PhyPort#" << +cMapItem.first << " -- Pattern played on CIC inputs is " << std::bitset<8>(cPattern) << RESET;
                        for(auto cCicInput: cMapItem.second)
                        {
                            cTaps[cCicInput.fOut]          = (float)cCicInput.fSamplingPhaseCIC;
                            cDelays[cCicInput.fBeLineIndx] = (float)cCicInput.fSamplingPhaseBE;
                            cErrors[cCicInput.fBeLineIndx] = cCicInput.fNErrors;
                            cTxBits[cCicInput.fBeLineIndx] = cCicInput.fBitsChecked;
                            float             cBER         = (float)cCicInput.fNErrors / cCicInput.fBitsChecked;
                            std::stringstream cOut;
                            cOut << cHeader.str() << "\tCIC_FE#" << +cCicInput.fFe << " PhyPort_In#" << +cCicInput.fOut << " FE_SLVS_Out# " << +cCicInput.fFeOut << " BeLine#" << +cCicInput.fBeLineIndx
                                 << " Pattern on line " << std::bitset<8>(cCicInput.fExpected) << " found " << +cCicInput.fNErrors << " errors when checking " << +cCicInput.fBitsChecked
                                 << " bits on BeLine#" << +cCicInput.fBeLineIndx << " approximate error rate to be " << cBER;
#ifdef __USE_ROOT__
                            std::stringstream cSummary, cResult;
                            cSummary << "CIC_FE#" << +cCicInput.fFe << "_PhyPort#" << +cMapItem.first << "_PhyPortIn#" << +cCicInput.fOut << "_FESLVSOut# " << +cCicInput.fFeOut << "_BeLine#"
                                     << +cCicInput.fBeLineIndx << "_LineSummary";
                            cResult << "NErrors_" << +cCicInput.fNErrors << "_NBitsTx_" << +cCicInput.fBitsChecked << "_Data_" << cCicInput.fData;
                            fResultFile->cd();
                            cParameter = cSummary.str();
                            cValue     = cResult.str();
                            if(cCicInput.fNErrors <= cCicInput.fBitsChecked / 8)
                                cCICinGoodLinesTree->Fill();
                            else
                                cCICinBadLinesTree->Fill();
#endif
                            if(cCicInput.fNErrors <= cCicInput.fBitsChecked / 8) continue;
                            LOG(INFO) << BOLDRED << cOut.str() << RESET;
                            if(std::find(cFailingPorts.begin(), cFailingPorts.end(), cMapItem.first) != cFailingPorts.end()) continue;
                            cFailingPorts.push_back(cMapItem.first);
                            uint32_t cLineId = (cMapItem.first << 24) | (cCicInput.fFe << 16) | (cCicInput.fFeOut << 8) | cCicInput.fBeLineIndx;
                            if(std::find(cBadLineList.begin(), cBadLineList.end(), cLineId) == cBadLineList.end()) cBadLineList.push_back(cLineId);
                        }
                        // only look at alignment pattern here
                        if(cPatternId > 0) continue;
#ifdef __USE_ROOT__
                        fDQMHistogram.fillAlignmentValues(cMapItem.first, cPhaseTapData, cSamplingData);
                        fDQMHistogram.fillAggregatedPatternCheck(cMapItem.first, cAggrErrors, cAggrTxBits);
#endif
                    } // loop over phy ports
                }     // all hybrids
            }         // all OGs
        }             // all boards

        // fill per ROC
        DetectorDataContainer cTestData;
        ContainerFactory::copyAndInitChip<std::map<uint8_t, float>>(*fDetectorContainer, cTestData);
        for(auto cBoard: *fDetectorContainer)
        {
            for(auto cOpticalGroup: *cBoard)
            {
                for(auto cHybrid: *cOpticalGroup)
                {
                    auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                    if(cCic == nullptr) continue;

                    // now .. convert from phy-port to hybrid
                    auto  cMapping = fCicInterface->getMapping(cCic);
                    auto& cCicInputMap =
                        fCicInputLines.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
                    for(auto cChip: *cHybrid)
                    {
                        if(cOpticalGroup->getFrontEndType() != FrontEndType::HYBRIDPS && (cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2)) continue;
                        auto& cErrors =
                            cTestData.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getObject(cChip->getId())->getSummary<std::map<uint8_t, float>>();
                        for(auto cMapItem: cCicInputMap)
                        {
                            for(auto cCicInput: cMapItem.second)
                            {
                                if(cCicInput.fFe != cMapping[cChip->getId()]) continue;
                                float cBER                = (float)cCicInput.fNErrors / cCicInput.fBitsChecked;
                                cErrors[cCicInput.fFeOut] = cBER;
                            }
                        } // loop over phy ports
                    }     // all ROCs
                }         // all hybrids
            }             // all OGs
        }                 // all boards

        cFailureMap[cPattern] = cFailingPorts;
        if(cFailingPorts.size() == 0)
            LOG(INFO) << BOLDGREEN << "No failing PhyPorts found for test pattern " << std::bitset<8>(cPattern) << RESET;
        else
            LOG(INFO) << BOLDRED << cFailingPorts.size() << " failing PhyPorts found for test pattern " << std::bitset<8>(cPattern) << RESET;
#ifdef __USE_ROOT__
        fDQMHistogram.fillPatternCheck(cPatternId, cPattern, cTestData);
#endif
        cPatternId++;
    } // all patterns

    bool cCheck            = false;
    auto currentTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    auto cDeltaTime_us     = currentTimeUTC_us - startTimeUTC_us;
    LOG(INFO) << BOLDYELLOW << "Time to run complete CicInputTester  " << cDeltaTime_us * 1e-6 << " seconds" << RESET;
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == nullptr) continue;

                auto& cBadLineList = cBadLines.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::vector<uint32_t>>();
                LOG(INFO) << BOLDYELLOW << "*************************************** Bad CIC IN lines in the hybrid : " << cBadLineList.size() << " *************************************" << RESET;
#ifdef __USE_ROOT__
                fillSummaryTree("CIC IN bad lines", cBadLineList.size());
#endif
            }
        }
    }
    if(!cCheck) return;

    cPatternId = 0;
    for(auto cMapItem: cFailureMap)
    {
        if(cMapItem.first != 0xAA) continue;

        std::vector<uint8_t> cAlignmentPattern;
        for(uint8_t cLineId = 0; cLineId < 6; cLineId++) { cAlignmentPattern.push_back(cMapItem.first); }
        SetInputPatterns(cAlignmentPattern);
        UpdatePatterns();
        StartInputPattern();

        for(auto cPort: cMapItem.second)
        {
            LOG(INFO) << BOLDRED << "Line check failed for sone  line(s) on PhyPort#" << +cPort << " for pattern " << std::bitset<8>(cMapItem.first)
                      << " will run manual scan of phases to check offline" << RESET;
            Manual2DSCan(cPort);
        }
        cPatternId++;
    } // failing patterns

    if(cFailureMap.size() == 0) { Manual2DSCan(-1); }
}

void CicInputTester::Stop()
{
    LOG(INFO) << BOLDBLUE << "Stopping CIC HybriInput tester" << RESET;
    StopInputPattern();
    ResetBe();
    // writeObjects();
    // dumpConfigFiles();
    // Destroy();
}

void CicInputTester::Pause() {}

void CicInputTester::Resume() {}
