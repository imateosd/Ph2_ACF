#include "ShortFinder.h"
#include "../Utils/CBCChannelGroupHandler.h"
#include "../Utils/CommonVisitors.h"
#include "../Utils/ContainerFactory.h"
#include "../Utils/Occupancy.h"
#include "../Utils/SSAChannelGroupHandler.h"
#include "../Utils/ThresholdAndNoise.h"
#include "../Utils/Visitor.h"
#include "D19cFWInterface.h"
#include "D19cTriggerInterface.h"
// #ifdef __USE_ROOT__
// #include "../DQMUtils/DQMHistogramPedeNoise.h"
// #endif
using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

// initialize the static member

ShortFinder::ShortFinder() : Tool() {}

ShortFinder::~ShortFinder() {}
void ShortFinder::Reset()
{
    // set everything back to original values .. like I wasn't here
    for(auto cBoard: *fDetectorContainer)
    {
        BeBoard* theBoard = static_cast<BeBoard*>(cBoard);
        LOG(INFO) << BOLDBLUE << "Resetting all registers on back-end board " << +cBoard->getId() << RESET;
        auto&                                         cBeRegMap = fBoardRegContainer.at(cBoard->getIndex())->getSummary<BeBoardRegMap>();
        std::vector<std::pair<std::string, uint32_t>> cVecBeBoardRegs;
        cVecBeBoardRegs.clear();
        for(auto cReg: cBeRegMap) cVecBeBoardRegs.push_back(make_pair(cReg.first, cReg.second));
        fBeBoardInterface->WriteBoardMultReg(theBoard, cVecBeBoardRegs);

        auto& cRegMapThisBoard = fRegMapContainer.at(cBoard->getIndex());

        for(auto cOpticalGroup: *cBoard)
        {
            auto& cRegMapThisOpticalGroup = cRegMapThisBoard->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cRegMapThisHybrid = cRegMapThisOpticalGroup->at(cHybrid->getIndex());
                LOG(INFO) << BOLDBLUE << "Resetting all registers on readout chips connected to FEhybrid#" << (cHybrid->getId()) << " back to their original values..." << RESET;
                for(auto cChip: *cHybrid)
                {
                    auto&                                         cRegMapThisChip = cRegMapThisHybrid->at(cChip->getIndex())->getSummary<ChipRegMap>();
                    std::vector<std::pair<std::string, uint16_t>> cVecRegisters;
                    cVecRegisters.clear();
                    for(auto cReg: cRegMapThisChip) cVecRegisters.push_back(make_pair(cReg.first, cReg.second.fValue));
                    fReadoutChipInterface->WriteChipMultReg(static_cast<ReadoutChip*>(cChip), cVecRegisters);
                }
            }
        }
    }
    resetPointers();
}
void ShortFinder::Print()
{
    for(auto cBoard: *fDetectorContainer)
    {
        auto& cShorts = fShorts.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cShortsThisOpticalGroup = cShorts->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cShortsHybrid = cShortsThisOpticalGroup->at(cHybrid->getIndex());
                for(auto cChip: *cHybrid)
                {
                    if(cChip->getFrontEndType() != FrontEndType::SSA && cChip->getFrontEndType() != FrontEndType::SSA2 && cChip->getFrontEndType() != FrontEndType::CBC3) continue;

                    auto& cShortsReadoutChip = cShortsHybrid->at(cChip->getIndex())->getSummary<ChannelList>();
                    // remove duplicates
                    std::sort(cShortsReadoutChip.begin(), cShortsReadoutChip.end());
                    cShortsReadoutChip.erase(std::unique(cShortsReadoutChip.begin(), cShortsReadoutChip.end()), cShortsReadoutChip.end());

                    if(cShortsReadoutChip.size() == 0)
                        LOG(INFO) << BOLDGREEN << "No shorts found in readout chip" << +cChip->getId() << " on FE hybrid " << +cHybrid->getId() << RESET;
                    else
                        LOG(INFO) << BOLDRED << "Found " << +cShortsReadoutChip.size() << " shorts in readout chip" << +cChip->getId() << " on FE hybrid " << +cHybrid->getId() << RESET;

#ifdef __USE_ROOT__
                    std::string param = Form("Shorts_%d", cChip->getId());
                    fillSummaryTree(param, cShortsReadoutChip.size());
#endif
                    for(auto cShort: cShortsReadoutChip)
                        LOG(DEBUG) << BOLDRED << "Possible short in channel " << +cShort << " in readout chip" << +cChip->getId() << " on FE hybrid " << +cHybrid->getId() << RESET;
                }
            }
        }
    }
}
void ShortFinder::Initialise()
{
    ReadoutChip* cFirstReadoutChip = static_cast<ReadoutChip*>(fDetectorContainer->at(0)->at(0)->at(0)->at(0));
    fWithCBC                       = (cFirstReadoutChip->getFrontEndType() == FrontEndType::CBC3);
    fWithSSA                       = (cFirstReadoutChip->getFrontEndType() == FrontEndType::SSA || cFirstReadoutChip->getFrontEndType() == FrontEndType::SSA2);
    LOG(INFO) << "With SSA set to " << ((fWithSSA) ? 1 : 0) << RESET;

    if(fWithCBC)
    {
        CBCChannelGroupHandler theChannelGroupHandler;
        theChannelGroupHandler.setChannelGroupParameters(16, 2); // 16*2*8
        setChannelGroupHandler(theChannelGroupHandler);
    }
    if(fWithSSA)
    {
        for(auto cBoard: *fDetectorContainer)
        {
            auto cFrontEndTypes = cBoard->connectedFrontEndTypes();
            for(auto cFrontEndType: cFrontEndTypes)
            {
                uint16_t               cRowsToSkip        = 1;
                uint16_t               cColsToSkip        = 4;
                size_t                 cNClustersPerGroup = NSSACHANNELS / (cRowsToSkip * cColsToSkip);
                SSAChannelGroupHandler theChannelGroupHandler;
                theChannelGroupHandler.setChannelGroupParameters(cNClustersPerGroup, 1);
                setChannelGroupHandler(theChannelGroupHandler, cFrontEndType);
                LOG(INFO) << BOLDBLUE << "OpenFinder with SSAs : Inject every " << +cRowsToSkip << " row(s). " << RESET;
            }
        }
    }

    // now read the settings from the map
    auto cSetting       = fSettingsMap.find("Nevents");
    fEventsPerPoint     = (cSetting != std::end(fSettingsMap)) ? boost::any_cast<double>(cSetting->second) : 10;
    cSetting            = fSettingsMap.find("ShortsPulseAmplitude");
    fTestPulseAmplitude = (cSetting != std::end(fSettingsMap)) ? boost::any_cast<double>(cSetting->second) : 0;

    if(fTestPulseAmplitude == 0)
        fTestPulse = 0;
    else
        fTestPulse = 1;

    // prepare container
    ContainerFactory::copyAndInitChannel<uint16_t>(*fDetectorContainer, fShortsContainer);
    ContainerFactory::copyAndInitChannel<uint16_t>(*fDetectorContainer, fHitsContainer);
    ContainerFactory::copyAndInitStructure<ChannelList>(*fDetectorContainer, fShorts);
    ContainerFactory::copyAndInitStructure<ChannelList>(*fDetectorContainer, fInjections);

    // retreive original settings for all chips and all back-end boards
    ContainerFactory::copyAndInitStructure<ChipRegMap>(*fDetectorContainer, fRegMapContainer);
    ContainerFactory::copyAndInitStructure<BeBoardRegMap>(*fDetectorContainer, fBoardRegContainer);
    for(auto cBoard: *fDetectorContainer)
    {
        fBoardRegContainer.at(cBoard->getIndex())->getSummary<BeBoardRegMap>() = static_cast<BeBoard*>(cBoard)->getBeBoardRegMap();
        auto& cRegMapThisBoard                                                 = fRegMapContainer.at(cBoard->getIndex());
        auto& cShorts                                                          = fShorts.at(cBoard->getIndex());
        auto& cInjections                                                      = fInjections.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cShortsOpticalGroup     = cShorts->at(cOpticalGroup->getIndex());
            auto& cInjectionsOpticalGroup = cInjections->at(cOpticalGroup->getIndex());
            auto& cRegMapThisOpticalGroup = cRegMapThisBoard->at(cOpticalGroup->getIndex());

            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cShortsHybrid     = cShortsOpticalGroup->at(cHybrid->getIndex());
                auto& cInjectionsHybrid = cInjectionsOpticalGroup->at(cHybrid->getIndex());
                auto& cRegMapThisHybrid = cRegMapThisOpticalGroup->at(cHybrid->getIndex());
                for(auto cChip: *cHybrid)
                {
                    cInjectionsHybrid->at(cChip->getIndex())->getSummary<ChannelList>().clear();
                    cShortsHybrid->at(cChip->getIndex())->getSummary<ChannelList>().clear();
                    cRegMapThisHybrid->at(cChip->getIndex())->getSummary<ChipRegMap>() = static_cast<ReadoutChip*>(cChip)->getRegMap();
                }
            }
        }
    }

    // force PSAS event type
    fEventTypes.clear();
    for(auto cBoard: *fDetectorContainer)
    {
        if(!fWithSSA) continue;
        fEventTypes.push_back(cBoard->getEventType());
        cBoard->setEventType(EventType::PSAS);
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid) { fReadoutChipInterface->WriteChipReg(cChip, "AnalogueAsync", 1); }
            }
        }
    }
}
void ShortFinder::Stop() { this->Reset(); }
void ShortFinder::Count(BeBoard* pBoard, const std::shared_ptr<ChannelGroupBase> pGroup)
{
    auto  cBitset              = std::bitset<NCHANNELS>(std::static_pointer_cast<const ChannelGroup<NCHANNELS>>(pGroup)->getBitset());
    auto& cThisShortsContainer = fShortsContainer.at(pBoard->getIndex());
    auto& cThisHitsContainer   = fHitsContainer.at(pBoard->getIndex());
    auto& cShorts              = fShorts.at(pBoard->getIndex());
    auto& cInjections          = fInjections.at(pBoard->getIndex());

    for(auto cOpticalGroup: *pBoard)
    {
        auto& cOpticalGroupShorts     = cThisShortsContainer->at(cOpticalGroup->getIndex());
        auto& cOpticalGroupHits       = cThisHitsContainer->at(cOpticalGroup->getIndex());
        auto& cShortsOpticalGroup     = cShorts->at(cOpticalGroup->getIndex());
        auto& cInjectionsOpticalGroup = cInjections->at(cOpticalGroup->getIndex());

        for(auto cHybrid: *cOpticalGroup)
        {
            auto& cHybridShorts     = cOpticalGroupShorts->at(cHybrid->getIndex());
            auto& cHybridHits       = cOpticalGroupHits->at(cHybrid->getIndex());
            auto& cShortsHybrid     = cShortsOpticalGroup->at(cHybrid->getIndex());
            auto& cInjectionsHybrid = cInjectionsOpticalGroup->at(cHybrid->getIndex());
            for(auto cChip: *cHybrid)
            {
                auto& cReadoutChipShorts     = cHybridShorts->at(cChip->getIndex());
                auto& cReadoutChipHits       = cHybridHits->at(cChip->getIndex());
                auto& cShortsReadoutChip     = cShortsHybrid->at(cChip->getIndex())->getSummary<ChannelList>();
                auto& cInjectionsReadoutChip = cInjectionsHybrid->at(cChip->getIndex())->getSummary<ChannelList>();
                for(size_t cIndex = 0; cIndex < cBitset.size(); cIndex++)
                {
                    if(cBitset[cIndex] == 0 && cReadoutChipShorts->getChannelContainer<uint16_t>()->at(cIndex) > THRESHOLD_SHORT * fEventsPerPoint)
                    {
                        if(std::find(cShortsReadoutChip.begin(), cShortsReadoutChip.end(), cIndex) == cShortsReadoutChip.end())
                        {
                            cShortsReadoutChip.push_back(cIndex);
                            LOG(INFO) << BOLDRED << "Possible short in channel " << +cIndex << " on FE#" << +cChip->getId() << RESET;
                        }
                    }
                    if(cBitset[cIndex] == 1 && cReadoutChipHits->getChannelContainer<uint16_t>()->at(cIndex) == fEventsPerPoint) { cInjectionsReadoutChip.push_back(cIndex); }
                }

                if(cInjectionsReadoutChip.size() == 0)
                {
                    LOG(INFO) << BOLDRED << "Problem injecting charge in readout chip" << +cChip->getId() << " on FE hybrid " << +cHybrid->getId() << " .. STOPPING PROCEDURE!" << RESET;
                    // exit(FAILED_INJECTION);
                    // throw std::runtime_error( "Problem injecting charge in readout chip " + std::to_string(cChip->getId()) );
                }
            }
        }
    }
}
void ShortFinder::FindShortsPS(BeBoard* pBoard)
{
#ifdef __USE_ROOT__
    // create TTree for shorts: shortsTree
    auto fShortsTree = new TTree("shortsTree", "Shorted channels in the hybrid");
    // create variables for TTree branches
    TString              fShortsTreeParameter = -1;
    std::vector<uint8_t> fShortsTreeValue     = {};
    // create branches
    fShortsTree->Branch("Chip", &fShortsTreeParameter);
    fShortsTree->Branch("Value", &fShortsTreeValue);
#endif

    // make sure that the correct trigger source is enabled
    // async injection trigger
    std::vector<std::pair<std::string, uint32_t>> cRegVec;
    cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.trigger_source", 12}); // Trigger source? 10 or 12?
    cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.cal_pulse", 1});
    cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.ps_async_en.antenna", 0});
    cRegVec.push_back({"fc7_daq_ctrl.fast_command_block.control.load_config", 0x1});
    fBeBoardInterface->WriteBoardMultReg(pBoard, cRegVec);

    // set thresholds
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    bool cValidChip = cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2;
                    if(!cValidChip) continue;
                    auto cThreshold = fReadoutChipInterface->ReadChipReg(cChip, "Threshold");
#if defined(__USE_ROOT__)
                    double cPedeMean  = getSummaryParameter(Form("AvgPedeSSA%d", cChip->getId()));
                    double cNoiseMean = getSummaryParameter(Form("AvgNoiseSSA%d", cChip->getId()));
                    if(cPedeMean != -1.0)
                    {
                        LOG(INFO) << BOLDYELLOW << "SSA#" << +cChip->getId() << "\t Pedestal :" << cPedeMean << " , Noise " << cNoiseMean << RESET;
                        cThreshold               = (uint16_t)(cPedeMean + 5 * cNoiseMean);
                        std::string tmpParameter = "thresholdForShorts_" + std::to_string(cChip->getId());
                        fillSummaryTree(tmpParameter, cThreshold);
                    }
#endif
                    LOG(INFO) << BOLDBLUE << "Setting Threshold to " << cThreshold << " on SSA#" << +cChip->getId() << RESET;
                    fReadoutChipInterface->WriteChipReg(cChip, "Threshold", cThreshold);
                } // chips
            }     // hybrids
        }         // links
    }             // board

    // set injected charge to something
    setSameDacBeBoard(static_cast<BeBoard*>(pBoard), "InjectedCharge", 30);
    std::vector<int> cGrpIndcs(0);
    // find test groups
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            for(auto cChip: *cHybrid)
            {
                auto cChnlHndlr = getChannelGroupHandlerContainer()
                                      ->getObject(pBoard->getId())
                                      ->getObject(cOpticalGroup->getId())
                                      ->getObject(cHybrid->getId())
                                      ->getObject(cChip->getId())
                                      ->getSummary<std::shared_ptr<ChannelGroupHandler>>();
                for(int cGrpIndx = 0; cGrpIndx < cChnlHndlr->getNumberOfGroups(); cGrpIndx++)
                {
                    if(std::find(cGrpIndcs.begin(), cGrpIndcs.end(), cGrpIndx) == cGrpIndcs.end()) cGrpIndcs.push_back(cGrpIndx);
                } // grps
            }     // chips
        }         // hybrids
    }             // links

    for(auto cTstGrp: cGrpIndcs)
    {
        LOG(INFO) << BOLDYELLOW << "Looking for shorts by injecting in Grp#" << cTstGrp << RESET;
        for(auto cOpticalGroup: *pBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    if(cChip->getFrontEndType() == FrontEndType::MPA) continue;
                    if(cChip->getFrontEndType() == FrontEndType::MPA2) continue;
                    std::stringstream cOut;
                    auto              cChnlHndlr = getChannelGroupHandlerContainer()
                                          ->getObject(pBoard->getId())
                                          ->getObject(cOpticalGroup->getId())
                                          ->getObject(cHybrid->getId())
                                          ->getObject(cChip->getId())
                                          ->getSummary<std::shared_ptr<ChannelGroupHandler>>();

                    for(int cGrpIndx = 0; cGrpIndx < cChnlHndlr->getNumberOfGroups(); cGrpIndx++)
                    {
                        if(cGrpIndx != cTstGrp) continue;
                        auto             cTestGroup = cChnlHndlr->getTestGroup(cGrpIndx);
                        std::bitset<120> cBitset    = std::bitset<120>(std::static_pointer_cast<const ChannelGroup<120>>(cTestGroup)->getBitset());
                        // LOG (INFO) << BOLDYELLOW << "SSA#" << +cChip->getId() << " Group#" << cGrpIndx << ":" << std::bitset<NSSACHANNELS>(cBitset) << RESET;
                        std::vector<std::pair<std::string, uint16_t>> cRegItems;
                        for(uint32_t cChnl = 0; cChnl < cChip->size(); cChnl++)
                        {
                            std::stringstream cRegName;
                            cRegName << "ENFLAGS_S" << +(1 + cChnl);
                            uint8_t     cAnalogCalib = cBitset[cChnl];
                            ChipRegMask cMask;
                            cMask.fNbits    = 1;
                            cMask.fBitShift = 4;
                            cChip->setRegBits(cRegName.str(), cMask, cAnalogCalib);
                            auto cReg = cChip->getReg(cRegName.str());
                            cRegItems.push_back(std::make_pair(cRegName.str(), cReg));
                        }
                        fReadoutChipInterface->WriteChipMultReg(cChip, cRegItems);
                    }
                } // chips
            }     // hybrids
        }         // links

        DetectorDataContainer* cOccupancyContainer = new DetectorDataContainer();
        ContainerFactory::copyAndInitStructure<Occupancy>(*fDetectorContainer, *cOccupancyContainer);
        fDetectorDataContainer = cOccupancyContainer;
        measureBeBoardData(pBoard->getIndex(), fEventsPerPoint, -1);
        for(auto cOpticalGroup: *pBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    if(cChip->getFrontEndType() == FrontEndType::MPA) continue;
                    if(cChip->getFrontEndType() == FrontEndType::MPA2) continue;

                    auto cChnlHndlr = getChannelGroupHandlerContainer()
                                          ->getObject(pBoard->getId())
                                          ->getObject(cOpticalGroup->getId())
                                          ->getObject(cHybrid->getId())
                                          ->getObject(cChip->getId())
                                          ->getSummary<std::shared_ptr<ChannelGroupHandler>>();

                    if(cChnlHndlr->getNumberOfGroups() < cTstGrp) continue;

                    auto&            cShorts = fShorts.getObject(pBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getObject(cChip->getId())->getSummary<ChannelList>();
                    auto             cTestGroup = cChnlHndlr->getTestGroup(cTstGrp);
                    std::bitset<120> cBitset    = std::bitset<120>(std::static_pointer_cast<const ChannelGroup<120>>(cTestGroup)->getBitset());
                    auto             cNShorts   = cShorts.size();
                    std::stringstream cHitListOthers, cHitList;
                    cHitList << "Hits on SSA#" << +cChip->getId() << "\t";
                    cHitListOthers << "Shorts on SSA#" << +cChip->getId() << "\t";
                    for(uint32_t cChnl = 0; cChnl < cChip->size(); cChnl++)
                    {
                        float& cOcc = fDetectorDataContainer->getObject(pBoard->getId())
                                          ->getObject(cOpticalGroup->getId())
                                          ->getObject(cHybrid->getId())
                                          ->getObject(cChip->getId())
                                          ->getChannel<Occupancy>(cChnl)
                                          .fOccupancy;
                        if(cBitset[cChnl] == 1)
                        {
                            cHitList << " (" << +cChnl << ":" << cOcc << "), ";
                            continue;
                        }
                        if(cOcc > THRESHOLD_SHORT)
                        {
                            if(std::find(cShorts.begin(), cShorts.end(), cChnl) == cShorts.end()) cShorts.push_back(cChnl);
                        }
                    }
                    if(cShorts.size() != cNShorts)
                    {
                        for(auto cChnl: cShorts) cHitListOthers << +cChnl << ", ";
                        LOG(INFO) << BOLDRED << cHitListOthers.str() << RESET;
                    }
                } // chips
            }     // hybrids
        }         // optical groups
    }             // test groups
}
void ShortFinder::FindShorts2S(BeBoard* pBoard)
{
    bool originalAllChannelFlag = this->fAllChan;
    // find pedestal and noise
    // configure test pulse on chip
    setSameDacBeBoard(static_cast<BeBoard*>(pBoard), "TestPulsePotNodeSel", 0xFF - fTestPulseAmplitude);
    setSameDacBeBoard(static_cast<BeBoard*>(pBoard), "TestPulseDelay", 0);
    uint16_t cDelay = fBeBoardInterface->ReadBoardReg(pBoard, "fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_test_pulse") - 1;
    /*
    // find pedestal
    LOG(INFO) << BOLDYELLOW << "Find pedestal by setting L1A latency set to " << +(cDelay+20) << RESET;
    this->SetTestAllChannels(true);
    DetectorDataContainer theOccupancyContainer_offLatency;
    fDetectorDataContainer = &theOccupancyContainer_offLatency;
    setSameDacBeBoard(static_cast<BeBoard*>(pBoard), "TriggerLatency", cDelay+20);
    fBeBoardInterface->ChipReSync(pBoard);
    ContainerFactory::copyAndInitStructure<Occupancy>(*fDetectorContainer, *fDetectorDataContainer);
    this->bitWiseScan("Threshold", fEventsPerPoint, 0.56);
    std::vector<uint16_t> cTh; cTh.clear();
    for(auto cBoard: *fDetectorContainer)
    {
        std::vector<uint16_t> cBoardTh;
        for(auto cOpticalGroup: *cBoard)
        {
            std::vector<uint16_t> cOgTh;
            for(auto cHybrid: *cOpticalGroup)
            {
                std::vector<uint16_t> cHybTh;
                for(auto cChip: *cHybrid)
                {
                    auto cDAC = fReadoutChipInterface->ReadChipReg(cChip,"Threshold");
                    cHybTh.push_back(cDAC);
                }//chips
                float cMeanThreshold = std::accumulate(cHybTh.begin(), cHybTh.end(), 0.)/cHybTh.size();
                cOgTh.push_back((uint16_t)cMeanThreshold);
            }//hybrids
            float cMeanThreshold = std::accumulate(cOgTh.begin(), cOgTh.end(), 0.)/cOgTh.size();
            cBoardTh.push_back((uint16_t)cMeanThreshold);
        }
        float cMeanThreshold = std::accumulate(cBoardTh.begin(), cBoardTh.end(), 0.)/cBoardTh.size();
        cTh.push_back((uint16_t)cMeanThreshold);
    }
    float cMeanThreshold = std::accumulate(cTh.begin(), cTh.end(), 0.)/cTh.size();
    LOG (INFO) << BOLDYELLOW << "Average off-latency pedestal is " <<  cMeanThreshold << RESET;
    //scan threshold around pedestal + extract noise
    std::vector<uint16_t> cDacList; cDacList.clear();
    for(int cOffset=-15; cOffset < 15; cOffset++) cDacList.push_back( cMeanThreshold + cOffset );
    std::vector<DetectorDataContainer*>    cOccVector;
    fRecycleBin.setDetectorContainer(fDetectorContainer);
    for(auto cContainer: cOccVector) fRecycleBin.free(cContainer);
    cOccVector.clear();
    for(auto i = 0u; i < cDacList.size(); i++) cOccVector.push_back(fRecycleBin.get(&ContainerFactory::copyAndInitStructure<Occupancy>, Occupancy()));
    this->scanDac("Threshold", cDacList, fEventsPerPoint, cOccVector);
    auto cThresholdAndNoiseContainer = new DetectorDataContainer();
    ContainerFactory::copyAndInitStructure<ThresholdAndNoise>(*fDetectorContainer, *cThresholdAndNoiseContainer);
    for(auto cBoard: *fDetectorContainer)
    {
        auto& cThNoiseThisBrd = cThresholdAndNoiseContainer->at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cThNoiseThisOG = cThNoiseThisBrd->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cThNoiseThisHybrid = cThNoiseThisOG->at(cHybrid->getIndex());
                for(auto cChip: *cHybrid)
                {
                    auto&              cThNoiseThisChip = cThNoiseThisHybrid->at(cChip->getIndex());
                    std::vector<float> cPedestalsThisChip(0);
                    std::vector<float> cNoiseThisChip(0);
                    for(size_t cChnl = 0; cChnl < cChip->size(); cChnl++)
                    {
                        // S-curve for this channel
                        std::vector<float> cW(cDacList.size(), 0);
                        std::vector<float> cV(cDacList.size(), 0);
                        for(size_t cIndx = 0; cIndx < cDacList.size(); cIndx++)
                        {
                            auto& cDataThisBrd    = cOccVector[cIndx]->at(cBoard->getIndex());
                            auto& cDataThisOG     = cDataThisBrd->at(cOpticalGroup->getIndex());
                            auto& cDataThisHybrid = cDataThisOG->at(cHybrid->getIndex());
                            auto& cDataThisChip   = cDataThisHybrid->at(cChip->getIndex());
                            cW[cIndx]             = cDataThisChip->getChannel<Occupancy>(cChnl).fOccupancy;
                            cV[cIndx]             = cDacList[cIndx];
                        }
                        auto cPedeNoise                                                   = evalNoise(cW, cV, true);
                        cThNoiseThisChip->getChannel<ThresholdAndNoise>(cChnl).fThreshold = cPedeNoise.first;
                        cThNoiseThisChip->getChannel<ThresholdAndNoise>(cChnl).fNoise     = cPedeNoise.second;
                        cPedestalsThisChip.push_back(cThNoiseThisChip->getChannel<ThresholdAndNoise>(cChnl).fThreshold);
                        cNoiseThisChip.push_back(cThNoiseThisChip->getChannel<ThresholdAndNoise>(cChnl).fNoise);
                        // if( cChnl%25 == 0 )
                        //     LOG (INFO) << BOLDMAGENTA << "\t\t... channel#" << +cChnl
                        //         << " pedestal is " << cThNoiseThisChip->getChannel<ThresholdAndNoise>(cChnl).fThreshold
                        //         << " noise is " << cThNoiseThisChip->getChannel<ThresholdAndNoise>(cChnl).fNoise
                        //         << RESET;
                    } // chnl loop
                    float cMeanNoise = std::accumulate(cNoiseThisChip.begin(),cNoiseThisChip.end(),0.)/cNoiseThisChip.size();
                    float cMeanPed = std::accumulate(cPedestalsThisChip.begin(),cPedestalsThisChip.end(),0.)/cPedestalsThisChip.size();
                    float cTargetTh = cMeanPed - 5.0*cMeanNoise;
                    fReadoutChipInterface->WriteChipReg(cChip,"Threshold",cTargetTh);
                    LOG (INFO) << BOLDYELLOW << "Cbc#" << +cChip->getId() << " target Th " << cTargetTh
                        << " mean pedestal is " <<  cMeanPed << " mean noise is " << cMeanNoise
                        << RESET;
                }
            }
        }
    }
    */

    // now on latency ... bitwise scan of TP
    LOG(INFO) << BOLDYELLOW << "Find <InjCharge> by setting L1A latency set to " << +(cDelay) << RESET;
    this->SetTestAllChannels(false);
    setSameDacBeBoard(static_cast<BeBoard*>(pBoard), "TriggerLatency", cDelay);
    fBeBoardInterface->ChipReSync(pBoard);
    DetectorDataContainer theOccupancyContainer;
    fDetectorDataContainer = &theOccupancyContainer;
    ContainerFactory::copyAndInitStructure<Occupancy>(*fDetectorContainer, *fDetectorDataContainer);
    this->bitWiseScan("TestPulsePotNodeSel", fEventsPerPoint, 0.96);
    std::vector<uint16_t> cInjCharge;
    for(auto cBoard: *fDetectorContainer)
    {
        // auto& cThNoiseThisBrd = cThresholdAndNoiseContainer->at(cBoard->getIndex());
        std::vector<uint8_t> cBrdInj;
        for(auto cOpticalGroup: *cBoard)
        {
            // auto& cThNoiseThisOG = cThNoiseThisBrd->at(cOpticalGroup->getIndex());
            std::vector<uint8_t> cOgInj;
            for(auto cHybrid: *cOpticalGroup)
            {
                // auto& cThNoiseThisHybrid = cThNoiseThisOG->at(cHybrid->getIndex());
                std::vector<uint8_t> cHybInj;
                for(auto cChip: *cHybrid)
                {
                    auto cTh  = fReadoutChipInterface->ReadChipReg(cChip, "Threshold");
                    auto cDAC = fReadoutChipInterface->ReadChipReg(cChip, "TestPulsePotNodeSel");
                    cDAC      = cDAC - 50;
                    fReadoutChipInterface->WriteChipReg(cChip, "TestPulsePotNodeSel", cDAC);
                    cHybInj.push_back(cDAC);
                    // auto&              cThNoiseThisChip = cThNoiseThisHybrid->at(cChip->getIndex());
                    // std::vector<float> cNoiseThisChip(0);
                    // for(size_t cChnl = 0; cChnl < cChip->size(); cChnl++)
                    // {
                    //     cNoiseThisChip.push_back(cThNoiseThisChip->getChannel<ThresholdAndNoise>(cChnl).fNoise);
                    // } // chnl loop
                    // float cMeanNoise = std::accumulate(cNoiseThisChip.begin(),cNoiseThisChip.end(),0.)/cNoiseThisChip.size();
                    // float cSetPoint = cTh + 3*cMeanNoise;
                    LOG(INFO) << BOLDYELLOW << "Cbc#" << +cChip->getId() << " threshold set to " << cTh << " bitwise scan injected charge finds 96% occ @ " << cDAC + 10
                              << " injected charge will be set to "
                              << cDAC
                              // << " mean noise is " << cMeanNoise
                              // << " will set threshold to " << cSetPoint
                              << RESET;
                    // fReadoutChipInterface->WriteChipReg(cChip,"Threshold",cSetPoint);
                }
                float cAvgInj = std::accumulate(cHybInj.begin(), cHybInj.end(), 0.) / cHybInj.size();
                cOgInj.push_back(cAvgInj);
            }
            float cAvgInj = std::accumulate(cOgInj.begin(), cOgInj.end(), 0.) / cOgInj.size();
            cBrdInj.push_back(cAvgInj);
        }
        float cAvgInj = std::accumulate(cBrdInj.begin(), cBrdInj.end(), 0.) / cBrdInj.size();
        cInjCharge.push_back(cAvgInj);
    }
    uint8_t cAvgInj = (uint8_t)(std::accumulate(cInjCharge.begin(), cInjCharge.end(), 0.) / cInjCharge.size());
    LOG(INFO) << BOLDYELLOW << "Average injected charge 0x" << std::hex << +cAvgInj << std::dec << " DAC units." << RESET;
    // scan cal-dac around pedestal + extract noise
    /*
    cDacList.clear();
    for(int cOffset=-255; cOffset < 255; cOffset++){
        if(  cAvgInj + cOffset < 0 ) continue;
        if(  cAvgInj + cOffset > 255 ) continue;

        cDacList.push_back( cAvgInj + cOffset );
    }
    fRecycleBin.setDetectorContainer(fDetectorContainer);
    for(auto cContainer: cOccVector) fRecycleBin.free(cContainer);
    cOccVector.clear();
    for(auto i = 0u; i < cDacList.size(); i++) cOccVector.push_back(fRecycleBin.get(&ContainerFactory::copyAndInitStructure<Occupancy>, Occupancy()));
    // scan dac in latched mode
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    static_cast<CbcInterface*>(fReadoutChipInterface)->selectLogicMode(cChip,"Latched",true,true);
                }
            }
        }
    }
    this->scanDac("TestPulsePotNodeSel", cDacList, fEventsPerPoint, cOccVector);
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    static_cast<CbcInterface*>(fReadoutChipInterface)->selectLogicMode(cChip,"Sampled",true,true);
                }
            }
        }
    }auto cThNoiseCalDAC = new DetectorDataContainer();
    ContainerFactory::copyAndInitStructure<ThresholdAndNoise>(*fDetectorContainer, *cThNoiseCalDAC);
    for(auto cBoard: *fDetectorContainer)
    {
        auto& cThNoiseThisBrd = cThNoiseCalDAC->at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cThNoiseThisOG = cThNoiseThisBrd->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cThNoiseThisHybrid = cThNoiseThisOG->at(cHybrid->getIndex());
                for(auto cChip: *cHybrid)
                {
                    auto&              cThNoiseThisChip = cThNoiseThisHybrid->at(cChip->getIndex());
                    std::vector<float> cPedestalsThisChip(0);
                    std::vector<float> cNoiseThisChip(0);
                    for(size_t cChnl = 0; cChnl < cChip->size(); cChnl++)
                    {
                        // S-curve for this channel
                        std::vector<float> cW(cDacList.size(), 0);
                        std::vector<float> cV(cDacList.size(), 0);
                        for(size_t cIndx = 0; cIndx < cDacList.size(); cIndx++)
                        {
                            auto& cDataThisBrd    = cOccVector[cIndx]->at(cBoard->getIndex());
                            auto& cDataThisOG     = cDataThisBrd->at(cOpticalGroup->getIndex());
                            auto& cDataThisHybrid = cDataThisOG->at(cHybrid->getIndex());
                            auto& cDataThisChip   = cDataThisHybrid->at(cChip->getIndex());
                            cW[cIndx]             = cDataThisChip->getChannel<Occupancy>(cChnl).fOccupancy;
                            cV[cIndx]             = 0xFF-cDacList[cIndx];
                            if(cChnl==100) LOG (INFO) << BOLDYELLOW << "Injected charge = " << cV[cIndx] << "\t" << cW[cIndx]  << RESET;
                        }
                        auto cPedeNoise                                                   = evalNoise(cW, cV, false);
                        cThNoiseThisChip->getChannel<ThresholdAndNoise>(cChnl).fThreshold = cPedeNoise.first;
                        cThNoiseThisChip->getChannel<ThresholdAndNoise>(cChnl).fNoise     = cPedeNoise.second;
                        cPedestalsThisChip.push_back(cThNoiseThisChip->getChannel<ThresholdAndNoise>(cChnl).fThreshold);
                        cNoiseThisChip.push_back(cThNoiseThisChip->getChannel<ThresholdAndNoise>(cChnl).fNoise);
                    } // chnl loop
                    float cMeanNoise = std::accumulate(cNoiseThisChip.begin(),cNoiseThisChip.end(),0.)/cNoiseThisChip.size();
                    float cMeanNoiseElec = cMeanNoise*0.086*1e-15/1.6021766e-19;
                    float cMeanPed = std::accumulate(cPedestalsThisChip.begin(),cPedestalsThisChip.end(),0.)/cPedestalsThisChip.size();
                    LOG (INFO) << BOLDYELLOW << "Cbc#" << +cChip->getId() << " mean pedestal is " <<  0xFF-cMeanPed << " mean noise is " << cMeanNoiseElec << " electrons." << RESET;
                }
            }
        }
    }
    #ifdef __USE_ROOT__
        DQMHistogramPedeNoise cDQMHistogramPedeNoise;
        cDQMHistogramPedeNoise.book(fResultFile, *fDetectorContainer, fSettingsMap);
        size_t cIndx=0;
        for(auto cContainer: cOccVector)
        {
            cDQMHistogramPedeNoise.fillSCurvePlots(cDacList[cIndx], 0, *cContainer);
            cIndx++;
        }
        cDQMHistogramPedeNoise.process();
    #endif
    */

    // now .. look for shorts
    int cFirstBrd   = -1;
    int cFirstLink  = -1;
    int cFirstHybrd = -1;
    int cFirstChip  = -1;
    for(const auto& cBoard: *fDetectorContainer)
    {
        if(cFirstBrd == -1) cFirstBrd = cBoard->getId();
        for(const auto& cOpticalGroup: *cBoard)
        {
            if(cFirstLink == -1) cFirstLink = cOpticalGroup->getId();
            for(const auto& cHybrid: *cOpticalGroup)
            {
                if(cFirstHybrd == -1) cFirstHybrd = cHybrid->getId();
                for(const auto& cChip: *cHybrid)
                {
                    if(cFirstChip == -1) cFirstChip = cChip->getId();
                } // chips
            }     // hybrids
        }         // links
    }             // boards
    auto cChnlHndlr = getChannelGroupHandlerContainer()->getObject(cFirstBrd)->getObject(cFirstLink)->getObject(cFirstHybrd)->getObject(cFirstChip)->getSummary<std::shared_ptr<ChannelGroupHandler>>();
    LOG(INFO) << BOLDBLUE << "Starting short finding loop for 2S hybrid " << cChnlHndlr->getNumberOfGroups() << " groups." << RESET;
    bool   cMaskChannelsFromOtherGroups = false;
    size_t cTestGroup                   = 0;
    for(auto cGroup: *cChnlHndlr.get())
    {
        // inject in each cbc
        for(auto cGroup: *getChannelGroupHandlerContainer()->at(0)->at(0)->at(0)->at(0)->getSummary<std::shared_ptr<ChannelGroupHandler>>().get())
        {
            for(auto cOpticalGroup: *pBoard)
            {
                for(auto cHybrid: *cOpticalGroup)
                {
                    for(auto cChip: *cHybrid)
                    {
                        fReadoutChipInterface->enableInjection(cChip, true);
                        fReadoutChipInterface->maskChannelsAndSetInjectionSchema(cChip, cGroup, cMaskChannelsFromOtherGroups, true);
                    }
                }
            }
        }

        // bitset for this group
        LOG(INFO) << BOLDBLUE << "Injecting charge into CBCs using test capacitor " << +cTestGroup << RESET;
        auto cBitset = std::bitset<NCHANNELS>(std::static_pointer_cast<const ChannelGroup<NCHANNELS>>(cGroup)->getBitset());
        LOG(DEBUG) << BOLDBLUE << "Test pulse channel mask is " << cBitset << RESET;

        auto& cThisShortsContainer = fShortsContainer.at(pBoard->getIndex());
        auto& cThisHitsContainer   = fHitsContainer.at(pBoard->getIndex());

        this->ReadNEvents(pBoard, fEventsPerPoint);
        const std::vector<Event*>& cEvents = this->GetEvents();
        for(auto cEvent: cEvents)
        {
            auto cEventCount = cEvent->GetEventCount();
            for(auto cOpticalGroup: *pBoard)
            {
                auto& cShortsContainer = cThisShortsContainer->at(cOpticalGroup->getIndex());
                auto& cHitsContainer   = cThisHitsContainer->at(cOpticalGroup->getIndex());

                for(auto cHybrid: *cOpticalGroup)
                {
                    auto& cHybridShorts = cShortsContainer->at(cHybrid->getIndex());
                    auto& cHybridHits   = cHitsContainer->at(cHybrid->getIndex());
                    for(auto cChip: *cHybrid)
                    {
                        auto& cReadoutChipShorts = cHybridShorts->at(cChip->getIndex());
                        auto& cReadoutChipHits   = cHybridHits->at(cChip->getIndex());

                        auto cHits = cEvent->GetHits(cHybrid->getId(), cChip->getId());
                        LOG(DEBUG) << BOLDBLUE << "\t\tGroup " << +cTestGroup << " FE" << +cHybrid->getId() << " .. CBC" << +cChip->getId() << ".. Event " << +cEventCount << " - " << +cHits.size()
                                   << " hits found/" << +cBitset.count() << " channels in test group" << RESET;
                        for(auto cHit: cHits)
                        {
                            if(cBitset[cHit] == 0)
                                cReadoutChipShorts->getChannelContainer<uint16_t>()->at(cHit) += 1;
                            else
                                cReadoutChipHits->getChannelContainer<uint16_t>()->at(cHit) += 1;
                        }
                    }
                }
            }
        }
        this->Count(pBoard, std::static_pointer_cast<ChannelGroup<NCHANNELS>>(cGroup));
        cTestGroup++;
    }
    this->SetTestAllChannels(originalAllChannelFlag);
}
void ShortFinder::FindShorts()
{
    // set-up for TP
    fAllChan                     = true;
    fMaskChannelsFromOtherGroups = !this->fAllChan;
    SetTestAllChannels(fAllChan);
    // enable TP injection
    enableTestPulse(true);

    TestPulseTriggerConfiguration cTPCnfg;
    cTPCnfg.fDelayAfterFastReset = 100;
    cTPCnfg.fDelayAfterTP        = 100;
    cTPCnfg.fDelayBeforeNextTP   = 200;
    cTPCnfg.fEnableFastReset     = 0;
    cTPCnfg.fEnableTP            = 1;
    cTPCnfg.fEnableL1A           = 1;
    // configure test pulse trigger
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        auto                  cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        D19cTriggerInterface* cTriggerInterface = dynamic_cast<D19cTriggerInterface*>(cInterface->getTriggerInterface());
        cTriggerInterface->ConfigureTestPulseFSM(cTPCnfg);
        LOG(INFO) << BOLDBLUE << "Starting short finding procedure on BeBoard#" << +cBoard->getIndex() << RESET;
        if(fWithCBC)
            this->FindShorts2S(static_cast<BeBoard*>(cBoard));
        else if(fWithSSA)
            this->FindShortsPS(static_cast<BeBoard*>(cBoard));
        else
            LOG(INFO) << BOLDRED << "\t....Short finding for this hybrid type not yet implemented." << RESET;
    }
}
void ShortFinder::Running()
{
    // Initialise();
    this->FindShorts();
    this->Print();
}
