/*!
 *
 * \file ResetTester.h
 * \brief ResetTester class
 * \author Sarah Seif El Nasr-Storey
 * \date 15/03/23
 *
 */

#ifndef ResetTester_h__
#define ResetTester_h__

#include "Tool.h"
#include <map>

using namespace Ph2_HwDescription;
typedef std::map<std::string, std::string> ResetList;
class ResetTester : public Tool
{
  public:
    ResetTester();
    ~ResetTester();

    void Initialize(void);

    // State machine
    void Running() override;
    void Stop() override;
    void Pause() override;
    void Resume() override;
    void GetSummary();

  private:
    // member functions
    DetectorDataContainer fReadCheckResult;
    bool                  ReadCheckROCs(Ph2_HwDescription::BeBoard* pBoard);
    bool                  ReadCheckCICs(Ph2_HwDescription::BeBoard* pBoard);

    // default register values
    std::map<std::string, uint8_t> fDefRegisterValues{{
                                                          "CIC_FE_ENABLE",
                                                          0xFF,
                                                      },
                                                      {"CBC_VCth2", 0x02},
                                                      {"SSA_Bias_D5DAC8", 0xf},
                                                      {"MPA_A0", 0x0F}};
    // timing
    std::chrono::seconds::rep fStartTime;
    std::chrono::seconds::rep fStopTime;
};
#endif
