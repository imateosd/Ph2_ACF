/*!
 *
 * \file Controller for PS hybrid test card
 *
 * \Support :
 *
 */

#if defined(__TCUSB__)
// #define 2SHYBRIDMAXV 1.32
#include "../Utils/ConsoleColor.h"
#include "../Utils/Utilities.h"
#include "../Utils/easylogging++.h"
#include "USB_a.h"
#include <map>
#include <stdint.h>
#include <string>

#ifndef TestCardControl2S_h__
#define TestCardControl2Ss_h__

class TC_PSFE;
class TestCardControl2S
{
  public:
    TestCardControl2S();
    ~TestCardControl2S();

    void Initialise(uint32_t pUsbBus, uint8_t pUsbDev);
    //
    void SetDefaultHybridVoltage();
    void ReadHybridVoltage(const std::string& pVoltageName, bool pStoreMeasurement = false);
    void ReadHybridCurrent(const std::string& pCurrentName, bool pStoreMeasurement = false);
    //
    void ConfigureUsb(uint32_t pUsbBus, uint8_t pUsbDev)
    {
        LOG(INFO) << BOLDYELLOW << "Configuring usb : "
                  << " Bus " << pUsbBus << " Device " << +pUsbDev << RESET;
        fUsbBus = pUsbBus;
        fUsbDev = pUsbDev;
    }
    std::map<std::string, std::pair<float, float>>                        GetMeasurements() { return fMeasurementsToStore; }
    std::pair<std::map<std::string, float>, std::map<std::string, float>> GetNominalValues()
    {
        std::pair<std::map<std::string, float>, std::map<std::string, float>> cNominalValues = {fNominalMeanValues, fNominalSigmaValues};
        return cNominalValues;
    }

  private:
    // Maps for electrical measurements on the hybrid:
    std::map<std::string, TC_2SFE_V2::measurement> fHybridVoltageMap = {{"Hybrid1V25", TC_2SFE_V2::measurement::_1V25_HYB},
                                                                        {"Hybrid1V25_reg", TC_2SFE_V2::measurement::_1V25_REG},
                                                                        {"Thermistor", TC_2SFE_V2::measurement::THERM_SENSE},
                                                                        {"TestCard1V4", TC_2SFE_V2::measurement::_1V4},
                                                                        {"TestCard3V3", TC_2SFE_V2::measurement::_3V3},
                                                                        {"AnalogueMux", TC_2SFE_V2::measurement::AMUX},
                                                                        {"AntennaPullup", TC_2SFE_V2::measurement::ANT_PULL}};
    std::map<std::string, TC_2SFE_V2::measurement> fHybridCurrentMap = {{"Hybrid1V40_current", TC_2SFE_V2::measurement::ISEN_1V4}, {"TestCard3V30_current", TC_2SFE_V2::measurement::ISEN_3V3}};

    std::map<std::string, float> fNominalMeanValues = {{"Hybrid1V25", 1236.4310341121266},
                                                       {"Hybrid1V25_reg", 1244.1210737888168},
                                                       {"TestCard1V4", 1675.8424141362029}, //?
                                                       {"TestCard3V3", 3323.8143318224252}, //?
                                                       {"Hybrid1V40_current", 255.97162774892954},
                                                       {"TestCard3V30_current", 194.86636225382486}};

    std::map<std::string, float> fNominalSigmaValues = {{"Hybrid1V25", 3.0473147263492248},
                                                        {"Hybrid1V25_reg", 2.7165582600487719},
                                                        {"TestCard1V4", 18.18226563265258},
                                                        {"TestCard3V3", 26.856663990799181},
                                                        {"Hybrid1V40_current", 18.27460612901044},
                                                        {"TestCard3V30_current", 16.117206716295954}};

    std::map<std::string, std::pair<float, float>> fMeasurementsToStore;

    int                     fVoltageMeasurementWait_ms = 100;
    int                     fNreadings                 = 3;
    std::pair<float, float> fVoltageMeasurement;
    std::pair<float, float> fCurrentMeasurement;
    uint32_t                fUsbBus;
    uint8_t                 fUsbDev;
};
#endif
#endif
