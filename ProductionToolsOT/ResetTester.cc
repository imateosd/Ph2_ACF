#include "ResetTester.h"
#include "RegisterTester.h"
using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

ResetTester::ResetTester() : Tool() {}

ResetTester::~ResetTester() {}

void ResetTester::Initialize(void)
{
    // read back original masks
    fStartTime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    LOG(INFO) << BOLDMAGENTA << "ResetTester::Initialise at " << fStartTime << " s from epoch." << RESET;

    for(const auto& cBoard: *this->fDetectorContainer)
    {
        for(const auto& cOpticalGroup: *cBoard)
        {
            for(const auto& cHybrid: *cOpticalGroup)
            {
                for(const auto& cChip: *cHybrid) { LOG(INFO) << BOLDYELLOW << "ResetTester Test ROC#" << +cChip->getId() << RESET; }
            }
        }
    }
}
// verify that read-back values match expected defaults on ROCs
bool ResetTester::ReadCheckROCs(BeBoard* pBoard)
{
    LOG(INFO) << BOLDYELLOW << "ResetTester::ReadCheckROCs BeBoard#" << +pBoard->getId() << RESET;
    for(const auto& cBoard: *fDetectorContainer)
    {
        if(cBoard->isOptical() == 0)
        {
            fBeBoardInterface->ChipReset(cBoard);
            continue;
        }
        for(const auto& cOpticalGroup: *cBoard)
        {
            auto& clpGBT = cOpticalGroup->flpGBT;
            if(clpGBT == nullptr) continue;
            auto cInterface = static_cast<D19clpGBTInterface*>(flpGBTInterface);
            for(const auto& cHybrid: *cOpticalGroup)
            {
                if(cOpticalGroup->getFrontEndType() == FrontEndType::OuterTracker2S) { cInterface->resetCBC(clpGBT, (cHybrid->getId() % 2 == 0)); } // for 2S module
                else
                {
                    cInterface->resetSSA(clpGBT, (cHybrid->getId() % 2 == 0));
                    cInterface->resetMPA(clpGBT, (cHybrid->getId() % 2 == 0));
                } // for PS module
            }     // hybrids
        }         // optical groups
    }             // board - send a hard reset

    bool                                cSuccess = true;
    std::map<FrontEndType, std::string> cList;
    cList[FrontEndType::SSA]  = "SSA";
    cList[FrontEndType::SSA2] = "SSA";
    cList[FrontEndType::MPA]  = "MPA";
    cList[FrontEndType::MPA2] = "MPA";
    cList[FrontEndType::CBC3] = "CBC";
    for(const auto& cBoard: *fDetectorContainer)
    {
        auto& cBoardSummary = fReadCheckResult.at(cBoard->getId());
        for(const auto& cOpticalGroup: *cBoard)
        {
            auto& cLinkSummary = cBoardSummary->at(cOpticalGroup->getId());
            for(const auto& cHybrid: *cOpticalGroup)
            {
                auto& cHybridSummary = cLinkSummary->at(cHybrid->getId())->getSummary<ResetList>();
                for(const auto& cChip: *cHybrid)
                {
                    auto cItem = cList.find(cChip->getFrontEndType());
                    if(cItem == cList.end()) continue;
                    auto              cSearchStr = cItem->second;
                    std::stringstream cNameStream;
                    cNameStream << cSearchStr << "#" << +cChip->getId() % 8 << "_Link#" << +cOpticalGroup->getId() << "_Hybrid#" << +cHybrid->getId() << "_";
                    for(auto cMapItem: fDefRegisterValues)
                    {
                        if(cMapItem.first.find(cSearchStr) == std::string::npos) continue;
                        auto cRegName = cMapItem.first.substr(cMapItem.first.find("_") + 1, cMapItem.first.length());
                        cNameStream << cRegName << "_ResetCheck";
                        uint8_t           cRegValue = fReadoutChipInterface->ReadChipReg(cChip, cRegName);
                        std::stringstream cResult;
                        cResult << "Expected" << +cMapItem.second << "_ReadBack" << +cRegValue << "_" << ((cRegValue == cMapItem.second) ? "PASS" : "FAIL");
                        LOG(INFO) << BOLDYELLOW << cResult.str() << RESET;
                        cHybridSummary[cNameStream.str()] = cResult.str();
                        cSuccess                          = cSuccess && (cRegValue == cMapItem.second);
                        if(cRegValue != cMapItem.second) { LOG(WARNING) << BOLDRED << cRegName << " could not be reset on ROC#" << +cChip->getId() << " on Hybrid#" << +cHybrid->getId() << RESET; }
                    } // for all registers
                }     // for all chips
            }         // for all hybrids
        }             // for all links
    }                 // check read-back value after reset on all boards
    return cSuccess;
}
bool ResetTester::ReadCheckCICs(BeBoard* pBoard)
{
    bool cSuccess = true;
    LOG(INFO) << BOLDYELLOW << "ResetTester::ReadCheckCICs BeBoard#" << +pBoard->getId() << RESET;
    for(const auto& cBoard: *fDetectorContainer)
    {
        if(cBoard->isOptical() == 0)
        {
            fBeBoardInterface->ChipReset(cBoard);
            continue;
        }
        for(const auto& cOpticalGroup: *cBoard)
        {
            auto& clpGBT = cOpticalGroup->flpGBT;
            if(clpGBT == nullptr) continue;
            auto cInterface = static_cast<D19clpGBTInterface*>(flpGBTInterface);
            for(const auto& cHybrid: *cOpticalGroup) { cInterface->resetCIC(clpGBT, (cHybrid->getId() % 2 == 0)); } // hybrids
        }                                                                                                           // optical groups
    }                                                                                                               // board - send a hard reset

    std::map<FrontEndType, std::string> cList;
    cList[FrontEndType::CIC]  = "CIC";
    cList[FrontEndType::CIC2] = "CIC";
    for(const auto& cBoard: *fDetectorContainer)
    {
        auto& cBoardSummary = fReadCheckResult.at(cBoard->getId());
        for(const auto& cOpticalGroup: *cBoard)
        {
            auto& cLinkSummary = cBoardSummary->at(cOpticalGroup->getId());
            for(const auto& cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == nullptr) continue;

                auto& cHybridSummary = cLinkSummary->at(cHybrid->getId())->getSummary<ResetList>();
                auto  cItem          = cList.find(cCic->getFrontEndType());
                if(cItem == cList.end()) continue;
                auto              cSearchStr = cItem->second;
                std::stringstream cNameStream;
                cNameStream << cSearchStr << "#" << +cCic->getId() % 8 << "_Link#" << +cOpticalGroup->getId() << "_Hybrid#" << +cHybrid->getId() << "_";
                for(auto cMapItem: fDefRegisterValues)
                {
                    if(cMapItem.first.find(cSearchStr) == std::string::npos) continue;
                    auto cRegName = cMapItem.first.substr(cMapItem.first.find("_") + 1, cMapItem.first.length());
                    cNameStream << cRegName << "_ResetCheck";
                    uint8_t           cRegValue = fCicInterface->ReadChipReg(cCic, cRegName);
                    std::stringstream cResult;
                    cResult << "Expected" << +cMapItem.second << "_ReadBack" << +cRegValue << "_" << ((cRegValue == cMapItem.second) ? "PASS" : "FAIL");
                    cHybridSummary[cNameStream.str()] = cResult.str();
                    cSuccess                          = cSuccess && (cRegValue == cMapItem.second);
                    if(cRegValue != cMapItem.second) { LOG(WARNING) << BOLDRED << cRegName << " could not be read from on ROC#" << +cCic->getId() % 8 << " on Hybrid#" << +cHybrid->getId() << RESET; }
                } // all registers defined in the test list
            }     // all hybrids
        }         // all links
    }             // check read-back values of ROCs - all boards
    return cSuccess;
}
void ResetTester::GetSummary()
{
#ifdef __USE_ROOT__
    fillSummaryTree("StartTime_ResetTest", fStartTime);
    fillSummaryTree("StopTime_ResetTest", fStopTime);
#endif
    for(const auto& cBoard: *fDetectorContainer)
    {
        auto& cBoardSummary = fReadCheckResult.at(cBoard->getId());
        for(const auto& cOpticalGroup: *cBoard)
        {
            auto& cLinkSummary = cBoardSummary->at(cOpticalGroup->getId());
            for(const auto& cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == nullptr) continue;

                auto& cCheckResult = cLinkSummary->at(cHybrid->getId())->getSummary<ResetList>();
                for(auto cResult: cCheckResult)
                {
                    LOG(INFO) << BOLDYELLOW << cResult.first << "\t" << cResult.second << RESET;
#ifdef __USE_ROOT__
                    std::vector<std::string> cTokens;
                    tokenize(cResult.second, cTokens, "_");
                    for(auto cToken: cTokens)
                    {
                        auto cPrsdRslt = parseSummaryTreeMsg(cToken);
                        fillSummaryTree(cResult.first + "_" + cPrsdRslt.fLabel, cPrsdRslt.fValue);
                    }
#endif
                }
            } // hybrids
        }     // links
    }         // boards
}
// State machine control functions
void ResetTester::Running()
{
    auto startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    // prepare container to hold test results
    fReadCheckResult.reset();
    ContainerFactory::copyAndInitHybrid<ResetList>(*fDetectorContainer, fReadCheckResult);
    // run test for all boards
    // test sends a hard reset
    // checks read-back register value against default
    for(const auto& cBoard: *fDetectorContainer)
    {
        ReadCheckCICs(cBoard);
        ReadCheckROCs(cBoard);
    }
    auto currentTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    auto cDeltaTime_us     = currentTimeUTC_us - startTimeUTC_us;
    LOG(INFO) << BOLDYELLOW << "Time to perform reset check " << cDeltaTime_us * 1e-6 << " seconds" << RESET;
}
void ResetTester::Stop()
{
    waitForRunToBeCompleted();
    fStopTime         = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    auto cDeltaTime_s = (fStopTime - fStartTime) * 1e-6;
    LOG(INFO) << BOLDMAGENTA << "ResetTester::Stopped at " << fStopTime << " s from epoch : tool ran for " << cDeltaTime_s << " seconds." << RESET;
    GetSummary();
}

void ResetTester::Pause() {}

void ResetTester::Resume() {}