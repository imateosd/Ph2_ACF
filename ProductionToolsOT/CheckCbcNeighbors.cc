#include "CheckCbcNeighbors.h"
#include "../Utils/ChannelGroupHandler.h"
#include "../Utils/ContainerFactory.h"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

CheckCbcNeighbors::CheckCbcNeighbors() : Tool() {}

CheckCbcNeighbors::~CheckCbcNeighbors() {}

void CheckCbcNeighbors::Initialise(void)
{
    fNEvents = findValueInSettings<double>("Nevents", 10);
}

void CheckCbcNeighbors::ReenableChannels(void)
{
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            //setSameDacBeBoard(cBoard, "Threshold", 1023);
            for(auto cHybrid: *cOpticalGroup)
            {
                // first disable everything on all chips
                for(auto cChip: *cHybrid)
                {
                    CbcInterface*              theCbcInterface = static_cast<CbcInterface*>(fReadoutChipInterface);
                    ChannelGroup<NCHANNELS, 1> cChannelMask;
                    cChannelMask.enableAllChannels();
                    theCbcInterface->maskChannelGroup(cChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask)));
                }
            }
        }
    }
}

void CheckCbcNeighbors::FillCBCLatLinesTree(std::string pParameter, std::string pValue)
{
#if defined(__USE_ROOT__)
    fResultFile->cd();
    if(gROOT->FindObject("CBCLatLinesTree") != nullptr) { fCBCLatLinesTree = static_cast<TTree*>(gROOT->FindObject("CBCLatLinesTree")); }
    else
    {
        fCBCLatLinesTree = new TTree("CBCLatLinesTree", "Bad inter-CBC lines");
        fCBCLatLinesTree->Branch("Parameter", &fCBCLatLinesTreeParameter);
        fCBCLatLinesTree->Branch("Value", &fCBCLatLinesTreeValue);
    }
    fCBCLatLinesTreeParameter = pParameter;
    fCBCLatLinesTreeValue     = pValue;
    fCBCLatLinesTree->Fill();
#endif
}

bool CheckCbcNeighbors::TestCbcConnectionsList(std::vector<std::pair<ReadoutChip*, ReadoutChip*>> chipPairs)
{
	if (chipPairs.size()==0) return true;
	LOG(INFO) <<"Testing the following CBC lateral connections..."<<RESET;
	for (auto chipPair: chipPairs)
	{
		LOG(INFO) << "Chip "<<chipPair.first->getId()<<" to Chip "<<chipPair.second->getId()<<RESET;
	}
	bool result;
	bool allPass = true;
	for(auto cBoard: *fDetectorContainer)
	{
		for(auto cOpticalGroup: *cBoard)
		{
			//setSameDacBeBoard(cBoard, "Threshold", 1023);
			for(auto cHybrid: *cOpticalGroup)
			{
				// first disable everything on all chips
				for(auto cChip: *cHybrid)
				{
					CbcInterface*              theCbcInterface = static_cast<CbcInterface*>(fReadoutChipInterface);
                                        ChannelGroup<NCHANNELS, 1> cChannelMask;
                                        cChannelMask.disableAllChannels();
                                        theCbcInterface->maskChannelGroup(cChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask)));
				}
				
			        std::vector<uint8_t> chip1Ids;	
			        std::vector<uint8_t> chip2Ids;
                                std::vector<uint16_t> chip1OrigOffsets; 	
                                std::vector<uint16_t> chip2OrigOffsets; 	
			        std::vector<uint8_t> failedChipIds;	
				// check lateral lines from Chip2 to Chip1 (read stub on Chip1)
				for(auto chipPair: chipPairs)
				{
					auto cChip1 = chipPair.first;
					auto cChip2 = chipPair.second;
					fReadoutChipInterface->WriteChipReg(cChip1, "LayerSwap", 0);
					fReadoutChipInterface->WriteChipReg(cChip1, "ClusterCut", 4);
					fReadoutChipInterface->WriteChipReg(cChip2, "LayerSwap", 0);
                                        fReadoutChipInterface->WriteChipReg(cChip2, "ClusterCut", 4);
			                auto cWindowOffset = fReadoutChipInterface->ReadChipReg(cChip1, "CoincWind&Offset34");
                                        chip1OrigOffsets.push_back(cWindowOffset);
					fReadoutChipInterface->WriteChipReg(cChip1, "CoincWind&Offset34", (6 << 4) | (cWindowOffset & 0xF)); // +6 half strips
				        cWindowOffset = fReadoutChipInterface->ReadChipReg(cChip2, "CoincWind&Offset12");
                                        chip2OrigOffsets.push_back(cWindowOffset);
                                        fReadoutChipInterface->WriteChipReg(cChip2, "CoincWind&Offset12", (cWindowOffset & 0xF0) | 10); // -6 half strips
					chip1Ids.push_back(cChip1->getId());
					chip2Ids.push_back(cChip2->getId());
				}
		                ChannelGroup<NCHANNELS, 1> cChannelMask_1;
                                ChannelGroup<NCHANNELS, 1> cChannelMask_2;
                                CbcInterface*              theCbcInterface = static_cast<CbcInterface*>(fReadoutChipInterface);
				// check lateral lines from Chip2 to Chip1 (read stub on Chip1)
				for (size_t itest=0; itest < fTestBToA_ReturnStub.size(); itest++)
				{
					for(auto chipPair: chipPairs)
					{
						auto cChip1 = chipPair.first;
						auto cChip2 = chipPair.second;

                std::vector<uint8_t> chip1Ids;
                std::vector<uint8_t> chip2Ids;
                std::vector<uint8_t> failedChipIds;
                // check lateral lines from Chip2 to Chip1 (read stub on Chip1)
                for(auto chipPair: chipPairs)
                {
                    auto cChip1 = chipPair.first;
                    auto cChip2 = chipPair.second;
                    fReadoutChipInterface->WriteChipReg(cChip1, "LayerSwap", 0);
                    fReadoutChipInterface->WriteChipReg(cChip1, "ClusterCut", 4);
                    fReadoutChipInterface->WriteChipReg(cChip2, "LayerSwap", 0);
                    fReadoutChipInterface->WriteChipReg(cChip2, "ClusterCut", 4);
                    auto cWindowOffset = fReadoutChipInterface->ReadChipReg(cChip1, "CoincWind&Offset34");
                    fReadoutChipInterface->WriteChipReg(cChip1, "CoincWind&Offset34", (6 << 4) | (cWindowOffset & 0xF)); // +6 half strips
                    cWindowOffset = fReadoutChipInterface->ReadChipReg(cChip2, "CoincWind&Offset12");
                    fReadoutChipInterface->WriteChipReg(cChip2, "CoincWind&Offset12", (cWindowOffset & 0xF0) | 10); // -6 half strips
                    chip1Ids.push_back(cChip1->getId());
                    chip2Ids.push_back(cChip2->getId());
                }
                ChannelGroup<NCHANNELS, 1> cChannelMask_1;
                ChannelGroup<NCHANNELS, 1> cChannelMask_2;
                CbcInterface*              theCbcInterface = static_cast<CbcInterface*>(fReadoutChipInterface);
                // check lateral lines from Chip2 to Chip1 (read stub on Chip1)
                for(size_t itest = 0; itest < fTestBToA_ReturnStub.size(); itest++)
                {
                    for(auto chipPair: chipPairs)
                    {
                        auto cChip1 = chipPair.first;
                        auto cChip2 = chipPair.second;

                        cChannelMask_1.disableAllChannels();
                        cChannelMask_2.disableAllChannels();
                        this->UnmaskChannels(fTestBToA_CBCA[itest], cChannelMask_1);
                        this->UnmaskChannels(fTestBToA_CBCB[itest], cChannelMask_2);
                        theCbcInterface->maskChannelGroup(cChip1, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask_1)));
                        theCbcInterface->maskChannelGroup(cChip2, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask_2)));
                    }
                    auto failedChipIds = CheckStubs(cHybrid->getId(), chip1Ids, fTestBToA_ReturnStub[itest]);
                    result             = (failedChipIds.size() == 0);
                    if(!result)
                    {
                        LOG(INFO) << RED << "FAILED" << std::endl << RESET;
                        auto failedChan = fTestBToA_TestedLatChannel[itest];
                        for(auto chipPair: chipPairs)
                        {
                            auto        chip1Id    = chipPair.first->getId();
                            auto        chip2Id    = chipPair.second->getId();
                            std::string cParameter = "LateralLine_CBC" + std::to_string(chip2Id) + "_to_CBC" + std::to_string(chip1Id) + "_Channel" + std::to_string(failedChan);
                            std::string cValue     = " ";

                            if (std::find(failedChipIds.begin(), failedChipIds.end(), chip1Id) != failedChipIds.end())
							{
						    		FillCBCLatLinesTree(cParameter, cValue);
							}
						}
					}
					allPass = allPass && result;
					failedChipIds.clear();
				}
				
				// check lateral lines from Chip1 to Chip2 (read stub on Chip2)
				for (size_t itest=0; itest < fTestAToB_ReturnStub.size(); itest++)
				{
					for(auto chipPair: chipPairs)
					{
						auto cChip1 = chipPair.first;
						auto cChip2 = chipPair.second;

                // check lateral lines from Chip1 to Chip2 (read stub on Chip2)
                for(size_t itest = 0; itest < fTestAToB_ReturnStub.size(); itest++)
                {
                    for(auto chipPair: chipPairs)
                    {
                        auto cChip1 = chipPair.first;
                        auto cChip2 = chipPair.second;

                        cChannelMask_1.disableAllChannels();
                        cChannelMask_2.disableAllChannels();
                        this->UnmaskChannels(fTestAToB_CBCA[itest], cChannelMask_1);
                        this->UnmaskChannels(fTestAToB_CBCB[itest], cChannelMask_2);
                        theCbcInterface->maskChannelGroup(cChip1, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask_1)));
                        theCbcInterface->maskChannelGroup(cChip2, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask_2)));
                    }
                    auto failedChipIds = CheckStubs(cHybrid->getId(), chip2Ids, fTestAToB_ReturnStub[itest]);
                    result             = (failedChipIds.size() == 0);
                    if(!result)
                    {
                        LOG(INFO) << RED << "FAILED" << std::endl << RESET;
                        auto failedChan = fTestAToB_TestedLatChannel[itest];
                        for(auto chipPair: chipPairs)
                        {
                            auto        chip1Id    = chipPair.first->getId();
                            auto        chip2Id    = chipPair.second->getId();
                            std::string cParameter = "LateralLine_CBC" + std::to_string(chip1Id) + "_to_CBC" + std::to_string(chip2Id) + "_Channel" + std::to_string(failedChan);
                            std::string cValue     = " ";

                            if (std::find(failedChipIds.begin(), failedChipIds.end(), chip1Id) != failedChipIds.end())
							{
						    	FillCBCLatLinesTree(cParameter, cValue);
							}
						}
					}
					allPass = allPass && result;
					failedChipIds.clear();
				}
                                for (uint8_t ipair=0; ipair<chipPairs.size(); ipair++)
                                {
                                    auto chipPair = chipPairs.at(ipair);
                                    fReadoutChipInterface->WriteChipReg(chipPair.first, "CoincWind&Offset34", chip1OrigOffsets.at(ipair));
                                    fReadoutChipInterface->WriteChipReg(chipPair.second, "CoincWind&Offset12", chip2OrigOffsets.at(ipair));
                                }
			}
		}
	}
	return allPass;
}

bool CheckCbcNeighbors::TestCbcNeighbors()
{
    bool allPass = true;
    FillCBCLatLinesTree(" ", " ");

    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            setSameDacBeBoard(cBoard, "Threshold", 1023);
            for(auto cHybrid: *cOpticalGroup)
            {
                // first disable everything on all chips
                for(auto cChip: *cHybrid)
                {
                    CbcInterface*              theCbcInterface = static_cast<CbcInterface*>(fReadoutChipInterface);
                    ChannelGroup<NCHANNELS, 1> cChannelMask;
                    cChannelMask.disableAllChannels();
                    theCbcInterface->maskChannelGroup(cChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask)));
                }

                std::vector<std::pair<ReadoutChip*, ReadoutChip*>> chipPairsToTest;
                // {0,1},{1,2},{2,3},{3,4},{4,5},{5,6},{6,7}
                for(auto cChip: *cHybrid)
                {
                    auto chipId = cChip->getId();
                    for(auto pChip: *cHybrid)
                    {
                        if(pChip->getId() == (chipId + 1))
                        {
                            // test one pair at a time for now (validated for up to two at a time)
                            chipPairsToTest.push_back(std::make_pair(cChip, pChip));
                            allPass = allPass & TestCbcConnectionsList(chipPairsToTest);
                            chipPairsToTest.clear();
                            break;
                        }
                    }
                }
            }
        }
    }

    int status = 1;
    if(allPass) { LOG(INFO) << BOLDGREEN << "CheckCbcNeighbors PASSED" << RESET; }
    else
    {
        LOG(INFO) << BOLDRED << "CheckCbcNeighbors FAILED" << RESET;
        status = 0;
    }
    fillSummaryTree("CBCLateralLinesTest_STATUS", status);
    return allPass;
}

std::vector<uint8_t> CheckCbcNeighbors::CheckStubs(uint8_t hybridId, std::vector<uint8_t> chipIds, bool shouldFindStubs = true)
{
    // bool result = true;
    std::vector<uint8_t> failedChipIds;
    // CbcInterface* theCbcInterface = static_cast<CbcInterface*>(fReadoutChipInterface);
    for(auto cBoard: *fDetectorContainer)
    {
        ReadNEvents(cBoard, 1);
        const std::vector<Event*>& cEvents = this->GetEvents();
        for(auto& cEvent: cEvents)
        {
            for(const auto cOpticalGroup: *cBoard)
            {
                for(const auto cHybrid: *cOpticalGroup)
                {
                    // auto cBxId = cEvent->BxId(cHybrid->getId());
                    for(const auto cChip: *cHybrid)
                    {
                        bool chipInList = false;
                        for(auto chipId: chipIds)
                        {
                            if(cChip->getId() == chipId)
                            {
                                chipInList = true;
                                break;
                            }
                        }
                        if(!chipInList) continue;

                        auto cStubs = cEvent->StubVector(cChip->getHybridId(), cChip->getId());
                        // auto              cLUT       = theCbcInterface->readLUT(cChip);
                        // auto              cThreshold = fReadoutChipInterface->ReadChipReg(cChip, "Threshold");
                        // auto              cHits      = cEvent->GetHits(cChip->getHybridId(), cChip->getId());
                        // std::stringstream cOut;
                        // cOut << " Hits in Channels :";
                        // for(auto cHit: cHits) cOut << " " << +cHit << ",";
                        // cOut << " Stubs [Seed,BendCode]:";
                        // for(auto cStub: cStubs) cOut << " [" << +cStub.getPosition() << "," << +cStub.getBend() << " ],";
                        // LOG(DEBUG) << BOLDYELLOW << "Chip#" << +cChip->getId() << " [ " << cThreshold << " ] " << cStubs.size() << " stubs and " << cHits.size() << " hits "
                        //          << " BxId " << cBxId << "\t..." << cOut.str() << RESET;
                        bool pass = ((cStubs.size() > 0) == shouldFindStubs);
                        // result = result && pass;
                        if(!pass) { failedChipIds.push_back(cChip->getId()); }
                    }
                }
            }
        }
    }
    // return result;
    return failedChipIds;
}

void CheckCbcNeighbors::UnmaskChannels(std::vector<uint8_t> pToUnmask, ChannelGroup<NCHANNELS, 1>& pChannelMask)
{
    for(size_t cChnl: pToUnmask)
    {
        LOG(DEBUG) << GREEN << "enabling " << +cChnl << RESET;
        pChannelMask.enableChannel(cChnl);
    }
}

void CheckCbcNeighbors::ConfigureCalibration() {}

void CheckCbcNeighbors::Running()
{
    LOG(INFO) << "Starting CheckCbcNeighbors measurement.";
    Initialise();
    TestCbcNeighbors();
    LOG(INFO) << "Done with CheckCbcNeighbors.";
    ReenableChannels();
}

void CheckCbcNeighbors::Stop(void)
{
    LOG(INFO) << "Stopping CheckCbcNeighbors measurement.";
    dumpConfigFiles();
    SaveResults();
    closeFileHandler();
    LOG(INFO) << "CheckCbcNeighbors stopped.";
}

void CheckCbcNeighbors::Pause() {}

void CheckCbcNeighbors::Resume() {}
