#ifndef __EMP__
#include "2STestCardControl.h"
#include "BackendAlignmentOT.h"
#include "CheckCbcNeighbors.h"
#include "CheckStubs.h"
#include "CicFEAlignment.h"
#include "CicInputTest.h"
#include "OTModuleStartUp.h"
#include "OpenFinder.h"
#include "PSTestCardControl.h"
#include "PedeNoise.h"
#include "PedestalEqualization.h"
#include "RegisterTester.h"
#include "ResetTester.h"
#include "ShortFinder.h"
#include "Utils/Timer.h"
#include "Utils/Utilities.h"
#include "Utils/argvparser.h"

#include "Utils/ConfigureInfo.h"
#include <cstring>

#if defined(__USE_ROOT__)
#include "TApplication.h"
#include "TROOT.h"
#endif

#if defined(__TCUSB__)
#include "USB_a.h"
#endif

#define __NAMEDPIPE__

#ifdef __NAMEDPIPE__
#include "gui_logger.h"
#endif

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;
using namespace CommandLineProcessing;
INITIALIZE_EASYLOGGINGPP

#define CHIPSLAVE 4
sig_atomic_t killProcess  = 0;
sig_atomic_t runCompleted = 0;

void interruptHandler(int handler) { killProcess = 1; }

void killProcessFunction(Tool* theTool)
{
    while(1)
    {
        usleep(250000);
        if(killProcess || runCompleted) break;
    }
    if(killProcess)
    {
        theTool->Destroy();
        abort();
    }
}

int main(int argc, char* argv[])
{
    // configure the logger
    el::Configurations conf(std::string(std::getenv("PH2ACF_BASE_DIR")) + "/settings/logger.conf");
    el::Loggers::reconfigureAllLoggers(conf);

    ArgvParser cmd;

    // init
    cmd.setIntroductoryDescription("CMS Ph2_ACF  Commissioning tool to perform the following procedures:\n-Timing / "
                                   "Latency scan\n-Threshold Scan\n-Stub Latency Scan");
    // error codes
    cmd.addErrorCode(0, "Success");
    cmd.addErrorCode(1, "Error");
    // options
    cmd.setHelpOption("h", "help", "Print this help page");

    cmd.defineOption("file", "Hw Description File . Default value: settings/Commission_2CBC.xml", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("file", "f");

    cmd.defineOption("skipAlignment", "Skip alignment : cic inputs [CIC] or back0end [BE] or both [all]", ArgvParser::OptionRequiresValue);
    cmd.defineOption("swAlign", "run software alignment");
    cmd.defineOption("2SFEH", "Run test for 2S-FEH");
    cmd.defineOption("is2SFEHR", "Specify subtype of 2S-FEH, for manual CIC IN phase alignment recovery");
    cmd.defineOption("PSFEH", "Run test for PS-FEH");
    cmd.defineOption("powerUp", "Power-up hybrid");
    cmd.defineOption("reconfigure", "Configure hybrid");

    cmd.defineOption("cicInTest", "Check CIC inputs");

    cmd.defineOption("ssaOutTest",
                     "Test selected SSA pair. Possible options: 01, 12, 23, 34, 45, 56, 67, ALL [Alignments must be skipped for this test]",
                     ArgvParser::OptionRequiresValue); // Alignments must be skipped?

    cmd.defineOption("stripToIjectOnSSALateralTestLHS", "stripToIjectOnSSALateralTestLHS - 116 to 119", ArgvParser::OptionRequiresValue);
    cmd.defineOption("stripToIjectOnSSALateralTestRHS", "stripToIjectOnSSALateralTestRHS - 0 to 3", ArgvParser::OptionRequiresValue);

    cmd.defineOption("tuneOffsets", "tune offsets on readout chips connected to CIC.");
    cmd.defineOptionAlternative("tuneOffsets", "t");

    cmd.defineOption("measurePedeNoise", "measure pedestal and noise on readout chips connected to CIC.");
    cmd.defineOptionAlternative("measurePedeNoise", "m");

    cmd.defineOption("checkSharedStubs", "Check stubs at boundary between chips", ArgvParser::NoOptionAttribute);
    cmd.defineOption("checkStubs", "Test stub lines", ArgvParser::NoOptionAttribute);

    cmd.defineOption("checkResets", "check if hard reset lines are working", ArgvParser::NoOptionAttribute);
    cmd.defineOption("checkVoltages", "check the voltages supplied to and measured from the hybrid, allowing a X% variation from the nominal value. Default: 10%.", ArgvParser::OptionRequiresValue);
    cmd.defineOption("checkAMUX", "Check the lines going into the SSA ADC input pad", ArgvParser::NoOptionAttribute);
    cmd.defineOption("findShorts", "look for shorts", ArgvParser::NoOptionAttribute);
    cmd.defineOption("findOpens", "perform latency scan with antenna on UIB", ArgvParser::NoOptionAttribute);
    cmd.defineOption("hybridId", "Serial Number of front-end hybrid. Default value: xxxx", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOption("USBBus", "USB device bus number", ArgvParser::OptionRequiresValue);
    cmd.defineOption("USBDev", "USB device device number", ArgvParser::OptionRequiresValue);
    cmd.defineOption("useGui",
                     "Support for running the test from the gui for hybrids testing. The named pipe for communication needs to be passed as the last parameter. Default: false",
                     ArgvParser::OptionRequiresValue);
    cmd.defineOption("output", "Output directory. Default: Results/", ArgvParser::OptionRequiresValue);

    cmd.defineOption("LateralRX_sampling", "Set the LateralRX_sampling register for SSA2", ArgvParser::OptionRequiresValue);

    cmd.defineOption("original", "Use original implementation of PS open finder", ArgvParser::NoOptionAttribute);

    // general
    cmd.defineOption("batch", "Run the application in batch mode", ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("batch", "b");

    cmd.defineOption("antennaValue", "Antenna value for test.", ArgvParser::OptionRequiresValue);
    int result = cmd.parse(argc, argv);
    if(result != ArgvParser::NoParserError)
    {
        LOG(INFO) << cmd.parseErrorDescription(result);
        exit(1);
    }

    // now query the parsing results
    std::string cHWFile            = (cmd.foundOption("file")) ? cmd.optionValue("file") : "settings/Commissioning.xml";
    bool        batchMode          = (cmd.foundOption("batch")) ? true : false;
    std::string cDirectory         = (cmd.foundOption("output")) ? cmd.optionValue("output") : "Results";
    std::string cHybridId          = (cmd.foundOption("hybridId")) ? cmd.optionValue("hybridId") : "xxxx";
    std::string cSkip              = (cmd.foundOption("skipAlignment")) ? cmd.optionValue("skipAlignment") : "";
    bool        cSSATest           = cmd.foundOption("ssaOutTest");
    std::string cSSAPair           = (cmd.foundOption("ssaOutTest")) ? cmd.optionValue("ssaOutTest") : "";
    uint8_t     cLateralRXSampling = 0;
    if(cmd.foundOption("LateralRX_sampling"))
    {
        std::string cLateralRXSamplingStr = cmd.optionValue("LateralRX_sampling");

        try
        {
            cLateralRXSampling = static_cast<uint8_t>(std::stoi(cLateralRXSamplingStr, nullptr, 16));
        }
        catch(const std::exception& e)
        {
            // Handle parsing error
            std::cerr << "Error parsing LateralRX_sampling option: " << e.what() << std::endl;
            // Add appropriate error handling code here
        }
    }

    // bool cSwAlign = (cmd.foundOption("swAlign"));

    // start time
    auto startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

    bool cGui = (cmd.foundOption("useGui"));

    std::stringstream outp;
    outp.str(std::string());
    if(cmd.foundOption("2SFEH"))
        outp << cDirectory << "/FEH_2S_" << cHybridId << "_" << startTimeUTC_us;
    else
        outp << cDirectory << "/FEH_PS_" << cHybridId << "_" << startTimeUTC_us;

    cDirectory = outp.str();
    outp.str(std::string());
    std::string cResultfile = "Hybrid";
    Tool        cTool;
    Timer       cGlobalTimer;
    cGlobalTimer.start();

    // std::thread softKillThread(killProcessFunction, &cTool);
    // softKillThread.detach();

    // struct sigaction act;
    // act.sa_handler = interruptHandler;
    // sigaction(SIGINT, &act, NULL);

    if(cGui)
    {
        // Initialize gui communication with named pipe
        LOG(INFO) << "Trying to initialize a named pipe " << cmd.optionValue("useGui")  << RESET;
        try{
            gui::init(cmd.optionValue("useGui").c_str());
            LOG(INFO) << "Correctly initialized!" << RESET;
        } catch(std::runtime_error& e){       
            LOG(INFO) << "FAILED to initialize named pipe with runtime error " << e.what() << RESET;
        }catch (const std::exception& e) {
            LOG(INFO) << "FAILED to initialize named pipe with exception" << e.what() << RESET;
        };
        gui::status("Initializing test");
        gui::progress(0 / 10.0);
    }

#if defined(__USE_ROOT__)
    LOG(INFO) << "Creating TApplication object" << RESET;
    TApplication cApp("Root Application", &argc, argv);
    LOG(INFO) << "Succesfully created TApplication cApp" << RESET;
    if(batchMode)
        gROOT->SetBatch(true);
    else
        LOG(INFO) << "Connecting TQObject to cApp" << RESET;
        TQObject::Connect("TCanvas", "Closed()", "TApplication", &cApp, "Terminate()");
#endif
    LOG(INFO) << "Connecting TQObject to cApp" << RESET;
    cTool.InitializeHw(cHWFile, outp);
    cTool.InitializeSettings(cHWFile, outp);
    LOG(INFO) << outp.str();

#if defined(__TCUSB__)
    auto cTCUSBbus = cTool.findValueInSettings<double>("HybridTCUSBBus", 0);
    if(cmd.foundOption("USBBus")) { cTCUSBbus = (uint32_t)(std::stoi(cmd.optionValue("USBBus"))); }
    auto cTCUSBdevice = cTool.findValueInSettings<double>("HybridTCUSBDevice", 0);
    if(cmd.foundOption("USBDev")) { cTCUSBdevice = (uint32_t)(std::stoi(cmd.optionValue("USBDev"))); }

#endif
    cTool.CreateResultDirectory(cDirectory, false, false);
#if defined(__USE_ROOT__)
    cTool.InitResultFile(cResultfile);
    cTool.bookSummaryTree();
#endif
    if(cGui)
    {
        gui::message("");
        gui::status("Setting voltage of the hybrid");
        gui::progress(0.5 / 10.0);

        gui::data("ResultsDirectory", cTool.getDirectoryName().c_str());
    }

#if defined(__TCUSB__) // Moved here because we will want to store values from "ReadHybridVoltage" on the summaryTree!
    try
    {
        Timer cLocalTimer;
        cLocalTimer.start();
        // int cEMeasurementsVariation = (cmd.foundOption("checkVoltages")) ? std::stoi((cmd.optionValue("checkVoltages"))) : 10;
        if(cmd.foundOption("2SFEH"))
        {
            TestCardControl2S cController;
            cController.Initialise(cTCUSBbus, cTCUSBdevice);
            if(cmd.foundOption("powerUp")) { cController.SetDefaultHybridVoltage(); }
            auto                                                     cMeasurements          = cController.GetMeasurements();
            auto                                                     cNominalValues         = cController.GetNominalValues();
            auto                                                     cNominalMeanValues     = cNominalValues.first;
            auto                                                     cNominalVarianceValues = cNominalValues.second;
            std::map<std::string, std::pair<float, float>>::iterator cMeasIterator;
            for(cMeasIterator = cMeasurements.begin(); cMeasIterator != cMeasurements.end(); cMeasIterator++)
            {
                auto cMeanMeasuredValue = cMeasIterator->second.first;
                cTool.fillSummaryTree("Meas_" + (cMeasIterator->first) + "_mean", cMeanMeasuredValue);
                cTool.fillSummaryTree("Meas_" + (cMeasIterator->first) + "_stddev", cMeasIterator->second.second);
            }
            for(cMeasIterator = cMeasurements.begin(); cMeasIterator != cMeasurements.end(); cMeasIterator++)
            {
                auto cMeanMeasuredValue            = cMeasIterator->second.first;
                auto cNominalMeanValueIterator     = cNominalMeanValues.find(cMeasIterator->first);
                auto cNominalVarianceValueIterator = cNominalVarianceValues.find(cMeasIterator->first);
                if(cNominalMeanValueIterator != cNominalMeanValues.end() && cNominalVarianceValueIterator != cNominalVarianceValues.end())
                {
                    auto cNominalMeanValue     = cNominalMeanValueIterator->second;
                    auto cNominalVarianceValue = cNominalVarianceValueIterator->second;

                    auto cDistanceFromNominal = abs((cNominalMeanValue) - (cMeanMeasuredValue)); // Nominal value - mean measured value
                    cTool.fillSummaryTree("Meas_" + (cMeasIterator->first) + "_error", cDistanceFromNominal);
                    if((cNominalMeanValue - 3 * cNominalVarianceValue) <= cMeanMeasuredValue && (cMeanMeasuredValue <= cNominalMeanValue + 3 * cNominalVarianceValue))
                    {
                        LOG(INFO) << BOLDBLUE << (cMeasIterator->first) << " is within" << BOLDGREEN << " the expected range" << BOLDBLUE << "." << RESET;
                        cTool.fillSummaryTree("Meas_" + (cMeasIterator->first) + "_status", 1.0);
                    }
                    else
                    {
                        LOG(ERROR) << BOLDBLUE << (cMeasIterator->first) << BOLDRED << " is off from the expected value" << BOLDBLUE << " more than " << BOLDRED << 3 * cNominalVarianceValue
                                   << BOLDBLUE << "." << RESET;
                        cTool.fillSummaryTree("Meas_" + (cMeasIterator->first) + "_status", 0.0);
                    }
                    if(!((cNominalMeanValue - 5 * cNominalVarianceValue) <= cMeanMeasuredValue &&
                         (cMeanMeasuredValue <= cNominalMeanValue + 5 * cNominalVarianceValue))) // if not in range [cNominalMean-cNominalVariance*5, cNominalMean+cNominalVariance*5]
                    {
                        LOG(ERROR) << BOLDBLUE << (cMeasIterator->first) << BOLDRED << " is off from the expected value" << BOLDBLUE << " more than " << BOLDRED << 5 * cNominalVarianceValue
                                   << BOLDBLUE << ". Stopping the TEST!!" << RESET;
                        cTool.fillSummaryTree("Meas_" + (cMeasIterator->first) + "_test_stop", 1.0);
                        // Raise failure
                        // throw std::runtime_error(cMeasIterator->first + " is off from the expected value more than " + std::to_string(5 * cNominalVarianceValue) + "!");
                    }
                }
                if(cMeasIterator->first == "Hybrid1V40_current" && cMeanMeasuredValue <= 10.0)
                {
                    LOG(ERROR) << "The measurement on " << cMeasIterator->first << " is " << +cMeanMeasuredValue << "mA! The hybrid is not connected! " << RESET;
                    cTool.fillSummaryTree("Hybrid_not_connected", 1.0);
                    // Raise failure
                    throw std::runtime_error("The hybrid is not properly connected. The measurement on " + cMeasIterator->first + " is " + std::to_string(cMeanMeasuredValue) + "mA!");
                }
            }
        }
        if(cmd.foundOption("PSFEH"))
        {
            PSTestCardControl cController;
            cController.Initialise(cTCUSBbus, cTCUSBdevice);
            if(cmd.foundOption("powerUp"))
            {
                cController.SetDefaultHybridVoltage();
                cController.ReadHybridVoltage("Hybrid1V25");
            }
            cController.SelectCIC(true);

            PSHybridTester cHybridTester;
            cHybridTester.Inherit(&cTool);
            cHybridTester.RunHybridETest();
        }
        cLocalTimer.stop();
        cLocalTimer.show("Total execution time: ");
    }
    catch(std::exception const& e)
    {
        LOG(ERROR) << BOLDRED << "Exception: " << e.what() << RESET;
        LOG(INFO) << BOLDYELLOW << "Will save test results obtained so far and.. stop test procedure" << RESET;

        if(cGui)
        {
            gui::message("Done");
            gui::status("Test complete");
            gui::progress(10.0 / 10.0);
        }
        cTool.SaveResults();
#if defined(__USE_ROOT__)
        cTool.WriteRootFile();
        cTool.CloseResultFile();
#endif
        cTool.Destroy();
        signal(SIGINT, SIG_DFL);
        runCompleted = 1;
#if defined(__USE_ROOT__)
        if(!batchMode) cApp.Run();
#endif
        cGlobalTimer.stop();
        cGlobalTimer.show("Total execution time: ");
        return 0;
    }
#endif

    if(cGui)
    {
        gui::message("");
        gui::status("Initializing the hybrid");
        gui::progress(1 / 10.0);

        gui::data("ResultsDirectory", cTool.getDirectoryName().c_str());
    }

    // module initialization sequence
    OTModuleStartUp cModuleStartUp(cHWFile);
    cModuleStartUp.setColdStart(cmd.foundOption("powerUp") ? 1 : 0);
    cModuleStartUp.Inherit(&cTool);
    cModuleStartUp.Initialize();

    // run register check
    // this can potentially modify the detector container
    // to remove ROCs where communication over I2C fails
    RegisterTester cRegisterCheck;
    cRegisterCheck.Inherit(&cTool);
    cRegisterCheck.Initialize();
    try
    {
        if(cGui)
        {
            gui::message("");
            gui::status("Running I2C tests");
            gui::progress(1.5 / 10.0);

            gui::data("ResultsDirectory", cTool.getDirectoryName().c_str());
        }
        cRegisterCheck.BasicCheck();

        // Read chip IDS
        if(cmd.foundOption("2SFEH"))
        {
            for(const auto cBoard: *cTool.fDetectorContainer)
            {
                for(auto cOpticalGroup: *cBoard)
                {
                    for(auto cHybrid: *cOpticalGroup)
                    {
                        for(auto cChip: *cHybrid)
                        {
                            uint32_t cFusedId = cRegisterCheck.fReadoutChipInterface->ReadChipFuseID(cChip);
                            LOG(INFO) << BOLDMAGENTA << "Hybrid#" << +cHybrid->getId() << " CBC#" << +cChip->getId() << " Fused Id is " << +cFusedId << RESET;
                            std::stringstream cCBC_Name;
                            cCBC_Name.str(std::string());
                            cCBC_Name << "CBC#" << cChip->getId() << "_Id";
                            cTool.fillSummaryTree(cCBC_Name.str(), cFusedId);
                        }
                    }
                }
            }
        }
        else if(cmd.foundOption("PSFEH"))
        {
            for(const auto cBoard: *cTool.fDetectorContainer)
            {
                for(auto cOpticalGroup: *cBoard)
                {
                    for(auto cHybrid: *cOpticalGroup)
                    {
                        for(auto cChip: *cHybrid)
                        {
                            uint32_t cFusedId = cRegisterCheck.fReadoutChipInterface->ReadChipFuseID(cChip);
                            LOG(INFO) << BOLDMAGENTA << "Hybrid#" << +cHybrid->getId() << " SSA#" << +cChip->getId() << " Fused Id is " << +cFusedId << RESET;
                            std::stringstream cSSA_Name;
                            cSSA_Name.str(std::string());
                            cSSA_Name << "SSA#" << cChip->getId() << "_Id";
                            cRegisterCheck.fillSummaryTree(cSSA_Name.str(), cFusedId);
                        }
                    }
                }
            }
        }

        // check resets if requested
        if(cmd.foundOption("checkResets"))
        {
            if(cGui)
            {
                gui::message("");
                gui::status("Running reset test");
                gui::progress(2 / 10.0);
            }

            ResetTester cResetCheck;
            cResetCheck.Inherit(&cRegisterCheck);
            cResetCheck.Initialize();
            cResetCheck.Start(startTimeUTC_us);
            cResetCheck.Stop();
        }

        // reconfigure if requested
        if(cmd.foundOption("reconfigure"))
        {
            Timer cLocalTimer;
            cLocalTimer.start();
            cModuleStartUp.Inherit(&cRegisterCheck);
            cModuleStartUp.StartUp();
            // cModuleStartUp.Start(startTimeUTC_us);
            // cModuleStartUp.waitForRunToBeCompleted();
            cLocalTimer.stop();
            cLocalTimer.show("Total execution time: ");
        }

        // if(cSwAlign) {
        //    for(const auto cBoard: *cTool.fDetectorContainer)
        //    {
        //        for(auto cOpticalGroup: *cBoard)
        //        {
        //            for(auto cHybrid: *cOpticalGroup)
        //            {
        //                for(auto cChip: *cHybrid)
        //                {
        //                    uint32_t cFusedId = cRegisterCheck.fReadoutChipInterface->ReadChipFuseID(cChip);
        //                    LOG(INFO) << BOLDMAGENTA << "Hybrid#" << +cHybrid->getId() << " SSA#" << +cChip->getId() << " Fused Id is " << +cFusedId << RESET;
        //                    std::stringstream cSSA_Name;
        //                    cSSA_Name.str(std::string());
        //                    cSSA_Name << "SSA#" << cChip->getId() << "_Id";
        //                    cRegisterCheck.fillSummaryTree(cSSA_Name.str(), cFusedId);
        //                }
        //            }
        //        }
        //    }
        //
        //}

        bool cSkipBeAlignment  = (cmd.foundOption("skipAlignment")) && (cSkip.find("BE") != std::string::npos);
        bool cSkipCicAlignment = (cmd.foundOption("skipAlignment")) && (cSkip.find("CIC") != std::string::npos);
        bool cSkipBoth         = (cmd.foundOption("skipAlignment")) && (cSkip.find("all") != std::string::npos);

        if(!cSkipBoth && !cSkipBeAlignment)
        {
            auto startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
            if(cGui)
            {
                gui::message("");
                gui::status("Aligning CIC OUT");
                gui::progress(2.5 / 10.0);
            }
            BackendAlignmentOT cAligner;
            cAligner.Inherit(&cRegisterCheck);
            cAligner.Initialise();
            cAligner.Align();
            cAligner.GetSummary();
            cAligner.Reset();
            // cAligner.Start(startTimeUTC_us);
            // cAligner.waitForRunToBeCompleted();
            auto currentTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
            auto cDeltaTime_us     = currentTimeUTC_us - startTimeUTC_us;
            LOG(INFO) << BOLDYELLOW << "Time to perform BE aligment  " << cDeltaTime_us * 1e-6 << " seconds" << RESET;
        }

        // align FEs - CIC

        bool cIs2SFEHR = cmd.foundOption("is2SFEHR");
        CicFEAlignment cCicAligner;
        cCicAligner.Inherit(&cRegisterCheck);

        if(!cSkipBoth && !cSkipCicAlignment)
        {
            auto startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
            if(cGui)
            {
                gui::message("");
                gui::status("Aligning CIC IN");
                gui::progress(3 / 10.0);
            }
            cCicAligner.Initialise();
            cCicAligner.AlignInputs(cIs2SFEHR);
            cCicAligner.Reset();
            cCicAligner.Stop();
            // cCicAligner.Start(startTimeUTC_us);
            auto currentTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
            auto cDeltaTime_us     = currentTimeUTC_us - startTimeUTC_us;
            LOG(INFO) << BOLDYELLOW << "Time to perform  CIC FE aligment  " << cDeltaTime_us * 1e-6 << " seconds" << RESET;
        }
        cCicAligner.waitForRunToBeCompleted();
        
        if(cmd.foundOption("checkStubs"))
        {
            if(cGui)
            {
                gui::message("");
                gui::status("Running stub lines test");
                gui::progress(3.25 / 10.0);
            }
            LOG(INFO) << BOLDMAGENTA << "Checking stubs on CBCs..." << RESET;

            CheckStubs cCheckStubs;
            cCheckStubs.Inherit(&cRegisterCheck);
            cCheckStubs.Start(startTimeUTC_us);
            cCheckStubs.waitForRunToBeCompleted();
        }
        
        if(cmd.foundOption("checkSharedStubs"))
        {
            if(cGui)
            {
                gui::message("");
                gui::status("Running lateral communication test");
                gui::progress(3.5 / 10.0);
            }
            LOG(INFO) << BOLDMAGENTA << "Checking stubs across CBC neighbors" << RESET;

            CheckCbcNeighbors cCheckCbcNeighbors;
            cCheckCbcNeighbors.Inherit(&cRegisterCheck);
            cCheckCbcNeighbors.Start(startTimeUTC_us);
            cCheckCbcNeighbors.waitForRunToBeCompleted();
        }
        
        
        // equalize thresholds on readout chips
        if(cmd.foundOption("tuneOffsets"))
        {
            auto startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
            if(cGui)
            {
                gui::message("");
                gui::status("Calibrating FE inputs");
                gui::progress(4 / 10.0);
            }
            PedestalEqualization cPedestalEqualization;
            cPedestalEqualization.Inherit(&cRegisterCheck);
            cPedestalEqualization.Start(startTimeUTC_us);
            cPedestalEqualization.waitForRunToBeCompleted();
            cPedestalEqualization.Stop();
            auto currentTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
            auto cDeltaTime_us     = currentTimeUTC_us - startTimeUTC_us;
            LOG(INFO) << BOLDYELLOW << "Time to perform  offset tuning  " << cDeltaTime_us * 1e-6 << " seconds" << RESET;
        }
        // measure noise on FE chips
        if(cmd.foundOption("measurePedeNoise"))
        {
            auto startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
            if(cGui)
            {
                gui::message("");
                gui::status("Measuring noise");
                gui::progress(4.5 / 10.0);
            }
            // bool cAllChan = true;
            LOG(INFO) << BOLDMAGENTA << "Measuring pedestal and noise" << RESET;
            PedeNoise cPedeNoise;
            cPedeNoise.Inherit(&cRegisterCheck);
            cPedeNoise.Inherit(&cRegisterCheck);
            cPedeNoise.Start(startTimeUTC_us);
            cPedeNoise.waitForRunToBeCompleted();
            cPedeNoise.Stop();
            auto currentTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
            auto cDeltaTime_us     = currentTimeUTC_us - startTimeUTC_us;
            LOG(INFO) << BOLDYELLOW << "Time to record S-curves  " << cDeltaTime_us * 1e-6 << " seconds" << RESET;
        }

        if(cmd.foundOption("checkAMUX"))
        {
            if(cmd.foundOption("PSFEH") && !cmd.foundOption("ssaOutTest"))
            {
                if(cGui)
                {
                    gui::message("");
                    gui::status("Checking the SSA ADC input lines");
                    gui::progress(5 / 10.0);
                }
                PSHybridTester cHybridTester;
                cHybridTester.Inherit(&cRegisterCheck);
                cHybridTester.SSATestAMUXLines();
            }
            else if(cmd.foundOption("ssaOutTest"))
            {
                LOG(WARNING) << "This test can only be run with the CIC enabled!"
                             << RESET; // If I test it in "SSA" mode, the CIC is not enabled! (And the CIC_RESET line is pulled low, which makes it harder to detect)
            }
            else
            {
                LOG(WARNING) << "This test can only be run with PSFEH!" << RESET;
            }
        }

        if(cmd.foundOption("cicInTest"))
        {
            if(cGui)
            {
                gui::message("");
                gui::status("Running the CIC IN test");
                gui::progress(5.25 / 10.0);
            }
            CicInputTester cCicInputTester(0, 0, 0xAA);
            cCicInputTester.Inherit(&cRegisterCheck);
            cCicInputTester.Initialise();
            cCicInputTester.Start(startTimeUTC_us);
            cCicInputTester.waitForRunToBeCompleted();
            cCicInputTester.Stop();
            // cCicInputTester.writeObjects();
            // cCicInputTester.dumpConfigFiles();
        }
        if(cmd.foundOption("findShorts"))
        {
            if(cGui)
            {
                gui::message("");
                gui::status("Testing for shorts in the analog channels");
                gui::progress(5.5 / 10.0);
            }
            ShortFinder cShortFinder;
            cShortFinder.Inherit(&cRegisterCheck);
            cShortFinder.Initialise();
            cShortFinder.Start(startTimeUTC_us);
            cShortFinder.waitForRunToBeCompleted();
        }
        if(cmd.foundOption("findOpens"))
        {
#if defined(__TCUSB__)
            if(cGui)
            {
                gui::message("");
                gui::status("Testing for opens in the analog channels");
                gui::progress(6 / 10.0);
            }
            auto cTCUSBbus = cTool.findValueInSettings<double>("HybridTCUSBBus", 0);
            if(cmd.foundOption("USBBus")) { cTCUSBbus = (uint32_t)(std::stoi(cmd.optionValue("USBBus"))); }
            auto cTCUSBdevice = cTool.findValueInSettings<double>("HybridTCUSBDevice", 0);
            if(cmd.foundOption("USBDev")) { cTCUSBdevice = (uint32_t)(std::stoi(cmd.optionValue("USBDev"))); }

            OpenFinder cOpenFinder;
            cOpenFinder.ConfigureUsb(cTCUSBbus, cTCUSBdevice);
            cOpenFinder.Inherit(&cRegisterCheck);
            cOpenFinder.ConfigureInjection();
            cOpenFinder.Initialise();
            if(cmd.foundOption("2SFEH"))
            {
                cOpenFinder.FindOpens2S();
                cOpenFinder.Print();
            }
            if(cmd.foundOption("PSFEH") && cmd.foundOption("original"))
                cOpenFinder.FindOpensPS_original();
            else if(cmd.foundOption("PSFEH"))
                cOpenFinder.FindOpensPS();
#endif
        }

        if(cSSATest && cmd.foundOption("PSFEH"))
        {
            bool cSuccess = true;
#if defined(__TCUSB__)
            if(cGui)
            {
                gui::status("Testing SSA outputs...");
                gui::progress(5 / 10.0);
            }

            BackendAlignmentOT cBackendAlignment;
            cBackendAlignment.Inherit(&cRegisterCheck);

            PSHybridTester cHybridTester;
            cHybridTester.Inherit(&cRegisterCheck);

            cHybridTester.SelectCIC(false); // Set the test card in SSA mode

            if(cmd.foundOption("stripToIjectOnSSALateralTestLHS"))
            {
                std::cout << "Im here" << std::endl;
                std::cout << cmd.optionValue("stripToIjectOnSSALateralTestLHS") << std::endl;
                cHybridTester.fInjectedStrip_SSALateralTest_LHS = (uint8_t)(std::stoi(cmd.optionValue("stripToIjectOnSSALateralTestLHS")));
            }

            if(cmd.foundOption("stripToIjectOnSSALateralTestRHS")) { cHybridTester.fInjectedStrip_SSALateralTest_RHS = (uint8_t)(std::stoi(cmd.optionValue("stripToIjectOnSSALateralTestRHS"))); }
            // cHybridTester.SSATestAMUXLines(); // If I test it in this mode, the CIC is not enabled! (And the CIC_RESET line is pulled low, which makes it harder to detect)
            if(cmd.foundOption("LateralRX_sampling"))
            {
                LOG(INFO) << "Setting LateralRX_sampling to " << unsigned(cLateralRXSampling) << RESET;
                cHybridTester.WriteLateralRxSamplingSSA(cTool.fDetectorContainer, cRegisterCheck, cLateralRXSampling);
                cHybridTester.ReadoutLateralRxSamplingSSA(cTool.fDetectorContainer, cRegisterCheck);
            }

            // ****************** Verification of clock and fast commant test
            // Disable the clock and T1 lines on some SSA
            /*for(const auto cBoard: *cTool.fDetectorContainer)
            {
                for(auto cOpticalGroup: *cBoard)
                {
                    for(auto cHybrid: *cOpticalGroup)
                    {
                        for(auto cChip: *cHybrid)
                        {
                            if (cChip->getId() == 5 || cChip->getId() == 0 || cChip->getId() == 2 || cChip->getId() == 6 || cChip->getId() == 7  )
                            {
                                auto cCLK_T1_settings = cHybridTester.fReadoutChipInterface->ReadChipReg(cChip, "SLVS_pad_current_Clk_T1");
                                cCLK_T1_settings = 0x00;
                                // [0:2] Controls the current provided by the CLOCK output SLVS transmitter. Set to 0 to disable the line.
                                // [5:3] Controls the current provided by the T1 output SLVS transmitter. Set to 0 to disable the line.
                                // [6] Enable/disable the transmission line termination for the CLOCK receivers. At reset it is disabled.
                                // [7] Enable/disable the transmission line termination for the T1 receivers. At reset it is disabled.

                                cHybridTester.fReadoutChipInterface->WriteChipReg(cChip, "SLVS_pad_current_Clk_T1", cCLK_T1_settings, true);
                                cHybridTester.fReadoutChipInterface->WriteChipReg(cChip, "SLVS_pad_current_Clk_T1", cCLK_T1_settings, true);
                                LOG(INFO) << BOLDMAGENTA << "Disabled output clock and T1 for SSA#" << +cChip->getId() << " (Register set to " << cCLK_T1_settings << ")" << RESET;
                            }
                        }
                    }
                }
            }*/

            if(!cSSAPair.empty())
            {
                if(cSSAPair != "ALL")
                {
                    cHybridTester.SSAPairSelect(cSSAPair);
                    LOG(INFO) << "Starting SSA CLK output test" << RESET;
                    cHybridTester.SSATestCLKOutput(cSSAPair);
                    LOG(INFO) << "Starting SSA FCMD output test" << RESET;
                    cHybridTester.SSATestFCMDOutput(cSSAPair);
                    LOG(INFO) << "Starting SSA outputs tests" << RESET;
                    cHybridTester.SSATestStubOutput(cSSAPair);
                    cHybridTester.SSATestL1Output(cSSAPair);
                    LOG(INFO) << "Starting inter-SSA communication test" << RESET;
                    cSuccess = cSuccess && cHybridTester.SSATestLateralCommunication(cSSAPair);
                }
                else
                {
                    std::string cCurrentSSAPair;

                    if(cGui)
                    {
                        gui::message("");
                        gui::status("Testing the SSA L1, stub and FCMD/CLK...");
                        gui::progress(6 / 10.0);
                    }

                    for(int i = 0; i < 7; i = i + 2) // Test only even pairs for L1 and SLVS outputs
                    {
                        cCurrentSSAPair = std::to_string(i) + std::to_string(i + 1);
                        cHybridTester.SSAPairSelect(cCurrentSSAPair);
                        LOG(INFO) << BOLDMAGENTA << "Running POGO test for SSA pair " << cCurrentSSAPair << RESET;
                        LOG(INFO) << "Starting SSA CLK output test" << RESET;
                        cHybridTester.SSATestCLKOutput(cCurrentSSAPair);
                        LOG(INFO) << "Starting SSA FCMD output test" << RESET;
                        cHybridTester.SSATestFCMDOutput(cCurrentSSAPair);
                        LOG(INFO) << "Starting SSA outputs tests" << RESET;
                        cHybridTester.SSATestStubOutput(cCurrentSSAPair);
                        cHybridTester.SSATestL1Output(cCurrentSSAPair);
                    }

                    if(cGui)
                    {
                        gui::message("");
                        gui::status("Testing the SSA lateral communication lines...");
                        gui::progress(6.5 / 10.0);
                    }

                    for(int i = 0; i < 7; i++)
                    {
                        cCurrentSSAPair = std::to_string(i) + std::to_string(i + 1);
                        cHybridTester.SSAPairSelect(cCurrentSSAPair);
                        LOG(INFO) << "Starting inter-SSA communication test for pair " << cCurrentSSAPair << RESET;
                        bool cResult = cHybridTester.SSATestLateralCommunication(cCurrentSSAPair);
                        cSuccess     = cResult && cSuccess;
                        LOG(INFO) << BOLDYELLOW << "DEBUG L800 - After cHybridTester.SSATestLateralCommunication(cCurrentSSAPair);" << cCurrentSSAPair << RESET;
                        // ************************ LATERAL COMMUNICATION SAMPLING ********************************
                        // The lateral communication sampling phase is set in the settings file used for the SSA2 [SSA2.txt]. It is set to 6 for every chip for both receivers (left and right)
                        // If the sampling phase must be changed, one must change the default value of "LateralRX_sampling" in the file. This register holds
                        // [2:0] RX_L_Phase
                        // [3]   RX_L_Edge320
                        // [6:4] RX_R_Phase
                        // [7]   RX_R_Edge320
                        // To find out the optimal phase for a hybrid, a sweep of the values can be performed in the lateral communication test, by calling the function with the argument
                        // pSweepPhaseSelector = true cHybridTester.SSATestLateralCommunication(cCurrentSSAPair, true); The good value will be stored in the summaryTree in the ROOT file as
                        // "LateralRX_sampling_SSAN_L" and ""LateralRX_sampling_SSAN_R"
                    }
                }
            }
#endif
            if(cSuccess) { LOG(INFO) << BOLDGREEN << "SSA test completed successfully!" << RESET; }
            else
            {
                LOG(ERROR) << BOLDRED << "SSA test failed!" << RESET;
            }
        }
    }
    catch(std::exception const& e)
    {
        std::stringstream cExitMessage;
        cExitMessage << "Exception Message : " << e.what();
        cTool.fillSummaryTree(cExitMessage.str(), 1);
        LOG(ERROR) << BOLDRED << "Exception: " << cExitMessage.str() << RESET;
        LOG(INFO) << BOLDYELLOW << "Will save test results obtained so far and.. stop test procedure" << RESET;
    }
    if(cGui)
    {
        gui::message("Done");
        gui::status("Test complete");
        gui::progress(10.0 / 10.0);
        gui::close();
    }
    cRegisterCheck.Stop();
    cTool.SaveResults();
#if defined(__USE_ROOT__)
    cTool.WriteRootFile();
    cTool.CloseResultFile();
#endif
    cTool.Destroy();
    signal(SIGINT, SIG_DFL);
    runCompleted = 1;
#if defined(__USE_ROOT__)
    if(!batchMode) cApp.Run();
#endif
    cGlobalTimer.stop();
    cGlobalTimer.show("Total execution time: ");
    return 0;
}
#endif
