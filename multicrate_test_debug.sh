
#!/bin/bash
source setup.sh
# Loop 50 times

# crates=(
# 	"CMSPH2-BRD00661"	L	passed
# 	"CMSPH2-BRD00851"	R	failed
# 	"CMSPH2-BRD00726"	L	passed
# 	"CMSPH2-BRD00734"  	R  	failed
# 	"CMSPH2-BRD00866"	L	failed
# 	"CMSPH2-BRD00732"	L	passed
# 	"CMSPH2-BRD00738"	R	failed
# 	"CMSPH2-BRD00852" 	R	failed
# 	"CMSPH2-BRD00857"	L	passed
# 	"CMSPH2-BRD00847"	L	passed
# 	"CMSPH2-BRD00858"	R	failed
# 	"CMSPH2-BRD00845")	R	failed




# crates=(
# 	CMSPH2-BRD00858
# 	CMSPH2-BRD00864
# 	CMSPH2-BRD00726
# 	CMSPH2-BRD00844
# 	)

# args=(
# 	"0 2"
# 	"1 1"
# 	"2 0"
# 	"2 2"
# 	)

for ((i=1; i<=10; i++))
do

crates=(
CMSPH2-BRD00855
CMSPH2-BRD00863
CMSPH2-BRD00858
CMSPH2-BRD00851
CMSPH2-BRD00870
CMSPH2-BRD00864
CMSPH2-BRD00840
CMSPH2-BRD00697
CMSPH2-BRD00726
CMSPH2-BRD00699
CMSPH2-BRD00844
CMSPH2-BRD00857
)

args=(
	"0 0"
	"0 1"
	"0 2"
	"0 3"
	"1 0"
	"1 1"
	"1 2"
	"1 3"
	"2 0"
	"2 1"
	"2 2"
	"2 3"
	)

echo "Starting the loop"

echo "i =" ${i}
for ((l=0; l<=11; l++))
do
	echo "l =" ${l}
	#echo ./ps_test_with_output.sh ${args[$l]} test ${crates[$l]} $j $k ">" debugTests/CICInputTest14_02/output_${crates[$l]}_test_$i.txt
	./ps_test_with_output_top.sh ${args[$l]} test ${crates[$l]} $j $k > debugTests/27_02/output_${crates[$l]}_test_$i.txt
	# ./ps_test_lateralSweep.sh ${args[$l]} test ${crates[$l]} $j $k > debugTests/lateralSweep/output_${crates[$l]}_test_$i.txt

done



# crates=(
# 	CMSPH2-BRD00846
# 	CMSPH2-BRD00859
# 	CMSPH2-BRD00661
# 	)

# args=(
# 	"1 2"
# 	"2 1"
# 	"2 3"
# 	)
crates=(
CMSPH2-BRD00730
CMSPH2-BRD00852
CMSPH2-BRD00854
CMSPH2-BRD00862
CMSPH2-BRD00846
CMSPH2-BRD00741
CMSPH2-BRD00850
CMSPH2-BRD00859
CMSPH2-BRD00847
CMSPH2-BRD00661
)
args=(
	"0 2"
	"0 3"
	"1 0"
	"1 1"
	"1 2"
	"1 3"
	"2 0"
	"2 1"
	"2 2"
	"2 3"
	)
echo "Starting the loop"
echo "i =" ${i}
for ((l=0; l<=9; l++))
do
	echo "l =" ${l}
	#echo ./ps_test_with_output.sh ${args[$l]} test ${crates[$l]} $j $k ">" debugTests/CICInputTest14_02/output_${crates[$l]}_test_$i.txt
	./ps_test_with_output_mid.sh ${args[$l]} test ${crates[$l]} $j $k > debugTests/27_02/output_${crates[$l]}_test_$i.txt
	# ./ps_test_lateralSweep.sh ${args[$l]} test ${crates[$l]} $j $k > debugTests/lateralSweep/output_${crates[$l]}_test_$i.txt

done



crates=(
	CMSPH2-BRD00662
	CMSPH2-BRD00842
	CMSPH2-BRD00841
	)

args=(
	"2 1"
	"2 2"
	"2 3"
	)


echo "Starting the loop"

echo "i =" ${i}
for ((l=0; l<=2; l++))
do
	echo "l =" ${l}
	#echo ./ps_test_with_output.sh ${args[$l]} test ${crates[$l]} $j $k ">" debugTests/CICInputTest14_02/output_${crates[$l]}_test_$i.txt
	./ps_test_with_output_bot.sh ${args[$l]} test ${crates[$l]} $j $k > debugTests/27_02/output_${crates[$l]}_test_$i.txt
	# ./ps_test_lateralSweep.sh ${args[$l]} test ${crates[$l]} $j $k > debugTests/lateralSweep/output_${crates[$l]}_test_$i.txt

done

done