import ROOT

def getPAValues(rootfilename):
    values = []
    rootfile = ROOT.TFile.Open(rootfilename)
    tree = rootfile.Get("frontendAlignmentTree")
    try:
        nentries = tree.GetEntries()
    except:
        #rootfile is corrupted, move on
        return values
    if nentries<=0:
        return values
    tree.GetEntry(0)
    #if tree.PAStatus == 0:
    #    return values
    for ientry in range(nentries):
        tree.GetEntry(ientry)
        values.append(tree.PAValue)
    return values
   
def passPhaseAlign(rootfilename):
    rootfile = ROOT.TFile.Open(rootfilename)
    tree = rootfile.Get("summaryTree")
    try:
        nentries = tree.GetEntries()
    except:
        #rootfile is corrupted, move on
        return False
    for ientry in range(nentries):
        tree.GetEntry(ientry)
        if tree.Parameter == "FE_PHASE_ALIGN":
            if tree.Value == 1:
                return True
            else:
                return False
    return False
 
def getTemperature(rootfilename):
    temp = -999
    rootfile = ROOT.TFile.Open(rootfilename)
    tree = rootfile.Get("summaryTree")
    try:
        nentries = tree.GetEntries()
    except:
        #rootfile is corrupted, move on
        return temp
    for ientry in range(nentries):
        tree.GetEntry(ientry)
        if tree.Parameter == "Meas_Thermistor_mean":
            temp = tree.Value
    return temp 

def main():
    ROOT.gROOT.SetBatch(True)
    histograms = []
    histograms_cold = []
    hybrid_ids = [] # keep track of which hybrids you have already used, to avoid double-counting
    hybrid_ids_cold = [] # keep track of which hybrids you have already used, to avoid double-counting
    for i in range(48):
        hist = ROOT.TH1F("h_"+str(i),"h_"+str(i),16,-0.5,15.5)
        hist2 = ROOT.TH1F("h2_"+str(i),"h2_"+str(i),16,-0.5,15.5)
        histograms.append(hist)
        histograms_cold.append(hist2)
    ifile = open("files.txt","r")
    for line in ifile:
        ifilename = line.strip()
        values = getPAValues(ifilename)
        if len(values)!=48:continue
        temperature = getTemperature(ifilename)
        #if ifilename.find("2SFEH18L")==-1: continue
        if ifilename.find("2SFEH18R")==-1: continue
        if not passPhaseAlign(ifilename): continue
        print (ifilename,temperature)
        hybrid_id = ifilename[ifilename.find("211000"):ifilename.find("211000")+9]
        if temperature>30 and temperature<50 and (hybrid_id not in hybrid_ids):
            hybrid_ids.append(hybrid_id)
            for i in range(48):
                histograms[i].Fill(values[i])
        elif temperature>-50 and temperature<-30 and (hybrid_id not in hybrid_ids_cold):
            hybrid_ids_cold.append(hybrid_id)
            for i in range(48):
                histograms_cold[i].Fill(values[i])

    ofile = open("most_likely_pavalues.txt","w")
    canv = ROOT.TCanvas()
    for i in range(48):
        hist = histograms[i]
        hist_cold = histograms_cold[i]
        hist.GetYaxis().SetRangeUser(0,1.5*max(hist.GetMaximum(),hist_cold.GetMaximum()))
        hist.SetTitle("")
        hist.SetStats(0)
        hist.SetLineWidth(2)
        hist_cold.SetLineWidth(2)
        hist.SetLineColor(ROOT.kBlue)
        hist_cold.SetLineColor(ROOT.kBlue)
        hist_cold.SetLineStyle(2)
        hist.GetXaxis().SetTitle("Phase alignment value")
        hist.GetYaxis().SetTitle("Number of entries")
        hist.Draw("hist")
        leg = ROOT.TLegend(0.5,0.7,0.88,0.88)
        leg.SetBorderSize(0)
        leg.AddEntry(hist,"40C (most likely value: %i)" % hist.GetBinCenter(hist.GetMaximumBin()))
        fe = int(i/6)
        line = (i % 6)
        ofile.write("FE%i Line%i: 40C, most likely value = %i \n" % (fe,line,hist.GetBinCenter(hist.GetMaximumBin())))
        if hist_cold.Integral()>0:
            hist_cold.Draw("hist same")
            leg.AddEntry(hist_cold,"-35C (most likely value: %i)" % hist_cold.GetBinCenter(hist_cold.GetMaximumBin()))
        leg.Draw("same")
        canv.SaveAs("plots/pavalues_fe%i_line%i.png" % (fe,line))  

                    

main()
