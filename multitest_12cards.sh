
#!/bin/bash
source setup.sh
# Loop 50 times

# crates=(
# 	"CMSPH2-BRD00661"	L	passed
# 	"CMSPH2-BRD00851"	R	failed
# 	"CMSPH2-BRD00726"	L	passed
# 	"CMSPH2-BRD00734"  	R  	failed
# 	"CMSPH2-BRD00866"	L	failed
# 	"CMSPH2-BRD00732"	L	passed
# 	"CMSPH2-BRD00738"	R	failed
# 	"CMSPH2-BRD00852" 	R	failed
# 	"CMSPH2-BRD00857"	L	passed
# 	"CMSPH2-BRD00847"	L	passed
# 	"CMSPH2-BRD00858"	R	failed
# 	"CMSPH2-BRD00845")	R	failed


crates=(
	CMSPH2-BRD00732
	)

args=(
	"1 0"
	)

echo "Starting the loop"
for ((i=1; i<=1; i++))
do
    echo "i =" ${i}
	for ((l=0; l<=1; l++))
	do
		echo "l =" ${l}
		#echo ./ps_test_with_output.sh ${args[$l]} test ${crates[$l]} $j $k ">" debugTests/CICInputTest14_02/output_${crates[$l]}_test_$i.txt
		./ps_test_with_output.sh ${args[$l]} test ${crates[$l]} $j $k #> debugTests/CICInputTest14_02/output_${crates[$l]}_test_$i.txt
		# ./ps_test_lateralSweep.sh ${args[$l]} test ${crates[$l]} $j $k > debugTests/lateralSweep/output_${crates[$l]}_test_$i.txt

    done
done
