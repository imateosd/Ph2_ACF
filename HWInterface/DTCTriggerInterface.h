#ifndef _DTCTriggerInterface_H__
#define _DTCTriggerInterface_H__

#if defined(__EMP__)

#include "TriggerInterface.h"

namespace Ph2_HwInterface
{
class DTCTriggerInterface : public TriggerInterface
{
  public: // constructors
    DTCTriggerInterface(const std::string& puHalConfigFileName, uint32_t pBoardId);
    DTCTriggerInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable);
    ~DTCTriggerInterface();

  public: // virtual functions
    bool SetNTriggersToAccept(uint32_t pNTriggersToAccept) override { return true; }
    void ResetTriggerFSM() override {}
    void ReconfigureTriggerFSM(std::vector<std::pair<std::string, uint32_t>> pTriggerConfig) override {}
    // void     Start() override { return true; }
    // void     Pause() override {}
    // void     Resume() override {}
    // bool     Stop() override { return true; }
    bool     RunTriggerFSM() override { return true; }
    uint32_t GetTriggerState() override { return 0; }
    bool     WaitForNTriggers(uint32_t pNTriggers) override { return true; }
    bool     SendNTriggers(uint32_t pNTriggers) override;
    void     PrintStatus() override {}

  private:
};
} // namespace Ph2_HwInterface
#endif
#endif