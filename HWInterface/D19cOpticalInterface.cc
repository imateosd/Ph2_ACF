#include "HWInterface/D19cOpticalInterface.h"

using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{
// ##########################################
// # Constructors #
// #########################################

D19cOpticalInterface::D19cOpticalInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable) : FEConfigurationInterface(pId, pUri, pAddressTable)
{
    fType = ConfigurationType::IC;
}

D19cOpticalInterface::D19cOpticalInterface(const std::string& puHalConfigFileName, uint32_t pBoardId) : FEConfigurationInterface(puHalConfigFileName, pBoardId) { fType = ConfigurationType::IC; }

D19cOpticalInterface::~D19cOpticalInterface() {}
// ##########################################
// # Chip Register read/write #
// #########################################
std::map<uint32_t, std::vector<uint16_t>> D19cOpticalInterface::ReadPSCounters(OpticalGroup* pLink)
{
    std::map<uint32_t, std::vector<uint16_t>> cPSCounterData;
    // auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBrdId](Ph2_HwDescription::BeBoard* x) { return x->getId() == pLink->getBeBoardId(); });
    // LOG (INFO) << BOLDYELLOW << "D19cOpticalInterface::Read all counters on Link#" << +pLink->getId() << RESET;
    flpGBTSlowControlWorkerInterface->Reset();
    flpGBTSlowControlWorkerInterface->SelectLink(pLink->getId());
    bool                     pVerify     = false;
    uint8_t                  cFunctionId = LpGBTSlowControlWorker::READ_FE;
    uint8_t                  cWorkerId   = LpGBTSlowControlWorker::BASE_ID + pLink->getId();
    std::vector<ChipRegItem> cRegisterBlock;
    // first.. all the SSAs
    std::vector<uint32_t> cCommands;
    size_t                cNReplies = 0;
    size_t                cNHeaders = 0;
    std::vector<uint32_t> cNCounters;
    for(const auto& cHybrid: *pLink)
    {
        for(const auto& cChip: *cHybrid)
        {
            uint8_t cChipId = cChip->getId() % 8;
            if(cChip->getFrontEndType() == FrontEndType::MPA2) continue;
            if(cChip->getFrontEndType() == FrontEndType::MPA) continue;

            uint8_t  cChipCode = cChip->getChipCode();
            uint8_t  cMasterId = cChip->getMasterId();
            uint16_t cNWords   = cChip->size() * 2;
            cCommands.push_back(cWorkerId << 24 | cFunctionId << 16 | (cNWords + 1) << 0);
            cCommands.push_back(cChipCode << 29 | cChipId << 26 | cMasterId << 24 | pVerify << 23);
            cNHeaders++;
            cNCounters.push_back(cNWords);
            for(uint16_t cChnl = 0; cChnl < cChip->size(); cChnl++)
            {
                uint32_t cBaseRegisterLSB, cBaseRegisterMSB;
                cBaseRegisterLSB = 0;
                cBaseRegisterMSB = 0;
                if(cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2)
                {
                    int cRowNumber   = 1 + cChnl / 120;
                    int cPixelNumber = 1 + cChnl % 120;
                    // to speed things up .. only read back counters from active channels
                    cBaseRegisterLSB = ((cRowNumber << 11) | (9 << 7) | cPixelNumber);
                    cBaseRegisterMSB = ((cRowNumber << 11) | (10 << 7) | cPixelNumber);
                    if(cChip->getFrontEndType() == FrontEndType::MPA2)
                    {
                        cBaseRegisterLSB -= 0x280;
                        cBaseRegisterMSB -= 0x280;
                    }
                }
                if(cChip->getFrontEndType() == FrontEndType::SSA)
                {
                    cBaseRegisterLSB = 0x0901 + cChnl;
                    cBaseRegisterMSB = 0x0801 + cChnl;
                }
                if(cChip->getFrontEndType() == FrontEndType::SSA2)
                {
                    cBaseRegisterLSB = 0x0580 + cChnl;
                    cBaseRegisterMSB = 0x0680 + cChnl;
                }

                // MSB
                ChipRegItem cReg_Counters_MSB;
                cReg_Counters_MSB.fAddress = cBaseRegisterMSB;
                cCommands.push_back(cReg_Counters_MSB.fAddress << 16);
                // LSB
                ChipRegItem cReg_Counters_LSB;
                cReg_Counters_LSB.fAddress = cBaseRegisterLSB;
                cCommands.push_back(cReg_Counters_LSB.fAddress << 16);
                cNReplies += 2;
            }
        }
    } // SSAs
    if(cCommands.size() != 0)
    {
        // auto startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        // Send commands to worker
        flpGBTSlowControlWorkerInterface->WriteCommand(cCommands);
        // auto stopTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        // LOG(INFO) << BOLDYELLOW << "Write CMD took " << (stopTimeUTC_us - startTimeUTC_us) * 1e-6 << " seconds." << RESET;
        // // Wait for worker to be done
        // size_t cExpectedReplies = cNReplies + cNHeaders;
        // size_t cReceivedReplies = 0;
        // // size_t cHeadersReceived = 0;
        // size_t cReadCycles = 0;
        // do {
        //     startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        //     auto cReplies   = flpGBTSlowControlWorkerInterface->ReadReply(LpGBTSlowControlWorker::BLOCK_SIZE); // N words + 1 header
        //     stopTimeUTC_us  = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        //     size_t cNRxd    = 0;
        //     for(auto cReply: cReplies)
        //     {
        //         if(cReply != 0x00)
        //         {
        //             // if(cReceivedReplies < 10) LOG(INFO) << BOLDYELLOW << "R#" << cReceivedReplies << "/" << cExpectedReplies << " : " << std::bitset<32>(cReply) << RESET;
        //             cNRxd++;
        //         }
        //     }
        //     cReceivedReplies += cNRxd;
        //     LOG(INFO) << BOLDYELLOW << " Read Cycle#" << cReadCycles << " : " << ((float)LpGBTSlowControlWorker::BLOCK_SIZE) * 32 * 1e-6 / ((stopTimeUTC_us - startTimeUTC_us) * 1e-6) << " MBits/s "
        //               << ((float)(cNRxd * 1e-3)) / ((stopTimeUTC_us - startTimeUTC_us) * 1e-6) << " kReplies/s" << RESET;
        //     cReadCycles++;
        // } while(cReceivedReplies < cExpectedReplies); //&& !flpGBTSlowControlWorkerInterface->IsDone(cFunctionId));
        // stopTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        // LOG(INFO) << BOLDYELLOW << " Received [ " << cReceivedReplies << " ] replies (i.e. ALL) in " << cReadCycles << " reads of the FIFO" << RESET;

        size_t cCounter = 0;
        while(!flpGBTSlowControlWorkerInterface->IsDone(cFunctionId))
        {
            // std::this_thread::sleep_for(std::chrono::microseconds(100));
            cCounter++;
        }
        // LOG (INFO) << BOLDYELLOW << "WaitDone after..." << +cCounter << RESET;
        // empty reply FIFO
        auto cReplies = flpGBTSlowControlWorkerInterface->ReadReply(cNReplies + cNHeaders); // N words + 1 header
        // LOG (INFO) << BOLDYELLOW << cReplies.size() << " asking for ... " << (cNReplies + cNHeaders) << " replies from CPB" << RESET;
        auto cIter = cReplies.begin() + 1;
        for(const auto& cHybrid: *pLink)
        {
            for(const auto& cChip: *cHybrid)
            {
                if(cChip->getFrontEndType() == FrontEndType::MPA2) continue;
                if(cChip->getFrontEndType() == FrontEndType::MPA) continue;

                std::vector<uint32_t> cMyReplies;
                std::copy(cIter, cIter + cChip->size() * 2, std::back_inserter(cMyReplies));
                auto              cMyIter = cMyReplies.begin();
                std::stringstream cOut;
                uint8_t           cType = (cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2) ? 1 : 0;
                uint32_t          cId   = (cChip->getBeBoardId() << (4 + 1 + 1 + 4)) | (pLink->getId() << (4 + 1 + 1)) | (cHybrid->getId() << (4 + 1)) | (cChip->getId() % 8 << (1)) | cType;
                cPSCounterData[cId].clear();
                do
                {
                    uint8_t  cReadBackMSB = ((*cMyIter) & (0xFF << 0)) >> 0;
                    uint8_t  cReadBackLSB = ((*(cMyIter + 1)) & (0xFF << 0)) >> 0;
                    uint16_t cCounter     = cReadBackMSB << 8 | cReadBackLSB;
                    cPSCounterData[cId].push_back(cCounter);
                    cOut << cCounter << ";";
                    cMyIter += 2;
                } while(cMyIter < cMyReplies.end());
                // LOG (INFO) << BOLDCYAN << cOut.str() << RESET;
                cIter += cChip->size() * 2 + 1;
            } // all SSAs
        }     // all hybrids - break-up replies for each set of counters
        // auto stopTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        // LOG (INFO) << BOLDYELLOW << "Full SSA counter readout took " << (stopTimeUTC_us-startTimeUTC_us)*1e-6 << " seconds." << RESET;
    } // SSAs

    {
        // auto startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        // lets split between even and odd chips
        std::vector<uint8_t> cSelect{0, 1};
        for(auto cSel: cSelect)
        {
            for(const auto& cHybrid: *pLink)
            {
                cCommands.clear();
                cNReplies = 0;
                cNHeaders = 0;
                for(const auto& cChip: *cHybrid)
                {
                    uint8_t cChipId = cChip->getId() % 8;
                    if(cChip->getFrontEndType() == FrontEndType::SSA2) continue;
                    if(cChip->getFrontEndType() == FrontEndType::SSA) continue;

                    if(cChipId % 2 != cSel) continue;

                    uint8_t  cChipCode = cChip->getChipCode();
                    uint8_t  cMasterId = cChip->getMasterId();
                    uint16_t cNWords   = cChip->size() * 2;
                    cCommands.push_back(cWorkerId << 24 | cFunctionId << 16 | (cNWords + 1) << 0);
                    cCommands.push_back(cChipCode << 29 | cChipId << 26 | cMasterId << 24 | pVerify << 23);
                    cNHeaders++;
                    for(uint16_t cChnl = 0; cChnl < cChip->size(); cChnl++)
                    {
                        uint32_t cBaseRegisterLSB, cBaseRegisterMSB;
                        cBaseRegisterLSB = 0;
                        cBaseRegisterMSB = 0;
                        if(cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2)
                        {
                            int cRowNumber   = 1 + cChnl / 120;
                            int cPixelNumber = 1 + cChnl % 120;
                            // to speed things up .. only read back counters from active channels
                            cBaseRegisterLSB = ((cRowNumber << 11) | (9 << 7) | cPixelNumber);
                            cBaseRegisterMSB = ((cRowNumber << 11) | (10 << 7) | cPixelNumber);
                            if(cChip->getFrontEndType() == FrontEndType::MPA2)
                            {
                                cBaseRegisterLSB -= 0x280;
                                cBaseRegisterMSB -= 0x280;
                            }
                        }
                        if(cChip->getFrontEndType() == FrontEndType::SSA)
                        {
                            cBaseRegisterLSB = 0x0901 + cChnl;
                            cBaseRegisterMSB = 0x0801 + cChnl;
                        }
                        if(cChip->getFrontEndType() == FrontEndType::SSA2)
                        {
                            cBaseRegisterLSB = 0x0580 + cChnl;
                            cBaseRegisterMSB = 0x0680 + cChnl;
                        }

                        // MSB
                        ChipRegItem cReg_Counters_MSB;
                        cReg_Counters_MSB.fAddress = cBaseRegisterMSB;
                        cCommands.push_back(cReg_Counters_MSB.fAddress << 16);
                        // LSB
                        ChipRegItem cReg_Counters_LSB;
                        cReg_Counters_LSB.fAddress = cBaseRegisterLSB;
                        cCommands.push_back(cReg_Counters_LSB.fAddress << 16);
                        cNReplies += 2;
                    }
                }
                if(cCommands.size() == 0) continue;
                // Send commands to worker
                flpGBTSlowControlWorkerInterface->WriteCommand(cCommands);
                // Wait for worker to be done
                size_t cCounter = 0;
                while(!flpGBTSlowControlWorkerInterface->IsDone(cFunctionId)) { cCounter++; }
                // LOG (INFO) << BOLDYELLOW << "WaitDone after..." << +cCounter << RESET;
                // empty reply FIFO
                auto cReplies = flpGBTSlowControlWorkerInterface->ReadReply(cNReplies + cNHeaders); // N words + 1 header
                // LOG (INFO) << BOLDYELLOW << cReplies.size() << " asking for ... " << (cNReplies + cNHeaders) << " replies from CPB" << RESET;
                auto cIter = cReplies.begin() + 1;
                for(const auto& cChip: *cHybrid)
                {
                    uint8_t cChipId = cChip->getId() % 8;
                    if(cChip->getFrontEndType() == FrontEndType::SSA2) continue;
                    if(cChip->getFrontEndType() == FrontEndType::SSA) continue;
                    if(cChipId % 2 != cSel) continue;

                    std::vector<uint32_t> cMyReplies;
                    std::copy(cIter, cIter + cChip->size() * 2, std::back_inserter(cMyReplies));
                    auto              cMyIter = cMyReplies.begin();
                    std::stringstream cOut;
                    uint8_t           cType = (cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2) ? 1 : 0;
                    uint32_t          cId   = (cChip->getBeBoardId() << (4 + 1 + 1 + 4)) | (pLink->getId() << (4 + 1 + 1)) | (cHybrid->getId() << (4 + 1)) | (cChip->getId() % 8 << (1)) | cType;
                    cPSCounterData[cId].clear();
                    do
                    {
                        uint8_t  cReadBackMSB = ((*cMyIter) & (0xFF << 0)) >> 0;
                        uint8_t  cReadBackLSB = ((*(cMyIter + 1)) & (0xFF << 0)) >> 0;
                        uint16_t cCounter     = cReadBackMSB << 8 | cReadBackLSB;
                        cOut << cCounter << ";";
                        cPSCounterData[cId].push_back(cCounter);
                        cMyIter += 2;
                    } while(cMyIter < cMyReplies.end());
                    // LOG (INFO) << BOLDCYAN << cOut.str() << RESET;
                    cIter += cChip->size() * 2 + 1;
                } // all MPAs - break-up replies
            }
        }
        // auto stopTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        // LOG (INFO) << BOLDYELLOW << "Full MPA counter readout took " << (stopTimeUTC_us-startTimeUTC_us)*1e-6 << " seconds." << RESET;
    } // MPAs

    return cPSCounterData;
}
bool D19cOpticalInterface::Read(Chip* pChip, std::vector<ChipRegItem>& pRegisterItems)
{
    std::lock_guard<std::recursive_mutex> theGuard(fMutex);
    flpGBTSlowControlWorkerInterface->SelectLink(pChip->getOpticalGroupId());
    uint8_t                  cFunctionId = (pChip->getFrontEndType() == FrontEndType::LpGBT) ? LpGBTSlowControlWorker::READ_IC : LpGBTSlowControlWorker::READ_FE;
    std::vector<ChipRegItem> cRegisterBlock;
    bool                     cSuccess = true;
    int                      cBlockId = 0;
    for(std::vector<ChipRegItem>::iterator cRegItemIter = pRegisterItems.begin(); cRegItemIter < pRegisterItems.end(); cRegItemIter++)
    {
        cRegisterBlock.push_back(*cRegItemIter);
        if(cRegisterBlock.size() == LpGBTSlowControlWorker::BLOCK_SIZE || cRegItemIter == pRegisterItems.end() - 1)
        {
            // Encode command frame
            auto cCommand = flpGBTSlowControlWorkerInterface->EncodeCommand(cFunctionId, pChip, cRegisterBlock);
            // Send commands to worker
            flpGBTSlowControlWorkerInterface->WriteCommand(cCommand);
            // Wait for worker to be done
            if(!flpGBTSlowControlWorkerInterface->WaitDone(cFunctionId))
            {
                LOG(ERROR) << BOLDRED << "D19cOpticalInterface::Read : Tool stuck ... Sending soft reset" << RESET;
                return false;
            }
            // Check if multilple tries happened
            uint8_t cTryCntr = flpGBTSlowControlWorkerInterface->GetTryCounter(cFunctionId);
            if(cTryCntr > 0)
            {
                uint8_t cMaxRetry = (pChip->getFrontEndType() == FrontEndType::LpGBT) ? fConfiguration.fMaxRetryIC : fConfiguration.fMaxRetryFE;
                LOG(ERROR) << BOLDRED << "D19cOpticalInterface::Read : Tried " << +cTryCntr << "/" << +cMaxRetry << " before success" << RESET;
            }
            // Get replies from worker
            auto cReplies = flpGBTSlowControlWorkerInterface->ReadReply(cRegisterBlock.size() + 1); // N words + 1 header
            // Verifiy reply frame integrity - only n_words in the header
            size_t cNWords = (cReplies[0] & (0xFFFF << 0)) >> 0;
            if(cNWords != cRegisterBlock.size())
            {
                LOG(ERROR) << BOLDRED << "D19cOpticalInterface::Read -- Corrupted CPB reply" << RESET;
                throw std::runtime_error("D19cOpticalInterface::Read -- Corrupted CPB reply");
            }
            // Decode reply frame and extract data
            for(size_t cReplyIdx = 0; cReplyIdx < cReplies.size(); cReplyIdx++)
            {
                if(cReplyIdx == 0) continue; // skip header
                uint8_t cErrorCode = (cReplies[cReplyIdx] & (0xFF << 8)) >> 8;
                uint8_t cReadBack  = (cReplies[cReplyIdx] & (0xFF << 0)) >> 0;
                if(cErrorCode != 0)
                {
                    if(pChip->getFrontEndType() == FrontEndType::LpGBT)
                        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::Read -- Error Code : " << +cErrorCode << RESET;
                    else
                        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::Read -- Error Code : " << +cErrorCode << " -- I2C Status : " << LpGBTSlowControlWorker::I2C_STATUS_MAP.at(cReadBack) << RESET;
                    LOG(ERROR) << BOLDRED << "Chip code : " << +pChip->getChipCode() << " -- Chip Id : " << +pChip->getId() << " -- Register address 0x" << std::hex
                               << +pRegisterItems.at(cBlockId * LpGBTSlowControlWorker::BLOCK_SIZE + cReplyIdx - 1).fAddress << std::dec << RESET;
                    cSuccess &= false;
                }
                else
                {
                    auto& cItem        = pRegisterItems.at(cBlockId * LpGBTSlowControlWorker::BLOCK_SIZE + cReplyIdx - 1);
                    auto  cRegisterMap = pChip->getRegMap();
                    auto  cIterator =
                        find_if(cRegisterMap.begin(), cRegisterMap.end(), [&cItem](const ChipRegPair& obj) { return obj.second.fAddress == cItem.fAddress && obj.second.fPage == cItem.fPage; });
                    if(cIterator != cRegisterMap.end()) { pChip->setReg(cIterator->first, cItem.fValue); }
                    cItem.fValue = cReadBack;
                }
            }
            cRegisterBlock.clear();
            cBlockId++;
        }
    }
    return cSuccess;
}

bool D19cOpticalInterface::Write(OpticalGroup* pLink)
{
    std::lock_guard<std::recursive_mutex> theGuard(fMutex);
    flpGBTSlowControlWorkerInterface->Reset();
    flpGBTSlowControlWorkerInterface->SelectLink(pLink->getId());
    bool                  cVerify     = false;
    uint8_t               cFunctionId = LpGBTSlowControlWorker::WRITE_FE;
    std::vector<uint32_t> cCommands;
    for(const auto& cHybrid: *pLink)
    {
        for(const auto& cChip: *cHybrid)
        {
            if(cChip->getFrontEndType() != FrontEndType::MPA) continue;
            if(cChip->getId() > 8) continue;
            std::vector<ChipRegItem> cRegisterBlock;
            size_t                   cRegsToWrite = 0;
            for(const auto& cReg: cChip->getRegMap())
            {
                if(cReg.second.fControlReg == 1) continue;
                ;
                if(cReg.second.fWrite == 0) continue;
                ;
                cRegisterBlock.push_back(cReg.second);
                // if(cChip->getFrontEndType()==FrontEndType::MPA)
                // {
                //     LOG (INFO) << BOLDYELLOW << cReg.first << RESET;
                // }
                cRegsToWrite++;
            }
            LOG(INFO) << BOLDYELLOW << "ROC#" << +cChip->getId() << " ---> write to " << cRegsToWrite << " registers." << RESET;
            auto cCmds = flpGBTSlowControlWorkerInterface->EncodeCommand(cFunctionId, cChip, cRegisterBlock, cVerify);
            std::copy(cCmds.begin(), cCmds.end(), std::back_inserter(cCommands));
        }
    } // fill command buffer

    // chunk up write buffer
    LOG(INFO) << BOLDYELLOW << "Writing " << cCommands.size() << " 32-bit words to CnfgBlk" << RESET;
    auto   cIter      = cCommands.begin();
    size_t cBlkNumber = 0;
    do
    {
        std::vector<uint32_t> cTmp;
        auto                  cStart = cIter;
        auto                  cEnd   = ((cIter + LpGBTSlowControlWorker::BLOCK_SIZE) < cCommands.end()) ? cIter + LpGBTSlowControlWorker::BLOCK_SIZE : cCommands.end();
        std::copy(cStart, cEnd, std::back_inserter(cTmp));
        LOG(INFO) << BOLDYELLOW << "Block#" << +cBlkNumber << "\t" << cTmp.size() << " 32-bit words" << RESET;
        // Send commands to worker
        flpGBTSlowControlWorkerInterface->WriteCommand(cTmp);
        // Wait for worker to be done
        if(!flpGBTSlowControlWorkerInterface->WaitDone(cFunctionId)) {}
        cIter = cEnd;
        cBlkNumber++;
    } while(cIter < cCommands.end());

    return true;
}
bool D19cOpticalInterface::Write(Chip* pChip, std::vector<ChipRegItem>& pRegisterItems, bool pVerify)
{
    std::lock_guard<std::recursive_mutex> theGuard(fMutex);
    flpGBTSlowControlWorkerInterface->SelectLink(pChip->getOpticalGroupId());
    uint8_t                  cFunctionId = (pChip->getFrontEndType() == FrontEndType::LpGBT) ? LpGBTSlowControlWorker::WRITE_IC : LpGBTSlowControlWorker::WRITE_FE;
    std::vector<ChipRegItem> cRegisterBlock;
    bool                     cSuccess = true;
    int                      cBlockId = 0;
    for(std::vector<ChipRegItem>::iterator cRegItemIter = pRegisterItems.begin(); cRegItemIter < pRegisterItems.end(); cRegItemIter++)
    {
        cRegisterBlock.push_back(*cRegItemIter);
        if(cRegisterBlock.size() == LpGBTSlowControlWorker::BLOCK_SIZE || cRegItemIter == pRegisterItems.end() - 1)
        {
            // Encode command frame
            auto cCommand = flpGBTSlowControlWorkerInterface->EncodeCommand(cFunctionId, pChip, cRegisterBlock, pVerify);
            // Send commands to worker
            flpGBTSlowControlWorkerInterface->WriteCommand(cCommand);
            // Wait for worker to be done
            if(!flpGBTSlowControlWorkerInterface->WaitDone(cFunctionId))
            {
                LOG(ERROR) << BOLDRED << "D19cOpticalInterface::Write : Tool stuck ... Sending soft reset" << RESET;
                return false;
            }
            // Check if multilple tries happened
            uint8_t cTryCntr = flpGBTSlowControlWorkerInterface->GetTryCounter(cFunctionId);
            if(cTryCntr > 0)
            {
                uint8_t cMaxRetry = (pChip->getFrontEndType() == FrontEndType::LpGBT) ? fConfiguration.fMaxRetryIC : fConfiguration.fMaxRetryFE;
                LOG(ERROR) << BOLDRED << "D19cOpticalInterface::Write : Tried " << +cTryCntr << "/" << +cMaxRetry << " before success" << RESET;
            }
            // Get replies from worker
            auto cReplies = flpGBTSlowControlWorkerInterface->ReadReply(cRegisterBlock.size() + 1); // N words + 1 header
            // Verifiy reply frame integrity - only n_words in the header
            size_t cNWords = (cReplies[0] & (0xFFFF << 0)) >> 0;
            if(cNWords != cRegisterBlock.size())
            {
                LOG(ERROR) << BOLDRED << "D19cOpticalInterface::Write -- Corrupted CPB reply" << RESET;
                throw std::runtime_error("D19cOpticalInterface::Write -- Corrupted CPB reply");
            }
            // Decode reply frame and extract data
            for(size_t cReplyIdx = 0; cReplyIdx < cReplies.size(); cReplyIdx++)
            {
                if(cReplyIdx == 0) continue; // skip header
                uint8_t cErrorCode = (cReplies[cReplyIdx] & (0xFF << 8)) >> 8;
                uint8_t cReadBack  = (cReplies[cReplyIdx] & (0xFF << 0)) >> 0;
                if(cErrorCode != 0)
                {
                    if(pChip->getFrontEndType() == FrontEndType::LpGBT)
                        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::Write -- Error Code : " << +cErrorCode << RESET;
                    else
                        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::Write -- Error Code : " << +cErrorCode << " -- I2C Status : " << LpGBTSlowControlWorker::I2C_STATUS_MAP.at(cReadBack) << RESET;
                    LOG(ERROR) << BOLDRED << "Chip code : " << +pChip->getChipCode() << " -- Chip Id : " << +pChip->getId() << " -- Register address 0x" << std::hex
                               << +pRegisterItems.at(cBlockId * LpGBTSlowControlWorker::BLOCK_SIZE + cReplyIdx - 1).fAddress << std::dec << RESET;
                    cSuccess &= false;
                }
                else
                {
                    auto& cItem        = pRegisterItems.at(cBlockId * LpGBTSlowControlWorker::BLOCK_SIZE + cReplyIdx - 1);
                    auto  cRegisterMap = pChip->getRegMap();
                    auto  cIterator =
                        find_if(cRegisterMap.begin(), cRegisterMap.end(), [&cItem](const ChipRegPair& obj) { return obj.second.fAddress == cItem.fAddress && obj.second.fPage == cItem.fPage; });
                    if(cIterator != cRegisterMap.end()) { pChip->setReg(cIterator->first, cItem.fValue); }
                    pChip->UpdateModifiedRegMap(cItem);
                }
                if(pVerify)
                {
                    if(cReadBack != pRegisterItems.at(cBlockId * LpGBTSlowControlWorker::BLOCK_SIZE + cReplyIdx - 1).fValue)
                    {
                        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::Write : Wrong value read back" << RESET;
                        cSuccess &= false;
                    }
                    pRegisterItems.at(cBlockId * LpGBTSlowControlWorker::BLOCK_SIZE + cReplyIdx - 1).fValue = cReadBack;
                }
            }
            cRegisterBlock.clear();
            cBlockId++;
        }
    }
    return cSuccess;
}

bool D19cOpticalInterface::SingleWrite(Chip* pChip, ChipRegItem& pRegisterItem)
{
    pChip->UpdateModifiedRegMap(pRegisterItem);
    std::vector<ChipRegItem> pRegisterItemTemp = {pRegisterItem};
    bool                     cSuccess          = Write(pChip, pRegisterItemTemp, false);
    pRegisterItem.fValue                       = pRegisterItemTemp.at(0).fValue;
    return cSuccess;
}

bool D19cOpticalInterface::SingleRead(Chip* pChip, ChipRegItem& pRegisterItem)
{
    std::vector<ChipRegItem> pRegisterItemTemp = {pRegisterItem};
    bool                     cSuccess          = Read(pChip, pRegisterItemTemp);
    pRegisterItem.fValue                       = pRegisterItemTemp.at(0).fValue;
    return cSuccess;
}

bool D19cOpticalInterface::MultiRead(Chip* pChip, std::vector<ChipRegItem>& pRegisterItems) { return Read(pChip, pRegisterItems); }

bool D19cOpticalInterface::MultiWrite(Chip* pChip, std::vector<ChipRegItem>& pRegisterItems) { return Write(pChip, pRegisterItems, false); }

bool D19cOpticalInterface::SingleWriteRead(Chip* pChip, ChipRegItem& pRegisterItem)
{
    std::vector<ChipRegItem> pRegisterItemTemp = {pRegisterItem};
    bool                     cSuccess          = Write(pChip, pRegisterItemTemp, true);
    pRegisterItem.fValue                       = pRegisterItemTemp.at(0).fValue;
    return cSuccess;
}

bool D19cOpticalInterface::MultiWriteRead(Chip* pChip, std::vector<ChipRegItem>& pRegisterItems) { return Write(pChip, pRegisterItems, true); }

bool D19cOpticalInterface::WriteI2C(Ph2_HwDescription::Chip* pChip, uint8_t pMasterId, uint8_t pMasterConfig, std::vector<uint32_t>& pSlaveData)
{
    std::lock_guard<std::recursive_mutex> theGuard(fMutex);
    flpGBTSlowControlWorkerInterface->SelectLink(pChip->getOpticalGroupId());
    uint8_t               cFunctionId = LpGBTSlowControlWorker::MULTI_BYTE_WRITE_I2C;
    std::vector<uint32_t> cDataBlock;
    bool                  cSuccess = true;
    int                   cBlockId = 0;
    for(std::vector<uint32_t>::iterator cDataIter = pSlaveData.begin(); cDataIter < pSlaveData.end(); cDataIter++)
    {
        cDataBlock.push_back(*cDataIter);
        if(cDataBlock.size() == LpGBTSlowControlWorker::BLOCK_SIZE || cDataIter == pSlaveData.end() - 1)
        {
            // Encode command frame
            auto cCommand = flpGBTSlowControlWorkerInterface->EncodeCommandI2C(cFunctionId, pChip, pMasterId, pMasterConfig, cDataBlock);
            // Send command to worker
            flpGBTSlowControlWorkerInterface->WriteCommand(cCommand);
            // Wait for worker to be done
            if(!flpGBTSlowControlWorkerInterface->WaitDone(cFunctionId))
            {
                LOG(ERROR) << BOLDRED << "D19cOpticalInterface::WriteI2C : Tool stuck ... Sending soft reset" << RESET;
                return false;
            }
            // Get replies from worker
            auto cReplies = flpGBTSlowControlWorkerInterface->ReadReply(cDataBlock.size() + 1); // N words + 1 header
            // Verifiy reply frame integrity - only n_words in the header
            size_t cNWords = (cReplies[0] & (0xFFFF << 0)) >> 0;
            if(cNWords != cDataBlock.size())
            {
                LOG(ERROR) << BOLDRED << "D19cOpticalInterface::WriteI2C -- Corrupted CPB reply" << RESET;
                throw std::runtime_error("D19cOpticalInterface::WriteI2C -- Corrupted CPB reply");
            }
            // Decode reply frame and extract data
            for(size_t cReplyIdx = 0; cReplyIdx < cReplies.size(); cReplyIdx++)
            {
                if(cReplyIdx == 0) continue; // skip header
                uint8_t cErrorCode = (cReplies[cReplyIdx] & (0xFF << 8)) >> 8;
                uint8_t cReadBack  = (cReplies[cReplyIdx] & (0xFF << 0)) >> 0;
                if(cErrorCode != 0)
                {
                    LOG(ERROR) << BOLDRED << "D19cOpticalInterface::WriteI2C -- Error Code : " << +cErrorCode << " -- I2C Status : " << LpGBTSlowControlWorker::I2C_STATUS_MAP.at(cReadBack) << RESET;
                    cSuccess &= false;
                }
            }
            cDataBlock.clear();
            cBlockId++;
        }
    }
    return cSuccess;
}

std::vector<uint16_t> D19cOpticalInterface::ReadI2C(Ph2_HwDescription::Chip* pChip, uint8_t pMasterId, uint8_t pMasterConfig, std::vector<uint32_t>& pSlaveData)
{
    std::lock_guard<std::recursive_mutex> theGuard(fMutex);
    flpGBTSlowControlWorkerInterface->SelectLink(pChip->getOpticalGroupId());
    uint8_t               cFunctionId = LpGBTSlowControlWorker::SINGLE_BYTE_READ_I2C;
    std::vector<uint32_t> cDataBlock;
    std::vector<uint16_t> cReadBackData;
    bool                  cSuccess = true;
    int                   cBlockId = 0;
    for(std::vector<uint32_t>::iterator cDataIter = pSlaveData.begin(); cDataIter < pSlaveData.end(); cDataIter++)
    {
        cDataBlock.push_back(*cDataIter);
        if(cDataBlock.size() == LpGBTSlowControlWorker::BLOCK_SIZE || cDataIter == pSlaveData.end() - 1)
        {
            // Encode command frame
            auto cCommand = flpGBTSlowControlWorkerInterface->EncodeCommandI2C(cFunctionId, pChip, pMasterId, pMasterConfig, cDataBlock);
            // Send command to worker
            flpGBTSlowControlWorkerInterface->WriteCommand(cCommand);
            // Wait for worker to be done
            if(!flpGBTSlowControlWorkerInterface->WaitDone(cFunctionId))
            {
                LOG(ERROR) << BOLDRED << "D19cOpticalInterface::ReadI2C : Tool stuck ... Sending soft reset" << RESET;
                return {};
            }
            // Get replies from worker
            auto cReplies = flpGBTSlowControlWorkerInterface->ReadReply(cDataBlock.size() + 1); // N words + 1 header
            // Verifiy reply frame integrity - only n_words in the header
            size_t cNWords = (cReplies[0] & (0xFFFF << 0)) >> 0;
            if(cNWords != cDataBlock.size())
            {
                LOG(ERROR) << BOLDRED << "D19cOpticalInterface::ReadI2C -- Corrupted CPB reply" << RESET;
                throw std::runtime_error("D19cOpticalInterface::ReadI2C -- Corrupted CPB reply");
            }
            // Decode reply frame and extract data
            for(size_t cReplyIdx = 0; cReplyIdx < cReplies.size(); cReplyIdx++)
            {
                if(cReplyIdx == 0) continue; // skip header
                uint8_t cErrorCode = (cReplies[cReplyIdx] & (0xFF << 8)) >> 8;
                uint8_t cReadBack  = (cReplies[cReplyIdx] & (0xFF << 0)) >> 0;
                if(cErrorCode != 0)
                {
                    LOG(ERROR) << BOLDRED << "D19cOpticalInterface::ReadI2C -- Error Code : " << +cErrorCode << " -- I2C Status : " << LpGBTSlowControlWorker::I2C_STATUS_MAP.at(cReadBack) << RESET;
                    cSuccess &= false;
                }
                cReadBackData.push_back(cErrorCode | cReadBack << 0);
            }
            cDataBlock.clear();
            cBlockId++;
        }
    }
    return cReadBackData;
}

bool D19cOpticalInterface::SingleMultiByteWriteI2C(Ph2_HwDescription::Chip* pChip, uint8_t pMasterId, uint8_t pMasterConfig, uint8_t pSlaveAddress, uint8_t pSlaveData)
{
    std::vector<uint32_t> cMasterData;
    cMasterData.push_back(pSlaveData << 8 | pSlaveAddress << 0);
    return WriteI2C(pChip, pMasterId, pMasterConfig, cMasterData);
}

bool D19cOpticalInterface::MultiMultiByteWriteI2C(Ph2_HwDescription::Chip* pChip, uint8_t pMasterId, uint8_t pMasterConfig, std::vector<uint32_t>& pSlaveData)
{
    return WriteI2C(pChip, pMasterId, pMasterConfig, pSlaveData);
}

uint8_t D19cOpticalInterface::SingleSingleByteReadI2C(Ph2_HwDescription::Chip* pChip, uint8_t pMasterId, uint8_t pMasterConfig, uint8_t pSlaveAddress)
{
    std::vector<uint32_t> cMasterData;
    cMasterData.push_back(pSlaveAddress << 0);
    auto cReadBackData  = ReadI2C(pChip, pMasterId, pMasterConfig, cMasterData);
    auto cReadBackValue = (cReadBackData[0] & 0xFF);
    return cReadBackValue;
}

std::vector<uint16_t> D19cOpticalInterface::MultiSingleByteReadI2C(Ph2_HwDescription::Chip* pChip, uint8_t pMasterId, uint8_t pMasterConfig, std::vector<uint32_t>& pSlaveData)
{
    return ReadI2C(pChip, pMasterId, pMasterConfig, pSlaveData);
}

} // namespace Ph2_HwInterface
