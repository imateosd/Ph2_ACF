#if defined(__EMP__)

#include "DTCTriggerInterface.h"

using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{
DTCTriggerInterface::DTCTriggerInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable) : TriggerInterface(pId, pUri, pAddressTable)
{
    LOG(INFO) << BOLDYELLOW << "DTCTriggerInterface::DTCTriggerInterface Constructor" << RESET;
}
DTCTriggerInterface::DTCTriggerInterface(const std::string& puHalConfigFileName, uint32_t pBoardId) : TriggerInterface(puHalConfigFileName, pBoardId)
{
    LOG(INFO) << BOLDYELLOW << "DTCTriggerInterface::DTCTriggerInterface Constructor" << RESET;
}
DTCTriggerInterface::~DTCTriggerInterface() {}

} // namespace Ph2_HwInterface
#endif