#ifndef _D19cSFPMonitoringInterface_H__
#define _D19cSFPMonitoringInterface_H__

#include "BeBoardFWInterface.h"
#include <string>

namespace Ph2_HwInterface
{
class D19cSFPMonitoringInterface : public RegManager
{
  public:
    D19cSFPMonitoringInterface(const std::string& puHalConfigFileName, uint32_t pBoardId);
    D19cSFPMonitoringInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable);
    D19cSFPMonitoringInterface(RegManager&& theRegManager);
    ~D19cSFPMonitoringInterface();

  public:
    float   Read(std::string pParameter, std::string pMezzanine, int pChannel);
    uint8_t getSFPSlot(uint8_t pLinkId);
    uint8_t getSFPId(uint8_t pLinkId);

  private:
    bool                               IsDone(std::string pMezzanine);
    bool                               fTimeout{false};
    uint32_t                           fWait_us{100};
    uint32_t                           fErrorCode{0};
    std::map<std::string, uint32_t>    fAddresses  = {{"Temperature", 96}, {"Voltage", 98}, {"BiasCurrent", 100}, {"TxPower", 102}, {"RxPower", 104}};
    std::map<std::string, float>       fConversion = {{"Temperature", 256.}, {"Voltage", 10.}, {"BiasCurrent", 500}, {"TxPower", 1e1}, {"RxPower", 1e1}};
    std::map<std::string, std::string> fUnits      = {{"Temperature", "Celsius"}, {"Voltage", "mV"}, {"BiasCurrent", "mA"}, {"TxPower", "dBm"}, {"RxPower", "dBm"}};
    // link map for each FMC
    // SFP cage# , SFP id
    std::map<uint8_t, uint8_t> fSlotMapFMC = {{0, 3}, {1, 2}, {2, 1}, {3, 0}, {4, 7}, {5, 6}, {6, 5}, {7, 4}};

    // FMC Maps
    std::map<uint32_t, std::string> fFMCMap = {{0, "NONE"}, {17, "OPTO_QUAD"}, {18, "OPTO_OCTA"}};
};
} // namespace Ph2_HwInterface
#endif
