if(NOT DEFINED ENV{OTSDAQ_CMSTRACKER_DIR})

    MESSAGE(STATUS " ")
    MESSAGE(STATUS "    ${BoldBlue}========================================================================================================${Reset}")
    MESSAGE(STATUS "    ${BoldYellow}HW INTERFACE${Reset} [stand-alone]: [${BoldCyan}Ph2_ACF/HWInterface/CMakeLists.txt${Reset}]. ${BoldRed}Begin...${Reset}")
    MESSAGE(STATUS " ")

    # Includes
    include_directories(${UHAL_UHAL_INCLUDE_PREFIX})
    include_directories(${UHAL_GRAMMARS_INCLUDE_PREFIX})
    include_directories(${UHAL_LOG_INCLUDE_PREFIX})
    include_directories(${CMAKE_CURRENT_SOURCE_DIR})
    include_directories(${PROJECT_SOURCE_DIR}/HWDescription)
    include_directories(${PROJECT_SOURCE_DIR}/HWInterface)
    include_directories(${PROJECT_SOURCE_DIR})
    if(${EMP_FOUND})
        include_directories(${EMP_INCLUDE_DIRS})
    endif(${EMP_FOUND})


    # Replace this with find_package
    if(${EMP_FOUND})
        link_directories(${EMP_LIB_DIRS})
        set(LIBS ${LIBS} ${EMP_LIBRARIES})
	endif(${EMP_FOUND})
    link_directories(${UHAL_UHAL_LIB_PREFIX})
    link_directories(${UHAL_LOG_LIB_PREFIX})
    link_directories(${UHAL_GRAMMARS_LIB_PREFIX})

    # Boost also needs to be linked
    include_directories(${Boost_INCLUDE_DIRS})
    link_directories(${Boost_LIBRARY_DIRS})
    set(LIBS ${LIBS} ${Boost_ITERATOR_LIBRARY} ${Boost_IOSTREAMS_LIBRARY})

    # Find root and link against it
    if(${ROOT_FOUND})
        include_directories(${ROOT_INCLUDE_DIRS})
        set(LIBS ${LIBS} ${ROOT_LIBRARIES})
    endif()

    ##
    # # FIND EMPand link against it
    # if(${EMP_FOUND})
    #     message("--     ${BoldCyan}#### EMP_INCLUDE_DIRS ${EMP_INCLUDE_DIRS} ####${Reset}")
    #     message("--     ${BoldCyan}#### EMP_LIBRARIES ${EMP_LIBRARIES} ####${Reset}")
    #     include_directories(${EMP_INCLUDE_DIRS})
    #     link_directories(${EMP_LIB_DIRS})
    #     set(LIBS ${LIBS} ${EMP_LIBRARIES})
    # endif()
    
    # Find source files
    file(GLOB HEADERS *.h)
    file(GLOB SOURCES *.cc)

    # Add the library
    add_library(Ph2_Interface STATIC ${SOURCES} ${HEADERS})

    set(LIBS ${LIBS} Ph2_Description  cactus_uhal_uhal cactus_uhal_log boost_regex)
    # Check for TestCard USBDriver
    TARGET_LINK_LIBRARIES(Ph2_Interface ${LIBS})
    #check for TestCard USBDriver
    if(${PH2_TCUSB_FOUND})
        message("--     ${BoldCyan}#### PH2_TCUSB_LIBRARY_DIRS ${PH2_TCUSB_LIBRARY_DIRS} ####${Reset}")
      include_directories(${PH2_TCUSB_INCLUDE_DIRS})
      link_directories(${PH2_TCUSB_LIBRARY_DIRS})
      set(LIBS ${LIBS} ${PH2_TCUSB_LIBRARIES} usb)
      set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} $ENV{TCUSBFlag}")
      if($ENV{UseTCUSBforROH})
          message(STATUS "    Building the ROH TestCards USB components")
          set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} $ENV{TCUSBforROHFlag}")
      endif($ENV{UseTCUSBforROH})
      if($ENV{UseTCUSBforSEH})
          message(STATUS "    Building the SEH TestCards USB components")
          set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} $ENV{TCUSBforSEHFlag}")
      endif($ENV{UseTCUSBforSEH})
    endif(${PH2_TCUSB_FOUND})

    ####################################
    ## EXECUTABLES
    ####################################

    file(GLOB BINARIES RELATIVE ${PROJECT_SOURCE_DIR}/HWInterface *.cc)

    message("--     ${BoldCyan}#### Building the following executables: ####${Reset}")
    foreach( sourcefile ${BINARIES} )
        string(REPLACE ".cc" "" name ${sourcefile})
        message(STATUS "    ${name}")
    endforeach(sourcefile ${BINARIES})
    message("--     ${BoldCyan}#### End ####${Reset}")

    MESSAGE(STATUS " ")
    MESSAGE(STATUS "    ${BoldYellow}HW INTERFACE${Reset} [stand-alone]: [${BoldCyan}Ph2_ACF/HWInterface/CMakeLists.txt${Reset}]. ${BoldGreen}DONE!${Reset}")
    MESSAGE(STATUS "    ${BoldBlue}========================================================================================================${Reset}")
    MESSAGE(STATUS " ")

else()

    MESSAGE(STATUS " ")
    MESSAGE(STATUS "    ${BoldBlue}========================================================================================================${Reset}")
    MESSAGE(STATUS "    ${BoldYellow}HW INTERFACE${Reset} [otsdaq]: [${BoldCyan}Ph2_ACF/HWInterface/CMakeLists.txt${Reset}]. ${BoldRed}Begin...${Reset}")
    MESSAGE(STATUS " ")

    include_directories(${UHAL_DIR}/uhal/include)
    include_directories(${UHAL_DIR}/log/include)
    include_directories(${UHAL_DIR}/grammars/include)

    cet_set_compiler_flags(
        EXTRA_FLAGS -Wno-reorder -Wl,--undefined
    )

    cet_make(LIBRARY_NAME Ph2_Interface
            LIBRARIES
            Ph2_Description
            #cactus_uhal_uhal
    )

    #file(GLOB HEADERS *.h)

    install_headers()
    install_source()

    MESSAGE(STATUS " ")
    MESSAGE(STATUS "    ${BoldYellow}HW INTERFACE${Reset} [otsdaq]: [${BoldCyan}Ph2_ACF/HWInterface/CMakeLists.txt${Reset}]. ${BoldGreen}DONE!${Reset}")
    MESSAGE(STATUS "    ${BoldBlue}========================================================================================================${Reset}")
    MESSAGE(STATUS " ")

endif()
